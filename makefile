# McMule makefile

GROUPS = mudec mudecrare mue misc
WGROUPS= mudec mudecrare mue misc
PRE = global_def.f95 functions.f95 phase_space.f95
POST = mat_el.f95 user.f95 integrands.f95 vegas_m.f95
MISC = hadr5n12.f func.c
.PHONY: groups $(GROUPS) pre

.DEFAULT_GOAL=mcmule
all: groups mcmule test

src=src/
include makefile.conf
lib: $(COLLIER)/libcollier.a hplog/hplog.o
FFLAGS += $(foreach g,$(GROUPS),-I$g/.obj)
LFLAGS+=-L$(COLLIER) -lcollier

src/mat_el.f95: $(GMODS)
	@echo "pymule iface $(GROUPS)"
	@echo "                 !!!!!!!!!!!!!!!!!!!!!!!!!" > $@
	@echo "                       MODULE MAT_EL" >> $@
	@echo "                 !!!!!!!!!!!!!!!!!!!!!!!!!" >> $@
	@echo "" >> $@
	@echo "  use functions" >> $@
	@for i in $(GROUPS) ; do make -s -C $$i iface; done >> $@
	@echo "                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> $@
	@echo "                       END MODULE MAT_EL" >> $@
	@echo "                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> $@

src//user.f95 : | tools/user-default.f95
	cp $| $@

# Dependencies on modules
.obj/mat_el.o      .obj/mat_el.mod     : src/mat_el.f95 $(GMODS) .obj/functions.mod
.obj/functions.o   .obj/functions.mod  : .obj/global_def.mod $(COLLIER)/libcollier.a
.obj/phase_space.o .obj/phase_space.mod: .obj/functions.mod
.obj/user.o        .obj/user.mod       : .obj/phase_space.mod
.obj/vegas_m.o     .obj/vegas_m.mod    : .obj/user.mod
.obj/integrands.o  .obj/integrands.mod : .obj/phase_space.mod .obj/mat_el.mod .obj/user.mod .obj/vegas_m.mod

.obj/mcmule.o: .obj/integrands.mod .obj/vegas_m.mod
.obj/test.o: .obj/integrands.mod .obj/vegas_m.mod


OBJ=$(foreach g,$(basename $(PRE) $(POST) $(MISC)), .obj/$g.o) $(GLIBS) hplog/hplog.o

pre: .obj/phase_space.mod .obj/functions.mod .obj/global_def.mod

# Build groups
define generate-grule = # $1:source
$1/$1_mat_el.mod: .obj/functions.mod .obj/global_def.mod
	make -C $1 all
endef
$(foreach _,$(WGROUPS),$(eval $(call generate-grule,$_)))


groups: $(foreach g,$(basename $(PRE)), .obj/$g.o)  $(WGROUPS)
$(WGROUPS):
		$(MAKE) -C $@


mcmule: .obj/mcmule.o $(OBJ)
		@$(MKVERSION)
		@echo "LD $@"
		@$(LD) -o $@ $^ .obj/version.o $(LFLAGS)
test: .obj/test.o $(OBJ)
		@$(MKVERSION)
		@echo "LD $@"
		@$(LD) -o $@ $^ .obj/version.o $(LFLAGS)

run-test:test
		./test

clean:
	rm -f .obj/*.o .obj/*.mod mcmule test
	for g in $(WGROUPS); do make -C $$g clean ; done
	rm -rf report.info report/ .obj/*.gcda .obj/*.gcno

very-clean:
	rm -f .obj/*.o .obj/*.mod mcmule test
	for g in $(GROUPS); do make -C $$g clean ; done
	rm -f $(COLLIER).tar.gz
	rm -rf $(COLLIER)/ hplog

hash:
		@for g in $(GROUPS) .; do make -s --no-print-directory -C $$g .obj/tree.sha ; done
		@cat .obj/tree.sha $(addsuffix /.obj/tree.sha,$(GROUPS)) | $(SHA1)


### Install Collier
$(COLLIER).tar.gz:
		@echo "----------------------------------------"
		@echo "            Install Collier             "
		@echo "----------------------------------------"
		@echo " I'm going to download Collier 1.2.3    "
		@echo " If, for whatever reason, you don't have"
		@echo " a working internet connection, download"
		@echo " Collier manually and continue.         "
		@curl https://collier.hepforge.org/downloads/?f=collier-1.2.3.tar.gz > $@

$(COLLIER)/makefile_basic: $(COLLIER).tar.gz
		tar -xf $<
		touch $@

$(COLLIER)/makefile: $(COLLIER)/makefile_basic
		echo "FC = $(FC)" > $@
		echo "FFLAGS = -ffixed-line-length-132  -fno-default-integer-8" >> $@
		echo "FFLAGS += -J\$$(MODDIR) -I\$$(MODDIR)" >> $@
		echo "FFLAGS += -O2 -funroll-loops -Wtabs" >> $@
		cat $< | tail -n +3 | sed "s/#LINK = STATIC/LINK = STATIC/g" >> $@


$(COLLIER)/libcollier.a: $(COLLIER)/makefile
		make -C $(COLLIER)


### Install hpLog
hplog/adpg_v1_0.gz:
		mkdir -p hplog/
		curl -L https://data.mendeley.com/datasets/35hhxm63z3/1/files/b698beca-f509-419c-81a1-97c8b1e1e673/adpg_v1_0.gz > $@

hplog/hplog.f: hplog/adpg_v1_0.gz
		cat $< | gzip -d | tail -n+4 > $@

hplog/hplog.o: hplog/hplog.f
		@echo "F   $@"
		@$(FC) -c -o $@ $<

report.info: test
		./$<
		lcov --capture --directory .  --output $@

report/index.html:report.info
		genhtml $< --output-directory report --ignore-errors source
