  PROGRAM TEST

  use collier
  use integrands
  use vegas_m

  implicit none
  integer npass(2), nfail(2)
  real(kind=prec) timing(2)
  character(len=47) :: blocktitle
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5, p6, p7

  call printhead
  call init_cll(5,folder_name="")
  call initcachesystem_cll(1,5)

  call initflavour("muoneOld")
  musq = Mm**2
  npass=0 ; nfail= 0
  call cpu_time(timing(1))
  pol1 = (/ 0., 0., 0., 0. /)


  call testloop
  call testmudecdixon
  call testmudecff
  call testmudecmatel
  call testmjmatel
  call testmudecsoft
  call testmjsoft
  call testmudecspeed
  call testmudecvegas
  call testmjvegas
  call testmudecpol

  call testmueff
  call testmuematel
  call testmuesoftn1
  call testmuesoftn2
  call testmuespeed
  call testmuevegas

  call testmupmatel
  call testmupsoftn1
  call testmupsoftn2
  call testmupvegas

  call testenvegas

  call testmudecrare
  call testmudecraren1
  call testmudecrarespeed
  call testmudecsoftrare
  call testmudecvegasrare

  call cpu_time(timing(2))
  print*
  write(*,'(A,F5.1,A)')"Test suite completed in ",timing(2)-timing(1),"s."
  write(*,'(A,I3,A,I3,A,I2)') "passing ",npass(1),'/',npass(1)+nfail(1),' tests, failing',nfail(1)

  if (nfail(1).gt.0) then
    write(*,'(A,I2,A)')char(27)//'[31mWe have failed ',nfail(1),' tests!'//char(27)//'[0m'
    stop 9
  endif


contains


  SUBROUTINE TESTLOOP
  implicit none
  integer::i
  real(kind=prec) :: Emin, Emax, Ebeam
  real(kind=prec) :: tmin, tmax, t
  real(kind=prec) :: umin, umax, u
  real :: e1(10000),e2(10000),e3(10000)
  real (kind=prec) :: a_cheb(10)


  call blockstart("Test Loop Functions against Collier")

101 continue
  print*, "----------------------------------------------"
  print*, "discb(s,m,M) & scalarc0ir6(s,m,M):"
  print*, "----------------------------------------------"
  Ebeam = 150000._prec; Emin = 1000._prec
  call check("DiscB(s,m,M)"      ,discb(scms,me,mm), real(discb_coll(scms,me,mm)))
  call check("ScalarC0IR6(s,m,M)",scalarc0ir6(scms,me,mm), real(scalarc0ir6_coll(scms,me,mm)))

102 continue
  print*, "----------------------------------------------"
  print*, "discb(t,m), scalarc0(t,m) & scalarc0ir6(t,m):"
  print*, "----------------------------------------------"
  tmin = -sq_lambda(scms,me,mm)**2/scms; tmax = 2*me**2-2*me*Emin

  do i=1,10000
    t = tmin+i*(tmax-tmin)/10000
    e1(i) = abs(1-discb(t,me) / real(discb_coll(t,me,me)))
    e2(i) = abs(1-scalarc0(t,me) / real(scalarc0_coll(me**2,me**2,t,0.,me,0.)))
    e3(i) = abs(1-scalarc0ir6(t,me) / real(scalarc0ir6_coll(t,me,me)))
    if(isnan(e1(i))) e1(i) = 1E6
    if(isnan(e2(i))) e2(i) = 1E6
    if(isnan(e3(i))) e3(i) = 1E6
  end do
  call check("DiscB(t,m)",       maxval(e1))
  call check("ScalarC0(t,m)",    maxval(e2))
  call check("ScalarC0IR6(t,m)", maxval(e3),threshold=1e-6)



103 continue
  print*, "----------------------------------------------"
  print*, "discb(t,M), scalarc0(t,M) & scalarc0ir6(t,M):"
  print*, "----------------------------------------------"
  tmin = -sq_lambda(scms,me,mm)**2/scms; tmax = 2*me**2-2*me*Emin
  do i=1,10000
    t = tmin+i*(tmax-tmin)/10000
    e1(i) = abs(1-discb(t,mm) / real(discb_coll(t,mm,mm)))
    e2(i) = abs(1-scalarc0(t,mm) / real(scalarc0_coll(mm**2,mm**2,t,0.,mm,0.)))
    e3(i) = abs(1-scalarc0ir6(t,mm) / real(scalarc0ir6_coll(t,mm,mm)))
    if(isnan(e1(i))) e1(i) = 1E6
    if(isnan(e2(i))) e2(i) = 1E6
    if(isnan(e3(i))) e3(i) = 1E6
  end do
  call check("DiscB(t,M)",       maxval(e1))
  call check("ScalarC0(t,M)",    maxval(e2))
  call check("ScalarC0IR6(t,M)", maxval(e3))


104 continue
  print*, "----------------------------------------------"
  print*, "discb(u,m,M) & scalarc0ir6(u,m,M):"
  print*, "----------------------------------------------"
  umin = me**2+mm**2-2*me*Ebeam
  umax = min(2*me**2+2*mm**2-scms+sq_lambda(scms,me,mm)**2/scms, (me-mm)**2)
  do i=1,10000
    u = umin+i*(umax-umin)/10000
    e1(i) = abs(1-discb(u,me,mm) / real(discb_coll(u,me,mm)))
    e2(i) = abs(1-scalarc0ir6(u,me,mm) / real(scalarc0ir6_coll(u,me,mm)))
    if(isnan(e1(i))) e1(i) = 1E6
    if(isnan(e2(i))) e2(i) = 1E6
  end do
  call check("DiscB(u,m,M)      ", maxval(e1))
  call check("ScalarC0IR6(u,m,M)", maxval(e2),threshold=5.e-8)


105 continue
  print*, "----------------------------------------------"
  print*,       "scalarc0ir6(u,m,M) around zero:"
  print*, "----------------------------------------------"
  umin = -50._prec; umax = 50._prec
  do i=1,10000
    u = umin+i*(umax-umin)/10000
    if(abs(scalarc0ir6(u,me,2*mm)) < 1E-8) then
      e1(i) = abs(scalarc0ir6(u,me,mm) - real(scalarc0ir6_coll(u,me,mm)))
    else
      e1(i) = abs(1-scalarc0ir6(u,me,mm) / real(scalarc0ir6_coll(u,me,mm)))
    end if
    if(isnan(e1(i))) e1(i) = 1E6
  end do
  call check("SCALARC0IR6(u~0,m,M)", maxval(e1),threshold=1e-8)


106 continue
  print*, "----------------------------------------------"
  print*,      "deltalph leptonic"
  print*, "----------------------------------------------"
  !! numerical stability
  !tmin = -1000.; tmax = 0.
  !open(1, file = 'deltalph0.dat', status = 'new')
  !do i=1,10000
  !  t = tmin+i*(tmax-tmin)/10000
  !  write(1,*), t, deltalph_l_0(t,.5_prec,1.)
  !end do
  !close(1)
  call check("deltalph_l_0", deltalph_l_0(-210.5_prec,.5_prec), 0.5385992225779098_prec ,1e-11)
  call check("deltalph_l_0 small q2", deltalph_l_0(-1.1E-2_prec,.5_prec), 0.0009293357080351963 ,1e-11)
  call check("deltalph_l_0 small q2", deltalph_l_0(-0.9E-2_prec,.5_prec), 0.0007610127063651012 ,1e-11)
  call check("deltalph_l_1", deltalph_l_1(-210.5_prec,.5_prec), 0.2688940282852546_prec ,1e-11)


107 continue
  print*, "----------------------------------------------"
  print*,      "clenshaw"
  print*, "----------------------------------------------"
  a_cheb = (/1.88877557e-01,   4.64127930e-16,  -2.47975524e-01,  -4.14821613e-16, 1.57872017e-01,&
             3.52356831e-16,  -9.30280327e-02,  -1.83143107e-16,   4.30685337e-02, 8.34423280e-17 /)
  call check("clenshaw", clenshaw(a_cheb, -5., 5., -0.995995995996,10), 0.558903091114, 1E-9)


108 continue
  print*, "----------------------------------------------"
  print*,      "ScalarD0IR16"
  print*, "----------------------------------------------"

  call check('ScalarD0IR16 a', &
     ScalarD0IR16(0.13, 0.46, 0.123, 0.6, 0.42, 0.5, 105.),-0.047341279733872865)
  call check('ScalarD0IR16 a', &
     ScalarD0IR16(0.13, 0.75, 0.8, 0.9, 0.61, 0.42, 0.24), -42.37199150333324)


109 continue
  print*, "----------------------------------------------"
  print*,      "cmpl. DiscB"
  print*, "----------------------------------------------"
  call check("DiscB(40., 0.5, 0.5)", DiscB(40.,0.5), -4.998872501140289)
  call check("DiscB(0.8, 0.5, 0.5)", DiscB(0.8,0.5), -1.1071487177940904)


! print*, "----------------------------------------------"
! print*,             "performance test (scalarc0ir6):"
! print*, "----------------------------------------------"
! smin = -100000.; smax = 100000.
! call cpu_time(t1)
! do i=1,10000000
!   e4(i) = scalarc0ir6(ss,me,mm)
! end do
! call cpu_time(t2)
! do i=1,10000000
!   e5(i) = scalarc0ir6_coll(ss,me,mm)
! end do
! call cpu_time(t3)
! print*, "in-house:", t2-t1
! print*, "collier:", t3-t2


  call blockend(20)
  END SUBROUTINE

  SUBROUTINE TESTMUDECDIXON
  use mudec, only: pm2enn, m2enn, m2ennl, pm2enng, m2enngav, pm2enngg
  use mudecrare, only: pm2ennee, pt2mnnee, pm2enneeg
  implicit none
  integer ranseed, i, j
  real(kind=prec), parameter :: MtauOld = 1776.82_prec
  real(kind=prec), parameter :: MelOld = 0.510998928_prec
  real(kind=prec), parameter :: MmuOld = 105.6583715_prec
  real(kind=prec) arr(14), matrix(4,4,3), weight, ans(3), pol2(4)

  call blockstart("mudec Dixon averaging")

  pol1 = (/ 0., 0., 0.8, 0. /)
  pol2 = (/ 0., 0., 0.0, 0. /)

  ranseed = 23313
  do i=1,5
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(4:5) = (/real(j)/4., real(i)/4. /)
      call psd4(arr(:5),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)
      matrix(i,j,1) = m2enn(p1, p2, p3, p4)
      matrix(i,j,2) = pm2enn(p1, pol1, p2, p3, p4)
      matrix(i,j,3) = m2ennl(p1, p2, p3, p4)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENN" , ans(1), pm2ennav (p1, pol2, p2, p3, p4))
  call check("PM2ENN" , ans(2), pm2ennav (p1, pol1, p2, p3, p4))
  call check(" M2ENNL", ans(3), pm2ennlav(p1, pol2, p2, p3, p4))


  ranseed = 23313
  do i=1,8
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(7:8) = (/real(j)/4., real(i)/4. /)
      call psd5_fks(arr(:8),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
      matrix(i,j,1) = pm2enng(p1, pol2, p2, p3, p4, p5)
      matrix(i,j,2) = pm2enng(p1, pol1, p2, p3, p4, p5)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNG", ans(1),  m2enngav(p1,       p2, p3,p4,p5))
  call check("PM2ENNG", ans(2), pm2enngav(p1, pol1, p2, p3,p4,p5))

  ranseed = 2105103485
  do i=1,11
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(10:11) = (/real(j)/4., real(i)/4. /)
      call psd6_fkss(arr(:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,p6,weight)
      matrix(i,j,1) = pm2enngg(p1, pol2, p2, p3, p4, p5,p6)
      matrix(i,j,2) = pm2enngg(p1, pol1, p2, p3, p4, p5,p6)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNGG", ans(1), pm2ennggav(p1, pol2, p2, p3,p4,p5, p6))
  call check("PM2ENNGG", ans(2), pm2ennggav(p1, pol1, p2, p3,p4,p5, p6))

  weight = 0.
  ranseed = 142169178
  do i=1,11
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(10:11) = (/real(j)/4., real(i)/4. /)
      call psd6_23_24_34_e56(arr(:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,Me,p6,Me,weight)
      matrix(i,j,1) = pm2ennee(p1, pol2, p2, p3, p4, p6,p5)
      matrix(i,j,2) = pm2ennee(p1, pol1, p2, p3, p4, p6,p5)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNEE", ans(1), pm2enneeav(p1, pol2, p2, p3,p4,p5, p6))
  call check("PM2ENNEE", ans(2), pm2enneeav(p1, pol1, p2, p3,p4,p5, p6))


  weight = 0.
  ranseed = 142169178
  do i=1,11
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(10:11) = (/real(j)/4., real(i)/4. /)
      call psd6_23_24_34_e56(arr(:11),p1,MtauOld,p2,MmuOld,p3,0._prec,p4,0._prec,p5,MelOld,p6,MelOld,weight)
      matrix(i,j,1) = pt2mnnee(p1, pol2, p2, p3, p4, p6,p5)
      matrix(i,j,2) = pt2mnnee(p1, pol1, p2, p3, p4, p6,p5)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" T2MNNEE", ans(1), pt2mnneeav(p1, pol2, p2, p3,p4,p5, p6))
  call check("PT2MNNEE", ans(2), pt2mnneeav(p1, pol1, p2, p3,p4,p5, p6))


  weight = 0.
  ranseed = 1599259816
  do i=1,14
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(13:14) = (/real(j)/4., real(i)/4. /)
      call psd7_27_37_47_2x5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,Me,p6,Me,p7,weight)
      matrix(i,j,1) = pm2enneeg(p1, pol2, p2, p3, p4, p6,p5,p7)
      matrix(i,j,2) = pm2enneeg(p1, pol1, p2, p3, p4, p6,p5,p7)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) e+(p5) e-(p6) g(p7)
    !!M1+(p1)->  M2+(p2)                          ,M2-(p5),M2+(p6),2\nu,\gamma(p7)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNEEG", ans(1), pm2enneegav(p1, pol2, p2, p3,p4,p5, p6, p7), 1e-5)
  call check("PM2ENNEEG", ans(2), pm2enneegav(p1, pol1, p2, p3,p4,p5, p6, p7), 1e-5)

  END SUBROUTINE


  SUBROUTINE TESTMUDECFF
  use mudec_h2lff
  implicit none
  call blockstart("mudec ff")
  call init_hpls(0.3, 0.01)

  call check("F1 a^1 ep^-1", ff1a1es(), +3.248495242049358989123344e0)
  call check("F1 a^1 ep^ 0", ff1a1ef(), +2.052181859329036520300261e1)
  call check("F1 a^1 ep^+1", ff1a1el(), +8.151753282417219295365189e1)
  call check("F1 a^2 ep^-2", ff1a2ed(), +5.276360668808661723318332e0)
  call check("F1 a^2 ep^-1", ff1a2es(), +6.666503005850382070713379e1)
  call check("F1 a^2 ep^ 0", ff1a2ef(), +4.808563950549486399879157e2)
  call check("F2 a^1 ep^ 0", ff2a1ef(), -1.188916479795774596375462e0)
  call check("F2 a^1 ep^+1", ff2a1el(), -5.077904525084570305436700e0)
  call check("F2 a^2 ep^-1", ff2a2es(), -3.862189527810646623624964e0)
  call check("F2 a^2 ep^ 0", ff2a2ef(), -3.964827420963052492573208e1)
  call check("F3 a^1 ep^ 0", ff3a1ef(), +2.307306240748826479956638e0)
  call check("F3 a^1 ep^+1", ff3a1el(), +6.929293647070190329817592e0)
  call check("F3 a^2 ep^-1", ff3a2es(), +7.495273345023355640864092e0)
  call check("F3 a^2 ep^ 0", ff3a2ef(), +7.679625302515490979416808e1)

  END SUBROUTINE

  SUBROUTINE TESTMUDECMATEL
  use vegas_m
  use mudec_mat_el, only: pm2ennlavz, pm2ennlav2
  implicit none
  integer i,ranseed
  real(kind=prec) :: arr(8), weight, pol2(4)
  real(kind=prec), parameter :: xicut = .8
  real(kind=prec) double, single, finite, lin
  real(kind=prec) :: ref

  musq = 0.4*mm**2
  pol1 = (/ 0., 0., 0.8, 0. /)
  pol2 = (/ 0., 0., 0.0, 0. /)

  call blockstart("mudec n matel")
  ranseed = 23313
  do i=1,5
    arr(i) = ran2(ranseed)
  enddo
  call psd4(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)

  finite = pm2ennav(p1,pol2,p2,p3,p4,single)
  call check(" m2enn  ep^ 0", finite, 342653161.41427898)
  call check(" m2enn  ep^ 1", single, -135609060.15321922)

  finite = pm2ennav(p1,pol1,p2,p3,p4,single)
  call check("pm2enn  ep^ 0", finite,  3.7773749153467834e8)
  call check("pm2enn  ep^ 1", single, -1.4480122101244792e8)


  finite = pm2ennlav(p1,pol2,p2,p3,p4, single)
  call check(" m2ennl ep^-1", single, 468783053.17430252)
  call check(" m2ennl ep^ 0", finite, 2739553395.0334105)

  finite = pm2ennlav(p1,pol1,p2,p3,p4, single)
  call check("pm2ennl ep^-1", single, 5.1678184975456995e8)
  call check("pm2ennl ep^ 0", finite, 3.0193229868427277e9)

  finite = pm2ennlav2(p1,pol2,p2,p3,p4, single, lin)
  call check(" m2ennl ep^-1", single, 468783053.17430252)
  call check(" m2ennl ep^ 0", finite, 2739553395.0334105)
  call check(" m2ennl ep^ 1", lin   , 1.106972974642307e10, threshold=1e-7)

  finite = pm2ennlav2(p1,pol1,p2,p3,p4, single, lin)
  call check("pm2ennl ep^-1", single, 5.1678184975456995e8)
  call check("pm2ennl ep^ 0", finite, 3.0193229868427277e9)
  call check("pm2ennl ep^ 1", lin   , 1.2200881677254194e10)

  finite = pm2ennlavz(p1,pol1,p2,p3,p4, single, lin)
  call check("pm2ennlz ep^-1", single, 5.1678184975456995e8, threshold=1e-4)
  call check("pm2ennlz ep^ 0", finite, 3.0193229868427277e9, threshold=1e-4)
  call check("pm2ennlz ep^ 1", lin   , 1.2200881677254194e10, threshold=1e-4)


  call check("pm2ennll ep^ 0", pm2ennllavz(p1, pol1, p2, p3, p4), 1.1406192697089768e12 / 8. / pi**2)

  xieik2 = 1.
  ref = 3.376185552957104e8 + 8.418077028309364e8 * log(xieik2) + 6.413410868182428e8 * log(xieik2)**2
  call check(" m2ennff exp. (xc=1.)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref)
  xieik2 = 0.3
  ref = 3.376185552957104e8 + 8.418077028309364e8 * log(xieik2) + 6.413410868182428e8 * log(xieik2)**2
  call check(" m2ennff exp. (xc=.3)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref)

  xieik2 = 1.
  call check("pm2ennff exp. (xc=1.)", pm2ennffavz(p1, pol1, p2, p3, p4), 2*3.7158802555700123e8)

  xieik2 = 1.
  ref = 3.386617722537384e8 + 8.418077028309393e8 * log(xieik2) + 6.413410868182429e8 * log(xieik2)**2
  call check(" m2ennff full (xc=1.)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref, threshold=5e-3)
  xieik2 = 0.3
  ref = 3.386617722537384e8 + 8.418077028309393e8 * log(xieik2) + 6.413410868182429e8 * log(xieik2)**2
  call check("pm2ennff full (xc=.3)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref, threshold=5e-3)

  call blockend(23)


  call blockstart("mudec n+1 matel")
  ranseed = 23313
  do i=1,8
    arr(i) = ran2(ranseed)
  enddo
  call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)

  print*, "----------------------------------------------"
  print*, "            fully massive electron            "
  print*, "----------------------------------------------"

  call check(" m2enng", pm2enngav(p1,pol2,p2,p3,p4,p5), 0.5*1.1090579644178507e8,1e-14)
  call check("pm2enng", pm2enngav(p1,pol1,p2,p3,p4,p5), 0.5*1.5351072643264967e8,1e-14)

  finite = pm2ennglav(p1,pol2,p2,p3,p4,p5,single)
  call check(" m2enngl ep^-1",single,7.367233751589806e7)
  call check(" m2enngl ep^ 0",finite,4.398220722023348e8)
  finite = pm2ennglav(p1,pol1,p2,p3,p4,p5,single)
  call check("pm2enngl ep^-1",single,1.019738770461223e8)
  call check("pm2enngl ep^ 0",finite,6.082171587230729e8)
  xieik1 = xicut
  finite = pm2enngfav(p1,pol2,p2,p3,p4,p5)
  call check(" m2enngf (0.8)",finite,8.193452982244721e7)
  finite = pm2enngfav(p1,pol1,p2,p3,p4,p5)
  call check("pm2enngf (0.8)",finite,1.128456057964394e8)


  print*, "----------------------------------------------"
  print*, "              massless electron               "
  print*, "----------------------------------------------"
  finite = pm2ennglav0(p1,pol2,p2,p3,p4,p5,single,double)
  call check(" m2enngl0 ep^-2",double,-8.82692430785778e6)
  call check(" m2enngl0 ep^-1",single,-1.67705979687696e7)
  call check(" m2enngl0 ep^ 0",finite,-4.29712088547953e7)
  finite = pm2ennglav0(p1,pol1,p2,p3,p4,p5,single,double)
  call check("pm2enngl0 ep^-2",double,-1.221796446430408e7)
  call check("pm2enngl0 ep^-1",single,-2.321335981607474e7)
  call check("pm2enngl0 ep^ 0",finite,-5.665112363482363e7)


  print*, "----------------------------------------------"
  print*, "             massified electron               "
  print*, "----------------------------------------------"
  finite = pm2ennglavz(p1,pol2,p2,p3,p4,p5,single)
  call check(" m2ennglz ep^-1",single,7.367807273857147e7)
  call check(" m2ennglz ep^ 0",finite,4.398352157488666e8)
  finite = pm2ennglavz(p1,pol1,p2,p3,p4,p5,single)
  call check("pm2ennglz ep^-1",single,1.019829833271501e8)
  call check("pm2ennglz ep^ 0",finite,6.116350803936851e8)
  xieik1 = xicut
  finite = pm2enngfavz(p1,pol2,p2,p3,p4,p5)
  call check(" m2enngfz (0.8)",finite,8.190620335173568e7)
  finite = pm2enngfavz(p1,pol1,p2,p3,p4,p5)
  call check("pm2enngfz (0.8)",finite,1.162004531454846e8)

  call blockend(20)

  musq=mm**2
  pol1 = (/ 0., 0., 0., 0. /)

  END SUBROUTINE

  SUBROUTINE TESTMJMATEL
  use vegas_m
  implicit none
  integer i,ranseed
  real(kind=prec) :: arr(5), weight, pol1(4), pol2(4)
  real(kind=prec), parameter :: xicut = 0.1
  real(kind=prec) double, single, finite

  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV

  pol1 = (/ 0., 0., 0., 0. /)
  pol2 = (/ 0., 0., 0.85, 0. /)

  print*, " "
  call blockstart("Majoron test")
  ranseed = 42
  do i=1,2
    arr(i) = ran2(ranseed)
  enddo
  call psd3(arr(1:2),p1,Mm,p2,Me,p3,Mj,weight)

  print*, "----------------------------------------------"
  print*, "                  mu --> e + J                "
  print*, "----------------------------------------------"

  !print*, "Mm:", sqrt(sq(p1)), "Me:", sqrt(sq(p2))
  !print*, "Mj:", sqrt(sq(p3)), "s2n:", s(pol2,p2)
  !print*, m2ejl(p1,pol2,p2,p3)


  call check("m2ej ", pm2ej (p1,pol1,p2,p3), 116.52663743110394,1e-16)
  call check("m2ej ", pm2ej (p1,pol2,p2,p3), 112.04758385058322,1e-16)
  call check("m2ejl", pm2ejl(p1,pol1,p2,p3), 1152.53482224409  ,1e-16)
  call check("m2ejl", pm2ejl(p1,pol2,p2,p3), 1108.20477641089  ,1e-16)



  pol1 = (/ 0., 0., 0., 0. /)
  pol2 = (/ 0., 0., 0.85, 0. /)

  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV

  ranseed = 4242
  do i=1,5
    arr(i) = ran2(ranseed)
  enddo
  call psd4_fks(arr,p1,Mm,p2,Me,p3,Mj,p4,weight)

  print*, "----------------------------------------------"
  print*, "             mu --> e + J + gamma             "
  print*, "----------------------------------------------"

  !print*, "Mm:", sqrt(sq(p1)), "Me:", sqrt(sq(p2))
  !print*, "Mj:", sqrt(sq(p3)), "Mg:", sqrt(sq(p4))
  !print*, "s14:", s(p1,p4), "s24:", s(p2,p4)
  !print*, "s2n:", s(pol1,p2), "s4n:", s(pol1,p4)
  !print*, "s2n:", s(pol2,p2), "s4n2:", s(pol2,p4)

  call check("m2ejg", pm2ejg(p1,pol1,p2,p3,p4), 48627.8811945461  ,1e-16)
  call check("m2ejg", pm2ejg(p1,pol2,p2,p3,p4), 10722.023858830065,1e-16)

  call blockend(6)

  END SUBROUTINE


  SUBROUTINE TESTMUDECSOFT
  use vegas_m
  implicit none
  integer i,j,ido, ranseed, lastgood(4)
  real(kind=prec) :: arr(8), weight
  real(kind=prec) :: full(4), lim(4), lastratio(4), ratio(4)
  real(kind=prec), parameter :: xicut = .8
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5),dimension(4) :: colours


  call blockstart("mudec \xi->0")

  ranseed = 23313

  do ido=1,5
    do i=1,8
      arr(i) = ran2(ranseed)
    enddo
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if (weight.lt.zero) cycle
    lastratio = 1.
    do i=2,10
      arr(1) = 0.8_prec/(10.**i)
      call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)

      xieik1 = xicut
      full = xiout**2*(/ pm2enngav  (p1,pol1,p2,p3,p4,p5) , &
                         pm2ennglav (p1,pol1,p2,p3,p4,p5) , &
                         pm2enngfav (p1,pol1,p2,p3,p4,p5) , &
                         pm2enngfavz(p1,pol1,p2,p3,p4,p5) /)
      lim =  (/ pm2enngav_s  (p1,pol1,p2,p3,p4) , &
                pm2ennglav_s (p1,pol1,p2,p3,p4) , &
                pm2enngfav_s (p1,pol1,p2,p3,p4) , &
                pm2enngfzav_s(p1,pol1,p2,p3,p4) /)


      print*, "        "
      print*, " WEIGHT: ", weight, xiout
      !print*, " full: ", full
      !print*, " lim: ", lim

      ratio = abs(1-lim/full)
      do j=1,4
        if (ratio(j)<lastratio(j)) then
          lastgood(j) = i
          lastratio(j) = ratio(j)
          colours(j) = norm
        else
          colours(j) = red
        endif
      enddo
      write(*,900) colours(1),ratio(1), colours(2),ratio(2), &
                   colours(3),ratio(3), colours(4),ratio(4), norm
    enddo
    call check("tree-level",lastratio(1),threshold=1.e-10)
    call check("real-virtual",lastratio(2),threshold=1.e-8)
    call check("fin mat-el",lastratio(3),threshold=5.e-8)
    !call check("fin(z) mat-el",lastratio(4),threshold=5.e-8)
    call check("fin(z) mat-el",lastratio(4),threshold=7.e-4)  ! This is way too lose
  enddo
  call blockend(8)

900 format("1-ratio: ",A,ES13.3, A,ES13.3,A,ES13.3,A,ES13.3,A)
  END SUBROUTINE

  SUBROUTINE TESTMJSOFT
  use vegas_m
  implicit none
  integer i,j,ido, ranseed, lastgood
  real(kind=prec) :: arr(8), weight
  real(kind=prec) :: full, lim, lastratio, ratio
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5),dimension(1) :: colours

  call blockstart("mudec majoron \xi->0")

  pol1 = 0.
  ranseed = 23313

  do ido=1,3
    do i=1,8
      arr(i) = ran2(ranseed)
    enddo
    call psd4_fks(arr(1:5),p1,Mm,p2,Me,p3,Mj,p4,weight)
    if (weight.lt.zero) cycle
    lastratio = 1.
    do i=2,8
      arr(1) = 0.8_prec/(10.**i)
      call psd4_fks(arr,p1,Mm,p2,Me,p3,Mj,p4,weight)

      full = xiout**2* pm2ejg(p1,pol1,p2,p3,p4)
      lim  =           pm2ejg_s(p1,pol1,p2,p3)

      print*, "        "
      print*, " WEIGHT: ", weight, xiout
      ratio = abs(1-lim/full)
      if (ratio<lastratio) then
        lastgood = i
        lastratio = ratio
        colours = norm
      else
        colours = red
      endif
      write(*,900) colours,ratio, norm
    enddo
    call check("tree-level",lastratio,threshold=1.e-15)
  enddo
  call blockend(3)

900 format("1-ratio: ",A,ES13.3,A)
  END SUBROUTINE



  SUBROUTINE TESTMUDECSPEED
  implicit none
  integer, parameter :: niter = 20000
  real (kind=prec) :: weight, arr(8), finite
  real (kind=prec) :: startt, endt, rate
  integer i, j, ranseed

  call blockstart("mudec performance")
  ! on my notebook I saw the following rates
  !   no optimisation:     ~32 kEv/s
  !   -O3 optimisation:   ~122 kEv/s
  !   MMA               : ~121 kEv/s
  !   MMA + -O3         : ~123 kEv/s
  ! -> use only O3 optimisation and don't bother with MMA optimisation

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if (weight.lt.zero) cycle
    finite=pm2ennglav(p1, pol1, p2, p3,p4,p5)
  enddo
  call cpu_time(endt)
  rate = niter/(endt-startt)
  call checkrange("event rate (Ev/s)", rate, 50.e2,1e50)



#ifdef RADMUDECCDR
  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if (weight.lt.zero) cycle
    finite=pm2ennglavcdr(p1, pol1, p2, p5)
  enddo
  call cpu_time(endt)
  call checkrange("speed-up", rate/(niter/(endt-startt)), 10.,100.)
  call blockend(2)
#else
  write(*,'(A)')char(27)//'[33m[CANT]'//char(27)//'[0m cannot compare event rate. Compile with USE_RADMUDEC_CDR=1 to enable'
  call blockend(1)
#endif

  END SUBROUTINE


  SUBROUTINE TESTMUDECRARE
  implicit none
  real(kind=prec) :: arr(11), weight, finite, single
  real(kind=prec) s12,s13,s14,s23,s24,s34,m12,m22,m32, deltaren
  integer i, j, ranseed
  complex(kind=prec) uvdump(46), pve1c(40)
   real(prec),parameter :: massMu  = 1000.*       0.105658372000000_prec
   real(prec),parameter :: massEl  = 1000.*       0.000510998930000_prec
   real(prec),parameter :: massTau = 1000.*       1.776820000000000_prec
  real(kind=prec), parameter :: MtauOld = 1776.82_prec
  real(kind=prec), parameter :: MelOld = 0.510998928_prec
  real(kind=prec), parameter :: MmuOld = 105.6583715_prec
  integer :: ndim
  procedure(integrand), pointer :: fxn


  Nhad = 0
  call blockstart("mudec rare")

  arr = (/  0.24198828748173506 , 0.29688027594821942 , 0.16379459616382441 , 0.64612797092642082 , &
            0.42594211662333048 , 0.66550862851375925 , 0.99940334884282045 , 0.99344043621461031 , &
            0.04421843807993386 , 0.14852028093172465 , 0.59387643773429266  /)
  call psd6_23_24_34(arr,p1,massTau,p2,massMu,p5,0._prec,p6,0._prec,p3,massEl,p4,massEl,weight)
  s12=s(p1,p2) ; s13=s(p1,p3) ; s14 = s(p1,p4)
  s23=s(p2,p3) ; s24=s(p2,p4)
  s34=s(p3,p4)
  m12 = sq(p1) ; m22 = sq(p2) ; m32 = sq(p3)

  musq = masstau**2

  pol1 = 0.
  finite = pt2mnneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * ( log(MelOld**2/MtauOld**2) + log(MmuOld**2/MtauOld**2) ) / 3 / pi * finite
  call check("GoSam polarised tau LO",finite,31804.075255941159, threshold=8e-8)
  finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4,single)
  ! fudge factor due to change in mass
  finite = finite + 0.9999999691696316* pt2mnneeav_a(p1,pol1,p2,p5,p6,p3,p4)
  call check("GoSam tau finite",finite,1386504.3246603629 + deltaren, threshold=1e-10)
  call check("GoSam tau single",single,138111.92455647807, threshold=6e-10)

  pol1 = (/ 0., 0., 0.85, 0. /)

  finite = pt2mnneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * ( log(MelOld**2/MtauOld**2) + log(MmuOld**2/MtauOld**2) ) / 3 / pi * finite
  call check("GoSam polarised tau LO",finite,36539.897503867433, threshold=4e-8)
  finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4,single)
  finite = finite +  pt2mnneeav_a(p1,pol1,p2,p5,p6,p3,p4)
  call check("GoSam polarised tau finite",finite,1592169.4648204616 + deltaren, threshold=4e-8)
  call check("GoSam polarised tau single",single,158677.62695853604, threshold=4e-8)
  pol1 = 0.

  call psd6_23_24_34(arr,p1,Mm,p2,Me,p5,0._prec,p6,0._prec,p3,Me,p4,Me,weight)
  musq = mm**2
  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)

  Ntau = 0
  finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * log(MelOld**2/MmuOld**2) / 3 / pi * finite
  call check("Math  mu LO    ",finite,4 * 34127.704521729924, threshold=1e-9)
  finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4)
  ! fudge factor due to changed mass
  finite = finite + 1.0000000139985779 * pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4)
  finite = finite + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)
  call check("GoSam mu finite",finite,2 * 1786955.3118638927 + deltaren, threshold=1e-9)
  call check("Math  mu finite",finite,2 * 1786955.3112681818 + deltaren)


  call psd6_26_2x5(arr, p1, Mm, p2, Me, p5, 0._prec, p6, 0._prec, p3, Me, p4, Me, weight)
  finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * log(Me**2/Mm**2) / 3 / pi * finite

  which_piece = 'm2enneeV'
  call initpiece(ndim, fxn)
  finite = fxn(arr, 1._prec, ndim)
  which_piece = 'm2enneeA'
  call initpiece(ndim, fxn)
  finite = finite + fxn(arr, 1._prec, ndim)
  which_piece = 'm2enneeAi'
  call initpiece(ndim, fxn)
  finite = finite + fxn(arr, 1._prec, ndim)
  which_piece = 'm2ennee0b'
  call initpiece(ndim, fxn)
  finite = finite - fxn(arr, 1._prec, ndim) * (-2 * alpha * log(Me**2/Mm**2) / 3 / pi)

  !print*,finite
  finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)&
    -deltaren
  !print*,finite * weight * convfac


  call psd6_26_2x5(arr, p1, MmuOld, p2, MelOld, p5, 0._prec, p6, 0._prec, p3, MelOld, p4, MelOld, weight)
  finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * log(Me**2/Mm**2) / 3 / pi * finite
  finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)&
  -deltaren


  !print*,finite * weight * convfac
  !pol1 = (/ 0., 0., 0.85, 0. /)
  !finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  !deltaren = -2 * alpha * log(MelOld**2/MmuOld**2) / 3 / pi * finite
  !call check("Math  polarised mu LO    ",finite,4 * 32036.4789853319, threshold=2e-9)
  !finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)
  !call check("GoSam polarised mu finite",finite,2 * 1673902.3152368125 + deltaren, threshold=1e-9)
  !call check("Math  polarised mu finite",finite,2 * 1673902.3146805929 + deltaren)
  !pol1 = 0.
  call blockend(9)
  Ntau = 1
  Nhad = 1


  END SUBROUTINE

  SUBROUTINE TESTMUDECRAREN1
  implicit none
  real(kind=prec) :: arr(14), weight
  integer ranseed, i
  real(prec),parameter :: massMu  = 1000.*       0.105658372000000_prec
  real(prec),parameter :: massEl  = 1000.*       0.000510998930000_prec
  real(prec),parameter :: massTau = 1000.*       1.776820000000000_prec
  real(kind=prec), parameter :: MtauOld = 1776.82_prec
  real(kind=prec), parameter :: MelOld = 0.510998928_prec
  real(kind=prec), parameter :: MmuOld = 105.6583715_prec
  real(kind=prec) :: ans

  arr = (/ 0.49236414417476559, 0.74261882343084062, 0.06857487863144673, &
           0.43968827667712695, 0.16877849338194051, 0.35107011611948979, &
           0.71298814734328664, 0.02663140860819268, 0.48311756563250341, &
           0.90734947217492223, 0.42003407667164250, 0.40900599802920279, &
           0.26408923713766908, 0.47278811840281654 /)

  call blockstart("mudec rare n+1")
  call psd7_27_37_47_fks(arr,p1,massTau,p2,massmu,p3,massel,p4,massel,p6,0._prec,p5,0._prec,p7,weight)

  pol1 = 0.
  ans = pt2mnneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("two flavour unpolarised", ans, 7.884885920418635, threshold=1e-7)


  pol1 = (/ 0., 0.,  0.85, 0. /)
  ans = pt2mnneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("two flavour polarised", ans, 10.394629796346027, threshold=1e-7)
  pol1 = 0.

  ranseed = 185548976
  weight = 0.
  do while (weight < zero)
    do i=1,14
      arr(i) = ran2(ranseed)
    enddo
    call psd7_27_37_47_e56_fks(arr,p1,MtauOld,p2,Mel,p3,MelOld,p4,MelOld,p6,0._prec,p5,0._prec,p7,weight)
  enddo

  ans = pm2enneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("one flavour unpolarised", ans, 2*29847.869327681507, threshold=1e-7)

  pol1 = (/ 0., 0.,  0.85, 0. /)
  ans = pm2enneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("one flavour polarised", ans, 2*33258.882371870175, threshold=1e-7)
  pol1 = 0.
  call blockend(4)

  END SUBROUTINE



  SUBROUTINE TESTMUDECRARESPEED
  implicit none
  integer, parameter :: niter = 2000
  real (kind=prec) :: weight, arr(11), finite
  real (kind=prec) :: startt, endt, rate
  integer i, j, ranseed

  call blockstart("mudec rare performance")
  ! on my notebook I saw the following rates
  !   no optimisation:     ~32 kEv/s
  !   -O3 optimisation:   ~122 kEv/s
  !   MMA               : ~121 kEv/s
  !   MMA + -O3         : ~123 kEv/s
  ! -> use only O3 optimisation and don't bother with MMA optimisation

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,11
      arr(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34(arr,p1,Mtau,p2,Mm,p5,0._prec,p6,0._prec,p3,Me,p4,Me,weight)
    if (weight.lt.zero) cycle
    finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4)
  enddo
  call cpu_time(endt)
  rate = niter/(endt-startt)
  print*,"tau->mu e e nu nu at ",rate,"ev/s"


  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,11
      arr(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34(arr,p1,Mtau,p2,Mm,p5,0._prec,p6,0._prec,p3,Me,p4,Me,weight)
    if (weight.lt.zero) cycle
    finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4)
  enddo
  call cpu_time(endt)
  rate = niter/(endt-startt)
  print*," mu-> e e e nu nu at ",rate,"ev/s"



  END SUBROUTINE




  SUBROUTINE TESTMUDECSOFTRARE
  use vegas_m
  implicit none
  integer i,j,ido, ranseed, lastgood(2)
  real(kind=prec) :: arr(14), weight
  real(kind=prec) :: full(2), lim(2), lastratio(2), ratio(2)
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5),dimension(2) :: colours


  call blockstart("mudec rare \xi->0")

  ranseed = 23313

    do i=1,14
      arr(i) = ran2(ranseed)
    enddo
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    lastratio = 1.
    do i=2,10
      arr(1) = 0.8_prec/(10.**i)

      call psd7_27_37_47_e56_fks(arr,p1,Mm,p2,Me,p3,Me,p4,Me,p6,0._prec,p5,0._prec,p7,weight)
      full(1) = 2*xiout**2*pm2enneegav(p1, pol1, p2, p5, p6, p3, p4, p7) / 2!identical particles
      lim(1) = pm2enneegav_s(p1, pol1, p2, p5, p6, p3, p4)

      call psd7_27_37_47_e56_fks(arr,p1,Mtau,p2,Mmu,p3,Mel,p4,Mel,p6,0._prec,p5,0._prec,p7,weight)
      full(2) = xiout**2*pt2mnneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
      lim(2) = pt2mnneegav_s(p1, pol1, p2, p5,p6, p3, p4)

      print*, "        "
      print*, " WEIGHT: ", weight, xiout
      !print*, " full: ", full
      !print*, " lim: ", lim

      ratio = abs(1-lim/full)
      do j=1,2
        if (ratio(j)<lastratio(j)) then
          lastgood(j) = i
          lastratio(j) = ratio(j)
          colours(j) = norm
        else
          colours(j) = red
        endif
      enddo
      write(*,900) colours(1),ratio(1), colours(2),ratio(2), norm
    enddo

900 format("1-ratio: ",A,ES13.3, A,ES13.3,A,ES13.3,A,ES13.3,A)
  END SUBROUTINE


  SUBROUTINE TESTMUDECVEGASRARE
  implicit none
  xinormcut = 0.8
  call blockstart("mudec rare VEGAS test")

  call initflavour("mu-eOld")
  call test_int("m2ennee0", 100, 5, 1491904.0819583267)
  call test_int("m2enneeA",   1, 5, 352800.89443587803)
  call test_int("m2enneeV",   1, 5, 19275093.914834786, 1e-6)
  call test_int("m2enneeC", 100, 5,-13679364.203549761)
  call test_int("m2enneeR",  10, 5,-1455364.7767046469, 1e-6)

  call initflavour("tau-mueOld")
  call test_int("t2mnnee0", 100, 5, 6207821421011.6162)
  call test_int("t2mnneeA",   1, 5, 2435058375408.4263)
  call test_int("t2mnneeV",   1, 5, 40061407944369.828, 1e-6)
  call test_int("t2mnneeC",   1, 5,-5690978885467.4033)
  call test_int("t2mnneeR",  10, 5,-6297935441132.4092, 1e-6)

  call initflavour("mu-eOld")
  call test_int("m2ennee0", ans= 1.9277276245011420E+06 * 4)
  call test_int("m2enneeA", ans= 10253781.273581261)
  call test_int("m2enneeV", ans= 297551776.80220687, tol=1e-6)
  call test_int("m2enneeC", ans=-1.3870464788334735E+07 * 4)
  call test_int("m2enneeR", ans=-4.4656947405253053E+06 * 2, tol=1e-6)

  call initflavour("tau-mueOld")
  call test_int("t2mnnee0", ans= 1453801515578.3940)
  call test_int("t2mnneeA", ans= 2660029104918.3130)
  call test_int("t2mnneeC", ans=-10108666258685.348)
  call test_int("t2mnneeV", ans= 30542510313705.488, tol=1e-6)
  call test_int("t2mnneeR", ans=-5009873795881.5859, tol=1e-6)

  call blockend(20)
  END SUBROUTINE



  SUBROUTINE TESTMUDECVEGAS
  xinormcut = 0.8
  xinormcut1 = 0.2
  xinormcut2 = 0.3

  call blockstart("mudec VEGAS test")

  call test_INT('m2enn0' ,10,10, 9.4776358248750947e5)
  call test_INT('m2ennF' ,10,10, 2.0428800686997913e6)
  call test_INT('m2ennR' ,30,20,-2.5134225092352391e6,tol=1e-9)
  call test_int('m2ennFF',10,10, 4.2141002255534222e6)
  call test_int('m2ennFF',10,10, 4.2167546747618970e6,tol=1e-2) ! mathematica integrated
  call test_INT('m2ennRF',30,20, 1.3457713344850269e5,tol=1e-6)
  call test_INT('m2ennRR',30,20,-1.4405263363634362e6,tol=1e-5)
  call test_INT('m2enng0',10,10, 3.9577787504757200e6)
  call test_INT('m2enngV',10,10, 2.8597064138902631e7)
  call test_INT('m2enngC',10,10,-2.1165166551666144e7)
  call test_INT('m2enngR',10,10,-1.5702598548625503e7,tol=1e-7)


  call test_INT('m2enn0' , ans= 3.9045252475613321e5)
  call test_INT('m2ennF' , ans= 4.3783934521940694e5)
  call test_INT('m2ennR' , ans= 2.8710631753298396e5)
  call test_INT('m2ennRF', ans=-1.4240528862183195e4)
  call test_INT('m2ennRR', ans=-7.5525264728961664e5)
  call test_INT('m2enng0', ans= 2.2278502253868300e6)
  call test_INT('m2enngV', ans= 1.8031926689830732e7)
  call test_INT('m2enngC', ans=-1.0670221913398478e7)
  call test_INT('m2enngR', ans=-1.7243071377636143e5)
  call blockend(20)


  END SUBROUTINE


  SUBROUTINE TESTMJVEGAS
  integer ran_seed, itmx, ncall
  real(kind=prec) :: avgi, sd, chi2a
  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV
  xinormcut = 0.8
  xinormcut1 = 0.2
  xinormcut2 = 0.3

  call blockstart("mu-> ej VEGAS test")
  call test_INT('m2ej0' , 10, 10, 6.6820352516171796e-3)
  call test_INT('m2ejF' , 10, 10, 1.1454455993068602e-2,tol=1e-9)
  call test_INT('m2ejR' , 30, 50,-9.6872301265035834e-3,tol=1e-1)
  call test_INT('m2ejg0', 30, 50, 1.6516930581277529e-2,tol=1e-1)

  call test_INT('m2ej0' , ans= 2.1031560563439560E-02)
  call test_INT('m2ejF' , ans= 3.6134033896218394E-02)
  call test_INT('m2ejR' , ans=-7.5011101589874639E-03)
  call test_INT('m2ejg0', ans= 1.6540966046387116E-05)

  call blockend(8)
  END SUBROUTINE



  SUBROUTINE TESTMUDECPOL
  implicit none
  real(kind=prec) :: xx(14), weight
  real(kind=prec) :: sing, ctsing, fin, ctfin, polN(4), polO(4)
  real(kind=prec) :: z, mat0, lim
  integer i, ranseed, ido

  polN = (/ 0., 0., 0.85, 0. /)
  polO = (/ 0., 0., 0.85, 0. /)

  musq = Mm**2

  call blockstart("mudec polarisation")
  ! Step 0: Assume that pm2ennav is correct
  ! Step 1: pole

  ranseed = 23313
  weight = 0.
  do while(weight < zero)
    do i=1,5
      xx(i) = ran2(ranseed)
    enddo
    call psd4(xx(1:5),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)
  enddo
  ctfin = Ieik(1.0,Mm,p1,p2, ctsing)
  fin = pm2ennlav(p1,polN,p2,p3,p4, sing)
  call check("pm2enn   -> pm2ennl  ", (alpha/2./pi) * ctsing * pm2ennav(p1, polO, p2, p3, p4) / sing, -1.)

  ! Step 2: construct soft limit
  weight = 0.
  do while(weight < zero)
    xx(1) = 1.e-8
    do i=2,8
      xx(i) = ran2(ranseed)
    enddo
    call psd5_fks(xx(1:8),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
  enddo
  call check("pm2enn   -> pm2enng  ",pm2enngav (p1, polN, p2, p3, p4, p5)*xiout**2 / pm2enngav_s (p1, polO, p2, p3, p4), 1., 1e-6)
  call check("pm2ennl  -> pm2enngl ",pm2ennglav(p1, polN, p2, p3, p4, p5)*xiout**2 / pm2ennglav_s(p1, polO, p2, p3, p4), 1., 1e-6)


  ! Step 3: construct double-soft limit
  weight = 0.
  do while(weight < zero)
    xx(1) = 1.e-8
    xx(2) = 2.e-8
    do i=3,11
      xx(i) = ran2(ranseed)
    enddo
    call psd6_fkss(xx(1:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,p6,weight)
  enddo
  call check("pm2enn   -> pm2enngg ", pm2ennggav   (p1, polN, p2, p3, p4, p5, p6) * xioutA**2 * xioutB**2 /&
                                     pm2ennggav_ss(p1, polO, p2, p3, p4), 1., 1e-6)

  ! Step 4: pm2enng -> tm2mnnee
  weight = 0.
  do while(weight < zero)
    do i=1,11
      xx(i) = ran2(ranseed)
    enddo
    xx(2) = 0.7
    ! Test this at phi=pi/4. This doesn't matter as the difference
    ! integrates to zero (cf. (B.28) of [hep-ph/9512328])
    xx(4) = 1./8.

    ! This only works for p6||n = (0,0,1,0).
    xx(7) = 0.
    call psd6_omega_m50_fks(xx(1:11),p1,Mm,p5,0.,p3,0._prec,p4,0._prec,p2,p6,weight)
  enddo
  call check("n||p6", cos_th(p6, polN), 1.)
  do ido=25,25
    xx(2) = 1-0.5**ido
    call psd6_omega_m50_fks(xx(1:11),p1,Mm,p5,0.,p3,0._prec,p4,0._prec,p2,p6,weight)

    z = p5(4)/(p6(4)+p5(4))
    lim = (4.*pi*alpha)* 1._prec/p6(4)/p5(4)* (1-2.*z+2*z**2)
    lim = lim * pm2enngav(p1,polO,p2,p3,p4,p5+p6)
    call check("pm2enng  -> pt2mnnee ", (1-yout) * pt2mnneeav(p1,polN, p2, p3, p4, p5, p6) / lim, 1._prec, 1.e-4)
    call check("pm2enng  -> pm2ennee ", (1-yout) * pm2enneeav(p1,polN, p2, p3, p4, p5, p6) / lim, 1._prec, 1.e-3)
  enddo


  ! Step 5a: pt2mnnee -> pt2mnneel
  weight = 0.
  do while(weight < zero)
    do i=1,5
      xx(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34_e56(xx(1:11),p1,Mtau,p2,Mmu,p3,0._prec,p4,0._prec,p5, Mel, p6, Mel, weight)
  enddo

  ctsing = - Isin(xieik1,Mtau,p1) - Isin(xieik1,Mtau,p2) - &
           Isin(xieik1,Mtau,p6) - Isin(xieik1,Mtau,p5) + &
           2*Isin(xieik1,Mtau,p1,p2) + 2*Isin(xieik1,Mtau,p1,p6) - &
           2*Isin(xieik1,Mtau,p1,p5) - 2*Isin(xieik1,Mtau,p2,p6) + &
           2*Isin(xieik1,Mtau,p2,p5) + 2*Isin(xieik1,Mtau,p6,p5)
  fin = pt2mnneelav(p1,polN,p2,p3,p4,p5,p6, sing)
  call check("pt2mnnee -> pt2mnneel", (alpha/2./pi) * ctsing * pt2mnneeav(p1, polO, p2, p3, p4, p5, p6) / sing, -1.)

  ! Step 5b: pm2ennee -> pm2enneel
  weight = 0.
  do while(weight < zero)
    do i=1,5
      xx(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34_e56(xx(1:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5, Me, p6, Me, weight)
  enddo

  ctsing = - Isin(xieik1,Mm,p1) - Isin(xieik1,Mm,p2) - &
           Isin(xieik1,Mm,p6) - Isin(xieik1,Mm,p5) + &
           2*Isin(xieik1,Mm,p1,p2) + 2*Isin(xieik1,Mm,p1,p6) - &
           2*Isin(xieik1,Mm,p1,p5) - 2*Isin(xieik1,Mm,p2,p6) + &
           2*Isin(xieik1,Mm,p2,p5) + 2*Isin(xieik1,Mm,p6,p5)
  fin = pm2enneelav(p1,polN,p2,p3,p4,p5,p6, sing)
  call check("pm2ennee -> pm2enneel", (alpha/2./pi) * ctsing * pm2enneeav(p1, polO, p2, p3, p4, p5, p6) / sing, -1.)

  ! Step 6a: pm2ennee -> pm2enneeg
  weight = 0.
  do while(weight < zero)
    xx(1) = 0.8e-9_prec
    do i=2,14
      xx(i) = ran2(ranseed)
    enddo
    call psd7_27_37_47_e56_fks(xx,p1,Mm,p2,Me,p3,Me,p4,Me,p6,0._prec,p5,0._prec,p7,weight)
  enddo

  call check('pm2ennee -> pm2enneeg', xiout**2*pm2enneegav  (p1, polN, p2, p5, p6, p3, p4, p7) / &
                                               pm2enneegav_s(p1, polO, p2, p5, p6, p3, p4), 1.,1e-7)

  ! Step 6b: pt2mnnee -> pt2mnneeg
  weight = 0.
  do while(weight < zero)
    xx(1) = 0.8e-9_prec
    do i=2,14
      xx(i) = ran2(ranseed)
    enddo
    call psd7_27_37_47_e56_fks(xx,p1,Mtau,p2,Mmu,p3,Mel,p4,Mel,p6,0._prec,p5,0._prec,p7,weight)
  enddo

  call check('pt2mnnee -> pt2mnneeg', xiout**2*pt2mnneegav  (p1, polN, p2, p5, p6, p3, p4, p7) / &
                                               pt2mnneegav_s(p1, polO, p2, p5, p6, p3, p4), 1.,1e-6)

  call blockend(11)
  END SUBROUTINE

  SUBROUTINE TESTMUEFF
  use mue_heavy_quark
  implicit none
  real(kind=prec) :: x
  call blockstart("mu-e: hqff")

  x = 0.3_prec
  call init_hpls(x)

  call check("F1 a^1 ep^-1", ff1a1es(),  0.442121271115681573581091)
  call check("F1 a^1 ep^ 0", ff1a1ef(),  0.234594784829855224209627)
  call check("F1 a^1 ep^+1", ff1a1el(),  1.274494642665142496985853)
  call check("F1 a^2 ep^-2", ff1a2ed(),  0.097735609186473004849643)
  call check("F1 a^2 ep^-1", ff1a2es(),  0.103719344466085404336585)
  call check("F1 a^2 ep^ 0", ff1a2ef(), -1.344874879032958801331997)

  call check("F2 a^1 ep^ 0", ff2a1ef(),  0.793828222632485269861151)
  call check("F2 a^1 ep^+1", ff2a1el(),  2.202309677167230247514140)
  call check("F2 a^2 ep^-1", ff2a2es(),  0.350968342837776651346997)
  call check("F2 a^2 ep^ 0", ff2a2ef(), -0.232631299413720632476900)

  call blockend(10)
  END SUBROUTINE



  SUBROUTINE TESTMUEMATEL
  use mue_mp2mpgl_pvred, only: em2emgl_eeee_pvred
  use mue_mp2mpgl_coll, only: em2emgl_eeee_coll
  implicit none
  real (kind=prec) :: x(2),y(5),y8(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q1Rest(4),q2Rest(4),q3Rest(4),q4Rest(4),q5Rest(4)
  real (kind=prec) :: single, finite, lin, pole
  real (kind=prec) :: weight
  integer ido

  musq = Mm**2

  call blockstart("mu-e matrix elements")

  print*, "----------------------------------------------"
  print*, "   Test Born and NLO Matrixelements"
  print*, "----------------------------------------------"
  call initflavour("mu-eOld", mys=100000._prec)
  x = (/0.75,0.5/)
  call psx2(x,p1,me,p2,me,q1,mm,q2,mm,weight)
  call check("ee2mm0" ,ee2mm(p1,p2,q1,q2), 228.431299598518,threshold=2e-8)
  call check("ee2mml fin",ee2mml(p1,p2,q1,q2,pole), 5342.29166320650,threshold=2e-8)
  call check("ee2mml sin",pole, 934.984197022799,threshold=2e-8)


  call initflavour("mu-eOld", mys=40000.)
  x = (/0.75,0.5/)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  q1Rest = boost_rf(p1,p1)
  q2Rest = boost_rf(p1,p2)
  q3Rest = boost_rf(p1,q1)
  q4Rest = boost_rf(p1,q2)
  !print*,"q1Rest",q1Rest
  !print*,"q2Rest",q2Rest
  !print*,"q3Rest",q3Rest
  !print*,"q4Rest",q4Rest
  !print*,"theta_e",acos(cos_th(q2Rest,q3Rest))
  !print*,"theta_m", acos(cos_th(q2Rest,q4Rest))
  !print*,"E_e", q3Rest(4)
  !print*,"E_m",q4Rest(4)
  !print*,"s:", sq(p1+p2)
  !print*,"t:", sq(p1-q1)
  !print*,"u:", sq(p1-q2)
  call check("born" ,em2em       (p1,p2,q1,q2           ), 2746.780043847221,threshold=2e-8)
  call check("NLOEE",em2eml_ee(p1,p2,q1,q2             ), 53962.06218413274,threshold=2e-8)
  call check("NLOMM",em2eml_mm(p1,p2,q1,q2           ), 234.43246950023965,threshold=2e-8)
  call check("NLOEM",em2eml_em(p1,p2,q1,q2           ),-3771.205716336125,threshold=2e-8)

  call check("a",      em2em_a     (p1,p2,q1,q2),  5371.745674161278,    threshold=2e-4)
  call check("aa",     em2em_aa    (p1,p2,q1,q2),  10020.042676600582,    threshold=4e-4)
  call check("alee",     em2em_alee  (p1,p2,q1,q2),  105531.01066674264,   threshold=2e-4)
  call check("nfee",     em2em_nfee  (p1,p2,q1,q2),  -9048.728983123658,    threshold=1e-7)


  musq = me**2
  call check("2loop",     em2emll_eeee  (p1,p2,q1,q2), 2.4105736994236756e7 / 4. / pi**2,    threshold=2e-4)
  musq = mm**2
  call check("2loop",     em2emll_eeee  (p1,p2,q1,q2), 4.788164395939787e7  / 4. / pi**2,    threshold=2e-4)
  xieik2 = 0.5_prec
  call check("2loop-ff",     em2emff_eeee  (p1,p2,q1,q2), 6467.095657687751,    threshold=2e-8)

  x = (/0.75,0.999/)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  !print*,"s:", sq(p1+p2)
  !print*,"t:", sq(p1-q1)
  !print*,"u:", sq(p1-q2)
  ! These are a bit too lax
  call check("born:" , em2em       (p1,p2,q1,q2           ),1.21424E9            ,threshold=2e-5)
  call check("NLOEE:", em2eml_ee(p1,p2,q1,q2           ),1.2665657434343555E10,threshold=2e-8)
  call check("NLOMM:", em2eml_mm(p1,p2,q1,q2           ),179903.10339936582   ,threshold=2e-5)
  call check("NLOEM:", em2eml_em(p1,p2,q1,q2           ),-7.279726741301247E7 ,threshold=2e-8)

  call check("a",      em2em_a   (p1,p2,q1,q2),  7.168124734275707E8,      threshold=2e-4)
  call check("aa",     em2em_aa  (p1,p2,q1,q2),  7.930951562048115E8,     threshold=4e-4)
  call check("alee",     em2em_alee(p1,p2,q1,q2),  7.476997935153882E9,      threshold=2e-4)


  print*, "----------------------------------------------"
  print*, "   Test Real Matrixelement"
  print*, "----------------------------------------------"
  call initflavour("mu-eOld", mys=100000._prec)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)
  call check("ee2mmg", ee2mmg(p1,p2,q1,q2,q3), 14.7004376744835,threshold=2e-8)

  call initflavour("mu-eOld", mys=40000.)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  !print*,"s12:",s(p1,p2)
  !print*,"s13:",s(p1,q1)
  !print*,"s14:",s(p1,q2)
  !print*,"s15:",s(p1,q3)
  !print*,"s23:",s(p2,q1)
  !print*,"s24:",s(p2,q2)
  !print*,"s25:",s(p2,q3)
  !print*,"s34:",s(q1,q2)
  !print*,"s35:",s(q1,q3)
  !print*,"s45:",s(q2,q3)
  call check("MatrixelementTestEE", em2emg_ee(p1,p2,q1,q2,q3,lin), 593.7420767086396,   threshold=1e-8)
  call check("MatrixelementTestMM", em2emg_mm(p1,p2,q1,q2,q3    ), 68.01192438278314,   threshold=1e-8)
  call check("MatrixelementTestEM", em2emg_em(p1,p2,q1,q2,q3    ),-343.3094424320352,   threshold=1e-8)
  call check("MatrixelementTestEElin", lin,-66.72551154843468)
  call check("vp3",             em2emg_aee(p1,p2,q1,q2,q3),   863.0013524834341,    threshold=1e-5)

  print*, "----------------------------------"
  print*, "   Test RV   Matrixelement"
  print*, "----------------------------------"
  q1Rest = boost_rf(p1,p1)
  q2Rest = boost_rf(p1,p2)
  q3Rest = boost_rf(p1,q1)
  q4Rest = boost_rf(p1,q2)
  q5Rest = boost_rf(p1,q3)
  finite=EM2EMGl_eeee_old(p1, p2, q1, q2, q3,single)
  call check("finite:",finite,11075.183615627779,threshold=2e-9)
  call check("single:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("FDFfin:",finite,11075.183615627779,threshold=2e-9)
  call check("FDFsin:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee_pvred(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("Red fin:",finite,11075.183615627779,threshold=2e-9)
  call check("Red sin:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee_Coll(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("Coll fin:",finite,11075.183615627779,threshold=2e-9)
  call check("Coll sin:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee_fullcoll(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("FullColl fin:",finite,11075.183615627779,threshold=2e-9)
  call check("FullColl sin:",single,1508.0369816372852,threshold=2e-9)


  print*, "----------------------------------"
  print*, "   Test fRV  Matrixelement"
  print*, "----------------------------------"

  xieik1 = 0.3
  finite = -4125.0198705752236_prec
  call check("agreement mu=mm", em2emgf_eeee(q1rest,q2rest,q3rest,q4rest,q5rest), finite,threshold=1e-9)
  musq = me**2
  call check("agreement mu=me", em2emgf_eeee(q1rest,q2rest,q3rest,q4rest,q5rest), finite,threshold=1e-9)
  musq = mm**2


  print*, "----------------------------------"
  print*, "   Test RR   Matrixelement"
  print*, "----------------------------------"

  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  call check("RR matrix element", em2emgg_eeee(p1,p2,q1,q2,q3,q4), 0.7542796938033984)



  print*, "----------------------------------"
  print*, "   Test OL               "
  print*, "----------------------------------"

  ran_seed =720620554
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4(y8, p1, me, p2, mm, q1, me, q2, mm, q3, me, q4, me, weight)

  !print*,"s12->",s(p1,p2)
  !print*,"s13->",s(p1,q1)
  !print*,"s14->",s(p1,q2)
  !print*,"s15->",s(p1,q3)
  !print*,"s16->",s(p1,q4)
  !print*,"s23->",s(p2,q1)
  !print*,"s24->",s(p2,q2)
  !print*,"s25->",s(p2,q3)
  !print*,"s26->",s(p2,q4)
  !print*,"s34->",s(q1,q2)
  !print*,"s35->",s(q1,q3)
  !print*,"s36->",s(q1,q4)
  !print*,"s45->",s(q2,q3)
  !print*,"s46->",s(q2,q4)
  !print*,"s56->",s(q3,q4)
  !print*,"m->",me,",M->",mm,"S->",scms

  call check("open electrons",em2emeeee(p1,p2,q1,q2,q3,q4) , 61.55058430918309 )



  call blockend(41)

  END SUBROUTINE



  SUBROUTINE TESTMUESOFTN1
  implicit none
  integer ido,j,lastgood(3)
  real(kind=prec) :: y(5), weight
  real(kind=prec) :: q1(4), q2(4), q3(4)
  real(kind=prec) :: full(3), lim(3), lastratio(3), ratio(3)
  real(kind=prec), parameter :: xicut = .8
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5),dimension(3) :: colours


  call blockstart("mu-e \xi->0")
  y = (/0.3,0.6,0.8,0.4,0.9/)

  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  lastratio = 1.
  do ido=2,10
    y(1) = 0.8_prec/(10.**ido)
    call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)

    xieik1 = xicut
    full = xiout**2*(/ em2emg_ee   (p1,p2,q1,q2,q3), &
                       em2emgl_eeee(p1,p2,q1,q2,q3), &
                       em2emgf_eeee(p1,p2,q1,q2,q3) /)
    lim =  (/ em2emg_ee_s   (p1,p2,q1,q2), &
              em2emgl_eeee_s(p1,p2,q1,q2), &
              em2emgf_eeee_s(p1,p2,q1,q2) /)


    print*, "        "
    print*, " WEIGHT: ", weight, xiout
    !print*, " full: ", full
    !print*, " lim: ", lim

    ratio = abs(1-lim/full)
    do j=1,3
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = ido
        lastratio(j) = ratio(j)
        colours(j) = norm
      else
        colours(j) = red
      endif
    enddo
    write(*,900) colours(1),ratio(1), colours(2),ratio(2), &
                 colours(3),ratio(3), norm
  enddo
  call check("tree-level",lastratio(1),threshold=2.e-10)
  call check("real-virtual",lastratio(2),threshold=1.e-8)
  call check("fin mat-el",lastratio(3),threshold=5.e-8)
  call blockend(3)

900 format("1-ratio: ",A,ES13.3, A,ES13.3,A,ES13.3,A)
  END SUBROUTINE


  SUBROUTINE TESTMUPAIRSOFTN1
  implicit none
  integer ido,j,lastgood(3)
  real(kind=prec) :: y(5), weight
  real(kind=prec) :: q1(4), q2(4), q3(4)
  real(kind=prec) :: full(1), lim(1), lastratio(1), ratio(1)
  real(kind=prec), parameter :: xicut = .8
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5),dimension(3) :: colours


  call blockstart("mu-pair \xi->0")
  call initflavour("muoneOld")

  y = (/0.3,0.6,0.8,0.4,0.9/)

  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)
  lastratio = 1.
  do ido=2,10
    y(1) = 0.8_prec/(10.**ido)
    call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)

    xieik1 = xicut
    full = xiout**2*(/ ee2mmg (p1,p2,q1,q2,q3) /)
    lim =  (/  ee2mmg_s(p1,p2,q1,q2) /)

    print*, "        "
    print*, " WEIGHT: ", weight, xiout
    !print*, " full: ", full
    !print*, " lim: ", lim

    ratio = abs(1-lim/full)
    do j=1,1
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = ido
        lastratio(j) = ratio(j)
        colours(j) = norm
      else
        colours(j) = red
      endif
    enddo
    write(*,900) colours(1),ratio(1), norm
  enddo
  call check("tree-level",lastratio(1),threshold=2.e-10)
  call blockend(1)

900 format("1-ratio: ",A,ES13.3, A,ES13.3,A,ES13.3,A)
  END SUBROUTINE




  SUBROUTINE TESTMUESOFTN2
  implicit none
  real (kind=prec) :: yrr(8), yold(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real(kind=prec) :: full(3), lim(3), lastratio(3), ratio(3)
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=10),dimension(3) :: colours
  real (kind=prec) :: weight
  integer ido,j,lastgood(3)

  call blockstart("mu-e \xi_{1,2} -> 0")

  ran_seed = 23312
  do ido = 1,8
    yold(ido) = ran2(ran_seed)
  enddo


  print*,"                 hard-soft         soft-hard         soft-soft"
  lastratio = 1.
  do ido = 1,15
    yrr = yold
    yrr(2) = 0.8_prec/(10.**ido)
    call psx4_fkss(yrr,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    full(1) = xioutA**2*xioutB**2*em2emgg_eeee   (p1,p2,q1,q2,q3,q4)
    lim (1) = xioutA**2*          em2emgg_eeee_hs(p1,p2,q1,q2,q3   )

    yrr = yold
    yrr(1) = 0.7_prec/(10.**ido)
    call psx4_fkss(yrr,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    full(2) = xioutA**2*xioutB**2*em2emgg_eeee   (p1,p2,q1,q2,q3,q4)
    lim (2) =           xioutB**2*em2emgg_eeee_sh(p1,p2,q1,q2,   q4)

    yrr = yold
    yrr(1) = 0.7_prec/(10.**ido)
    yrr(2) = 0.8_prec/(10.**ido)
    call psx4_fkss(yrr,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    full(3) = xioutA**2*xioutB**2*em2emgg_eeee   (p1,p2,q1,q2,q3,q4)
    lim (3) =                     em2emgg_eeee_ss(p1,p2,q1,q2      )

    ratio = abs(1-lim/full)
    do j=1,3
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = ido
        lastratio(j) = ratio(j)
        colours(j) = norm//char(0)
      else
        colours(j) = red
      endif
    enddo
    write(*,900) colours(1),ratio(1), colours(2),ratio(2), &
                 colours(3),ratio(3), norm

  enddo

  call check("hard-soft",lastratio(1),threshold=5.e-15)
  call check("soft-hard",lastratio(2),threshold=5.e-15)
  call check("soft-soft",lastratio(3),threshold=5.e-15)
  call blockend(3)

900 format("1-ratio: ",A,ES13.3, A,ES13.3,A,ES13.3,A)

  ENDSUBROUTINE


  SUBROUTINE TESTMUPMATEL
  implicit none
  real (kind=prec) :: x(2),y(5),y8(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q1Rest(4),q2Rest(4),q3Rest(4),q4Rest(4)
  real (kind=prec) :: single, finite, lin,logxinormcutmin,logxinormcutmax,qq2,qq2min,qq2max
  real (kind=prec) :: weight,mp,wgt,st2, der, errder, deg, errdeg, t1, t2, res
  real (kind=prec) :: s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,mm2,logs15,mp2,logmu,discb1,img
  complex (kind=prec) ::pvc0,pvd0
  integer ido, ido1, ido2

  call blockstart("mu-p matrix elements")

  print*, "----------------------------------------------"
  print*, "   Test Born and NLO Matrixelements"
  print*, "----------------------------------------------"
  !muse setup
  call initflavour(flav='museOld')
  musq = 0.4*Mm**2
  x = (/0.75,0.5/)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("born",    mp2mp           (p1,p2,q1,q2),  18837.406746645658,     threshold=2e-8)
  call check("vertex",  mp2mpl          (p1,p2,q1,q2),  21927.285286760474,     threshold=2e-8)

  call check("a",      mp2mp_a         (p1,p2,q1,q2),  47857.08259776869,      threshold=2e-4)
  call check("aa",     mp2mp_aa        (p1,p2,q1,q2),  109573.78377049905,     threshold=4e-4)
  call check("al",     mp2mp_al        (p1,p2,q1,q2),  55707.02577201581,     threshold=2e-4)
  call initflavour(flav='muse210')
  call check("nf",     mp2mp_nf        (p1,p2,q1,q2),  -30840.848957943635,      threshold=1e-5) !FIXME

  !mesa setup
  call initflavour(flav='mesaOld')
  musq = 0.4*Mm**2
  x = (/ .2, 0.8831901644527639_prec /)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("born"     ,mp2mp          (p1,p2,q1,q2),  644977.9103846863,      threshold=2e-8)
  call check("vertex"   ,mp2mpl         (p1,p2,q1,q2),  1.9247985754475035E7,   threshold=2e-8)

  call check("a",      mp2mp_a         (p1,p2,q1,q2),  1.2256782492071397E6,   threshold=2e-4)
  call check("aa",     mp2mp_aa        (p1,p2,q1,q2),  2.2383011773421E6,   threshold=4e-4)
  call check("al",     mp2mp_al        (p1,p2,q1,q2),  3.657774491259393E7,    threshold=2e-4)
  call check("nf",     mp2mp_nf        (p1,p2,q1,q2),  -1.9831514232263186E6,    threshold=1e-6) !FIXME

  musq = Me**2
  call check("2loop",   mp2mpll         (p1,p2,q1,q2), 5.17979802434469086789e9 / 4 / pi**2)
  musq = 0.4*Mm**2
  call check("2loop",   mp2mpll         (p1,p2,q1,q2), 2.3995497614231266e10 / 4 / pi**2)
  xieik2 = 0.5_prec
  call check("2loop-ff",   mp2mpff         (p1,p2,q1,q2), 2.303599628847155e7, threshold=1e-8)


!  !hadronic vacuum polarization for muse
!  open(1, file = 'had_vp.dat')
!  qq2min = log(1E-3_prec); qq2max=log(1._prec)
!  do ido=1,1000
!    qq2 = exp(qq2min+ido*(qq2max-qq2min)/1000)
!    call hadr5n12(-sqrt(qq2),st2,der,errder,deg,errdeg)
!    write(1,*) qq2, der
!  end do
!  close(1)


  print*, "----------------------------------------------"
  print*, "   Test Real Matrixelement"
  print*, "----------------------------------------------"
  !muse setup
  call initflavour(flav='museOld')
  musq = 0.4*Mm**2
  y = (/0.01,0.6,0.8,0.999,0.01/)
  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  call check("real",    mp2mpg(p1,p2,q1,q2,q3),         3232.2957734744705,     threshold=1e-8)
  call check("vp3",     mp2mpg_a(p1,p2,q1,q2,q3),      9232.487512371086,     threshold=5e-4)

  finite=mp2mpgl(p1, p2, q1, q2, q3,single)
  call check("finite:",finite,5123.0906294102315,threshold=2e-9)
  call check("single:",single,1556.5959984999608,threshold=2e-9)

  !mesa setup
  call initflavour(flav='mesaOld')
  musq = 0.4*Mm**2
  y = (/0.01,0.6,0.8,0.999,0.01/)
  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  call check("real",    mp2mpg(p1,p2,q1,q2,q3),         3745.5539945814576,     threshold=1e-8)
  call check("vp2",     mp2mpg_a(p1,p2,q1,q2,q3),      9794.03437352276,        threshold=5e-4)


  finite=mp2mpgl(p1, p2, q1, q2, q3,single)
  call check("finite:",finite,122276.90242421204,threshold=2e-9)
  call check("single:",single,13692.78485167788,threshold=2e-9)

  !call cpu_time(t1)
  !do ido1 = 1, 100000
  !  do ido2 = 1,5
  !    y(ido2) = ran2(ran_seed)
  !  enddo
  !  call psx3_fks(y,scms,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  !  res=mp2mpgl(p1,p2,q1,q2,q3,single)
  !enddo
  !call cpu_time(t2)
  !print*, "time:", t2-t1

  print*, "----------------------------------"
  print*, "   Test RR   Matrixelement"
  print*, "----------------------------------"
  !muse setup
  call initflavour(flav='museOld')
  y8 = (/ 1.4402016072721237E-002, 5.4684134225679658E-002, 7.6243930997440604E-002, 0.43174827398348081,&
  &0.39324084035737505, 0.19880388639811619, 0.29691869313685182, 0.31247555106527919 /)
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  call check("RR matrix element", mp2mpgg(p1,p2,q1,q2,q3,q4), 50.30347548212909)


  !mesa setup
  call initflavour(flav='mesaOld')
  y8 = (/ 1.4402016072721237E-002, 5.4684134225679658E-002, 7.6243930997440604E-002, 0.43174827398348081,&
  &0.39324084035737505, 0.19880388639811619, 0.29691869313685182, 0.31247555106527919 /)
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  call check("RR matrix element", mp2mpgg(p1,p2,q1,q2,q3,q4), 213.1622536973318)






  call blockend(25)
  END SUBROUTINE

  SUBROUTINE EVENTSMESA
  implicit none
  real (kind=prec) :: y2(2),y5(5),y8(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: p1lab(4),p2lab(4),q1lab(4),q2lab(4),q3lab(4),q4lab(4)
  real (kind=prec) :: weight,wgt,fi,si,li, xicut
  integer ido, ido1
  call initflavour('mesaOld')
  musq = Me**2

  xicut = 2/sqrt(scms)*10._prec !MeV

  ran_seed = 2331
  do ido1 = 1,4
    weight = 0.
    print*,"(* Event id=",ran_seed,"*)"
    print*, "{"
    do while (weight .eq.0)
      do ido = 1,2
        y2(ido) = ran2(ran_seed)
      enddo
      call psx2(y2,p1,me,p2,mm,q1,me,q2,mm,weight)
    end do
    p1lab = boost_rf(p2,p1)
    p2lab = boost_rf(p2,p2)
    q1lab = boost_rf(p2,q1)
    q2lab = boost_rf(p2,q2)
    write(*,900) 1,p1, sqrt(abs(sq(p1)))
    write(*,900) 2,p2, sqrt(abs(sq(p2)))
    write(*,900) 3,q1, sqrt(abs(sq(q1)))
    write(*,900) 4,q2, sqrt(abs(sq(q2)))
    write(*,901) 1,p1lab
    write(*,901) 2,p2lab
    write(*,901) 3,q1lab
    write(*,901) 4,q2lab

    print*,sq(p1+p2),sq(p1-q1)

    write(*,902) "F1 & F2, born", mp2mp(p1,p2,q1,q2),','
    write(*,902) "F1=1, F2=0, born", em2em(p1,p2,q1,q2,li), ','
    fi = mp2mpl(p1,p2,q1,q2,si)
    write(*,903) "F1 & F2, OL", si,fi,','
    fi = em2eml_ee(p1,p2,q1,q2,si)
    write(*,903) "F1=1, F2=0, OL", si,fi - li/em2em(p1,p2,q1,q2) * si

    fi = em2emeik_ee(p1, p2, q1, q2, xicut, si)
    write(*,903) "F1 & F2, eik, cms", si*mp2mp(p1,p2,q1,q2), fi*mp2mp(p1,p2,q1,q2)
    write(*,903) "F1=1, F2=0, eik, cms", si*em2em(p1,p2,q1,q2), fi*em2em(p1,p2,q1,q2)

    fi = em2emeik_ee(p1lab, p2lab, q1lab, q2lab, xicut, si)
    write(*,903) "F1 & F2, eik lab", si*mp2mp(p1lab,p2lab,q1lab,q2lab), fi*mp2mp(p1lab,p2lab,q1lab,q2lab)
    write(*,903) "F1=1, F2=0, eik lab", si*em2em(p1lab,p2lab,q1lab,q2lab), fi*em2em(p1lab,p2lab,q1lab,q2lab)
    print*, "},"
    print*
  enddo

  ran_seed = 2331
  do ido1 = 1,4
    weight = 0.
    print*,"(* Event id=",ran_seed,"*)"
    print*, "{"
    do while (weight .eq.0)
      do ido = 1,5
        y5(ido) = ran2(ran_seed)
      enddo
      select case(ido1)
        case(3)
          y5(2) = 0.999
          call psx3_fks(y5,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
        case(4)
          y5(2) = 0.99
          call psx3_fks35(y5,p1,me,p2,mm,q1,q2,q3,weight)
        case default
          call psx3_fks35(y5,p1,me,p2,mm,q1,q2,q3,weight)
      end select
    end do
    p1lab = boost_rf(p2,p1)
    p2lab = boost_rf(p2,p2)
    q1lab = boost_rf(p2,q1)
    q2lab = boost_rf(p2,q2)
    q3lab = boost_rf(p2,q3)
    write(*,900) 1,p1, sqrt(abs(sq(p1)))
    write(*,900) 2,p2, sqrt(abs(sq(p2)))
    write(*,900) 3,q1, sqrt(abs(sq(q1)))
    write(*,900) 4,q2, sqrt(abs(sq(q2)))
    write(*,900) 5,q3, sqrt(abs(sq(q3)))
    write(*,901) 1,p1lab
    write(*,901) 2,p2lab
    write(*,901) 3,q1lab
    write(*,901) 4,q2lab
    write(*,901) 5,q3lab

    write(*,902) "F1 & F2, born", mp2mpg(p1,p2,q1,q2,q3),','
    write(*,902) "F1=1, F2=0, born", em2emg_ee(p1,p2,q1,q2,q3), ','
    fi = mp2mpgl(p1,p2,q1,q2,q3,si)
    write(*,903) "F1 & F2, OL", si,fi,','
    fi = em2emgl_eeee(p1,p2,q1,q2,q3,si)
    write(*,903) "F1=1, F2=0, OL", si,fi

    fi = em2emeik_ee(p1, p2, q1, q2, xicut, si)
    write(*,903) "F1 & F2, eik, cms", si*mp2mpg(p1,p2,q1,q2,q3), fi*mp2mpg(p1,p2,q1,q2,q3)
    write(*,903) "F1=1, F2=0, eik, cms", si*em2emg_ee(p1,p2,q1,q2,q3), fi*em2emg_ee(p1,p2,q1,q2,q3)

    fi = em2emeik_ee(p1lab, p2lab, q1lab, q2lab, xicut, si)
    write(*,903) "F1 & F2, eik, lab", si*mp2mpg(p1lab,p2lab,q1lab,q2lab,q3lab), fi*mp2mpg(p1lab,p2lab,q1lab,q2lab,q3lab)
    write(*,903) "F1=1, F2=0, eik, lab", si*em2emg_ee(p1lab,p2lab,q1lab,q2lab,q3lab), fi*em2emg_ee(p1lab,p2lab,q1lab,q2lab,q3lab)
    print*, "},"
    print*
  enddo

  ran_seed = 2331
  do ido1 = 1,4
    weight = 0.
    print*,"(* Event id=",ran_seed,"*)"
    print*, "{"
    do while (weight .eq.0)
      do ido = 1,8
        y8(ido) = ran2(ran_seed)
      enddo
      call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    end do
    write(*,900) 1,p1, sqrt(abs(sq(p1)))
    write(*,900) 2,p2, sqrt(abs(sq(p2)))
    write(*,900) 3,q1, sqrt(abs(sq(q1)))
    write(*,900) 4,q2, sqrt(abs(sq(q2)))
    write(*,900) 5,q3, sqrt(abs(sq(q3)))
    write(*,900) 6,q4, sqrt(abs(sq(q4)))
    write(*,901) 1,boost_rf(p2,p1)
    write(*,901) 2,boost_rf(p2,p2)
    write(*,901) 3,boost_rf(p2,q1)
    write(*,901) 4,boost_rf(p2,q2)
    write(*,901) 5,boost_rf(p2,q3)
    write(*,901) 6,boost_rf(p2,q4)
    write(*,902) "F1 & F2", mp2mpgg(p1,p2,q1,q2,q3,q4),','
    write(*,902) "F1=1, F2=0", em2emgg_eeee(p1,p2,q1,q2,q3,q4)
    print*, "},"
    print*
  enddo

900 FORMAT("  p",I1,"cms -> { ",F20.14,",",F20.14,",",F20.14,",",F20.14," }, (* sqrt(p^2) = ",F14.10," *)")
901 FORMAT("  p",I1,"lab -> { ",F20.14,",",F20.14,",",F20.14,",",F20.14," },")
902 FORMAT('  ans["',A,'"] -> ',F23.14,A)
903 FORMAT('  ans["',A,'"] -> ',F25.14,"/ep + ", F25.14,A)
  END SUBROUTINE



  SUBROUTINE TESTMUPSOFTN1
  implicit none
  integer ido,j,lastgood(3)
  real(kind=prec) :: y(5), weight
  real(kind=prec) :: q1(4), q2(4), q3(4)
  real(kind=prec) :: full(3), lim(3), lastratio(3), ratio(3)
  real(kind=prec), parameter :: xicut = .8
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5),dimension(3) :: colours


  call blockstart("mu-p \xi->0")
  !mesa setup
  call initflavour("mesaOld")
  musq = 0.4*Mm**2
  !!muse setup
  !call initflavour("muse")
  !musq = 0.4*Mm**2

  y = (/0.01,0.6,0.8,0.999,0.01/)

  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  lastratio = 1.
  do ido=2,10
    y(1) = 0.8_prec/(10.**ido)
    call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)

    xieik1 = xicut
    full = xiout**2*(/ mp2mpg (p1,p2,q1,q2,q3), &
                       mp2mpgl(p1,p2,q1,q2,q3), &
                       mp2mpgf(p1,p2,q1,q2,q3) /)
    lim =  (/  mp2mpg_s(p1,p2,q1,q2), &
              mp2mpgl_s(p1,p2,q1,q2), &
              mp2mpgf_s(p1,p2,q1,q2) /)


    print*, "        "
    print*, " WEIGHT: ", weight, xiout
    !print*, " full: ", full
    !print*, " lim: ", lim

    ratio = abs(1-lim/full)
    do j=1,3
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = ido
        lastratio(j) = ratio(j)
        colours(j) = norm
      else
        colours(j) = red
      endif
    enddo
    write(*,900) colours(1),ratio(1), colours(2),ratio(2), &
                 colours(3),ratio(3), norm
  enddo
  call check("tree-level",lastratio(1),threshold=2.e-9)
  call check("real-virtual",lastratio(2),threshold=2.e-7)
  call check("fin mat-el",lastratio(3),threshold=3.e-7)
  call blockend(3)
900 format("1-ratio: ",A,ES13.3, A,ES13.3,A,ES13.3,A)
  END SUBROUTINE



  SUBROUTINE TESTMUPSOFTN2
  implicit none
  real (kind=prec) :: yrr(8), yold(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real(kind=prec) :: full(3), lim(3), lastratio(3), ratio(3)
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=10),dimension(3) :: colours
  real (kind=prec) :: weight
  integer ido,j,lastgood(3)

  call blockstart("mu-p \xi_{1,2} -> 0")
  !mesa setup
  call initflavour('mesaOld')
  musq = 0.4*Mm**2
  !muse setup
  call initflavour('museOld')
  musq = 0.4*Mm**2

  yold = (/ 1.4402016072721237E-002, 5.4684134225679658E-002, 7.6243930997440604E-002, 0.43174827398348081,&
  &0.39324084035737505, 0.19880388639811619, 0.29691869313685182, 0.31247555106527919 /)

  print*,"                 hard-soft         soft-hard         soft-soft"
  lastratio = 1.
  do ido = 1,15
    yrr = yold
    yrr(2) = 0.8_prec/(10.**ido)
    call psx4_fkss(yrr,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    full(1) = xioutA**2*xioutB**2*mp2mpgg   (p1,p2,q1,q2,q3,q4)
    lim (1) = xioutA**2*          mp2mpgg_hs(p1,p2,q1,q2,q3   )

    yrr = yold
    yrr(1) = 0.7_prec/(10.**ido)
    call psx4_fkss(yrr,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    full(2) = xioutA**2*xioutB**2*mp2mpgg   (p1,p2,q1,q2,q3,q4)
    lim (2) =           xioutB**2*mp2mpgg_sh(p1,p2,q1,q2,   q4)

    yrr = yold
    yrr(1) = 0.7_prec/(10.**ido)
    yrr(2) = 0.8_prec/(10.**ido)
    call psx4_fkss(yrr,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    full(3) = xioutA**2*xioutB**2*mp2mpgg   (p1,p2,q1,q2,q3,q4)
    lim (3) =                     mp2mpgg_ss(p1,p2,q1,q2      )

    ratio = abs(1-lim/full)
    do j=1,3
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = ido
        lastratio(j) = ratio(j)
        colours(j) = norm//char(0)
      else
        colours(j) = red
      endif
    enddo
    write(*,900) colours(1),ratio(1), colours(2),ratio(2), &
                 colours(3),ratio(3), norm

  enddo

  call check("hard-soft",lastratio(1),threshold=2.e-14)
  call check("soft-hard",lastratio(2),threshold=2.e-14)
  call check("soft-soft",lastratio(3),threshold=2.e-08)
  call blockend(3)
900 format("1-ratio: ",A,ES13.3, A,ES13.3,A,ES13.3,A)
  ENDSUBROUTINE



  SUBROUTINE TESTMUESPEED
  implicit none
  integer, parameter :: niter = 20000
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: weight, arr(5), finite
  real (kind=prec) :: startt, endt, rateold,ratenew
  integer i, j, ranseed


  call blockstart("mu-e performance")

  ! On my notebook I observed the following event rates
  !   CDR (ref)   :  5.6 kEv/s
  !   FDF (MMA+O3): 53.0 kEv/s
  !   FDF (O3)    : 52.0 kEv/s
  ! It seems that O3 optimisation suffices!

  ranseed = 2332

  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite=EM2EMGl_eeee_old(p1, p2, q1, q2, q3)
  enddo
  call cpu_time(endt)
  rateold=niter/(endt-startt)
  print*,"old rate is",rateold,"Ev/s"


  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite=EM2EMGl_eeee(p1, p2, q1, q2, q3)
  enddo
  call cpu_time(endt)
  ratenew=niter/(endt-startt)
  print*,"new rate is",ratenew,"Ev/s"

  call checkrange("speed-up", ratenew/rateold, 2.,100.)
  call blockend(1)

  END SUBROUTINE



  SUBROUTINE TESTMUEVEGAS
  call initflavour('muoneOld')
  xinormcut = 0.1
  xinormcut1 = 0.2
  xinormcut2 = 0.3
  flavour = 'muone'

  call blockstart("mu-e VEGAS test")

  call test_INT('em2em0'     , 10,10, 1.1828190311575372e-2)

  call test_INT('em2emC'     , 10,10,-3.1837804185881446e-1)
  call test_INT('em2emV'     , 10,10, 2.1718958403108052e-1)
  call test_INT('em2emFEE'   , 10,10,-9.8182294303004777e-2)
  call test_INT('em2emREE'   , 10,10, 9.3139963218761626e-2,tol=1e-6)
  call test_INT('em2emREE15' , 10,10, 9.9096467547199599e-2,tol=1e-7)
  call test_INT('em2emREE35' , 10,10, 2.8401993476435260e-2)
  call test_INT('em2emFEM'   , 10,10,-1.3995790879510414e-3)
  call test_INT('em2emREM'   , 10,10,-9.9824157208016208e-4)
  call test_INT('em2emFMM'   , 10,10,-1.6553506833546212e-3)
  call test_INT('em2emRMM'   , 10,10, 1.2100152435387461e-3)

  call test_INT('em2emFFEEEE'  , 10,10, 4.1721180353060405e-1)
  call test_INT('em2emRFEEEE'  , 50, 2, 1.9791062877848167e-1,tol=1e-7)
  call test_INT('em2emRFEEEE15', 50, 2, 1.2805370759045959e-1,tol=1e-7)
  call test_INT('em2emRFEEEE35', 50, 2, 3.5949834273937766e-3,tol=1e-5)
  call test_INT('em2emRREEEE'  ,100,10,-1.3763715110320837e-1,tol=1e-1)
  call test_INT('em2emRREEEE1516' ,100,10,-1.6174543046156975e-1,tol=1e-3)
  call test_INT('em2emRREEEE3536' ,100,10,-6.1427885310768642e-2,tol=1e-5)

  call initflavour('muone') !FIXME
  call test_INT('em2emA'     , 10,10, 1.9196083524176403E-2)
  call test_INT('em2emAA'    , 10,10, 3.1846800255125769E-2)
  call test_INT('em2emAFEE'  , 10,10,-1.6146448993116519E-1)
  call test_INT('em2emAREE'  , 10,10, 1.5204633916591184E-1,tol=1e-4)
  call test_INT('em2emAREE15', 10,10, 1.4750460401568438E-1,tol=1e-7)
  call test_INT('em2emAREE35', 10,10, 4.9887581273650450E-2)
  call test_INT('em2emNFEE'  , 10,10,-2.4858051866680716E-2)


  call initflavour('muoneOld')
  call test_INT('em2em0'     , ans= 2.4464166800133684e-4)

  call test_INT('em2emC'     , ans=-7.8544896823162201e-3)
  call test_INT('em2emV'     , ans= 4.8088819349105076e-3)
  ! the matrix element has been recoded, hence this won't work
  ! as well
  call test_INT('em2emFEE'   , ans=-2.8923937585840407e-3, tol=2e-8)
  call test_INT('em2emREE'   , ans= 1.4585312619770318e-3)
  call test_INT('em2emREE15' , ans= 4.6725206492827182e-5)
  call test_INT('em2emREE35' , ans= 8.8948347945374596e-5)
  call test_INT('em2emFEM'   , ans= 2.0159899589563388e-4)
  call test_INT('em2emREM'   , ans=-2.1410390100640320e-3)
  call test_INT('em2emFMM'   , ans=-3.5481300107151944e-4)
  call test_INT('em2emRMM'   , ans= 7.8945088374948616e-4)

  call test_INT('em2emFFEEEE'  , ans= 1.7318001437010282e-2)
  call test_INT('em2emRFEEEE'  , ans= 6.6134255434314483e-4)
  call test_INT('em2emRFEEEE15', ans=-1.2477601548757956e-4)
  call test_INT('em2emRFEEEE35', ans=-1.3263369327043330e-4)
  call test_INT('em2emRREEEE'  , ans= 8.3634224006500814e-5)
  call test_INT('em2emRREEEE1516' , ans= 8.3634224006500814e-5)
  call test_INT('em2emRREEEE3536' , ans=-8.0641949969533703e-5)

  call initflavour('muone') !FIXME
  call test_INT('em2emA'     , ans= 6.3804672562645267E-4)
  call test_INT('em2emAA'    , ans= 1.4920175662651589E-3)
  call test_INT('em2emAFEE'  , ans=-7.5436141645584447E-3)
  call test_INT('em2emAREE'  , ans= 3.8603193159651591E-3)
  call test_INT('em2emAREE15', ans= 1.1926538069912609E-4)
  call test_INT('em2emAREE35', ans= 2.3049393718801796E-4)
  call test_INT('em2emNFEE'  , ans=-1.4256817356154434E-3)
  call initflavour('muoneOld')

  call blockend(50)
  END SUBROUTINE


  SUBROUTINE TESTMUPVEGAS
  call initflavour('mesaOld')
  musq = 0.4*Mm**2
  xinormcut = 0.1
  xinormcut1 = 0.2
  xinormcut2 = 0.3
  flavour = 'mesa'

  call blockstart("mu-p VEGAS test")

  call test_INT('mp2mp0'     , 10,10, 1.1288760522737466e-2)

  call test_INT('mp2mpF'     , 10,10,-9.3350532413600501e-2)
  call test_INT('mp2mpR'     , 10,10, 1.0768963480232964e-1,tol=1e-5)
  call test_INT('mp2mpR15'   , 10,10, 7.9209568211157183e-2)
  call test_INT('mp2mpR35'   , 10,10, 2.7054307527194940e-2,tol=1e-9)

  call test_INT('mp2mpFF'    , 50, 2, 3.9432974169604190e-1,tol=1e-6)
  call test_INT('mp2mpRF'    , 50, 2, 9.7970276783658536e-2,tol=1e-6)
  call test_INT('mp2mpRF15'  , 50, 2, 7.8272368835807016e-2,tol=1e-6)
  call test_INT('mp2mpRF35'  , 50, 2, 5.7658870988090086e-3,tol=1e-4)
  call test_INT('mp2mpRR'    ,100,10,-1.2387719311662963e-1,tol=1e-4)
  call test_INT('mp2mpRR1516',100,10,-8.1718753773589808e-2,tol=1e-5)
  call test_INT('mp2mpRR3536',100,10,-3.6328529915545367e-2,tol=1e-6)

  call initflavour('mesa')
  call test_INT('mp2mpA'    , 10,10, 1.8099468713135621E-2)
  call test_INT('mp2mpAA'   , 10,10, 2.9758640781542025E-2,tol=1e-9)
  call test_INT('mp2mpAF'   , 10,10,-1.5148221664811728E-1)
  call test_INT('mp2mpAR'   , 10,10, 1.6220705159994842E-1,tol=1e-4)
  call test_INT('mp2mpAR15' , 10,10, 1.1647317655773050E-1,tol=1e-9)
  call test_INT('mp2mpAR35' , 10,10, 4.7238016766979515E-2,tol=1e-9)
  call test_INT('mp2mpNF'   , 10,10,-2.3003789651698164E-2)
  call initflavour('mesaOld')


  call test_INT('mp2mp0'     , ans= 3.9511734268486817e-4)

  call test_INT('mp2mpF'     , ans=-4.4119624114931045e-3)
  call test_INT('mp2mpR'     , ans= 2.0481738761686592e-3)
  call test_INT('mp2mpR15'   , ans= 1.6025268186203367e-2)
  call test_INT('mp2mpR35'   , ans= 2.4624782469790161e-4)

  call test_INT('mp2mpFF'    , ans= 2.4951018303275962e-2)
  call test_INT('mp2mpRF'    , ans=-4.4227618733129323e-3)
  call test_INT('mp2mpRF15'  , ans=-3.8532663414918802e-2)
  call test_INT('mp2mpRF35'  , ans= 2.6460295158396013e-4)
  call test_INT('mp2mpRR'    , ans=-6.2094398902672813e-4)
  call test_INT('mp2mpRR1516', ans= 9.5832623556730168e-2)
  call test_INT('mp2mpRR3536', ans= 1.3283433559306398e-3)

  call initflavour('mesa')
  call test_INT('mp2mpA'    , ans= 9.1972301814468216E-4)
  call test_INT('mp2mpAA'   , ans= 1.9633711342101133E-3)
  call test_INT('mp2mpAF'   , ans=-1.0269818573203757E-2)
  call test_INT('mp2mpAR'   , ans= 4.6753900827664046E-3)
  call test_INT('mp2mpAR15' , ans= 3.6308942729704190E-2)
  call test_INT('mp2mpAR35' , ans= 5.6397789846375133E-4)
  call test_INT('mp2mpNF'   , ans=-1.8884127546685819E-3)
  call initflavour('mesaOld')


  call blockend(38)
  END SUBROUTINE


  SUBROUTINE TESTENVEGAS
  call initflavour("mu-eOld", mys = 40.)
  xinormcut = 0.1
  xinormcut1 = 0.03
  xinormcut2 = 0.10
  flavour = 'e-S'

  call blockstart("e-n VEGAS test")
  call test_INT('ee2nn0'  , 10, 10, 1.7427427889387065e1)
  call test_INT('ee2nnF'  , 10, 10,-5.3513966848862644e1)
  call test_INT('ee2nnR'  , 40, 10, 1.0107779150486646e1)
  call test_INT('ee2nnS'  , 10, 10,-1.2604480122265484e4)
  call test_INT('ee2nnI'  , 40, 10,-9.6442786106342089e2)
  call test_INT('ee2nnRF' ,  1,  5,-6.4689974289350999e1,tol=1e-8)
  call test_INT('ee2nnRR' , 40, 10, 2.3199972540830519e1)
  call test_INT('ee2nnSS' , 10, 10, 8.5048478566686881e3)
  call test_INT('ee2nnCC' , 10, 10,-2.6391720886604771e3)

  call test_INT('ee2nn0'  ,ans= 3.3184952487331172E+00 * 4)
  call test_INT('ee2nnF'  ,ans=-1.0222512448954381E+01 * 4)
  call test_INT('ee2nnR'  ,ans=-1.0137072212127549E+01)
  call test_INT('ee2nnS'  ,ans=-2.1951969786467507E+03 * 4)
  call test_INT('ee2nnI'  ,ans=-1.8618277458869266E+03)
  call test_INT('ee2nnRF' ,ans=-1.3917124753830541E+02)
  call test_INT('ee2nnRR' ,ans= 2.5520507366844871E+01)
  call test_INT('ee2nnSS' ,ans= 1.5016103108120092E+03 * 4)
  call test_INT('ee2nnCC' ,ans=-4.7493488640214088E+02 * 4)

  call blockend(18)
  END SUBROUTINE



  SUBROUTINE BLOCKSTART(msg)
  implicit none
  integer nsp
  character(len=*) msg
  blocktitle = msg
  npass(2)=0 ; nfail(2)= 0
  call cpu_time(timing(2))

  nsp = (47-len(msg))/2
  print*, "----------------------------------------------"
  print*, repeat(' ',nsp)//msg//repeat(' ',nsp)
  print*, "----------------------------------------------"

  END SUBROUTINE
  SUBROUTINE BLOCKEND(n)
  implicit none
  integer n
  real(kind=prec) :: now
  call cpu_time(now)
  print*
  write(*,900) trim(blocktitle), char(10),npass(2), n, now-timing(2)
  print*
900 format("Test block ",A," complete.",A,"Passed ",I2,"/",I2," tests in ",F5.1,"s")
  END SUBROUTINE

  SUBROUTINE CHECK(msg, val, ref, threshold)
  implicit none
  character(len=*) msg
  real(kind=prec) val, rel, tthreshold
  real(kind=prec), optional :: threshold, ref
  character(len=15) :: green = char(27)//'[32m[PASS]'//char(27)//'[0m'
  character(len=15) :: red = char(27)//'[31m[FAIL]'//char(27)//'[0m'


  tthreshold = 1.e-10
  if (present(threshold)) tthreshold = threshold
  if (present(ref)) then
    rel = abs(1-real(val)/real(ref))
  else
    rel = abs(val)
  endif

  if (rel .lt. tthreshold) then
    write(*,900)green,msg, rel
    npass = npass + 1
  else
    write(*,901)red,msg, rel,tthreshold
    nfail = nfail + 1
  endif
900 format(A,' ',A,' at ', ES13.5)
901 format(A,' ',A,' at ', ES13.5, ' (threshold: ',ES13.5,')')
  END SUBROUTINE


  SUBROUTINE CHECKrange(msg, val, min, max)
  implicit none
  character(len=*) msg
  real(kind=prec) val, min,max
  character(len=15) :: green = char(27)//'[32m[PASS]'//char(27)//'[0m'
  character(len=15) :: red = char(27)//'[31m[FAIL]'//char(27)//'[0m'


  if (min.lt.val.and.val.lt.max) then
    write(*,900)green,msg,val,min,max
    npass = npass + 1
  else
    write(*,901)red,msg,val,min,max
    nfail = nfail + 1
  endif
900 format(A,' ',A,' is ',ES13.5,' in (',ES8.2,',',ES8.2,')')
901 format(A,' ',A,' is ',ES13.5,' not in (',ES8.2,',',ES8.2,')')
  END SUBROUTINE

  SUBROUTINE TEST_INT(piece, ncall, itmx, ans, tol)
  implicit none
  character (len=*) :: piece
  real(kind=prec) :: avgi, sd, chi2a, ans
  real(kind=prec) :: arr(maxdim)
  real(kind=prec), optional :: tol
  real(kind=prec) :: threshold
  integer, optional :: ncall, itmx
  integer ran_seed, ndim, i, j
  procedure(integrand), pointer :: fxn

  ran_seed = 12145
  which_piece = piece
  call initpiece(ndim, fxn)

  if (present(tol)) then
    threshold = tol
  else
    threshold = 1e-10
  endif

  if (present(itmx)) then
    call vegas(ndim,1000*ncall,itmx,fxn,avgi,sd,chi2a,ran_seed, silent=.true.)
  else
    avgi = 0.
    do while (abs(avgi) < zero)
      do i=1,ndim
        arr(i) = ran2(ran_seed)
      enddo
      avgi = fxn(arr(1:ndim), 1., ndim)
    enddo
  endif
  call check(which_piece, avgi, ans, threshold=threshold)
  END SUBROUTINE

  SUBROUTINE PRINTHEAD
  implicit none
  integer nsp
  print*,"+----------------------------------------------------------+"
  print*,"|          Running test suite for monte-carlo              |"
  print*,"|                                                          |"
  print*,"| Source version: ",fullsha," |"
  if(index(gitbranch,"not-git") > 0) then
  print*,"| Source tracking not active. Cannot determine version     |"
  else
  print*,"| Git revision  : ",gitrev," |"
  do nsp = 1,len(gitbranch)
    if (iachar(gitbranch(nsp:nsp)) == 0) exit
  enddo
  print*, "| Git branch is "//gitbranch(1:nsp-1)//'.'//repeat(' ', 43-nsp)//'|'
  endif
  print*,"+----------------------------------------------------------+"
  ENDSUBROUTINE

  END PROGRAM TEST
