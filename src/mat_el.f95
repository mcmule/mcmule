                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  !! From file mudec_pm2ennggav.f95
  use mudec, only: pm2ennggav!!(p1, n1, p2, p3, p4, p5, p6)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5) g(p6)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5) g(p6)
    !! for massive (and massless) electron
    !! average over neutrino tensor taken


  !! From file mudec_pm2ennglav.flex.opt.f95
  use mudec, only: pm2ennglav!!(p1,n,p2,p3,p4,p5,pole)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngfav!!(p1,n,p2,p3,p4,p5)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
    !! for massive electron finite matrix element
    !! average over neutrino tensor taken

  use mudec, only: pm2ennglav0!!(p1,n,p2,p3,p4,p5,single, double)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
    !! for massless electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennglavz!!(p1,n,p2,p3,p4,p5,single)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
    !! for massified electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngfavz!!(p1,n,p2,p3,p4,p5)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
    !! for massified electron, finite matrix element
    !! average over neutrino tensor taken


  !! From file mudec_mat_el.f95
  use mudec, only: pm2ej!!(p1,n1,p2,p3)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  use mudec, only: pm2ejl!!(p1,n1,p2,p3,sing)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  use mudec, only: pm2ejg!!(p1,n1,p2,p3,p4)
  !! mu+(p1) -> e+(p2) J(p3) y(p4)
  !! mu-(p1) -> e-(p2) J(p3) y(p4)

  use mudec, only: pm2ennav!!(q1,n1,q2, q3, q4, lin)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4)
    !! for massive (and massless) electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennlav!!(q1, n1, q2, q3, q4, pole,cdr)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! average over neutrino tensor taken
    !! for massive electron

  use mudec, only: pm2enngav!!(q1,n,q2,q3,q4,q5, linear)
    !! mu+(p1) -> e+(p2) y(q5) + ( \nu_e \bar{\nu}_\mu )
    !! mu-(p1) -> e-(p2) y(q5) + ( \bar{\nu}_e \nu_\mu )
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennlavz!!(p1, n1, p2, p3, p4, pole, lin)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massified electrons
    !! average over neutrino tensor taken

  use mudec, only: pm2ennllavz!!(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massified electrons
    !! average over neutrino tensor taken

  use mudec, only: pm2ennffavz!!(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massified electrons
    !! average over neutrino tensor taken

  use mudec, only: pm2ennfav!!(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngcav!!(p1, n1, p2, p3, p4, p5)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
    !! for massive electron

  use mudec, only: pm2ejf!!(p1,n1,p2,p3)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)


  !! From file mudec.f95
  use mudec, only: pm2ejg_s!!(q1,n1,q2,q3)
  !! mu+(p1) -> e+(p2) J(p3) y(ksoft)
  !! mu-(p1) -> e-(p2) J(p3) y(ksoft)

  use mudec, only: pm2ennggav_s!!(q1,n1,q2,q3,q4,q5)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(ksoft)
    !! for massive (and massless) electron

  use mudec, only: pm2enngav_s!!(q1,n1,q2,q3,q4,lin)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennglav_s!!(q1,n1,q2,q3,q4, sing)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngfav_s!!(q1,n1,q2,q3,q4)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngfzav_s!!(q1,n1,q2,q3,q4)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massified electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennggav_sh!!(p1, n1, p2,q3,q4, p6)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(ksoft) y(p6)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(ksoft) y(p6)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennggav_hs!!(p1, n1, p2,q3,q4, p5)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(p5) y(ksoft)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(p5) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennggav_ss!!(p1, n1, p2,q3,q4)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(ksoft) y(ksoft)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(ksoft) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken


  !! From file mudecrare_mat_el.f95
  use mudecrare, only: pt2mnneeav!!(p1,n1,p2,qA,qB,p3,p4)
    !! tau+(p1) -> mu+(p2) \nu_mu() \bar{\tau}_\mu() e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e( )  \nu_\tau() e+(p3) e-(p4)

  use mudecrare, only: pm2enneeav!!(p1,n1,p2,qa,qb,p3,p4)
    !! mu+(p1) -> e+(p2) \nu_mu(qA) \bar{\nu}_\mu(qB) e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e(qA)  \nu_\tau(qB) e-(p4) e+(p3)

  use mudecrare, only: pt2mnneelav!!(p1,n1,p2,qA,qB,p3,p4, single)
    !! tau+(p1) -> mu+(p2) \nu_mu() \bar{\tau}_\mu() e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e( )  \nu_\tau() e+(p3) e-(p4)

  use mudecrare, only: pm2enneelav!!(p1,n1,p2,qA,qB,p3,p4, single)
    !! mu+(p1) -> e+(p2) \nu_mu() \bar{\tau}_\mu() e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e( )  \nu_\tau() e-(p4) e+(p3)

  use mudecrare, only: pt2mnneegav!!(p1,n1,p2,qA,qB,p3,p4,p7)
    !! M1^+(p1)->M2^+(p2),M3^-(p3),M3^+(p4),2\nu,\gamma(p7)

  use mudecrare, only: pm2enneegav!!(p1,n1,p2,qA,qB,p3,p4,p7)
    !!  M1^+(p1)->M2^+(p2),M2^-(p3),M2^+(p4),2\nu,\gamma(p7)

  use mudecrare, only: pm2enneecav!!(p1, n1, p2, p3, p4, p5, p6)
    !! mu+(p1) -> e+(p2) \nu_mu(qA) \bar{\nu}_\mu(qB) e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e(qA)  \nu_\tau(qB) e-(p4) e+(p3)
    !! for massive electron

  use mudecrare, only: pt2mnneecav!!(p1, n1, p2, p3, p4, p5, p6)
    !! tau+(p1) -> mu+(p2) \nu_mu(qA) \bar{\nu}_\mu(qB) e+(p4) e-(p3)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e(qA)  \nu_\tau(qB) e-(p4) e+(p3)
    !! for massive electron

  use mudecrare, only: pt2mnneeav_a!!(p1,n1,p2,qA,qB,p3,p4)
    !! tau+(p1) -> mu+(p2) \nu_mu() \bar{\tau}_\mu() e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e( )  \nu_\tau() e+(p3) e-(p4)

  use mudecrare, only: pm2enneeav_a!!(p1,n1,p2,qA,qB,p3,p4)
    !! mu+(p1) -> e+(p2) \nu_mu() \bar{\tau}_\mu() e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e( )  \nu_\tau() e-(p4) e+(p3)
    !! for massive electron

  use mudecrare, only: pm2enneeav_ai!!(p1,n1,p2,qA,qB,p3,p4)
    !! mu+(p1) -> e+(p2) \nu_mu() \bar{\tau}_\mu() e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e( )  \nu_\tau() e-(p4) e+(p3)
    !! for massive electron


  !! From file mudecrare.f95
  use mudecrare, only: pm2enneegav_s!!(q1,n1,q2,qA,qB,q3,q4)
    !!  M1^+(p1)->M2^+(p2),M2^-(p3),M2^+(p4),2\nu,\gamma(p7)

  use mudecrare, only: pt2mnneegav_s!!(q1,n1,q2,qA,qB,q3,q4)
    !!  M1^+(p1)->M2^+(p2),M3^-(p3),M3^+(p4),2\nu,\gamma(p7)


  !! From file mue_em2emggeeee.f95
  use mue, only: em2emgg_eeee_old!!(p1,p2,p3,p4,p5,p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) y(p5) y(p6)
    !! for massive (and massless) electrons


  !! From file mue_mp2mpgg.opt.f95
  use mue, only: em2emgg_eeee!!(p1,p2,p3,p4,p5,p6)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(p5) y(p6)
    !! for massive electrons

  use mue, only: mp2mpgg!!(p1,p2,p3,p4,p5,p6)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5) y(p6)
    !! for massive muons


  !! From file mue_em2emeeee.f95
  use mue, only: em2emeeee!!(p1,p2,p3,p4,p5,p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) e-(p5) e-(p6)
    !! for massive (and massless) electrons


  !! From file mue_em2emgleeee.f95
  use mue, only: em2emgl_eeee_old!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) y(p5)
    !! for massive electrons, cdr?


  !! From file mue_mp2mpgl_coll.opt.f95
  use mue, only: em2emgl_eeee_coll!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(p5)
    !! for massive electrons

  use mue, only: mp2mpgl_coll!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons


  !! From file mue_mp2mpgl_pvred.opt.f95
  use mue, only: em2emgl_eeee_pvred!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(p5)
    !! for massive electrons

  use mue, only: mp2mpgl_pvred!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons


  !! From file mue_mp2mpgl_fullcoll.opt.f95
  use mue, only: em2emgl_eeee_fullcoll!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(p5)
    !! for massive electrons

  use mue, only: mp2mpgl_fullcoll!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(p5)
    !! for massive muons


  !! From file mue_mat_el.f95
  use mue, only: em2em!!(p1,p2,q1,q2, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons

  use mue, only: em2em_a!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons

  use mue, only: em2eml_ee!!(p1,p2,q1,q2, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2emg_ee!!(p1,p2,q1,q2,q3, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive (and massless) electrons

  use mue, only: em2eml_mm!!(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2emg_mm!!(p1,p2,q1,q2,q3,lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive (and massless) electrons

  use mue, only: em2eml_em!!(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2emg_em!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive (and massless) electrons

  use mue, only: em2emeik_ee!!(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) + whatever
    !! for massive electrons

  use mue, only: em2emll_eeee!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emff_eeee!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons, requires xieik1 = xieik2

  use mue, only: em2emgf_eeee!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive electrons

  use mue, only: em2emgl_eeee!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive electrons

  use mue, only: em2em_aa!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated

  use mue, only: em2em_alee!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated

  use mue, only: em2emg_aee!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive electrons, numerically interpolated

  use mue, only: em2em_nfee!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated

  use mue, only: ee2mm!!(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons

  use mue, only: ee2mml!!(p1, p2, p3, p4,sing)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons

  use mue, only: ee2mmg!!(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4) g(p5)
    !! for massive electrons & positrons

  use mue, only: mp2mp!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive (and massless) muons

  use mue, only: mp2mp_a!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mpl!!(p1,p2,q1,q2,pole)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mpg!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive (and massless) muons

  use mue, only: mp2mpll!!(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mpff!!(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mp_aa!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mp_al!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mpg_a!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mp_nf!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mpgl!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons

  use mue, only: mp2mpgf!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons

  use mue, only: em2emf_ee!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emf_em!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emf_mm!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2em_afee!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: ee2mmf!!(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons and positrons

  use mue, only: mp2mpf!!(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons

  use mue, only: mp2mp_af!!(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons

  use mue, only: em2eml!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emc!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons


  !! From file mue.f95
  use mue, only: em2emg_ee_s!!(p1,p2,q1,q2, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons

  use mue, only: em2emg_mm_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons

  use mue, only: em2emg_em_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons

  use mue, only: em2emg_aee_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons

  use mue, only: em2emgl_eeee_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive electrons

  use mue, only: em2emgf_eeee_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive electrons

  use mue, only: em2emgg_eeee_sh!!(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft) y(q4)
    !! for massive electrons

  use mue, only: em2emgg_eeee_hs!!(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3) y(ksoft)
    !! for massive electrons

  use mue, only: em2emgg_eeee_ss!!(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft) y(ksoft)
    !! for massive electrons

  use mue, only: ee2mmg_s!!(p1,p2,q1,q2)
    !! e-(p1) e+(p2) -> mu+(q1) mu-(q2) y(ksoft)
    !! for massive electrons and positrons

  use mue, only: mp2mpg_s!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive (and massless) muons

  use mue, only: mp2mpgl_s!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive muons

  use mue, only: mp2mpg_a_s!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive muons

  use mue, only: mp2mpgf_s!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive muons

  use mue, only: mp2mpgg_sh!!(p1, p2, q1, q2, q4)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft) y(q3)
    !! for massive muons

  use mue, only: mp2mpgg_hs!!(p1, p2, q1, q2, q3)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft) y(q4)
    !! for massive muons

  use mue, only: mp2mpgg_ss!!(p1, p2, q1, q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft) y(ksoft)
    !! for massive muons


  !! From file misc_ee2nngg.f95
  use misc, only: ee2nnggav!!(p1, p2,p3,p4, q2,q3)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q2) y(q3)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q2) y(q3)
    !! for massive electrons
    !! average over neutrino tensor taken


  !! From file misc_mat_el.f95
  use misc, only: ee2nneik!!(p1, p2, xicut, pole, lin)
    !! e+ (p1) e-(p2) -> whatever
    !! e- (p1) e+(p2) -> whatever
    !! for massive electrons

  use misc, only: ee2nn!!(p1, p2, q1, q2, linear)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  use misc, only: ee2nnl!!(p1, p2, q1, q2, pole, lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! Package-X convention

  use misc, only: ee2nnlav!!(p1, p2, p3, p4, pole, lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4)
    !! for massive electrons
    !! Package-X convention, m>0
    !! average over neutrino tensor taken

  use misc, only: ee2nng!!(p1, p2, q1, q2, q3)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! for massive electrons

  use misc, only: ee2nngav!!(p1, p2, q1, q2, q3,lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nnav!!(p1, p2, q1, q2, lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nnglav!!(p1, p2, q1, q3, q2,pole)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q3) y(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q3) y(q2)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngf!!(p1, p2, p3, p4, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q2)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nnf!!(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  use misc, only: ee2nns!!(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  use misc, only: ee2nnss!!(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  use misc, only: ee2nncc!!(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons


  !! From file misc.f95
  use misc, only: ee2nng_s!!(p1,p2,q1,q2,lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngl_s!!(p1,p2,q1,q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngf_s!!(p1,p2,q1,q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngg_sh!!(p1, p2, p3, p4, q3)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(q3)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(q3)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngg_hs!!(p1, p2, p3, p4, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q4) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q4) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngg_ss!!(p1, p2, p3, p4)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken


                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
