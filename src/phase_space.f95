                    !!!  ------------------  !!!
                         MODULE PHASE_SPACE
                    !!!  ------------------  !!!



  USE FUNCTIONS

  IMPLICIT NONE

  integer :: count_rm
  real (kind=prec) :: xiout, yout, Omyout, qsoft(4), yout2, yout3, yout4
  type(mlm):: ksoft

  real (kind=prec) :: xioutA, xioutB, youtA, youtB, qsoftA(4), qsoftB(4)
  type(mlm):: ksoftA, ksoftB


  CONTAINS


  SUBROUTINE PAIR_DEC(random_array,min,q3,m3,q4,m4,enough_energy)

         !!  q3^3 = m3^2;  q4^2 = m4^2;  (q3+q4)^2 = min^2     !!

  real (kind=prec), intent(in) :: random_array(2),min,m3,m4
  real (kind=prec), intent(out) :: q3(4),q4(4)
  integer, intent(out) :: enough_energy
  real (kind=prec) :: pp, e3, e4,                               &
            phi3, sin_th3, cos_th3, sin_phi3,  cos_phi3

  if(min > m3+m4) then
    enough_energy = 1
  else
    enough_energy = 0
    q3 = 0._prec; q4 = 0._prec;
    return
  endif

         ! Generate q3 and q4 in rest frame of q3+q4

  e3 = 0.5*(min+(m3**2-m4**2)/min)
  e4 = 0.5*(min+(m4**2-m3**2)/min)
  pp = 0.5*sq_lambda(min**2,m3,m4)/min

  phi3 = 2*pi*random_array(1)
  cos_th3 = 2*random_array(2) - 1._prec
  sin_th3 = sqrt(1 - cos_th3**2)
  sin_phi3 = sin(phi3)
  cos_phi3 = cos(phi3)

  q3 = (/ pp*sin_th3*cos_phi3, pp*sin_th3*sin_phi3, pp*cos_th3, e3/)
  q4 = (/ -pp*sin_th3*cos_phi3, -pp*sin_th3*sin_phi3, -pp*cos_th3, e4/)

  END SUBROUTINE PAIR_DEC





  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!                             !!
  !!           MAJORON           !!
  !!                             !!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  SUBROUTINE PSD3(ra,q1,m1,q2,m2,q3,m3,weight)

  !!                in  c.m.f.                !!
  !!               q1 = q2 + q3               !!
  !!  q1^2 = Mm^2;  q2^2 = m2^2; q3^2 = m3^2  !!

  real (kind=prec), intent(in) :: ra(2), m1, m2, m3
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4)
  integer :: enough_energy

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  call pair_dec(ra(1:2),m1,q2,m2,q3,m3,enough_energy)
  if(enough_energy == 0) then
     weight = 0._prec
     return
  endif
  weight = sq_lambda(m1**2,m2,m3)/(8*pi*m1**2)

  END SUBROUTINE PSD3


  SUBROUTINE PSD4_FKS(ra,q1,m1,q2,m2,q3,m3,q4,weight)

  !!                     in  c.m.f.                     !!
  !!                  q1 = q2 + q3 + q4                 !!
  !!                  mu = el + mj + ph                 !!

  real (kind=prec), intent(in) :: ra(5), m1, m2, m3
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4)
  integer :: enough_energy
  real (kind=prec) :: xi2, y2, qq23(4), minv23
  real (kind=prec) :: al, ca, sa

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi2 = ra(1)
  xiout = xi2  !! global
  y2 = 2*ra(2) - 1._prec
  yout = y2    !! global

  al = 2*pi*ra(3)
  ca = cos(al); sa = sin(al)

  q4 = (/ -sqrt(1._prec - y2**2)*sa, sqrt(1._prec - y2**2)*ca, y2, 1._prec /)
  qsoft = 0.5*m1*q4  !! global, will be rotated below
  ksoft = make_mlm(qsoft)  !! global
  q4 = 0.5*m1*xi2*q4

  qq23 = q1-q4
  minv23 = sq(qq23)
  if(minv23 < 0) goto 999
  minv23 = sqrt(minv23)

  weight = (m1**2)/(16*pi**2)
  call pair_dec(ra(4:5),minv23, q2, m2, q3, m3,enough_energy)

  q2 = boost_back(qq23, q2)
  q3 = boost_back(qq23, q3)
  if(enough_energy == 0) goto 999
  weight = weight*sq_lambda(minv23**2,m2,m3)/(8*pi*minv23**2)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  weight = 0.
  return

  END SUBROUTINE PSD4_FKS

  SUBROUTINE PSD4(ra,q1,m1,q2,m2,q3,m3,q4,m4,weight)

       !!                       in  c.m.f.                    !!
       !!                   q1 = q2 + q3 + q4                 !!
       !!  q1^2 = Mm^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(5), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4)
  integer :: enough_energy
  real (kind=prec) :: qq3(4), minv3

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  minv3 = ra(1)*m1
  weight = minv3*m1/pi
  call pair_dec(ra(2:3),m1,q2,m2,qq3,minv3,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(m1**2,m2,minv3)/m1**2/pi

  call pair_dec(ra(4:5),minv3,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv3**2,m3,m4)/minv3**2/pi

  q3 = boost_back(qq3, q3)
  q4 = boost_back(qq3, q4)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD4



  SUBROUTINE PSD5(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,weight)


  real (kind=prec), intent(in) :: ra(8), m1, m2, m3, m4, m5
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4)
  integer :: enough_energy
  real (kind=prec) :: qq3(4), minv3, qq4(4), minv4

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  minv3 = ra(1)*m1
  weight = minv3*m1/pi
  call pair_dec(ra(2:3),m1,q2,m2,qq3,minv3,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(m1**2,m2,minv3)/m1**2/pi

  minv4 = ra(4)*m1
  weight = weight*minv4*m1/pi
  call pair_dec(ra(5:6),minv3,q3,m3,qq4,minv4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv3**2,m3,minv4)/minv3**2/pi

  call pair_dec(ra(7:8),minv4,q4,m4,q5,m5,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv4**2,m4,m5)/minv4**2/pi

  q4 = boost_back(qq4, q4)
  q5 = boost_back(qq4, q5)

  q3 = boost_back(qq3, q3)
  q4 = boost_back(qq3, q4)
  q5 = boost_back(qq3, q5)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD5



  SUBROUTINE PSD6(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,m6,weight)


  real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4, m5, m6
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4), q6(4)
  integer :: enough_energy
  real (kind=prec) :: qq3(4), minv3, qq4(4), minv4, qq5(4), minv5

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  minv3 = ra(1)*m1
  weight = minv3*m1/pi
  call pair_dec(ra(2:3),m1,q2,m2,qq3,minv3,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(m1**2,m2,minv3)/m1**2/pi

  minv4 = ra(4)*m1
  weight = weight*minv4*m1/pi
  call pair_dec(ra(5:6),minv3,q3,m3,qq4,minv4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv3**2,m3,minv4)/minv3**2/pi

  minv5 = ra(7)*m1
  weight = weight*minv5*m1/pi
  call pair_dec(ra(8:9),minv4,q4,m4,qq5,minv5,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv4**2,m4,minv5)/minv4**2/pi

  call pair_dec(ra(10:11),minv5,q5,m5,q6,m6,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv5**2,m5,m6)/minv5**2/pi

  q5 = boost_back(qq5, q5)
  q6 = boost_back(qq5, q6)

  q4 = boost_back(qq4, q4)
  q5 = boost_back(qq4, q5)
  q6 = boost_back(qq4, q6)

  q3 = boost_back(qq3, q3)
  q4 = boost_back(qq3, q4)
  q5 = boost_back(qq3, q5)
  q6 = boost_back(qq3, q6)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  q6 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD6


  SUBROUTINE PSD6_23_24_34(ra,q1,m1,q2,m2,q5,m5,q6,m6,q3,m3,q4,m4,weight)
    ! A massive 1->5 FKS-ish phase space.
    !
    ! q2 should be the unique particle (electron) and q3-q4 are the
    ! identical particles (postirons)
    !
    !
    !                     _/  q
    !                     /|   2
    !  ---<---*~~~~~~~~~~*
    !         |          |
    !         ^          ^  q
    !         | q        |   3
    !            4
    ! The 'spectator' neutrinos are q5 and q6
    !
    ! Start by generating p2 and p3 at an angle * = arccos(y2)
    !
    !
    !                  ^ p2
    !                 |||
    !                 |||
    !    p3         __|||
    !     --__     /  |||
    !         --__/  *|||
    !             --__|||
    !                 |||
    !
    ! Generate p4 at an angle * = arccos(y3) and rotating by an
    ! angle phi w.r.t. to p3
    !
    !                 |||      / p4
    !              _--|||--_  /
    !             -_  |||  _-/
    !    p3         --___-- /
    !     --__        |||__/
    !         --__    |||*/
    !             --__|||/
    !                 |||


    implicit none
    real(kind=prec), intent(in) :: ra(11), m1,m2,m3,m4,m5,m6
    real(kind=prec), intent(out) :: weight
    real(kind=prec), intent(out), dimension(4) :: q1,q2,q3,q4,q5,q6
    integer enough_energy
    real(kind=prec) y1, y2, E1, E2, Ee, pp1, pp2, pe
    real(kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
    real(kind=prec) :: qq56(4), minv56

    weight = 1._prec

    ! Generate muon
    q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

    ! Generate the angles
    y1 = 2*ra(1) - 1._prec  ! Electron and positron 1 (part. 3)
    y2 = 2*ra(2) - 1._prec  ! Electron and positron 2 (part. 4)

    ! Generate the energies
    E1 = 0.5*m1*ra(3)  ! Positron 1
    E2 = 0.5*m1*ra(4)  ! Positron 2
    Ee = 0.5*m1*ra(5)  ! Electron

    ! Check whether the chared leptons got enough energy for their mass
    if (Ee - m2 < zero  .or.  E1 - m3 < zero  .or.  E2 - m4 < zero) goto 999
    ! Check if there is *any* energy left
    if (m1 - Ee - E1 - E2 < zero) goto 999

    ! Compute the value of the momentum
    Pe  = sqrt(Ee**2-m2**2)
    Pp1 = sqrt(E1**2-m3**2)
    Pp2 = sqrt(E2**2-m4**2)

    ! Generate the electron
    q2 = (/ 0._prec, 0._prec, Pe, Ee /)
    weight = weight * Pe * m1 / (2*pi)**2 / 2

    ! Generate the first positron
    q3 = (/ 0._prec, Pp1*sqrt(1._prec - y1**2), Pp1*y1, E1 /)
    weight = weight * Pp1 * m1 / (2*pi)**2 / 2

    ! Generate the second positron
    !         /  0  \
    ! q  = R  | sin |
    !  4    z \ cos /

    al = 2*pi*ra(6)
    ca = cos(al); sa = sin(al)
    sb = sqrt(1._prec - y2**2)
    q4 = (/ -Pp2*sa*sb, Pp2*ca*sb, Pp2*y2, E2 /)
    weight = weight * Pp2 * m1 / (2*pi)**2 / 2

          ! generate euler angles and rotate all momenta

    al = 2*pi*ra(7)
    cos_be = 1 - 2*ra(8)
    ga = 2*pi*ra(9)

    ca = cos(al); sa = sin(al)
    cb = cos_be
    sb = sqrt(1 - cb**2)
    cc = cos(ga); sc = sin(ga)

    euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
    euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
    euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
    euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

    q2 = matmul(euler_mat,q2)
    q3 = matmul(euler_mat,q3)
    q4 = matmul(euler_mat,q4)

    ! Generate the spectators
    qq56 = q1-q2-q3-q4
    minv56 = sq(qq56)
    if(minv56 < 0) goto 999
    minv56 = sqrt(minv56)

    call pair_dec(ra(10:11),minv56,q5,m5,q6,m6,enough_energy)
    if(enough_energy == 0) goto 999
    weight = weight*0.125*sq_lambda(minv56**2,m5,m6)/minv56**2/pi

    q5 = boost_back(qq56, q5)
    q6 = boost_back(qq56, q6)


    return

999 continue
    weight = 0._prec
    q1 = 0._prec
    q2 = 0._prec
    q3 = 0._prec
    q4 = 0._prec
    q5 = 0._prec
    q6 = 0._prec

    return
  END SUBROUTINE PSD6_23_24_34





  SUBROUTINE PSD7(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,m6,q7,m7,weight)

  real (kind=prec), intent(in) :: ra(14), m1, m2, m3, m4, m5, m6, m7
  real (kind=prec), intent(out) :: weight,q1(4),q2(4),q3(4),q4(4),q5(4),q6(4),q7(4)
  integer :: enough_energy
  real (kind=prec) :: qq3(4),minv3,qq4(4),minv4,qq5(4),minv5,qq6(4),minv6

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  minv3 = ra(1)*m1
  weight = minv3*m1/pi
  call pair_dec(ra(2:3),m1,q2,m2,qq3,minv3,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(m1**2,m2,minv3)/m1**2/pi

  minv4 = ra(4)*m1
  weight = weight*minv4*m1/pi
  call pair_dec(ra(5:6),minv3,q3,m3,qq4,minv4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv3**2,m3,minv4)/minv3**2/pi

  minv5 = ra(7)*m1
  weight = weight*minv5*m1/pi
  call pair_dec(ra(8:9),minv4,q4,m4,qq5,minv5,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv4**2,m4,minv5)/minv4**2/pi

  minv6 = ra(10)*m1
  weight = weight*minv6*m1/pi
  call pair_dec(ra(11:12),minv5,q5,m5,qq6,minv6,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv5**2,m5,minv6)/minv5**2/pi

  call pair_dec(ra(13:14),minv6,q6,m6,q7,m7,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv6**2,m6,m7)/minv6**2/pi

  q6 = boost_back(qq6, q6)
  q7 = boost_back(qq6, q7)

  q5 = boost_back(qq5, q5)
  q6 = boost_back(qq5, q6)
  q7 = boost_back(qq5, q7)

  q4 = boost_back(qq4, q4)
  q5 = boost_back(qq4, q5)
  q6 = boost_back(qq4, q6)
  q7 = boost_back(qq4, q7)

  q3 = boost_back(qq3, q3)
  q4 = boost_back(qq3, q4)
  q5 = boost_back(qq3, q5)
  q6 = boost_back(qq3, q6)
  q7 = boost_back(qq3, q7)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  q6 = 0.
  q7 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD7



  SUBROUTINE PSD5_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,weight)

    !! q5 (photon) massless

  real (kind=prec), intent(in) :: ra(8), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4)
  integer :: enough_energy
  real (kind=prec) :: qq34(4), minv34
  real (kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
  real (kind=prec) :: xi5, y2, eme, pme, w

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi5 = ra(1)
  xiout = xi5  !! global
  y2 = 2*ra(2) - 1._prec
  yout = y2    !! global
  w = 0.5*m1*xi5

  eme = m1*ra(3)
  if(eme-m2 < zero .or. m1-eme-w < zero)  goto 999
  pme = sqrt(eme**2-m2**2)
  q2 = (/ 0._prec, pme*sqrt(1._prec - y2**2), pme*y2, eme /)
  q5 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  qsoft = 0.5*m1*q5  !! global, will be rotated below
  q5 = 0.5*m1*xi5*q5  !! = w*q5

          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(6)
  cos_be = 1 - 2*ra(4)
  ga = 2*pi*ra(5)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  q2 = matmul(euler_mat,q2)
  q5 = matmul(euler_mat,q5)
  qsoft = matmul(euler_mat,qsoft)     !! global
  ksoft = make_mlm(qsoft)  !! global

  qq34 = q1-q2-q5
  minv34 = sq(qq34)
  if(minv34 < 0) goto 999
  minv34 = sqrt(minv34)

  weight = m1**3*pme/(4.*(2._prec*pi)**4)
!!  weight = xi5*weight   !! into matrix element

          ! generate remaining neutrino momenta

  call pair_dec(ra(7:8),minv34,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv34**2,m3,m4)/minv34**2/pi

  q3 = boost_back(qq34, q3)
  q4 = boost_back(qq34, q4)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD5_FKS



  SUBROUTINE PSD6_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,weight)

    !! q6 (photon) massless

  real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4, m5
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4), q6(4)
  integer :: enough_energy
  real (kind=prec) :: qq345(4), minv345, qq34(4), minv34
  real (kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
  real (kind=prec) :: xi6, y2, eme, pme, w

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi6 = ra(1)
  xiout = xi6  !! global
  y2 = 2*ra(2) - 1._prec
  yout = y2    !! global
  w = 0.5*m1*xi6

  eme = m1*ra(3)
  if(eme-m2 < zero .or. m1-eme-w < zero)  goto 999
  pme = sqrt(eme**2-m2**2)
  q2 = (/ 0._prec, pme*sqrt(1._prec - y2**2), pme*y2, eme /)
  q6 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  qsoft = 0.5*m1*q6  !! global, will be rotated below
  q6 = 0.5*m1*xi6*q6  !! = w*q6

          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(6)
  cos_be = 1 - 2*ra(4)
  ga = 2*pi*ra(5)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  q2 = matmul(euler_mat,q2)
  q6 = matmul(euler_mat,q6)
  qsoft = matmul(euler_mat,qsoft)     !! global
  ksoft = make_mlm(qsoft)  !! global

  qq345 = q1-q2-q6
  minv345 = sqrt(sq(qq345))

  weight = m1**3*pme/(4.*(2._prec*pi)**4)
!!  weight = xiout*weight   !! into matrix element

          ! generate remaining photon plus neutrino-pair momenta

  minv34 = ra(9)*m1
  weight = weight*minv34*m1/pi
  call pair_dec(ra(10:11),minv345,q5,m5,qq34,minv34,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv345**2,minv34,m5)/minv345**2/pi

          ! generate remaining neutrino momenta

  call pair_dec(ra(7:8),minv34,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv34**2,m3,m4)/minv34**2/pi

  q3 = boost_back(qq34, q3)
  q4 = boost_back(qq34, q4)

  q3 = boost_back(qq345, q3)
  q4 = boost_back(qq345, q4)
  q5 = boost_back(qq345, q5)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  q6 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD6_FKS



  SUBROUTINE PSD6_B_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,weight)

    !! q6 (photon) massless
    !! HERE Collinear limit ra(2) -> 0  !! (NOT ra(2) -> 1)

  real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4, m5
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4), q6(4)
  integer :: enough_energy
  real (kind=prec) :: qq345(4), minv345, qq34(4), minv34
  real (kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
  real (kind=prec) :: xi6, y2, eme, pme, w, Omy2

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi6 = ra(1)
  xiout = xi6  !! global
  Omy2 = 2*ra(2)
  y2 = 1._prec - Omy2
  Omyout = Omy2 !! global
  yout = y2    !! global
  w = 0.5*m1*xi6

  eme = m1*ra(3)
  if(eme-m2 < zero .or. m1-eme-w < zero)  goto 999
  pme = sqrt(eme**2-m2**2)
  q2 = (/ 0._prec, pme*sqrt(Omy2*(1._prec+y2)), pme*y2, eme /)
  q6 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  qsoft = 0.5*m1*q6  !! global, will be rotated below
  q6 = 0.5*m1*xi6*q6  !! = w*q6

          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(6)
  cos_be = 1 - 2*ra(4)
  ga = 2*pi*ra(5)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  q2 = matmul(euler_mat,q2)
  q6 = matmul(euler_mat,q6)
  qsoft = matmul(euler_mat,qsoft)     !! global
  ksoft = make_mlm(qsoft)  !! global

  qq345 = q1-q2-q6
  minv345 = sqrt(sq(qq345))

  weight = m1**3*pme/(4.*(2._prec*pi)**4)
!!  weight = xiout*weight   !! into matrix element

          ! generate remaining photon plus neutrino-pair momenta

  minv34 = ra(9)*m1
  weight = weight*minv34*m1/pi
  call pair_dec(ra(10:11),minv345,q5,m5,qq34,minv34,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv345**2,minv34,m5)/minv345**2/pi

          ! generate remaining neutrino momenta

  call pair_dec(ra(7:8),minv34,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv34**2,m3,m4)/minv34**2/pi

  q3 = boost_back(qq34, q3)
  q4 = boost_back(qq34, q4)

  q3 = boost_back(qq345, q3)
  q4 = boost_back(qq345, q4)
  q5 = boost_back(qq345, q5)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  q6 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD6_B_FKS



  SUBROUTINE PSD5_25(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,weight)

    !! suitable for Born term (NOT for FKS subtraction)
    !! q5 (photon) massless
    !! HERE Collinear limit ra(2) -> 0  !! (NOT ra(2) -> 1)

  real (kind=prec), intent(in) :: ra(8), m1, m2, m3, m4, m5
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4)
  integer :: enough_energy
  real (kind=prec) :: qq34(4), minv34
  real (kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
  real (kind=prec) :: xi5, y2, eme, pme, w, Omy2

  if (m5 > zero) call crash("PS_5_FKS0")

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi5 = ra(1)
  xiout = xi5  !! global
  Omy2 = 2*ra(2)
  y2 = 1._prec - Omy2
  Omyout = Omy2 !! global
  yout = y2    !! global
  w = 0.5*m1*xi5

  eme = m1*ra(3)
  if(eme-m2 < zero .or. m1-eme-w < zero)  goto 999
  pme = sqrt(eme**2-m2**2)
  q2 = (/ 0._prec, pme*sqrt(Omy2*(1._prec+y2)), pme*y2, eme /)
  q5 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  qsoft = 0.5*m1*q5  !! global, will be rotated below
  q5 = 0.5*m1*xi5*q5  !! = w*q5

          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(6)
  cos_be = 1 - 2*ra(4)
  ga = 2*pi*ra(5)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  q2 = matmul(euler_mat,q2)
  q5 = matmul(euler_mat,q5)
  qsoft = matmul(euler_mat,qsoft)     !! global
  ksoft = make_mlm(qsoft)  !! global

  qq34 = q1-q2-q5
  minv34 = sq(qq34)
  if(minv34 < 0) goto 999
  minv34 = sqrt(minv34)

  weight = m1**3*pme/(4.*(2._prec*pi)**4)
  weight = xi5*weight   !! HERE NOT into matrix element

          ! generate remaining neutrino momenta

  call pair_dec(ra(7:8),minv34,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv34**2,m3,m4)/minv34**2/pi

  q3 = boost_back(qq34, q3)
  q4 = boost_back(qq34, q4)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD5_25


  SUBROUTINE PSD7_27_37_47_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,m6,q7,weight)

    ! Phase space for 1 -> 2,3,4,5,6+7
    ! 7 massless FKS photon may be collinear to 2, 3 or 4
    ! yout => y2
    ! Neutrinos = q5, q6

    implicit none
    real(prec), intent(in) :: ra(14), m1,m2,m3,m4,m5,m6
    real(prec), intent(out) :: weight
    real(prec), intent(out), dimension(4) :: q1,q2,q3,q4,q5,q6,q7
    integer enough_energy
    real(prec) xi7, y2, y3, y4, E2, E3, E4, pme, energy_left, w
    real(prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
    real(prec) :: qq56(4), minv56

    ! Generate FKS parameters
    xi7 = ra(1)
    xiout = xi7

    y2 = 2*ra(2) - 1._prec
    y3 = 2*ra(3) - 1._prec
    y4 = 2*ra(4) - 1._prec


    yout = y2            !as!
    yout2 = y2
    yout3 = y3
    yout4 = y4


    ! Generate muon
    q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)
    w = 0.5*m1*xi7
    energy_left = m1 - w


    weight = 1._prec !(2*pi)**4

    ! Generate FKS photon
    q7 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
    qsoft = 0.5*m1*q7  !! global, will be rotated below
    q7 = 0.5*m1*xi7*q7  !! = w*q5

    weight = weight * m1**2 / (4._prec * (2*pi)**2)

    ! Generate electron 2
    E2 = m1*ra(5)
    if(E2-m2 < zero) goto 999
    pme = sqrt(E2**2-m2**2)
    energy_left = energy_left - E2
    if(E2-m2 < zero .or. energy_left < zero) goto 999
    q2 = (/ 0._prec, pme*sqrt(1._prec - y2**2), pme*y2, E2 /)
    weight = weight * pme * m1 / (2*pi)**2


    ! Generate electron 3
    !         / 0   \
    ! q  = R  | sin |
    !  3    z \ cos /
    E3 = m1*ra(6)
    if(E3-m3 < zero) goto 999
    pme = sqrt(E3**2-m3**2)
    energy_left = energy_left - E3
    if(E3-m3 < zero .or. energy_left < zero) goto 999

    al = 2*pi*ra(7)
    ca = cos(al); sa = sin(al)
    sb = sqrt(1._prec - y3**2)
    q3 = (/ -pme*sa*sb, pme*ca*sb, pme*y3, E3 /)
    weight = weight * pme * m1 / (2*pi)**2




    ! Generate electron 3
    E4 = m1*ra(8)
    if(E4-m4 < zero) goto 999
    pme = sqrt(E4**2-m4**2)
    energy_left = energy_left - E4
    if(E4-m4 < zero .or. energy_left < zero) goto 999

    al = 2*pi*ra(9)
    ca = cos(al); sa = sin(al)
    sb = sqrt(1._prec - y4**2)
    q4 = (/ -pme*sa*sb, pme*ca*sb, pme*y4, E4 /)
    weight = weight * pme * m1 / (2*pi)**2



          ! generate euler angles and rotate all momenta

    al = 2*pi*ra(12)
    cos_be = 1 - 2*ra(10)
    ga = 2*pi*ra(11)

    ca = cos(al); sa = sin(al)
    cb = cos_be
    sb = sqrt(1 - cb**2)
    cc = cos(ga); sc = sin(ga)

    euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
    euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
    euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
    euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

    q2 = matmul(euler_mat,q2)
    q3 = matmul(euler_mat,q3)
    q4 = matmul(euler_mat,q4)

    q7 = matmul(euler_mat,q7)
    qsoft = matmul(euler_mat,qsoft)     !! global
    ksoft = make_mlm(qsoft)  !! global


    qq56 = q1-q2-q3-q4-q7
    minv56 = sq(qq56)
    if(minv56 < 0) goto 999
    minv56 = sqrt(minv56)

    call pair_dec(ra(13:14),minv56,q5,m5,q6,m6,enough_energy)
    if(enough_energy == 0) goto 999
    weight = weight*0.125*sq_lambda(minv56**2,m5,m6)/minv56**2/pi

    q5 = boost_back(qq56, q5)
    q6 = boost_back(qq56, q6)


    return

999 continue
    weight = 0._prec
    q1 = 0._prec
    q2 = 0._prec
    q3 = 0._prec
    q4 = 0._prec
    q5 = 0._prec
    q6 = 0._prec
    q7 = 0._prec

    return
  END SUBROUTINE PSD7_27_37_47_FKS



  SUBROUTINE PSD6_23_24_34_E56(ra,q1,m1,q3,m3,q5,m5,q6,m6,q2,m2,q4,m4,weight)
    ! A massive 1->5 FKS-ish phase space.   FOR TAILS Mm-Etot
    !
    ! q2 should be the unique particle (electron) and q3-q4 are the
    ! identical particles (postirons)
    !
    !
    !                     _/  q
    !                     /|   2
    !  ---<---*~~~~~~~~~~*
    !         |          |
    !         ^          ^  q
    !         | q        |   3
    !            4
    ! The 'spectator' neutrinos are q5 and q6
    !
    ! Start by generating p2 and p3 at an angle * = arccos(y2)
    !
    !
    !                  ^ p2
    !                 |||
    !                 |||
    !    p3         __|||
    !     --__     /  |||
    !         --__/  *|||
    !             --__|||
    !                 |||
    !
    ! Generate p4 at an angle * = arccos(y3) and rotating by an
    ! angle phi w.r.t. to p3
    !
    !                 |||      / p4
    !              _--|||--_  /
    !             -_  |||  _-/
    !    p3         --___-- /
    !     --__        |||__/
    !         --__    |||*/
    !             --__|||/
    !                 |||
    !
    ! This version is optimised for the tail!!!!!
    !


    implicit none
    real(prec), intent(in) :: ra(11), m1,m2,m3,m4,m5,m6
    real(prec), intent(out) :: weight
    real(prec), intent(out), dimension(4) :: q1,q2,q3,q4,q5,q6
    integer enough_energy
    real(prec) y1, y2, E1, E2, Ee, pp1, pp2, pe
    real(prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
    real(prec) :: qq56(4), minv56

    weight = 1._prec

    ! Generate muon
    q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

    ! Generate the angles
    y1 = 2*ra(1) - 1._prec  ! Electron and positron 1 (part. 3)
    y2 = 2*ra(2) - 1._prec  ! Electron and positron 2 (part. 4)

    ! Generate the energies
!    E1 = 0.5*m1*ra(3)  ! Positron 1
!    E2 = 0.5*m1*ra(4)  ! Positron 2
!    Ee = 0.5*m1*ra(5)  ! Electron


    weight = weight * ra(3)**2 * 8

    E1 = ra(3)*m1*ra(4)
    E2 = ra(3)*m1*ra(5)
    Ee = ra(3)*m1-E1-E2


    ! Check whether the chared leptons got enough energy for their mass
    if (Ee - m2 < zero  .or.  E1 - m3 < zero  .or.  E2 - m4 < zero) goto 999
    ! Check if there is *any* energy left
    if (m1 - Ee - E1 - E2 < zero) goto 999

    ! Compute the value of the momentum
    Pe  = sqrt(Ee**2-m2**2)
    Pp1 = sqrt(E1**2-m3**2)
    Pp2 = sqrt(E2**2-m4**2)

    ! Generate the electron
    q2 = (/ 0._prec, 0._prec, Pe, Ee /)
    weight = weight * Pe * m1 / (2*pi)**2 / 2

    ! Generate the first positron
    q3 = (/ 0._prec, Pp1*sqrt(1._prec - y1**2), Pp1*y1, E1 /)
    weight = weight * Pp1 * m1 / (2*pi)**2 / 2

    ! Generate the second positron
    !         /  0  \
    ! q  = R  | sin |
    !  4    z \ cos /

    al = 2*pi*ra(6)
    ca = cos(al); sa = sin(al)
    sb = sqrt(1._prec - y2**2)
    q4 = (/ -Pp2*sa*sb, Pp2*ca*sb, Pp2*y2, E2 /)
    weight = weight * Pp2 * m1 / (2*pi)**2 / 2

          ! generate euler angles and rotate all momenta

    al = 2*pi*ra(7)
    cos_be = 1 - 2*ra(8)
    ga = 2*pi*ra(9)

    ca = cos(al); sa = sin(al)
    cb = cos_be
    sb = sqrt(1 - cb**2)
    cc = cos(ga); sc = sin(ga)

    euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
    euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
    euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
    euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

    q2 = matmul(euler_mat,q2)
    q3 = matmul(euler_mat,q3)
    q4 = matmul(euler_mat,q4)

    ! Generate the spectators
    qq56 = q1-q2-q3-q4
    minv56 = sq(qq56)
    if(minv56 < 0) goto 999
    minv56 = sqrt(minv56)

    call pair_dec(ra(10:11),minv56,q5,m5,q6,m6,enough_energy)
    if(enough_energy == 0) goto 999
    weight = weight*0.125*sq_lambda(minv56**2,m5,m6)/minv56**2/pi

    q5 = boost_back(qq56, q5)
    q6 = boost_back(qq56, q6)


    return

999 continue
    weight = 0._prec
    q1 = 0._prec
    q2 = 0._prec
    q3 = 0._prec
    q4 = 0._prec
    q5 = 0._prec
    q6 = 0._prec

    return
  END SUBROUTINE PSD6_23_24_34_E56






  SUBROUTINE PSD7_27_37_47_E56_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,m6,q7,weight)
    ! Phase space for 1 -> 2,3,4,5,6+7  FOR TAILS Mm-Etot
    ! 7 massless FKS photon may be collinear to 2, 3 or 4
    ! yout => y2
    ! Neutrinos = q5, q6

    implicit none
    real(prec), intent(in) :: ra(14), m1,m2,m3,m4,m5,m6
    real(prec), intent(out) :: weight
    real(prec), intent(out), dimension(4) :: q1,q2,q3,q4,q5,q6,q7
    integer enough_energy
    real(prec) xi7, y2, y3, y4, E2, E3, E4, pme, energy_left, w
    real(prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
    real(prec) :: qq56(4), minv56

    ! Generate FKS parameters
    xi7 = ra(1)
    xiout = xi7

    y2 = 2*ra(2) - 1._prec
    y3 = 2*ra(3) - 1._prec
    y4 = 2*ra(4) - 1._prec


    yout2 = y2
    yout3 = y3
    yout4 = y4


    ! Generate muon
    q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)
    w = 0.5*m1*xi7
    energy_left = m1 - w


    weight = 1._prec !(2*pi)**4

    ! Generate FKS photon
    q7 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
    qsoft = 0.5*m1*q7  !! global, will be rotated below
    q7 = 0.5*m1*xi7*q7  !! = w*q5

    weight = weight * m1**2 / (4._prec * (2*pi)**2)

    !evis = ra(5)*m1
    !E2 = evis*ra(6)
    !E3 = evis*ra(8)
    !E3 = evis*(1-ra(6))

    weight = weight * ra(5)**2

    E2 = ra(5)*m1*ra(6)
    E3 = ra(5)*m1*ra(8)
    E4 = ra(5)*m1-E2-E3

    ! Generate electron 2
    if(E2-m2 < zero) goto 999
    pme = sqrt(E2**2-m2**2)
    energy_left = energy_left - E2
    if(E2-m2 < zero .or. energy_left < zero) goto 999
    q2 = (/ 0._prec, pme*sqrt(1._prec - y2**2), pme*y2, E2 /)
    weight = weight * pme * m1 / (2*pi)**2


    ! Generate electron 3
    !         / 0   \
    ! q  = R  | sin |
    !  3    z \ cos /
    if(E3-m3 < zero) goto 999
    pme = sqrt(E3**2-m3**2)
    energy_left = energy_left - E3
    if(E3-m3 < zero .or. energy_left < zero) goto 999

    al = 2*pi*ra(7)
    ca = cos(al); sa = sin(al)
    sb = sqrt(1._prec - y3**2)
    q3 = (/ -pme*sa*sb, pme*ca*sb, pme*y3, E3 /)
    weight = weight * pme * m1 / (2*pi)**2




    ! Generate electron 3
    if(E4-m4 < zero) goto 999
    pme = sqrt(E4**2-m4**2)
    energy_left = energy_left - E4
    if(E4-m4 < zero .or. energy_left < zero) goto 999

    al = 2*pi*ra(9)
    ca = cos(al); sa = sin(al)
    sb = sqrt(1._prec - y4**2)
    q4 = (/ -pme*sa*sb, pme*ca*sb, pme*y4, E4 /)
    weight = weight * pme * m1 / (2*pi)**2



          ! generate euler angles and rotate all momenta

    al = 2*pi*ra(12)
    cos_be = 1 - 2*ra(10)
    ga = 2*pi*ra(11)

    ca = cos(al); sa = sin(al)
    cb = cos_be
    sb = sqrt(1 - cb**2)
    cc = cos(ga); sc = sin(ga)

    euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
    euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
    euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
    euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

    q2 = matmul(euler_mat,q2)
    q3 = matmul(euler_mat,q3)
    q4 = matmul(euler_mat,q4)

    q7 = matmul(euler_mat,q7)
    qsoft = matmul(euler_mat,qsoft)     !! global
    ksoft = make_mlm(qsoft)  !! global


    qq56 = q1-q2-q3-q4-q7
    minv56 = sq(qq56)
    if(minv56 < 0) goto 999
    minv56 = sqrt(minv56)

    call pair_dec(ra(13:14),minv56,q5,m5,q6,m6,enough_energy)
    if(enough_energy == 0) goto 999
    weight = weight*0.125*sq_lambda(minv56**2,m5,m6)/minv56**2/pi

    q5 = boost_back(qq56, q5)
    q6 = boost_back(qq56, q6)


    return

999 continue
    weight = 0._prec
    q1 = 0._prec
    q2 = 0._prec
    q3 = 0._prec
    q4 = 0._prec
    q5 = 0._prec
    q6 = 0._prec
    q7 = 0._prec

    return
  END SUBROUTINE PSD7_27_37_47_E56_FKS



  SUBROUTINE PSD6_omega_m50_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,q6,weight)

    !! q6 (photon) massless,  q5 (photon) massless

  real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4), q6(4)
  integer :: enough_energy
  real (kind=prec) :: qq34(4), minv34
  real (kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
  real (kind=prec) :: xi6, y2, eme, pme, w, y5, phi5, Omy5, s5, c5, e5, e34
  real (kind=prec) :: c1k, c2sq, c3k, q2Pq6(4)

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi6 = ra(1)
  xiout = xi6  !! global
  y2 = 2*ra(2) - 1._prec
  yout = y2    !! global
  w = 0.5*m1*xi6

  Omy5 = 2*ra(3)
  y5 = 1._prec - Omy5
  phi5 = 2*pi*ra(4)
  s5 = sin(phi5); c5 = cos(phi5)

  eme = m1*ra(5)
  if(eme-m2 < zero .or. m1-eme-w < zero) goto 999
  pme = sqrt(eme**2-m2**2)
  q2 = (/ 0._prec, pme*sqrt(1._prec - y2**2), pme*y2, eme /)
  q6 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  qsoft = 0.5*m1*q6  !! global, will be rotated below
  q6 = 0.5*m1*xi6*q6  !! = w*q6
  q5 = (/ s5*Sqrt(Omy5*(1.+y5)),Sqrt(1-y2**2)*y5 + c5*y2*Sqrt(Omy5*(1.+y5)), &
          y2*y5-c5*Sqrt(1-y2**2)*Sqrt(Omy5*(1.+y5)), 1._prec  /)


  minv34 = ra(9)*m1
  c1k = M1 - eme - w
  q2Pq6 = q2+q6
  c2sq = minv34**2 + sum(q2Pq6(1:3)*q2Pq6(1:3))
  c3k = sum(q2Pq6(1:3)*q5(1:3))
  e5 = (C1k**2 - C2sq)/(2.*(C1k + C3k))
  if(e5 < zero) goto 999
  q5 = e5*q5
  e34 = m1-e5-w-eme

          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(6)
  cos_be = 1 - 2*ra(7)
  ga = 2*pi*ra(8)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  q2 = matmul(euler_mat,q2)
  q6 = matmul(euler_mat,q6)
  q5 = matmul(euler_mat,q5)
  qsoft = matmul(euler_mat,qsoft)     !! global
  ksoft = make_mlm(qsoft)  !! global

  weight = m1**3*pme*e5/(e5+e34+c3k)/(2.**3*(2._prec*pi)**5)
!!  weight = xiout*weight   !! into matrix element

          ! generate remaining neutrino momenta

  qq34 = q1-q2-q5-q6

  weight = weight*minv34*m1/pi
  call pair_dec(ra(10:11),minv34,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv34**2,m3,m4)/minv34**2/pi

  q3 = boost_back(qq34, q3)
  q4 = boost_back(qq34, q4)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  q6 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD6_omega_m50_FKS



  SUBROUTINE PSD6_25_26_m50_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,weight)

    !! q6 (photon) massless,  q5 (photon) massless

  real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4,m5
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4),q6(4)
  integer :: enough_energy
  real (kind=prec) :: qq34(4), minv34
  real (kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
  real (kind=prec) :: xi6, y2, eme, pme, w, xi5, y5
  real (kind=prec) :: c5, s5, phi5, Omy5, Omy2, sq2, sq5

  if (m5>zero) call crash("PS_6S_FKS")
  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi6 = ra(1)
  xiout = xi6  !! global
  Omy2 = 2*ra(2)
  y2 = 1._prec - Omy2
  yout = y2    !! global
  w = 0.5*m1*xi6

  eme = m1*ra(3)
  if(eme-m2 < zero .or. m1-eme-w < zero) goto 999
  pme = sqrt(eme**2-m2**2)
  sq2 = sqrt(Omy2*(1._prec+y2))  !! sqrt(1-y2**2)
  q2 = (/ 0._prec, pme*sq2, pme*y2, eme /)
  q6 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  qsoft = 0.5*m1*q6  !! global, will be rotated below
  q6 = 0.5*m1*xi6*q6  !! = w*q6


  xi5 = ra(9)
  Omy5 = 2*ra(10)
  y5 = 1._prec - Omy5
  phi5 = 2*pi*ra(11)

  c5 = cos(phi5) ; s5 = sin(phi5)

  sq5 = sqrt(Omy5*(1._prec+y5))  !! sqrt(1-y5**2)
  q5 = (/ -c5*sq5, y5*sq2 + s5*y2*sq5, y2*y5 - s5*sq2*sq5, 1._prec /)
  q5 = 0.5*m1*xi5*q5

  if (eme-m2 < zero .or. m1-eme-q6(4)-q5(4) < zero) goto 999

          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(6)
  cos_be = 1 - 2*ra(4)
  ga = 2*pi*ra(5)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  q2 = matmul(euler_mat,q2)
  q6 = matmul(euler_mat,q6)
  q5 = matmul(euler_mat,q5)
  qsoft = matmul(euler_mat,qsoft)     !! global
  ksoft = make_mlm(qsoft)  !! global

  qq34 = q1-q2-q6-q5
  minv34 = sq(qq34)
  if(minv34 < 0) goto 999
  minv34 = sqrt(minv34)

  weight = m1**3*pme/(4.*(2._prec*pi)**4)
!!  weight = xi6*weight   !! into matrix element
  weight = weight * 1/(4*(2._prec*pi)**2) * m1**2 * xi5

          ! generate remaining neutrino momenta

  call pair_dec(ra(7:8),minv34,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv34**2,m3,m4)/minv34**2/pi

  q3 = boost_back(qq34, q3)
  q4 = boost_back(qq34, q4)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  q6 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD6_25_26_m50_FKS




  SUBROUTINE PSD6_FKSS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,q6,weight)

    !! q6 (photon) massless,  q5 (photon) massless, weight *does not* contain xi5*xi6

  real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4),q6(4)
  integer :: enough_energy
  real (kind=prec) :: qq34(4), minv34
  real (kind=prec) :: sa,sb,sc,ca,cb,cc,al,cos_be,ga, euler_mat(4,4)
  real (kind=prec) :: xi6, y2, eme, pme, w, xi5, y5
  real (kind=prec) :: c5, s5, phi5, Omy5, Omy2, sq2, sq5

  q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)

  xi5 = ra(1)
  xi6 = ra(2)

  xioutA = xi5  !! global
  xioutB = xi6  !! global

  Omy2 = 2*ra(3)
  y2 = 1._prec - Omy2
  yout = y2    !! global
  w = 0.5*m1*xi6 + 0.5*m1*xi5

  eme = m1*ra(4)
  if(eme-m2 < zero .or. m1-eme-w < zero)  goto 999
  pme = sqrt(eme**2-m2**2)
  sq2 = sqrt(Omy2*(1._prec+y2))  !! sqrt(1-y2**2)


  Omy5 = 2*ra(5)
  y5 = 1._prec - Omy5
  phi5 = 2*pi*ra(6)
  sq5 = sqrt(Omy5*(1._prec+y5))  !! sqrt(1-y5**2)
  c5 = cos(phi5) ; s5 = sin(phi5)

  q2 = (/ 0._prec, pme*sq2, pme*y2, eme /)
  q6 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)

  qsoftB = 0.5*m1*q6  !! global, will be rotated below
  q6 = 0.5*m1*xi6*q6


  q5 = (/ -c5*sq5, y5*sq2 + s5*y2*sq5, y2*y5 - s5*sq2*sq5, 1._prec /)
  qsoftA = 0.5*m1*q5
  q5 = 0.5*m1*xi5*q5


          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(7)
  cos_be = 1 - 2*ra(8)
  ga = 2*pi*ra(9)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
       (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
       (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
       (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
       (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  q2 = matmul(euler_mat,q2)
  q6 = matmul(euler_mat,q6)
  q5 = matmul(euler_mat,q5)

  qsoftA = matmul(euler_mat,qsoftA)     !! global
  ksoftA = make_mlm(qsoftA)  !! global
  qsoftB = matmul(euler_mat,qsoftB)     !! global
  ksoftB = make_mlm(qsoftB)  !! global


  qq34 = q1-q2-q6-q5
  minv34 = sq(qq34)
  if(minv34 < 0) goto 999
  minv34 = sqrt(minv34)

  weight = m1**3*pme/(4.*(2._prec*pi)**4)
!!  weight = xi5*xi6*weight   !! into matrix element
  weight = weight * 1/(4*(2._prec*pi)**2) * m1**2

          ! generate remaining neutrino momenta

  call pair_dec(ra(10:11),minv34,q3,m3,q4,m4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv34**2,m3,m4)/minv34**2/pi

  q3 = boost_back(qq34, q3)
  q4 = boost_back(qq34, q4)

  return
999 continue
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  q6 = 0.
  weight = 0.
  return
  END SUBROUTINE PSD6_FKSS









  SUBROUTINE PSX2(ra,p1,m1,p2,m2,q1,m3,q2,m4,weight)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2                 !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(2), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), q1(4), q2(4)
  integer :: enough_energy
  real (kind=prec) :: e1,e2

  e1 = (scms + m1**2 - m2**2) / 2 / sqrt(scms)
  e2 = sqrt(scms) - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)

  !TODO change factor 4 in mat_el of ee2nn
  weight = 1._prec
  call pair_dec(ra(1:2),sqrt(scms), q1, m3, q2, m4,enough_energy)

  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(scms,m3,m4)/scms/pi


  return
999 continue
  p1 = 0.
  p2 = 0.
  q1 = 0.
  q2 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX2


  SUBROUTINE PSX3_FKS(ra,p1,m1,p2,m2,q1,m3,q2,m4,q3,weight)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2                 !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(5), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), q1(4), q2(4), q3(4)
  integer :: enough_energy
  real (kind=prec) :: e1,e2, xi3, y3, qq12(4), minv12, al, ca,sa

  e1 = (scms + m1**2 - m2**2) / 2 / sqrt(scms)
  e2 = sqrt(scms) - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)

  xi3 = ra(1)
  xiout = xi3  !! global
  y3 = 2*ra(2) - 1._prec
  yout = y3    !! global

  al = 2*pi*ra(3)
  ca = cos(al); sa = sin(al)


  q3 = (/ -sqrt(1._prec - y3**2)*sa, sqrt(1._prec - y3**2)*ca, y3, 1._prec /)
  qsoft = 0.5*sqrt(scms)*q3  !! global, will be rotated below
  ksoft = make_mlm(qsoft)  !! global
  q3 = 0.5*sqrt(scms)*xi3*q3

  qq12 = p1+p2-q3
  minv12 = sq(qq12)
  if(minv12 < 0) goto 999
  minv12 = sqrt(minv12)


  weight = scms/(16*pi**2)
  call pair_dec(ra(4:5),minv12, q1, m3, q2, m4,enough_energy)

  q1 = boost_back(qq12, q1)
  q2 = boost_back(qq12, q2)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv12**2,m3,m4)/minv12**2/pi


  return
999 continue
  p1 = 0.
  p2 = 0.
  q1 = 0.
  q2 = 0.
  q3 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX3_FKS


  SUBROUTINE PSX4_FKSS(ra,p1,m1,p2,m2,q1,m3,q2,m4,q3,q4,weight)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2 + q3 + q4       !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!
  real (kind=prec), intent(in) :: ra(8), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), q1(4), q2(4), q3(4), q4(4)
  integer :: enough_energy
  real (kind=prec) :: xia, ya, xib, yb, ca, sa, cb, sb, al, be, e1, e2, minv12, qq12(4)

  e1 = (scms + m1**2 - m2**2) / 2 / sqrt(scms)
  e2 = sqrt(scms) - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)


  xia =  ra(1)      ; xib =  ra(2)
  ya = 2*ra(3) - 1. ; yb = 2*ra(4) - 1.

  al = 2*pi*ra(5)   ; be = 2*pi*ra(6)

  ca = cos(al); sa = sin(al)
  cb = cos(be); sb = sin(be)

  q3 = (/ -sqrt(1._prec - ya**2)*sa, sqrt(1._prec - ya**2)*ca, ya, 1._prec /)
  q4 = (/ -sqrt(1._prec - yb**2)*sb, sqrt(1._prec - yb**2)*cb, yb, 1._prec /)

  qsoftA = 0.5*sqrt(scms)*q3  !! global, will be rotated below
  ksoftA = make_mlm(qsoftA)  !! global
  q3 = 0.5*sqrt(scms)*xiA*q3
  xioutA = xiA
  youtA = ya

  qsoftB = 0.5*sqrt(scms)*q4  !! global, will be rotated below
  ksoftB = make_mlm(qsoftB)  !! global
  q4 = 0.5*sqrt(scms)*xiB*q4
  xioutB = xiB
  youtB = yb

  qq12 = p1+p2-q3-q4
  minv12 = sq(qq12)
  if(minv12 < 0) goto 999
  minv12 = sqrt(minv12)

  weight = scms**2/(16*pi**2)**2
  !weight = xia*xib*weight ! to integrands
  call pair_dec(ra(7:8),minv12, q1, m3, q2, m4,enough_energy)
  q1 = boost_back(qq12, q1)
  q2 = boost_back(qq12, q2)

  if(enough_energy == 0) goto 999


  weight = weight*0.125*sq_lambda(minv12**2,m3,m4)/minv12**2/pi


  return
999 continue
  p1 = 0.
  p2 = 0.
  q1 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX4_FKSS



  SUBROUTINE PSX4(ra,p1,mi1,p2,mi2,q2,m2,q3,m3,q4,m4,q5,m5,weight)


  real (kind=prec), intent(in) :: ra(8), m2, m3, m4, m5, mi1, mi2
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), q2(4), q3(4), q4(4), q5(4)
  integer :: enough_energy
  real (kind=prec) :: e2
  real (kind=prec) :: qq3(4), minv3, qq4(4), minv4, e1, m1

  m1 = sqrt(scms)
  e1 = (scms + mi1**2 - mi2**2) / 2 / sqrt(scms)
  e2 = sqrt(scms) - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-mi1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-mi2**2) ,e2/)


  minv3 = ra(1)*m1
  weight = minv3*m1/pi
  call pair_dec(ra(2:3),m1,q2,m2,qq3,minv3,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(m1**2,m2,minv3)/m1**2/pi

  minv4 = ra(4)*m1
  weight = weight*minv4*m1/pi
  call pair_dec(ra(5:6),minv3,q3,m3,qq4,minv4,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv3**2,m3,minv4)/minv3**2/pi

  call pair_dec(ra(7:8),minv4,q4,m4,q5,m5,enough_energy)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv4**2,m4,m5)/minv4**2/pi

  q4 = boost_back(qq4, q4)
  q5 = boost_back(qq4, q5)

  q3 = boost_back(qq3, q3)
  q4 = boost_back(qq3, q4)
  q5 = boost_back(qq3, q5)

  return
999 continue
  p1 = 0.
  p2 = 0.
  q2 = 0.
  q3 = 0.
  q4 = 0.
  q5 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX4





  SUBROUTINE PSX3_35_FKS(ra,p1,m1,p2,m2,p3,p4,p5,weight, sol)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2                 !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(5), m1, m2
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4)
  integer :: ssol
  real (kind=prec) :: e1,e2, xi, y
  real (kind=prec) :: v3, e3,sqrts, euler_mat(4,4)
  real (kind=prec) :: al, cos_be, ga, ca, sa, cb, sb, cc, sc
  real (kind=prec) :: sqroot, xi1, ximax, den
  integer, optional :: sol


  weight=1.
  ssol = 1
  if (present(sol)) ssol=sol

  sqrts=sqrt(scms)
  xi1 = (m1**2-m2**2-2*m1*sqrts+scms)/(scms-m1*sqrts)
  ximax = 1-(m1+m2)**2/scms

  if (ssol == 2) then
    xi = xi1 + ra(1)*(-xi1 + ximax)
    weight = (ximax-xi1)
  else
    xi = ra(1)
  endif

  xiout = xi  !! global
  y = 2*ra(2) - 1._prec
  yout = y    !! global

  if (xi.gt.ximax) goto 999
  if (y.gt.0 .and. xi.gt.xi1) goto 999

  sqroot = m1**4 + (m2**2  + scms*(-1 + xi))**2 &
         + m1**2*(-2*m2**2 + scms*(-2 + 2*xi + xi**2*(-1 + y**2)))

  if (sqroot.lt.0) goto 999

  den = sqrts * (-4+xi*(4 + (y**2-1)*xi))

  e3 = (xi-2)*(m1**2 - m2**2 + scms - scms*xi)/den

  if (ssol == 1) then
    e3 = e3 + sqrt(sqroot) * y * xi / den
  else
    if (  (y.gt.0) .or. (xi.lt.xi1)  ) goto 999
    e3 = e3 - sqrt(sqroot) * y * xi / den
  endif


  e1 = (scms + m1**2 - m2**2) / 2 / sqrts
  e2 = sqrts - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)


  v3 = sqrt(1-m1**2/e3**2)
  p3 = (/ 0._prec, v3*sqrt(1.-y**2),v3*y,1._prec /) * e3

  p5 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  qsoft = 0.5*sqrts*p5  !! global, will be rotated below
  p5 = 0.5*sqrts*xi*p5  !! = w*q5


          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(3)
  cos_be = 1 - 2*ra(4)
  ga = 2*pi*ra(5)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
     (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
     (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
     (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
     (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  p3 = matmul(euler_mat,p3)
  p5 = matmul(euler_mat,p5)

  p4 = p1+p2-p3-p5

  qsoft = matmul(euler_mat,qsoft)     !! global
  ksoft = make_mlm(qsoft)  !! global

  weight = weight*sqrts*v3*e3/(32*pi**3)/abs(xi-2-y*xi/v3)

  return

999 continue
  p1 = 0.
  p2 = 0.
  p3 = 0.
  p4 = 0.
  p5 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX3_35_FKS


  SUBROUTINE PSX4_35_36_FKSS(ra,p1,m1,p2,m2,p3,p4,p5,p6,weight, sol)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2                 !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(8), m1, m2
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4), p6(4)
  integer :: ssol
  real (kind=prec) :: e1,e2, xi5,xi6, y5,y6
  real (kind=prec) :: v3, e3,sqrts, euler_mat(4,4), p5soft(4), p6soft(4)
  real (kind=prec) :: al, cos_be, ga, ca, sa, cb, sb, cc, sc, phi56, c56, s56, s5, s6
  real (kind=prec) :: sqroot, ximax, den, m1sq, m2sq, jacobian_delta, e3cutcond, e3cut
  integer, optional :: sol


  weight=1.
  ssol = 1
  if (present(sol)) ssol=sol

  sqrts = sqrt(scms)
  ximax = 1-(m1+m2)**2/scms
  m1sq  = m1**2
  m2sq  = m2**2

  !Generate initial states
  e1 = (scms + m1**2 - m2**2) / 2 / sqrts
  e2 = sqrts - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)

  !Generate the two photons (w/o energy for now)
  xi5 =  ra(1)      ; xi6 =  ra(2)
  y5 = 2*ra(3) - 1. ; y6 = 2*ra(4) - 1.
  s5 =  sqrt(1._prec - y5**2); s6 = sqrt(1._prec - y6**2)
  phi56 = 2*pi*ra(5)
  c56 = cos(phi56); s56 = sin(phi56)

  xioutA = xi5
  youtA = y5
  xioutB = xi6
  youtB = y6

  p5soft = sqrts/2*(/ 0._prec, s5, y5, 1._prec /)
  p6soft = sqrts/2*(/ s6*s56, s6*c56, y6, 1._prec /)

  !Determine electron energy
  e3 = -2*(m1sq - m2sq)*(-2 + xi5 + xi6) + scms*(-2 + xi5 + xi6)&
       *(-2 + 2*xi5 + 2*xi6 - xi5*xi6 + c56*s5*s6*xi5*xi6 + xi5*xi6*y5*y6)

  sqroot = 4*m1sq**2 + (-2*m2sq + scms*(2 + xi5*(-2 + xi6) - 2*xi6))**2&
           - 4*m1sq*(2*m2sq + scms*(2 + xi5**2 + xi5*(-2 + xi6)&
           + (-2 + xi6)*xi6)) + 2*scms*xi5*xi6*(2*(m1sq + m2sq&
           + scms*(-1 + xi5)) - scms*(-2 + xi5)*xi6&
           + c56*s5*s6*scms*xi5*xi6)*y5*y6&
           + scms*xi6*(c56*s5*s6*xi5*(-4*m1sq + 4*(m2sq + scms*(-1 + xi5))&
           - 2*scms*(-2 + xi5)*xi6 + c56*s5*s6*scms*xi5*xi6)&
           + 4*m1sq*xi6*y6**2) + scms*xi5**2*y5**2*(4*m1sq + scms*xi6**2*y6**2)

  den = 2*sqrts*((2 - xi5 - xi6)**2 - (xi5*y5 + xi6*y6)**2)

  if(sqroot<0) goto 999

  if (ssol == 1) then
    e3 = (e3 - sqrt(sqroot)*(y5*xi5+y6*xi6))/den
  else
    e3 = (e3 + sqrt(sqroot)*(y5*xi5+y6*xi6))/den
  endif

  !!Check whether delta func solution
  if((xi5.gt.zero) .or. (xi6.gt.zero)) then
    e3cut = (2*m1sq - 2*m2sq + scms*(2 + xi5*(-2 + xi6) - 2*xi6)&
           - scms*xi5*xi6*(c56*s5*s6 + y5*y6))/2/sqrts/(2 - xi5 - xi6)
    e3cutcond = xi5*y5+xi6*y6
    if(e3cutcond.gt.0 .and. e3.gt.e3cut) goto 999
    if(e3cutcond.lt.0 .and. e3.lt.e3cut) goto 999
  elseif (ssol == 2) then
    goto 999
  endif

  if(e3<m1) goto 999

  !Generate p3
  v3 = sqrt(1-m1**2/e3**2)
  p3 = e3*(/ 0._prec, 0._prec, v3, 1._prec /)

  !Generate euler angles and rotate all momenta
  al = 2*pi*ra(6)
  cos_be = 1 - 2*ra(7)
  ga = 2*pi*ra(8)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
     (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
     (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
     (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
     (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  p3 = matmul(euler_mat,p3)
  p5soft = matmul(euler_mat,p5soft)
  p6soft = matmul(euler_mat,p6soft)

  p5 = xi5*p5soft
  p6 = xi6*p6soft

  !Save global stuff
  qsoftA = p5soft
  ksoftA = make_mlm(qsoftA)

  qsoftB = p6soft
  ksoftB = make_mlm(qsoftB)

  !Generate p4 with momentum conservation
  p4 = p1+p2-p3-p5-p6
  if(p4(4)<m2) goto 999


  jacobian_delta = (sqrts*((e3**2 - m1sq)*(-2 + xi5 + xi6)&
                    - e3**2*v3*(xi5*y5 + xi6*y6)))/((e3**2 - m1sq))
  weight = weight*scms**2*v3*e3/16/(2*pi)**5/abs(jacobian_delta)

  return

999 continue
  p1 = 0.
  p2 = 0.
  p3 = 0.
  p4 = 0.
  p5 = 0.
  p6 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX4_35_36_FKSS


  SUBROUTINE GEN_MOM(gen, x, masses, vecs, weight)
  real(kind=prec), intent(in)  :: masses(:), x(:)
  real(kind=prec), intent(out) :: vecs(:,:), weight
  procedure(), pointer :: gen => null()

  vecs = 0.
  select case(size(masses))
    case(2)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), weight)
    case(3)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), weight)
    case(4)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), weight)
    case(5)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), masses(5), weight)
    case(6)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), masses(5), vecs(:,6), masses(6), weight)
    case(7)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), masses(5), vecs(:,6), masses(6), vecs(:,7), masses(7), weight)
  end select

  END SUBROUTINE
  SUBROUTINE GEN_MOM_FKS(gen, x, masses, vecs, weight)
  real(kind=prec), intent(in)  :: masses(:), x(:)
  real(kind=prec), intent(out) :: vecs(:,:), weight
  procedure(), pointer :: gen => null()

  vecs = 0.
  select case(size(masses))
    case(3)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), weight)
    case(4)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), weight)
    case(5)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), weight)
    case(6)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), masses(5), vecs(:,6), weight)
    case(7)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), masses(5), vecs(:,6), masses(6), vecs(:,7), weight)
  end select

  END SUBROUTINE
  SUBROUTINE GEN_MOM_FKSS(gen, x, masses, vecs, weight)
  real(kind=prec), intent(in)  :: masses(:), x(:)
  real(kind=prec), intent(out) :: vecs(:,:), weight
  procedure(), pointer :: gen => null()

  vecs = 0.
  select case(size(masses))
    case(4)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), vecs(:,4), weight)
    case(5)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), vecs(:,5), weight)
    case(6)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), vecs(:,6), weight)
    case(7)
      call gen(x, vecs(:,1), masses(1), vecs(:,2), masses(2), vecs(:,3), masses(3), vecs(:,4), masses(4), &
              vecs(:,5), masses(5), vecs(:,6), vecs(:,7), weight)
  end select

  END SUBROUTINE




  SUBROUTINE PSD6_P_25_26_m50_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,weight)
  real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4,m5
  real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4), q4(4), q5(4),q6(4)

  call psd6_25_26_m50_fks(ra, q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,weight)

  if (s(q2,q6) > s(q2,q5)) weight = 0.

  END SUBROUTINE

  SUBROUTINE PSD6_26_2x5(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,m6,weight)
  real(kind=prec), intent(in) :: ra(11), m1,m2,m3,m4,m5,m6
  real(kind=prec), intent(out) :: weight
  real(kind=prec), intent(out), dimension(4) :: q1,q2,q3,q4,q5,q6
  call psd6_23_24_34(ra,q1,m1,q5,m5,q3,m3,q4,m4,q2,m2,q6,m6,weight)
  END SUBROUTINE

  SUBROUTINE PSD7_27_37_47_2x5_FKS(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,m6,q7,weight)
  real(kind=prec), intent(in) :: ra(14), m1,m2,m3,m4,m5,m6
  real(kind=prec), intent(out) :: weight
  real(kind=prec), intent(out), dimension(4) :: q1,q2,q3,q4,q5,q6,q7
  call psd7_27_37_47_fks(ra,q1,m1,q5,m5,q2,m2,q6,m6,q3,m3,q4,m4,q7,weight)
  END SUBROUTINE

  SUBROUTINE PSX3_P_15_FKS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,weight)
  real (kind=prec), intent(in) :: ra(5), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4)

  if (abs(m3-m1) > zero) call crash("PSX3_P_15_FKS")
  if (abs(m4-m2) > zero) call crash("PSX3_P_15_FKS")
  call psx3_fks(ra,p1,m1,p2,m2,p3,m1,p4,m2,p5,weight)
  if (s(p1,qsoft).gt.s(p3,qsoft)) weight = 0.

  END SUBROUTINE


  SUBROUTINE PSX3_P13_35_FKS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,weight)
  real (kind=prec), intent(in) :: ra(5), m1, m2,m3,m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4)

  if (abs(m3-m1) > zero) call crash("PSX3_P13_35_FKS")
  if (abs(m4-m2) > zero) call crash("PSX3_P13_35_FKS")
  call psx3_35_fks(ra,p1,m1,p2,m2,p3,p4,p5,weight,1)
  if (s(p1,qsoft).lt.s(p3,qsoft)) weight = 0.

  END SUBROUTINE


  SUBROUTINE PSX3_coP13_35_FKS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,weight)
  real (kind=prec), intent(in) :: ra(5), m1, m2,m3,m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4)

  if (abs(m3-m1) > zero) call crash("PSX3_coP13_35_FKS")
  if (abs(m4-m2) > zero) call crash("PSX3_coP13_35_FKS")
  call psx3_35_fks(ra,p1,m1,p2,m2,p3,p4,p5,weight,2)
  if (s(p1,qsoft).lt.s(p3,qsoft)) weight = 0.

  END SUBROUTINE




  SUBROUTINE PSX4_P_15_16_FKSS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,p6,weight)
  real (kind=prec), intent(in) :: ra(8), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4), p6(4)

  if (abs(m3-m1) > zero) call crash("PSX4_P_15_16_FKSS")
  if (abs(m4-m2) > zero) call crash("PSX4_P_15_16_FKSS")
  call psx4_fkss(ra, p1, m1, p2, m2, p3, m1, p4, m2, p5,p6, weight)
  if (min(s(p1,qsoftA),s(p1,qsoftB)).gt.min(s(p3,qsoftA),s(p3,qsoftB))) weight = 0.

  END SUBROUTINE

  SUBROUTINE PSX4_P_35_36_FKSS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,p6,weight)
  real (kind=prec), intent(in) :: ra(8), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4), p6(4)

  if (abs(m3-m1) > zero) call crash("PSX4_P_35_36_FKSS")
  if (abs(m4-m2) > zero) call crash("PSX4_P_35_36_FKSS")
  call psx4_35_36_fkss(ra, p1, m1, p2, m2, p3, p4,p5,p6, weight,1)
  if (min(s(p1,qsoftA),s(p1,qsoftB)).lt.min(s(p3,qsoftA),s(p3,qsoftB))) weight = 0.

  END SUBROUTINE

  SUBROUTINE PSX4_coP_35_36_FKSS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,p6,weight)
  real (kind=prec), intent(in) :: ra(8), m1, m2,m3,m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4), p6(4)

  if (abs(m3-m1) > zero) call crash("PSX4_coP_35_36_FKSS")
  if (abs(m4-m2) > zero) call crash("PSX4_coP_35_36_FKSS")
  call psx4_35_36_fkss(ra, p1, m1, p2, m2, p3, p4,p5,p6, weight,2)
  if (min(s(p1,qsoftA),s(p1,qsoftB)).lt.min(s(p3,qsoftA),s(p3,qsoftB))) weight = 0.

  END SUBROUTINE



                  !!!  ----------------------  !!!
                       END MODULE PHASE_SPACE
                  !!!  ----------------------  !!!





