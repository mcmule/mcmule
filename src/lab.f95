  PROGRAM LAB

#ifdef COLLIER
  use collier
#endif
  use integrands
  implicit none
  character(len=200) :: arg
  character(len=2000) :: imsg
  character(len=30) :: msg
  integer, parameter :: u = 18
  real(kind=prec) :: x(17),ans
  integer ndim, ios
  procedure(integrand), pointer :: fxn

  runninglab = .true.

#ifdef COLLIER
  call init_cll(5,folder_name="")
#endif
  call ltini
  call get_command_argument(1, arg)
  if (len_trim(arg) == 0) then
    print*,"Usage ./lab filename"
    stop
  endif
  open(u, file=trim(arg), form='unformatted')

  do
    read(u, iostat=ios, iomsg=imsg) msg, which_piece, flavour, xinormcut, delcut, ndim, x
    if (ios > 0) then
      print*,"Read failed. reason = ",ios,imsg
      print*,x
      exit
    elseif (ios < 0) then
      exit
    endif
    xinormcut1 = xinormcut
    xinormcut2 = delcut

    call initflavour
    call initpiece(ndim, fxn)

    ans = fxn(x(1:ndim), 0.1_prec, ndim)
    print*,ans

  enddo

  close(u)



  END PROGRAM LAB
