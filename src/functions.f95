
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE FUNCTIONS
                          !!!!!!!!!!!!!!!!!!!!!!

  USE GLOBAL_DEF
  USE collier


  IMPLICIT NONE

  TYPE MLM   ! MassLess Momentum
    real(kind=prec), dimension(4) :: momentum
  END TYPE MLM

  INTERFACE S
    module procedure Sversion1, Sversion2, Sversion3, Sversion4
  END INTERFACE

  INTERFACE SQ
    module procedure SQversion1
  END INTERFACE

  INTERFACE EIK
    module procedure eik00, eik0M, eikM0, eikMM
  END INTERFACE

  INTERFACE IREG
    module procedure ireg_00, ireg_0M, ireg_M0, ireg_MM, ireg_SELF
  END INTERFACE
  INTERFACE ISIN
    module procedure isin_00, isin_0m, isin_m0, isin_MM, isin_SELF
  END INTERFACE
  INTERFACE ILIN
    module procedure ilin_MM, ilin_SELF
  END INTERFACE

  INTERFACE DISCB
    module procedure discbSMN, discbSMM
  END INTERFACE
  !Untested
  !INTERFACE DISCB_CPLX
  !  module procedure discbSMN_CPLX, discbSMM_CPLX
  !END INTERFACE
  INTERFACE SCALARC0IR6
    module procedure scalarc0ir6SMM, scalarc0ir6SMN
  END INTERFACE
  !Untested
  !INTERFACE SCALARC0IR6_CPLX
  !  module procedure scalarc0ir6SMM_CPLX, scalarc0ir6SMN_CPLX
  !END INTERFACE
  INTERFACE SCALARC0
    module procedure scalarc0SM
  END INTERFACE



  CONTAINS


  SUBROUTINE SWAPMOM(vecs, i, j)
  real(kind=prec) :: vecs(:,:)
  real(kind=prec) :: dummy(size(vecs(:,1)))
  integer :: i, j

  dummy = vecs(:,i)
  vecs(:,i) = vecs(:,j)
  vecs(:,j) = dummy

  END SUBROUTINE SWAPMOM


  FUNCTION MAKE_MLM(qq)
  real(kind=prec), intent(in) :: qq(4)
  type(mlm) :: make_mlm

  make_mlm%momentum = qq

  END FUNCTION MAKE_MLM



  FUNCTION Sversion1(k1,k2)
     ! S(k1,k2) = 2 k1.k2

  type(mlm), intent(in) :: k1,k2
  real (kind=prec) :: Sversion1,dot_dot,q1(4),q2(4)
  q1 = k1%momentum
  q2 = k2%momentum

  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion1 =  2*dot_dot

  END FUNCTION Sversion1


  FUNCTION Sversion2(q1,q2)
     ! S(q1,q2) =  2 q1.q2

  real (kind=prec), intent(in) :: q1(4),q2(4)
  real (kind=prec) :: Sversion2,dot_dot
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion2 =  2*dot_dot

  END FUNCTION Sversion2



  FUNCTION Sversion3(k1,q2)
     ! S(q1,q2) =  2 q1.q2

  type(mlm), intent(in) :: k1
  real (kind=prec), intent(in) :: q2(4)
  real (kind=prec) :: q1(4)
  real (kind=prec) :: Sversion3,dot_dot
  q1 = k1%momentum
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion3 =  2*dot_dot

  END FUNCTION Sversion3



  FUNCTION Sversion4(q1,k2)
     ! S(q1,q2) = 2 q1.q2

  type(mlm), intent(in) :: k2
  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: q2(4)
  real (kind=prec) :: Sversion4,dot_dot
  q2 = k2%momentum
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion4 =  2*dot_dot

  END FUNCTION Sversion4



  FUNCTION SQversion1(q1)
     ! SQ(q1) = (q1)^2

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: SQversion1

  SQversion1  = q1(4)**2 - q1(1)**2 - q1(2)**2 - q1(3)**2

  END FUNCTION SQversion1


!==============  eikonal factors  ==============!


  FUNCTION EIK00(k1,kg,k2)

  type(mlm), intent(in) :: k1,k2,kg
  real (kind=prec) :: eik00

  eik00 = 2*s(k1,k2)/(s(k1,kg)*s(kg,k2))

  END FUNCTION EIK00


  FUNCTION EIK0M(k1,kg,q2)

  type(mlm), intent(in) :: k1,kg
  real (kind=prec), intent(in) :: q2(4)
  real (kind=prec) :: eik0M

  eik0m = 2*s(k1,q2)/(s(k1,kg)*s(kg,q2)) - 2*sq(q2)/(s(kg,q2)**2)

  END FUNCTION EIK0M


  FUNCTION EIKM0(q1,kg,k2)

  type(mlm), intent(in) :: k2,kg
  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: eikM0

  eikm0 = 2*s(k2,q1)/(s(k2,kg)*s(kg,q1)) - 2*sq(q1)/(s(kg,q1)**2)

  END FUNCTION EIKM0


  FUNCTION EIKMM(q1,kg,q2)

  type(mlm), intent(in) :: kg
  real (kind=prec), intent(in) :: q1(4), q2(4)
  real (kind=prec) :: eikMM

  eikmm = 2*s(q1,q2)/(s(q1,kg)*s(kg,q2))     &
       - 2*sq(q1)/(s(kg,q1)**2) - 2*sq(q2)/(s(kg,q2)**2)

  END FUNCTION EIKMM



!===========  end of eikonal factors and AP splitting kernels ===========!



  recursive FUNCTION LI2(x, img) result(di)

  !Optional argument img needed for x > 1.0

   real (kind=prec):: X,Y,T,S,A,PI3,PI6,ZERO,ONE,HALF,MALF,MONE,MTWO
   real (kind=prec):: C(0:18),H,ALFA,B0,B1,B2,LI2_OLD
   real (kind=prec):: di
   real (kind=prec), optional :: img
   integer :: i

   DATA ZERO /0.0_prec/, ONE /1.0_prec/
   DATA HALF /0.5_prec/, MALF /-0.5_prec/
   DATA MONE /-1.0_prec/, MTWO /-2.0_prec/
   DATA PI3 /3.289868133696453_prec/, PI6 /1.644934066848226_prec/

   DATA C( 0) / 0.4299669356081370_prec/
   DATA C( 1) / 0.4097598753307711_prec/
   DATA C( 2) /-0.0185884366501460_prec/
   DATA C( 3) / 0.0014575108406227_prec/
   DATA C( 4) /-0.0001430418444234_prec/
   DATA C( 5) / 0.0000158841554188_prec/
   DATA C( 6) /-0.0000019078495939_prec/
   DATA C( 7) / 0.0000002419518085_prec/
   DATA C( 8) /-0.0000000319334127_prec/
   DATA C( 9) / 0.0000000043454506_prec/
   DATA C(10) /-0.0000000006057848_prec/
   DATA C(11) / 0.0000000000861210_prec/
   DATA C(12) /-0.0000000000124433_prec/
   DATA C(13) / 0.0000000000018226_prec/
   DATA C(14) /-0.0000000000002701_prec/
   DATA C(15) / 0.0000000000000404_prec/
   DATA C(16) /-0.0000000000000061_prec/
   DATA C(17) / 0.0000000000000009_prec/
   DATA C(18) /-0.0000000000000001_prec/

   if(x > 1.00000000001_prec) then
      if (present(img)) then
      di = PI6 - log(x)*log(x-1) - LI2(1-x)
      img = -log(x)*pi
      return
      else
     call crash("LI2")
     endif
   elseif(x > 1.0_prec) then
     x = 1._prec
   endif

   ! This works well
   !IF(X > 0.99999999999_prec) THEN
   ! this keeps the error for x->1 well below 10^-13
   IF(1-X < 1e-14) THEN
    LI2_OLD=PI6
    di = Real(LI2_OLD,prec)
    RETURN
   ELSE IF(X .EQ. MONE) THEN
    LI2_OLD=MALF*PI6
    di = Real(LI2_OLD,prec)
    RETURN
   END IF
   T=-X
   IF(T .LE. MTWO) THEN
    Y=MONE/(ONE+T)
    S=ONE
    A=-PI3+HALF*(LOG(-T)**2-LOG(ONE+ONE/T)**2)
   ELSE IF(T .LT. MONE) THEN
    Y=MONE-T
    S=MONE
    A=LOG(-T)
    A=-PI6+A*(A+LOG(ONE+ONE/T))
   ELSE IF(T .LE. MALF) THEN
    Y=(MONE-T)/T
    S=ONE
    A=LOG(-T)
    A=-PI6+A*(MALF*A+LOG(ONE+T))
   ELSE IF(T .LT. ZERO) THEN
    Y=-T/(ONE+T)
    S=MONE
    A=HALF*LOG(ONE+T)**2
   ELSE IF(T .LE. ONE) THEN
    Y=T
    S=ONE
    A=ZERO
   ELSE
    Y=ONE/T
    S=MONE
    A=PI6+HALF*LOG(T)**2
   END IF

   H=Y+Y-ONE
   ALFA=H+H
   B1=ZERO
   B2=ZERO
   DO  I = 18,0,-1
     B0=C(I)+ALFA*B1-B2
     B2=B1
     B1=B0
   ENDDO
   LI2_OLD=-(S*(B0-H*B2)+A)
         ! Artificial conversion
   di = Real(LI2_OLD,prec)

  END FUNCTION LI2

  FUNCTION LI3(x)
   ! This was hacked from LI2 to also follow C332
   !! Trilogarithm for arguments x < = 1.0
   ! In theory this could also produce Re[Li [x]] for x>1
   !                                        3

   real (kind=prec):: X,S,A
   real (kind=prec):: CA(0:18),HA,ALFAA,BA0,BA1,BA2, YA
   real (kind=prec):: CB(0:18),HB,ALFAB,BB0,BB1,BB2, YB
   DATA CA(0) / 0.4617293928601208/
   DATA CA(1) / 0.4501739958855029/
   DATA CA(2) / -0.010912841952292843/
   DATA CA(3) / 0.0005932454712725702/
   DATA CA(4) / -0.00004479593219266303/
   DATA CA(5) / 4.051545785869334e-6/
   DATA CA(6) / -4.1095398602619446e-7/
   DATA CA(7) / 4.513178777974119e-8/
   DATA CA(8) / -5.254661564861129e-9/
   DATA CA(9) / 6.398255691618666e-10/
   DATA CA(10) / -8.071938105510391e-11/
   DATA CA(11) / 1.0480864927082917e-11/
   DATA CA(12) / -1.3936328400075057e-12/
   DATA CA(13) / 1.8919788723690422e-13/
   DATA CA(14) / -2.6097139622039465e-14/
   DATA CA(15) / 3.774985548158685e-15/
   DATA CA(16) / -5.671361978114946e-16/
   DATA CA(17) / 1.1023848202712794e-16/
   DATA CA(18) / -5.0940525990875006e-17/
   DATA CB(0) / -0.016016180449195803/
   DATA CB(1) / -0.5036424400753012/
   DATA CB(2) / -0.016150992430500253/
   DATA CB(3) / -0.0012440242104245127/
   DATA CB(4) / -0.00013757218124463538/
   DATA CB(5) / -0.000018563818526041144/
   DATA CB(6) / -2.841735345177361e-6/
   DATA CB(7) / -4.7459967908588557e-7/
   DATA CB(8) / -8.448038544563037e-8/
   DATA CB(9) / -1.5787671270014e-8/
   DATA CB(10) / -3.0657620579122164e-9/
   DATA CB(11) / -6.140791949281482e-10/
   DATA CB(12) / -1.2618831590198e-10/
   DATA CB(13) / -2.64931268635803e-11/
   DATA CB(14) / -5.664711482422879e-12/
   DATA CB(15) / -1.2303909436235178e-12/
   DATA CB(16) / -2.7089360852246495e-13/
   DATA CB(17) / -6.024075373994343e-14/
   DATA CB(18) / -1.2894320641440237e-14/
   real (kind=prec):: Li3
   real (kind=prec), parameter :: zeta2 = 1.6449340668482264365
   real (kind=prec), parameter :: zeta3 = 1.2020569031595942854
   integer :: i


   if(x > 1.00000000001_prec) then
     call crash("LI3")
   elseif(x > 1.0_prec) then
     x = 1._prec
   endif

   IF(X > 0.999999_prec) THEN
    LI3=zeta3
    RETURN
   ELSE IF(X .EQ. -1._prec) THEN
    LI3=-0.75_prec*zeta3
    RETURN
   END IF
   IF(X .LE. -1._prec) THEN
    YA=1._prec/x ; YB=0._prec
    S=-1._prec
    A=-LOG(-X)*(zeta2+LOG(-x)**2/6._prec)
   ELSE IF(X .LE. 0._prec) THEN
    YA=x ; YB=0._prec
    S=-1._prec
    A=0._prec
   ELSE IF(X .LE. 0.5_prec) THEN
    YA=0._prec ; YB=x
    S=-1._prec
    A=0._prec
   ELSE IF(X .LE. 1._prec) THEN
    YA=(x-1._prec)/x ; YB=1._prec-x
    S=1._prec
    A=zeta3 + zeta2*Log(x) - (Log(1._prec - X)*Log(X)**2)/2._prec + Log(X)**3/6._prec
   ELSE IF(X .LE. 2._prec) THEN
    YA=1._prec - X ; YB=(X-1._prec)/X
    S=1._prec
    A=zeta3 + zeta2*Log(x) - (Log(X - 1._prec)*Log(X)**2)/2._prec + Log(X)**3/6._prec
   ELSE
    YA=0._prec ; YB=1._prec/X
    S=-1._prec
    A=2*zeta2*Log(x)-Log(x)**3/6._prec
   END IF


   HA=-2._prec*YA-1._prec ; HB= 2._prec*YB
   ALFAA=HA+HA ; ALFAB = HB+HB

   BA0 = 0. ; BA1=0. ; BA2=0.
   BB0 = 0. ; BB1=0. ; BB2=0.
   DO  I = 18,0,-1
     BA0=CA(I)+ALFAA*BA1-BA2 ; BA2=BA1 ; BA1=BA0
     BB0=CB(I)+ALFAB*BB1-BB2 ; BB2=BB1 ; BB1=BB0
   ENDDO
   Li3 = A + S * (  (BA0 - HA*BA2) + (BB0 - HB*BB2) )

  END FUNCTION LI3




  FUNCTION LN(arg1, arg2)

    !! log(sij/mm) for positive and negative sij

  complex (kind=prec) :: Ln
  real(kind=prec), intent(in) :: arg1, arg2
  real(kind=prec) :: abs_part

  abs_part = 1._prec

  if(arg1 > 0._prec) then
    ln = log(arg1/arg2)
  elseif(arg1 < 0._prec ) then
    ln = log(-arg1/arg2) - imag*abs_part*pi
  else
    call crash("LNc1")
  endif

  if(arg2 < 0._prec) call crash("LN")

  END FUNCTION LN



  FUNCTION DILOG(arg1,arg2)

    !! dilogarithm of 1 - arg1/arg2 for pos and neg arg2

  complex (kind=prec) :: Dilog
  real(kind=prec), intent(in) :: arg1,arg2
  real(kind=prec) :: abs_part

  abs_part = 1._prec

  if(arg1/arg2 > 0._prec) then
    dilog = li2(1._prec - arg1/arg2)
  elseif(arg1 > 0._prec .and. arg2 < 0._prec) then
    dilog = pi**2/6 - li2(arg1/arg2)                             &
        + (log(-arg2/arg1) - imag*abs_part*pi)*log(1._prec - arg1/arg2)
  else
   print*, "arg1, arg2: ",arg1,arg2
   call crash("DILOG")
  endif

  END FUNCTION DILOG


  FUNCTION CLENSHAW(a_cheb, xmin, xmax, x, n)
  real (kind=prec) :: clenshaw
  integer :: n
  real (kind=prec), intent(in) :: a_cheb(n), xmin, xmax, x
  real (kind=prec) :: d(n), xnorm
  integer :: j

  xnorm = (-2*x+xmin+xmax)/(xmin-xmax)
  d = a_cheb
  do j = n, 3, -1
    d(j-1) = d(j-1) + 2*xnorm*d(j)
    d(j-2) = d(j-2) - d(j)
  end do
  clenshaw = d(1)+xnorm*d(2)

  END FUNCTION CLENSHAW




!======================    soft integrals    ======================!


  FUNCTION ISIN_00(xicut,epcmf,k1,k2) ! both massless
  real (kind=prec), intent(in) :: epcmf,xicut
  type(mlm), intent(in) :: k1,k2
  real (kind=prec) isin_00, q1(4), q2(4)
  q1 = k1%momentum
  q2 = k2%momentum

  isin_00 = -(log(s(k1,k2)/musq)-log(4*q1(4)*q2(4)/xicut**2/epcmf**2))
  END FUNCTION ISIN_00

  FUNCTION ISIN_M0(xicut,epcmf,q1,k2)  ! first massive second massless
  real (kind=prec), intent(in) :: epcmf,xicut,q1(4)
  type(mlm), intent(in) :: k2
  real (kind=prec) isin_m0,m12,q2(4)
  m12 = sq(q1)
  q2 = k2%momentum
  isin_m0 = -(log(s(q1,k2)/musq)-0.5*log(4.*m12**2*q2(4)**2/xicut**2/epcmf**2/musq))
  END FUNCTION ISIN_M0
  FUNCTION ISIN_0M(xicut,epcmf,k1,q2)
           ! first massless second massive, "same" as M0
  real (kind=prec), intent(in) :: epcmf,xicut,q2(4)
  type(mlm), intent(in) :: k1
  real (kind=prec) isin_0m,m12,q1(4)
  m12 = sq(q1)
  q1 = k1%momentum
  isin_0m = -(log(s(q2,k1)/musq)-0.5*log(4.*m12**2*q1(4)**2/xicut**2/epcmf**2/musq))
  END FUNCTION ISIN_0M

  FUNCTION ISIN_SELF(xicut,epcmf,q1)  ! massive self-eikonal
  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  real (kind=prec) :: isin_self

  isin_self = -1

  END FUNCTION ISIN_SELF

  FUNCTION ISIN_MM(xicut,epcmf,qk,ql)  ! both massive eq(A.11) of FFMS

  real (kind=prec), intent(in) :: qk(4),ql(4),epcmf,xicut
  real (kind=prec) :: isin_mm, vkl
  real (kind=prec) :: KdotL, mk2, ml2


  mk2 = sq(qk)
  ml2 = sq(ql)
  KdotL = 0.5_prec*s(ql,qk)
  vkl = sqrt(1. - mk2*ml2/KdotL**2)


  if(vkl > 1.0E-4) then
     isin_mm = - 0.5_prec/vkl*log((1.+vkl)/(1.-vkl))
  else
     isin_mm = - 0.5* (2 + (2*vkl**2)/3. + (2*vkl**4)/5. + (2*vkl**6)/7. )
  endif


  END FUNCTION ISIN_MM


  FUNCTION IREG_00(xicut,epcmf,k1,k2) ! both massless

  real (kind=prec), intent(in) :: epcmf,xicut
  type(mlm), intent(in) :: k1,k2
  real (kind=prec) :: ireg_00, e1, e2, arg1, arg2, arg24
  real (kind=prec) :: log1, log2, log24, twologtwo, q1(4), q2(4)

  q1 = k1%momentum
  q2 = k2%momentum

  e1 = q1(4); e2 = q2(4)
  arg1 = xicut**2*epcmf**2/musq
  arg2 = s(k1,k2)/e1/e2
  arg24 = arg2/4._prec
  log1 = log(arg1)
  log2 = log(arg2)
  log24 = log(arg24)
  twologtwo = 0.960906027836403_prec    ! 2 (log 2)^2

  if(arg2 > 4.000001_prec) call crash("IREG")
  if(arg2 > 3.999999_prec) arg2 = 3.999999_prec

  ireg_00 = 0.5_prec*log1**2 + log1*log24 - li2(arg24)          &
          + 0.5_prec*log2**2 - log(4._prec - arg2)*log24        &
          - twologtwo

  select case(cgamma)
  case("exp")
    ireg_00 = ireg_00 - pi**2/12.
  case("gam")
    ireg_00 = ireg_00 - 0. ! do nothing
  case default
    call crash("IREG_00")
  end select

    ! NOTE  -pi^2/12 term to change convention in prefactor
    ! Note that the overall factor 1/8/pi**2 is NOT included
    ! since the psi_mn have also a different normalization

  END FUNCTION IREG_00


  FUNCTION IREG_M0(xicut,epcmf,q1,k2)  ! first massive second massless

  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  type(mlm), intent(in) :: k2
  real (kind=prec) :: ireg_m0, ml, ek, el, bl, q2(4)
  real (kind=prec) :: logX, logSQ, logKK, KdotL

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  q2 = k2%momentum
  ek = q2(4); el = q1(4)
  KdotL = 0.5_prec*s(q1,k2)
  ml = sqrt(0.5_prec*s(q1,q1))
  bl = sqrt(q1(1)**2+q1(2)**2+q1(3)**2)/el
!  bl = sqrt(1._prec - (ml/el)**2)

  logKK = log(KdotL/ml/ek)
  ireg_m0 = logX*(logX+logSQ+2.*logKK) + 0.25*logSQ**2              &
          - 0.25*(log((1.+bl)/(1.-bl)))**2                          &
          + 0.5*(log(KdotL/(1.-bl)/el/ek))**2  + logSQ*logKK        &
          - li2(1. - (1.+bl)*ek*el/KdotL)                           &
          + li2(1. - KdotL/(1.-bl)/ek/el)

  select case(cgamma)
  case("exp")
    ireg_m0 = ireg_m0 - pi**2/8.
  case("gam")
    ireg_m0 = ireg_m0 - pi**2/12.
  case default
    call crash("IREG_M0")
  end select

    ! NOTE -pi^2/12 -> -pi^2/12 -pi^2/24 = -pi^2/8
    ! to change convention in prefactor

  END FUNCTION IREG_M0



  FUNCTION IREG_0M(xicut,epcmf,k1,q2)
           ! first massless second massive, "same" as M0

  real (kind=prec), intent(in) :: q2(4),epcmf,xicut
  type(mlm), intent(in) :: k1
  real (kind=prec) :: ireg_0m, ml, ek, el, bl
  real (kind=prec) :: logX, logSQ, logKK, KdotL

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  ek = k1%momentum(4); el = q2(4)
  KdotL = 0.5_prec*s(k1,q2)
  ml = sqrt(0.5_prec*s(q2,q2))
  bl = sqrt(q2(1)**2+q2(2)**2+q2(3)**2)/el
!  bl = sqrt(1._prec - (ml/el)**2)

  logKK = log(KdotL/ml/ek)
  ireg_0m = logX*(logX+logSQ+2.*logKK)  + 0.25*logSQ**2             &
          - 0.25*(log((1.+bl)/(1.-bl)))**2                          &
          + 0.5*(log(KdotL/(1.-bl)/el/ek))**2  + logSQ*logKK        &
          - li2(1. - (1.+bl)*ek*el/KdotL)                           &
          + li2(1. - KdotL/(1.-bl)/ek/el)

  select case(cgamma)
  case("exp")
    ireg_0m = ireg_0m - pi**2/8.
  case("gam")
    ireg_0m = ireg_0m - pi**2/12.
  case default
    call crash("IREG_0M")
  end select

    ! NOTE -pi^2/12 -> -pi^2/12 -pi^2/24 = -pi^2/8
    ! to change convention in prefactor

  END FUNCTION IREG_0M



  FUNCTION IREG_SELF(xicut,epcmf,q1)  ! massive self-eikonal

  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  real (kind=prec) :: ireg_self, el, bl
  real (kind=prec) :: logX, logSQ

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  el = q1(4)
  bl = sqrt(q1(1)**2+q1(2)**2+q1(3)**2)/el

  if(bl > 1.0E-4) then
     ireg_self = 2.*logX + logSQ - 1._prec/bl*log((1.+bl)/(1.-bl))
  else
     ireg_self = 2.*logX + logSQ - (2 + (2*bl**2)/3. + (2*bl**4)/5. + (2*bl**6)/7. )
  endif

  END FUNCTION IREG_SELF




  FUNCTION IREG_MM(xicut,epcmf,qk,ql)  ! both massive eq(A.12) of FFMS

  real (kind=prec), intent(in) :: qk(4),ql(4),epcmf,xicut
  real (kind=prec) :: ireg_mm, vkl, akl, bk, bl, nu, lam_nu_2
  real (kind=prec) :: KdotL, mk2, ml2, ek, el, LogV, LogX

  ek = qk(4)
  el = ql(4)
  bk = sqrt(qk(1)**2+qk(2)**2+qk(3)**2)/ek
  bl = sqrt(ql(1)**2+ql(2)**2+ql(3)**2)/el

  mk2 = sq(qk)
  ml2 = sq(ql)
  KdotL = 0.5_prec*s(ql,qk)
  vkl = sqrt(1. - mk2*ml2/KdotL**2)
  akl = (1.+vkl)*KdotL/mk2
  lam_nu_2 = akl**2*mk2 - ml2  ! 2 \lambda \nu
  nu = 0.5_prec*lam_nu_2/(akl*ek - el)
!  lam = akl*ek - el
!  nu = 0.5_prec*(akl**2*mk2 - ml2)/lam

  logX = log((xicut*epcmf)**2/musq)
  logV = log((1.+vkl)/(1.-vkl))

  ireg_mm = jaux(akl*ek, akl*ek*bk, nu, lam_nu_2)   &
          - jaux(el, el*bl, nu, lam_nu_2)
  ireg_mm = 0.5*(1.+vkl)*KdotL**2*ireg_mm/mk2
  ireg_mm = ireg_mm + 0.5*logV*logX/vkl

  END FUNCTION IREG_MM


  FUNCTION JAUX(xx,yy,nu,lam_nu_2)

  real(kind=prec) :: jaux, xx, yy, nu, lam_nu_2

  jaux = (log((xx-yy)/(xx+yy)))**2        &
       + 4*Li2(1._prec - (xx+yy)/nu)      &
       + 4*Li2(1._prec - (xx-yy)/nu)

  jaux = jaux/lam_nu_2

  END FUNCTION JAUX



  FUNCTION ILIN_SELF(xicut,epcmf,q1)  ! massive self-eikonal
  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  real (kind=prec) :: ilin_self, el, bl, x
  real (kind=prec) :: logX, logSQ, logC, log0, log10

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  el = q1(4)
  bl = sqrt(q1(1)**2+q1(2)**2+q1(3)**2)/el
  logC = logSQ+2*logX

  if(bl > 1.0E-4) then
    x = (1-bl) / (1+bl)
    log0 = log(x)
    log10 = -log0*log(1.-x) - Li2(x)

    ilin_self = -(3*log0**2+12*log10+6*log0*logC+2*pi**2+3*logC**2*bl)/6./bl

  else
    ilin_self = - 4. - 0.5_prec*logC**2 + 2*logC*(1._prec + bl**2/3. + bl**4/5. + bl**6/7.)
  endif

  END FUNCTION ILIN_SELF

  FUNCTION ILIN_MM(xicut,epcmf,qk,ql)  ! both massive eq(A.12) of FFMS
  real (kind=prec), intent(in) :: qk(4),ql(4),epcmf,xicut
  real (kind=prec) :: ilin_mm, bl, mk2, ml2,x, log0, log10, logC,logStuff


  if (abs(qk(1))+abs(qk(2))+abs(qk(3)).gt.zero) call crash("ILIN_MM only for CMS of qk yet!")

  mk2 = sq(qk)
  ml2 = sq(ql)
  bl = sqrt(1. - mk2*ml2/(0.5*s(qk,ql))**2)

  logC = log(epcmf**2/musq) + 2*log(xicut)

  x = (1-bl) / (1+bl)
  log0 = log(x)
  log10 = -log0*log(1.-x) - Li2(x)

  logStuff = -0.5 * log0**2*log(1.-x) - Li3(x) - 2*Li3(1-x) + z3

  ilin_mm = (log0**3 + 3*log0**2*logC + 12*log10*logC + 3*log0*logC**2 +  12*logStuff + log0*Pi**2 + 2*logC*Pi**2)/(12.*bl)


  END FUNCTION ILIN_MM



  FUNCTION IEIK(xicut, epcmf, q1, q2, pole)
  implicit none
  real(kind=prec) :: xicut, epcmf, ieik
  real(kind=prec) :: q1(4), q2(4)
  real(kind=prec), optional, intent(out) :: pole

  ieik = 2*Ireg(xicut, Epcmf, q1, q2) - Ireg(xicut, Epcmf, q1) - Ireg(xicut, Epcmf, q2)

  if (present(pole)) then
    pole = 2*Isin(xicut, Epcmf, q1, q2) - Isin(xicut, Epcmf, q1) - Isin(xicut, Epcmf, q2)
  endif
  END FUNCTION IEIK




!=====================  end soft integrals ====================!


!===================== running of effective em coupling ====================!


  !!! WARNING: Only for space-like momenta q2 < 0 !!!

  !The effective value of the fine structure constant alphaQED at energy
  !E is alphaQED(E)=alphaQED(0)/(1-deltalph)


  !!! leptonic !!!

  !! 1-loop

  FUNCTION DELTALPH_L_0(q2,ml)
  real (kind=prec), intent(in) :: q2, ml
  real (kind=prec) :: deltalph_l_0

  if(abs(q2)<1E-2) then
    deltalph_l_0 = -q2/(15*ml**2*pi)-q2**2/(140*ml**4*pi)-q2**3/(945*ml**6*pi)&
                   -q2**4/(5544*ml**8*pi)-q2**5/(30030*ml**10*pi)
    return
  end if

  deltalph_l_0 = -alpha*(12*ml**2+5*q2+3*DiscB(q2,ml)*(2*ml**2+q2))/(9.*q2*pi)

  END FUNCTION DELTALPH_L_0

  !! 2-loop

  FUNCTION DELTALPH_F(x)
  real (kind=prec), intent(in) :: x
  real (kind=prec) :: deltalph_f

  deltalph_f = 6*Li3(x)-4*Li2(x)*Log(x)-Log(x)**2*Log(1-x)

  END FUNCTION DELTALPH_F

  FUNCTION DELTALPH_G(x)
  real (kind=prec), intent(in) :: x
  real (kind=prec) :: deltalph_g

  deltalph_g = 2*Li2(x)+2*Log(x)*Log(1-x)+x*Log(x)**2/(1-x)

  END FUNCTION DELTALPH_G


  FUNCTION DELTALPH_L_1(q2,ml)
  real (kind=prec), intent(in) :: q2, ml
  real (kind=prec) :: deltalph_l_1, x, z
  real (kind=prec), parameter :: zeta3 = 1.2020569031595942854

  if(abs(q2)<zero) then
    deltalph_l_1 = 0._prec
    return
  end if

  z = - ml**2/q2; x = 2*z/(1+2*z+sqrt(1+4*z))
  deltalph_l_1 = 5 - 52*z + 4*(-1 + 4*z**2)*(6*zeta3 - 2*deltalph_f(x) + deltalph_f(x**2))&
                 - 8*(-1 + 2*z)*sqrt(1 + 4*z)*(deltalph_g(x) - deltalph_g(x**2))&
                 + 6*(1 - 6*z)*sqrt(1 + 4*z)*Log(x) - 4*z*(4 + z)*Log(x)**2
  deltalph_l_1 = -alpha*deltalph_l_1/24/pi**2
  deltalph_l_1 = deltalph_l_1

  END FUNCTION DELTALPH_L_1


  FUNCTION DELTALPH_H(q2)
  real (kind=prec), intent(in) :: q2
  real (kind=prec) :: deltalph_h, der, errder, deg, errdeg
  real (kind=prec), parameter :: st2 = 0.23153_prec

  call hadr5n12(sign(0.001*sqrt(abs(q2)),q2),st2,der,errder,deg,errdeg)
  der = 137.035999*der
  deltalph_h = alpha*der

  END FUNCTION DELTALPH_H

  FUNCTION DELTALPH_0_TOT(q2)
  real (kind=prec), intent(in) :: q2
  real (kind=prec) :: deltalph_0_tot

  deltalph_0_tot = 0._prec

  if(nel==1) deltalph_0_tot = deltalph_0_tot + deltalph_l_0(q2,mel)
  if(nmu==1) deltalph_0_tot = deltalph_0_tot + deltalph_l_0(q2,mmu)
  if(ntau==1) deltalph_0_tot = deltalph_0_tot + deltalph_l_0(q2,mtau)
  if(nel==1) deltalph_0_tot = deltalph_0_tot + deltalph_h(q2)

  END FUNCTION DELTALPH_0_TOT

  FUNCTION DELTALPH_L_1_TOT(q2)
  real (kind=prec), intent(in) :: q2
  real (kind=prec) :: deltalph_l_1_tot

  deltalph_l_1_tot = 0._prec

  if(nel==1) deltalph_l_1_tot = deltalph_l_1_tot + deltalph_l_1(q2,mel)
  if(nmu==1) deltalph_l_1_tot = deltalph_l_1_tot + deltalph_l_1(q2,mmu)
  if(ntau==1) deltalph_l_1_tot = deltalph_l_1_tot + deltalph_l_1(q2,mtau)

  END FUNCTION DELTALPH_L_1_TOT




!===================== end running of effective em coupling ====================!



  FUNCTION QTIL(cc,xicut,epcmf,q1)

  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  character (len=1), intent(in) :: cc
  real (kind=prec) :: qtil, e1, gamma, gammap, ccol

  e1 = q1(4)

  select case(cc)
  case("l")
    gamma = 1.5_prec
    gammap = (6.5_prec - 2*pi**2/3.)
    ccol = 1._prec
  case("q")
    gamma = 1.5_prec*Cf
    gammap = (6.5_prec - 2*pi**2/3.)*Cf
    ccol = Cf
  case("g")
    gamma = (11.*Nc - 4*Tf*5.)/6.  !!Nf -> 5
    gammap = 67.*Nc/9. - 2*pi**2*Nc/3. - 23.*Tf*5./9.   !!Nf -> 5
    ccol = Nc
  case default
    call crash("QTIL")
  end select

  qtil = gammap - gamma*log(2*delcut*e1**2/musq) +                 &
         2*ccol*log(2*e1/epcmf/xicut)*                             &
                 log(epcmf*delcut*e1*xicut/musq)

  END FUNCTION QTIL




  FUNCTION BOOST_BACK(rec,mo)   !!boosts to cms system

  real (kind=prec), intent(in):: rec(4),mo(4)
  real (kind=prec)  :: cosh_a, energy,  dot_dot,   &
                       n_vec(3), boost_back(4)

  energy = rec(4)**2 - rec(1)**2 - rec(2)**2 - rec(3)**2

  if(energy < 1.0E-16_prec) then
    throw_away = 1
    energy = 1.0E-12_prec
  else
    energy = sqrt(energy)
  end if

  cosh_a = rec(4)/energy
  n_vec = - rec(1:3)/energy  ! 1/sinh_a omitted

  dot_dot = sum(n_vec*mo(1:3))  ! \vec{n} \dot \vec{m}

  boost_back(1:3) =    &
     mo(1:3) + n_vec*(dot_dot/(cosh_a + 1) - mo(4))
  boost_back(4) = mo(4)*cosh_a - dot_dot

  END FUNCTION BOOST_BACK



  FUNCTION BOOST_RFREC(rec,mo1)   !!boosts mo to rest-frame of rec

  real (kind=prec), intent(in):: rec(4),mo1(4)
  real (kind=prec)  :: energy, beta(3), gamma, betaSQ, dot_dot, betaABS
  real (kind=prec)  :: boost_rfrec(4)

  energy = rec(4)
  beta = rec(1:3)/energy
  betaSQ = beta(1)**2 + beta(2)**2 + beta(3)**2
  betaABS = sqrt(betaSQ)
  if(betaSQ > zero) then
    beta = beta/betaABS
  else
    beta = (/0._prec,0._prec,0._prec /)
  endif
  gamma = 1./sqrt(1._prec-betaSQ)

  dot_dot = sum(beta*mo1(1:3))  ! \vec{beta} \dot \vec{m}

  boost_rfrec(1:3) =    &
     mo1(1:3) + beta*dot_dot*(gamma-1.) - beta*betaABS*gamma*mo1(4)

  boost_rfrec(4) = gamma*(mo1(4)-betaABS*dot_dot)

!!$  boost_rfrec(1:3) =    &
!!$     mo1(1:3) + beta*(dot_dot*(gamma-1.)/betaSQ - gamma*mo1(4))
!!$
!!$  boost_rfrec(4) = gamma*(mo1(4)-dot_dot)

  END FUNCTION BOOST_RFREC



  ! this used to be in phase_space but we need it here as well

  FUNCTION SQ_LAMBDA(ss,m1,m2) !! square root of \lambda(s,m1,m2)

  implicit none
  real (kind=prec) ss,m1,m2,sq_lambda

  sq_lambda = (ss - (m1+m2)**2)*(ss - (m1-m2)**2)
  sq_lambda = sqrt(sq_lambda)

  END FUNCTION SQ_LAMBDA
  FUNCTION CSQ_LAMBDA(ss,m1,m2) !! complex square root of \lambda(s,m1,m2)

  implicit none
  real (kind=prec) ss,m1,m2
  complex (kind=prec) csq_lambda

  csq_lambda = (ss - (m1+m2)**2)*(ss - (m1-m2)**2)
  csq_lambda = sqrt(csq_lambda)

  END FUNCTION CSQ_LAMBDA

!======================    Package X abbr.   ======================!



!!! Collier implementation (complex valued) !!!

  FUNCTION DISCB_COLL(s,m1,m2)
  ! Package X function DiscB[s,m1,m2]
  real(kind=prec), intent(in) :: s,m1,m2
  complex(kind=prec) :: pvb, discb_coll

  call B0_cll(pvb, cmplx(s), cmplx(m1**2), cmplx(m2**2))

  discb_coll = pvb - 2 + 2*log(m2) + (m1**2-m2**2+s)*log(m1/m2)/s
  END FUNCTION DISCB_COLL


  FUNCTION SCALARC0_COLL(s1,s12,s2,m1,m2,m3)
  ! Package X function ScalarC0[s1,s12,s2,m1,m2,m3]
  real(kind=prec), intent(in) :: s1,s12,s2,m1,m2,m3
  complex(kind=prec) :: scalarc0_coll, pvc

  call C0_cll(pvc, &
      cmplx(s1),cmplx(s12),cmplx(s2)  ,  &
      cmplx(m1**2),cmplx(m2**2),cmplx(m3**2) )

  scalarc0_coll = pvc
  END FUNCTION SCALARC0_COLL


  FUNCTION SCALARC0IR6_COLL(s,m1,m2)
  ! Package X function ScalarC0IR6[s,m1,m2]
  real(kind=prec), intent(in) :: s, m1,m2
  complex(kind=prec) :: scalarc0ir6_coll, pvc

  call C0_cll(pvc, &
      cmplx(m1**2),cmplx(s),cmplx(m2**2)  ,  &
      cmplx(0.),cmplx(m1**2),cmplx(m2**2) )

  if(s==0) then
    scalarc0ir6_coll = 0.
    return
  end if
  scalarc0ir6_coll = -0.5*s*DiscB(s,m1,m2)/CSQ_LAMBDA(s,m1,m2)**2
  scalarc0ir6_coll = scalarc0ir6_coll * ( 2*log(m1/m2) - 4*log(m1) )
  scalarc0ir6_coll = scalarc0ir6_coll + pvc
  END FUNCTION SCALARC0IR6_COLL

!!! IN-HOUSE IMPLEMENTATION !!!
!!! WARNING: Real valued implementations only tested rigorously for     !!!
!!!          kinematic region relevant for muone@NLO. Complex valued    !!!
!!!          loop functions untested. That's why corresponding          !!!
!!!          interface disabled.


!!! Real-valued: One mass !!!

  FUNCTION DISCBSMM(s,m)
  ! Package X function Re[DiscB[s,m,m]]
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: discbsmm, beta, arg

  if(abs(s)<1.0E-7_prec) then
    discbsmm = -2.0_prec+s/6/m**2+s**2/60/m**4
    return
  end if
  beta = 1-4*m**2/s
  if (beta < 0) then
    beta = sqrt(-beta)
    discbsmm = -beta * atan2(2*beta/(1+beta**2), (beta**2-1)/(1+beta**2))
  else
    beta = sqrt(beta)
    arg = (beta-1)/(1+beta)

    discbsmm = beta*log(abs(arg))
  endif
  END FUNCTION DISCBSMM


  FUNCTION SCALARC0IR6SMM(s,m)
  ! Package X function Re[ScalarC0IR6[s,m,m]]
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: scalarc0ir6smm,beta,arg1,arg2,log1,log2,polylog

  if(abs(s)<1.0E-7_prec) then
    scalarc0ir6smm = s/12/m**4+s**2/40/m**6
    return
  end if

  beta = sqrt(1-4*m**2/s)
  arg1 = (beta-1)/(1+beta)
  arg2 = 2*beta/(1+beta)

  log1 = log(abs(arg1))
  log2 = log(arg2)
  polylog = Li2(-arg1)

  scalarc0ir6smm = -(pi**2-3*log1**2+12*log1*log2+12*polylog)/(6*s*beta)

  !If arg1<0, we have to include contribution from imaginary part of log1**2
  if(arg1<0) scalarc0ir6smm = scalarc0ir6smm - 3*pi**2/(6*s*beta)
  END FUNCTION SCALARC0IR6SMM


  FUNCTION SCALARC0SM(s,m)
  ! Package X function Re[ScalarC0[m^2, m^2, s, 0, m, 0]]
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: scalarc0sm, beta, arg

  beta = sqrt(1-4*m**2/s)
  arg = (beta-1)/(1+beta)

  scalarc0sm = (4*pi**2+3*log(abs(arg))**2+12*Li2(arg))/(6*s*beta)

  !If s>0, i.e. beta<1, we have to include contribution from imaginary part of log1**2
  if(s>0) scalarc0sm = scalarc0sm - 3*pi**2/(6*s*beta)
  END FUNCTION SCALARC0SM


  !!! Complex-valued: One mass !!!


  FUNCTION DISCBSMM_CPLX(s,m)
  ! Package X function Re[DiscB[s,m,m]]
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: beta, arg
  complex(kind=prec)::discbsmm_cplx

  beta = sqrt(1-4*m**2/s)
  arg = (beta-1)/(1+beta)

  discbsmm_cplx = beta*log(abs(arg))
  if(arg<0) discbsmm_cplx = discbsmm_cplx + imag*pi*beta
  END FUNCTION DISCBSMM_CPLX


  FUNCTION SCALARC0IR6SMM_CPLX(s,m)
  ! Package X function ScalarC0IR6[s,m,m]
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: beta,arg1,arg2,log1,log2,polylog
  complex(kind=prec)::scalarc0ir6smm_cplx

  beta = sqrt(1-4*m**2/s)
  arg1 = (beta-1)/(1+beta)
  arg2 = 2*beta/(1+beta)

  log1 = log(abs(arg1))
  log2 = log(arg2)
  polylog = Li2(-arg1)

  scalarc0ir6smm_cplx = -(pi**2-3*log1**2+12*log1*log2+12*polylog)/(6*s*beta)

  !If arg1<0, we have to include contribution from imaginary part of log1**2
  if(arg1<0) scalarc0ir6smm_cplx = scalarc0ir6smm_cplx - 3*pi**2/(6*s*beta) - imag*pi*(-6*log1+12*log2)/(6*s*beta)
  END FUNCTION SCALARC0IR6SMM_CPLX


  !!! Real-valued: Two masses !!!


  FUNCTION DISCBSMN(s,m1,m2)
  ! Package X function Re[DiscB[s,m,m]]
  real(kind=prec), intent(in) :: s,m1,m2
  real(kind=prec) :: discbsmn,v,sqlambda,arg1,arg2

  if (abs(s/(m1**2-m2**2)) .lt. 1.e-2_prec) then
    arg1 = m1**2 - m2**2
    arg2 = m1**2 + m2**2
    discbsmn = arg1*log(m1/m2) / s - 1 - arg2*log(m1/m2)/arg1
  endif
  sqlambda = sq_lambda(s,m1,m2)
  arg1 = s-m1**2-m2**2
  v = sqlambda/abs(arg1)
  arg2 = (1-v)/(1+v)

  discbsmn = sign(1.,arg1)*sqlambda*log(arg2)/(2*s)
  END FUNCTION DISCBSMN


 FUNCTION SCALARC0IR6SMN(s,m1,m2)
  ! Package X function Re[ScalarC0IR6[s,m1,m2]]
  real(kind=prec), intent(in) :: s, m1,m2
  real(kind=prec) :: sql,arg1,arg2,arg3,arg4,img,m1sq,m2sq
  real(kind=prec) :: log1,log2,log3,log4,log5,log6,log7,log8,polylog1,polylog2
  real(kind=prec)::scalarc0ir6smn

  m1sq = m1**2; m2sq = m2**2
  !If s small numerical instabilities are encountered -> use expansion in s
  if(abs(s/(m1**2+m2**2))<0.00009) then
    scalarc0ir6smn = s*(-m1sq+m2sq+(m1sq+m2sq)*log(m1/m2))/(m1sq-m2sq)**3&
                     + s**2*3*(-3*m1sq**2+3*m2sq**2+2*(m1sq**2+4*m1sq*m2sq+m2sq**2)&
                     *log(m1/m2))/4/(m1sq-m2sq)**5
    return
  end if

  sql = sq_lambda(s,m1,m2)
  !Here the logs where the imaginary part matters
  arg1 = (2*s)/(m1sq- m2sq + s - sql)
  arg2 = (m1sq - m2sq - s + sql)/(2.*s)
  arg3 = 2*m1sq/(m1sq - m2sq + s - sql)!Numerically more stable than: (m1sq - m2sq + s + sql)/(2.*s)
  arg4 = (-2*s)/(-m1sq + m2sq + s + sql)
  log1 = log(abs(arg1)); log2 = log(abs(arg2))
  log3 = log(abs(arg3)); log4 = log(abs(arg4))

  log5 = log(abs((m1*m2)/s))
  log6 = log(abs((2*m1*m2)/(m1sq + m2sq - s - sql)))
  log7 = log(abs((-m1sq + m2sq - s + sql)/(-m1sq + m2sq + s + sql)))
  log8 = log(abs(sql/s))
  polylog1 = Li2((-m1sq + m2sq - s + sql)/(2.*sql),img)
  polylog2 = Li2((-m1sq + m2sq + s + sql)/(2.*sql),img)

  scalarc0ir6smn = (-log1**2 - log2**2 - 2*log1*log3 + log3**2 + 2*log2*log4&
                   + log4**2 + 4*log5*log6 - 4*log7*log8 + 4*polylog1 - 4*polylog2)/(4.*sql)

  !if arg1-arg4 negative, then also have to take pi's imaginary part into account
  !for all other logs there is no such contribution in the physical region
  if(arg1<0) scalarc0ir6smn = scalarc0ir6smn + pi**2/(4.*sql)
  if(arg2<0) scalarc0ir6smn = scalarc0ir6smn + pi**2/(4.*sql)
  if(arg3<0) scalarc0ir6smn = scalarc0ir6smn - pi**2/(4.*sql)
  if(arg4<0) scalarc0ir6smn = scalarc0ir6smn - pi**2/(4.*sql)
  if(arg1<0 .AND. arg3<0) scalarc0ir6smn = scalarc0ir6smn + 2*pi**2/(4.*sql)
  if(arg2<0 .AND. arg4<0) scalarc0ir6smn = scalarc0ir6smn - 2*pi**2/(4.*sql)
  END FUNCTION SCALARC0IR6SMN





  !!! Complex-valued: Two masses !!!


  FUNCTION DISCBSMN_CPLX(s,m1,m2)
  ! Package X function DiscB[s,m,m]
  real(kind=prec), intent(in) :: s,m1,m2
  real(kind=prec) :: v,sqlambda,arg1,arg2
  complex(kind=prec)::discbsmn_cplx

  sqlambda = sq_lambda(s,m1,m2)
  arg1 = s-m1**2-m2**2
  v = sqlambda/abs(arg1)
  arg2 = (1-v)/(1+v)

  discbsmn_cplx = sign(1.,arg1)*sqlambda*log(arg2)/(2*s)
  if(arg1>0) discbsmn_cplx = discbsmn_cplx + imag*pi*sqlambda/s
  END FUNCTION DISCBSMN_CPLX


  FUNCTION SCALARC0IR6SMN_CPLX(s,m1,m2)
  ! Package X function ScalarC0IR6[s,m1,m2]
  real(kind=prec), intent(in) :: s, m1,m2
  real(kind=prec) :: sql,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,img_poly1,img_poly2
  real(kind=prec) :: log1,log2,log3,log4,log5,log6,log7,log8,polylog1,polylog2
  complex(kind=prec)::scalarc0ir6smn_cplx

  !If s small numerical instabilities are encountered -> use expansion in s
  if(abs(s/(m1**2+m2**2))<0.00009) then
    scalarc0ir6smn_cplx = s*(-m1**2+m2**2+(m1**2+m2**2)*log(m1/m2))/(m1**2-m2**2)**3&
                     + s**2*3*(-3*m1**4+3*m2**4+2*(m1**4+4*m1**2*m2**2+m2**4)&
                     *log(m1/m2))/4/(m1**2-m2**2)**5
    return
  end if

  sql = sq_lambda(s,m1,m2)
  arg1 = (2*s)/(m1**2 - m2**2 + s - sql); arg2 = (m1**2 - m2**2 - s + sql)/(2.*s)
  arg3 = (m1**2 - m2**2 + s + sql)/(2.*s); arg4 = (-2*s)/(-m1**2 + m2**2 + s + sql)
  arg5 = (m1*m2)/s; arg6 = (2*m1*m2)/(m1**2 + m2**2 - s - sql)
  arg7 = (-m1**2 + m2**2 - s + sql)/(-m1**2 + m2**2 + s + sql); arg8 = sql/s
  log1 = log(abs(arg1)); log2 = log(abs(arg2))
  log3 = log(abs(arg3)); log4 = log(abs(arg4))
  log5 = log(abs(arg5)); log6 = log(abs(arg6))
  log7 = log(abs(arg7)); log8 = log(abs(arg8))
  polylog1 = Li2((-m1**2 + m2**2 - s + sql)/(2.*sql),img_poly1)
  polylog2 = Li2((-m1**2 + m2**2 + s + sql)/(2.*sql),img_poly2)

  scalarc0ir6smn_cplx = (-log1**2 - log2**2 - 2*log1*log3 + log3**2 + 2*log2*log4&
                   + log4**2 + 4*log5*log6 - 4*log7*log8 + 4*polylog1 - 4*polylog2)/(4.*sql)&
                   + imag*(img_poly1-img_poly2)/sql

  !Contributions from imaginary parts of log if arg's < 0
  if(arg1<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx + pi**2/(4.*sql) + imag*pi*(-log1-log3)/(2.*sql)
  end if
  if(arg2<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx + pi**2/(4.*sql) + imag*pi*(-log2+log4)/(2.*sql)
  end if
  if(arg3<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx - pi**2/(4.*sql) + imag*pi*(log3-log1)/(2.*sql)
  end if
  if(arg4<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx - pi**2/(4.*sql) + imag*pi*(log4+log2)/(2.*sql)
  end if
  if(arg5<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx + imag*pi*log6/sql
  end if
  if(arg6<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx + imag*pi*log5/sql
  end if
  if(arg7<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx - imag*pi*log8/sql
  end if
  if(arg8<0) then
    scalarc0ir6smn_cplx = scalarc0ir6smn_cplx - imag*pi*log7/sql
  end if
  if(arg1<0 .AND. arg3<0) scalarc0ir6smn_cplx = scalarc0ir6smn_cplx + pi**2/(2.*sql)
  if(arg2<0 .AND. arg4<0) scalarc0ir6smn_cplx = scalarc0ir6smn_cplx - pi**2/(2.*sql)
  if(arg5<0 .AND. arg6<0) scalarc0ir6smn_cplx = scalarc0ir6smn_cplx - pi**2/sql
  if(arg7<0 .AND. arg8<0) scalarc0ir6smn_cplx = scalarc0ir6smn_cplx + pi**2/sql
  END FUNCTION SCALARC0IR6SMN_CPLX


!!! Loop Tools implementation (complex valued) !!!


  FUNCTION SCALARD0IR16(s2,s3,s12,s23,m1,m2,m3)
  real(kind=prec), intent(in) :: s2,s3,s12,s23,m1,m2,m3
  real(kind=prec) :: scalard0ir16
  complex(kind=prec) :: pv
  musq = 1.

  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)

  call D0_cll(pv, cmplx(m1**2), cmplx(m3**2), cmplx(s3), cmplx(s2), cmplx(s23), cmplx(s12),&
      cmplx(m1**2), cmplx(0.), cmplx(m3**2), cmplx(m2**2))

  scalard0ir16 = real(DiscB(s23,m1,m3))*s23 / SQ_LAMBDA(s23,m1,m3)**2 / (m2**2-s12)
  scalard0ir16 = scalard0ir16 * ( log(m1/m3) -log(m1**2/musq) )
  scalard0ir16 = scalard0ir16 + real(pv)
  END FUNCTION SCALARD0IR16


!=====================  end Package X abbr ====================!



!======================    Former user.f95   ======================!

  FUNCTION BOOST_RF(rec,mo)   !!boosts mo to (non-unique) rest frame of rec

  real (kind=prec), intent(in):: rec(4), mo(4)
  real (kind=prec)  :: energy,  mass, dot_dot, boost_rf(4)
  integer :: throw_away

  mass = rec(4)**2 - rec(1)**2 - rec(2)**2 - rec(3)**2

  if(mass < 1.0E-16_prec) then  ! rec must not be massless
    throw_away = 1
    mass = 1.0E-12_prec
  else
    mass = sqrt(mass)
  end if

  energy = rec(4)
  dot_dot = sum(rec(1:3)*mo(1:3))

  boost_rf(4) = (energy*mo(4)-dot_dot)/mass
  boost_rf(1:3) =    &
     mo(1:3) + rec(1:3)*(dot_dot/(energy+mass) - mo(4))/mass

  END FUNCTION BOOST_RF



  FUNCTION ETA(k1)   ! pseudo-rapidity

  real (kind=prec), intent(in) :: k1(4)
  real (kind=prec):: eta
  real (kind=prec) :: kv

  kv = sqrt(k1(1)**2+k1(2)**2+k1(3)**2)

  eta = 0.5*log((kv+k1(3))/(kv-k1(3)))

  END FUNCTION ETA



  FUNCTION RAP(k1)   ! rapidity

  real (kind=prec), intent(in) :: k1(4)
  real (kind=prec):: rap

  rap = 0.5*log((k1(4)+k1(3))/(k1(4)-k1(3)))

  END FUNCTION RAP



  FUNCTION PT(q1)   ! transverse momentum

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec):: pt

  pt = sqrt(q1(1)**2 + q1(2)**2)

  END FUNCTION PT



  FUNCTION ABSVEC(q1)   ! magnitude of 3-vector

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec):: absvec

  absvec = sqrt(q1(1)**2 + q1(2)**2 + q1(3)**2)

  END FUNCTION ABSVEC



  FUNCTION RIJ(k1,k2)

  real (kind=prec), intent(in) :: k1(4), k2(4)
  real (kind=prec) ::  rij, yij, phiij, Dres

  Dres = 0.7_prec

  yij = 0.5*log((k1(4)+k1(3))*(k2(4)-k2(3))/(k1(4)-k1(3))/(k2(4)+k2(3)))
  phiij = k1(1)*k2(1) + k1(2)*k2(2)
  phiij = phiij/sqrt(k1(1)**2+k1(2)**2)/sqrt(k2(1)**2+k2(2)**2)
  phiij = acos(phiij)

!  phiij = atan2(k1(2),k1(1)) - atan2(k2(2),k2(1))
!  if(phiij > pi) phiij = phiij - 2*pi
!  if(phiij < -pi) phiij = phiij + 2*pi

  rij = (yij**2 + phiij**2)/Dres**2

  END FUNCTION RIJ



  FUNCTION COS_TH(k1,k2)

  real (kind=prec), intent(in) :: k1(4), k2(4)
  real (kind=prec) ::  cos_th, mag1, mag2

  mag1 = sqrt(k1(1)**2 + k1(2)**2 + k1(3)**2)
  mag2 = sqrt(k2(1)**2 + k2(2)**2 + k2(3)**2)

  if ( (mag1 < zero).or.(mag2 < zero) ) then
    cos_th = 0.
    return
  endif
  cos_th = sum(k1(1:3)*k2(1:3))/mag1/mag2

  END FUNCTION COS_TH


  FUNCTION PHI(q1)   ! -pi < azimuthal angle < pi
  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec):: phi

  if(max(abs(q1(1)),abs(q1(2))) < zero) then
    phi = 100*pi    !! throw away
  else
    phi = atan2(q1(2),q1(1))
  endif

  END FUNCTION PHI



!======================  end user.f95        ======================!




  FUNCTION FUCKOFF(VAL, ARR, MSG)
  implicit none
  real(kind=prec) :: fuckoff, val, arr(:)
  character(len=*),optional :: msg
  character(len=30) :: mmsg
  character(len=10) :: slurmid

  if (runninglab) then
    fuckoff = val
    return
  endif
  if (isnan(val)) then
    fuckoff = 0.
    if (present(msg)) then
      mmsg = msg
    else
      mmsg = "no message"
    endif
    if (pcounter(2) .lt. 10000) then
      call getenv('SLURM_JOB_ID', slurmid)
      open(unit=8,action='WRITE',form="UNFORMATTED",&
           position='append',file='bad_points'//trim(slurmid)//'.bp' )
      write(8) mmsg, which_piece, flavour, xinormcut, delcut, size(arr), &
        reshape(arr, (/ 17 /), pad=[0._prec] )
      close(unit=8)
    endif
    pcounter(2) = pcounter(2)+1
  else
    fuckoff = val
    pcounter(1) = pcounter(1)+1
  endif


  END FUNCTION FUCKOFF



                            !!!!!!!!!!!!!!!!!!!!!!!!!
                              END MODULE FUNCTIONS
                            !!!!!!!!!!!!!!!!!!!!!!!!!



