  PROGRAM MCMULE

#ifdef COLLIER
  use collier
#endif
  use integrands
  use vegas_m

  implicit none

  integer :: ndim
  procedure(integrand), pointer :: fxn
  integer :: ncall_ad, itmx_ad, ncall, itmx, nenter_ad, nenter
  integer :: initial_ran_seed, ad_print, fu_print, nsp
  character (len=1) ::  ad_char, fu_char
  real (kind=prec) :: avgi, sd, chi2a
  character(len=900) :: filename


!  read(5,*) nenter_brute
  read(5,*) nenter_ad
  read(5,*) itmx_ad
  read(5,*) nenter
  read(5,*) itmx
  read(5,*) ran_seed
  read(5,*) xinormcut
  read(5,*) delcut
  read(5,*) which_piece
  read(5,*) flavour

  initial_ran_seed = ran_seed
  ncall_ad = 1000*nenter_ad
  ncall = 1000*nenter

  xinormcut1 = xinormcut
  xinormcut2 = delcut

  call initflavour
  call initpiece(ndim, fxn)

      !!!!!!!!!!      OUTPUT     !!!!!!!!!

  ad_print = ncall_ad
  ad_char = ' '
  if(ncall_ad > 999999999) then
    ad_print = ncall_ad/1000000000
    ad_char = 'G'
  elseif(ncall_ad > 999999) then
    ad_print = ncall_ad/1000000
    ad_char = 'M'
  elseif(ncall_ad > 999) then
    ad_print = ncall_ad/1000
    ad_char = 'K'
  endif

  fu_print = ncall
  fu_char = ' '
  if(ncall > 999999999) then
    fu_print = ncall/1000000000
    fu_char = 'G'
  elseif(ncall > 999999) then
    fu_print = ncall/1000000
    fu_char = 'M'
  elseif(ncall > 999) then
    fu_print = ncall/1000
    fu_char = 'K'
  endif




  call init_cll(5,folder_name="")

  write(6,*) '  - * - * - * - * - * - * - * -     '
  write(6,*) '       Version information          '
  write(6,*) " Full SHA: ", fullsha(1:7)
  write(6,*) " Git  SHA: ", gitrev (1:7)
  do nsp = 1,len(gitbranch)
    if (iachar(gitbranch(nsp:nsp)) == 0) exit
  enddo
  write(6,*) ' Git banch: ', gitbranch(1:nsp-1)
  write(6,*) '  - * - * - * - * - * - * - * -     '

  call inituser
  if (iachar(filenamesuffix(1:1)) == 0) then
    write (filename, 900) trim(which_piece),trim(flavour),ran_seed,xinormcut,delcut,itmx,fu_print,fu_char
  else
    write (filename, 901) trim(which_piece),trim(flavour),ran_seed,xinormcut,delcut,itmx,fu_print,fu_char,trim(filenamesuffix)
  endif

  write(6,*) '  - * - * - * - * - * - * - * -     '
  write(6,*) '        '

  call vegas (ndim,ncall_ad,itmx_ad,fxn,avgi,sd,chi2a,ran_seed)
  call vegas1(ndim,ncall   ,itmx   ,fxn,avgi,sd,chi2a,ran_seed,filename)


  write(6,"(a,i3,a,i3,a,a,i3,a,i3,a,a,i10)")             &
      ' points:   ',itmx_ad,'*',ad_print,ad_char,' + ',   &
      itmx,'*',fu_print,fu_char,                         &
      '         random seed: ', initial_ran_seed
  if ( (which_piece == "ee2nnRV").or.(which_piece == "ee2nnRR") ) then
  write(6,"(a,a,a,f7.5,a,f7.5)")     &
      ' part: ', which_piece,    &
      '    xicut1: ', xinormcut1,'    xicut2: ', xinormcut2
  else
  write(6,"(a,a,a,f7.5,a,f7.5)")     &
      ' part: ', which_piece,    &
      '    xicut: ', xinormcut,'    delcut: ', delcut
  endif
  if (flavour == "e-s") then
  write(6,"(a,es11.5,a,es11.5)")     &
      ' S:  ', Scms,'   Mout:  ', Me
  else
  write(6,"(a,i10,a,i10)")     &
      ' points on phase space ',sum(pcounter), ' thereof fucked up ',pcounter(2)
  endif
  write(6,*) '        '
  write(6,*) '        '
  write(6,*) '        '
!  write(6,"(a,es13.5,a,es13.5,a,f4.2)")     &
!      ' result, error:  {' ,conv*avgi ,',', conv*sd,' };   chisq:  ', chi2a
  write(6,"(a,es13.5,a,es13.5,a,f4.2)")     &
      ' result, error:  {' ,avgi ,',', sd,' };   chisq:  ', chi2a
  write(6,*) '        '
  write(6,*) '  - * - * - * - * - * - * - * -     '


900  format("out/",A,'_',A,'_S',I0.10,"X",f7.5,"D",f7.5,'_ITMX',I0.3,'x',I0.3,A,".vegas")
901  format("out/",A,'_',A,'_S',I0.10,"X",f7.5,"D",f7.5,'_ITMX',I0.3,'x',I0.3,A,"_O",A,".vegas")
  END PROGRAM MCMULE

