double (*__integrands_MOD_matel)();
double (*__integrands_MOD_matel_s)();
double (*__integrands_MOD_matel_sh)();
double (*__integrands_MOD_matel_hs)();
double (*__integrands_MOD_matel_ss)();


void set_func_(int*what, void*dest){
  switch(*what){
    case 0b000:
      __integrands_MOD_matel = dest;
      break;
    case 0b001:
      __integrands_MOD_matel_s = dest;
      break;
    case 0b010:
      __integrands_MOD_matel_hs = dest;
      break;
    case 0b100:
      __integrands_MOD_matel_sh = dest;
      break;
    case 0b110:
      __integrands_MOD_matel_ss = dest;
      break;
  }
}
