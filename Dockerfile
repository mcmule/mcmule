FROM yulrich/mcmule-pre:1.0.0

LABEL maintainer="yannick.ulrich@psi.ch"
LABEL version="1.0"
LABEL description="The full McMule suite"
RUN pip3 install git+https://gitlab.psi.ch/mcmule/pymule.git
COPY . /monte-carlo
RUN cd /monte-carlo && ./configure
RUN cd /monte-carlo && make
