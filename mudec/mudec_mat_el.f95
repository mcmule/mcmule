
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MUDEC_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use mudec_pm2enngg, only: pm2enngg
  use mudec_pm2ennggav, only: pm2ennggav
  use mudec_pm2ennglav, only: pm2ennglav, pm2enngfav, &
                              pm2ennglav0,pm2ennglavz,pm2enngfavz


  implicit none

  contains


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!     MATRIX   ELEMENTS       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!           MAJORON           !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! TREE LEVEL SQUARED AMPLITUDE
  FUNCTION PM2EJ(p1,n1,p2,p3)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  real (kind=prec) :: p1(4), n1(4), p2(4), p3(4)
  real (kind=prec) :: Mmu, Mel, Mj, Mmu2, Mel2, Mj2, nq
  real (kind=prec) :: pm2ej

  Mmu2 = sq(p1)
  Mel2 = sq(p2)
  Mj2 = sq(p3)
  Mmu = sqrt(Mmu2)
  Mel = sqrt(Mel2)
  Mj = sqrt(Mj2)

  nq = s(n1,p2)

  pm2ej = CRj*(2*CLj*Mel*Mmu + CRj*(Mel2 - Mj2 + Mmu2 - Mmu*nq)) &
    + CLj*(2*CRj*Mel*Mmu + CLj*(Mel2 - Mj2 + Mmu2 + Mmu*nq))

  END FUNCTION PM2EJ


! ONE LOOP MIXED AMPLITUDE
  FUNCTION PM2EJL(p1,n1,p2,p3,sing)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  real (kind=prec) :: p1(4), n1(4), p2(4), p3(4)
  real (kind=prec) :: Mmu2, Mel2, Mj2, Mmu4, Mel4, Mj4, Mmu, Mel, Mj
  real (kind=prec) :: CLj2, CRj2
  real (kind=prec) :: ss0, ss1, nq, mu2
  real (kind=prec) :: LogMe, LogMu, LogMeMu
  real (kind=prec) :: pm2ejl
    real (kind=prec), optional :: sing

  Mmu2 = sq(p1)
  Mel2 = sq(p2)
  Mj2 = sq(p3)
  Mmu4 = sq(p1)**2
  Mel4 = sq(p2)**2
  Mj4 = sq(p3)**2
  Mmu = sqrt(sq(p1))
  Mel = sqrt(sq(p2))
  Mj = sqrt(sq(p3))

  CLj2 = CLj**2
  CRj2 = CRj**2

  nq = s(n1,p2)

  ss0 = Mel2 + Mmu2 - Mj2
  ss1 = Mel4 + Mmu4 + Mj4 - 2*Mmu2*Mj2 - 2*Mel2*Mmu2 - 2*Mel2*Mj2

  mu2 = musq
  LogMe = log(Mel2/mu2)
  LogMu = log(Mmu2/mu2)
  LogMeMu = log(Mel2/Mmu2)

  pm2ejl = (-8 + 3*LogMe + 3*LogMu)*(4*CLj*CRj*Mel*Mmu &
    + CRj2*(-(Mmu*nq) + ss0) + CLj2*(Mmu*nq + ss0)) &
    + 2*(6*(4*CLj*CRj*Mel*Mmu + CRj2*(-(Mmu*nq) + ss0) &
    + CLj2*(Mmu*nq + ss0)) + (LogMeMu*(-Mel2 + 2*Mj2 &
    + Mmu2)*(4*CLj*CRj*Mel*Mmu + CRj2*(-(Mmu*nq) + ss0) &
    + CLj2*(Mmu*nq + ss0)))/Mj2 + ((CRj2*(2*Mel**6 + 2*Mj**4*Mmu2 &
    + 2*Mmu**5*(Mmu - nq) + 2*Mel2*(Mj4 - Mmu**3*(Mmu - 2*nq) &
    + Mj2*Mmu*(-4*Mmu + nq)) - 2*Mel4*(2*Mj2 + Mmu*(Mmu + nq)) &
    + Mj2*(-4*Mmu4 + 2*Mmu**3*nq - LogMeMu*Mmu*nq*ss0 &
    + LogMeMu*ss0**2)) + CLj2*(2*Mel**6 + 2*Mj4*Mmu2 &
    - 2*Mel4*(2*Mj2 + Mmu*(Mmu - nq)) + 2*Mmu**5*(Mmu + nq) &
    + 2*Mel**2*(Mj4 - Mj2*Mmu*(4*Mmu + nq) - Mmu**3*(Mmu + 2*nq)) &
    + Mj2*(-4*Mmu4 - 2*Mmu**3*nq + LogMeMu*Mmu*nq*ss0 &
    + LogMeMu*ss0**2)) + 4*CLj*CRj*Mel*Mmu*(LogMeMu*Mj**2*ss0 &
    + 2*ss1))*DiscB(Mj2,Mel,Mmu))/ss1 - (2*LogMe*(4*CLj*CRj*Mel*Mmu &
    + CRj2*(-(Mmu*nq) + ss0) + CLj2*(Mmu*nq + ss0))*(2*ss1 &
    + Mj2*ss0*DiscB(Mj2,Mel,Mmu)))/ss1 + 2*ss0*(4*CLj*CRj*Mel*Mmu &
    + CRj2*(-(Mmu*nq) + ss0) + CLj2*(Mmu*nq &
    + ss0))*ScalarC0IR6(Mj2,Mel,Mmu))

  pm2ejl = pm2ejl*alpha/4._prec/pi

  if (present(sing)) then
  sing = 4*CLj*CRj*Mel*Mmu + CRj**2*(Mel2 - Mj2 + Mmu*(Mmu - nq)) &
         + CLj**2*(Mel2 - Mj2 + Mmu*(Mmu + nq))
  sing = (2.*alpha/pi)*sing
  endif

  END FUNCTION PM2EJL


! PHOTON EMISSION SQUARED AMPLITUDE
  FUNCTION PM2EJG(p1,n1,p2,p3,p4)
  !! mu+(p1) -> e+(p2) J(p3) y(p4)
  !! mu-(p1) -> e-(p2) J(p3) y(p4)

  real (kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4)
  real (kind=prec) :: Mmu2, Mel2, Mj2, Mmu, Mel, Mj
  real (kind=prec) :: s14, s24, s2n, s4n
  real (kind=prec) :: pm2ejg

  s14 = s(p1,p4)
  s24 = s(p2,p4)

  s2n = s(n1,p2)
  s4n = s(n1,p4)

  Mmu2 = sq(p1)
  Mel2 = sq(p2)
  Mj2 = sq(p3)
  Mmu = sqrt(Mmu2)
  Mel = sqrt(Mel2)
  Mj = sqrt(Mj2)

  pm2ejg = (8*alpha*pi*(-8*CLj*CRj*Mel*Mmu*(Mel2*s14*(s14 - s24) &
    + Mj2*s14*s24 - (Mmu2 - s14)*(s14 - s24)*s24) &
    + CLj**2*(-2*Mel2**2*s14*(s14 - s24) + Mel2*(2*Mj2*s14*(s14 &
    - 2*s24) - (s14 - s24)*(2*Mmu2*(s14 - s24) + 2*s14*s24 &
    - Mmu*s24*s4n + 2*Mmu*s14*(s2n + s4n))) + s24*(2*Mj2**2*s14 &
    + (s14 - s24)*(2*Mmu2**2 - 2*Mmu2*s14 + s14*(s14 - s24) &
    - Mmu*s14*s2n + Mmu**3*(2*s2n + s4n)) + Mj2*(2*s14*(s14 - s24) &
    + Mmu2*(-4*s14 + 2*s24) + Mmu*s24*s4n - Mmu*s14*(2*s2n + s4n)))) &
    + CRj**2*(-2*Mel2**2*s14*(s14 - s24) + Mel2*(2*Mj2*s14*(s14 &
    - 2*s24) - (s14 - s24)*(2*Mmu2*(s14 - s24) + 2*s14*s24 &
    + Mmu*s24*s4n - 2*Mmu*s14*(s2n + s4n))) + s24*(2*Mj2**2*s14 &
    + (s14 - s24)*(2*Mmu2**2 - 2*Mmu2*s14 + s14*(s14 - s24) &
    + Mmu*s14*s2n - Mmu**3*(2*s2n + s4n)) + Mj2*(2*s14*(s14 &
    - s24) + Mmu2*(-4*s14 + 2*s24) - Mmu*s24*s4n + Mmu*s14*(2*s2n &
    + s4n))))))/(s14**2*s24**2)

  END FUNCTION PM2EJG





                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!        MUON  DECAY          !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4)
  ! for massive (and massless) electron
  FUNCTION M2ENN(q1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: s12,s13,s14,s23,s24,s34
  real (kind=prec) :: m2enn

  s12 = s(q1,q2); s13 = s(q1,q3); s14 = s(q1,q4);
  s23 = s(q2,q3); s24 = s(q2,q4);
  s34 = s(q3,q4);

  m2enn = 4.*s13*s24

  m2enn = 8.*GF**2*m2enn

  m2enn = 0.5*m2enn  ! spin average

  END FUNCTION M2ENN



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4)
  ! for massive (and massless) electron
  ! average over neutrino tensor taken
  FUNCTION M2ENNav(q1,q2, q3, q4, lin)
  real (kind=prec) :: q1(4),q2(4), q3(4), q4(4)
  real (kind=prec), optional :: lin
  real (kind=prec) :: s12
  real (kind=prec) :: m2ennav, Mm2, Me2

   !call crash("m2ennav")

  s12 = s(q1,q2);
  Mm2 = sq(q1)
  Me2 = sq(q2)

  m2ennav = (-8*Me2*Mm2)/3. + 2*Me2*s12 + 2*Mm2*s12 - (4*s12**2)/3.

  m2ennav = 8.*GF**2*m2ennav

  m2ennav = 0.5*m2ennav  ! spin average

  if (present(lin)) then
    ! d-dim neutrino
    lin = (8*me2*Mm2)/9. - 2*me2*s12 - 2*Mm2*s12 + (16*s12**2)/9.
    lin = 8.*GF**2*lin

    lin = 0.5*lin  ! spin average

  endif

  END FUNCTION M2ENNav

  FUNCTION PM2ENNav(q1,n1,q2, q3, q4, lin)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4)
    !! for massive (and massless) electron
    !! average over neutrino tensor taken

  real (kind=prec) :: q1(4),q2(4),n1(4), q3(4), q4(4)
  real (kind=prec), optional :: lin
  real (kind=prec) :: s12, s2nm
  real (kind=prec) :: pm2ennav, me2, mm2

  mm2 = sq(q1) ; me2 = sq(q2)

  s12 = s(q1,q2); s2nm = sqrt(mm2) * s(n1,q2)

  pm2ennav = (-8*Me2*Mm2)/3. + 2*Me2*s12 + 2*Mm2*s12 - (4*s12**2)/3.
  pm2ennav = pm2ennav - 2./3. * s2nm * (3*me2+mm2-2*s12)
  pm2ennav = 8.*GF**2*pm2ennav
  pm2ennav = 0.5*pm2ennav  ! spin average

  if (present(lin)) then
    ! d-dim neutrino
    lin = 2./9.*(8.*s12*(s12 - s2nm) + Mm2*(-9*s12 + 7*s2nm) + me2*(4*Mm2 - 9*s12 + 9*s2nm))
    lin = 8.*GF**2*lin
    lin = 0.5*lin  ! spin average
  endif
  END FUNCTION PM2ENNav


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4)
  ! for massless electron
  FUNCTION M2ENNL0(q1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: s12, mu2, mm2, mmx
  real (kind=prec) :: LogMM,LogSM,Log1,Li2SM
  real (kind=prec) :: m2ennl0, delta, logz,img

  s12 = s(q1,q2)

  mm2 = sq(q1)
  mmx = sqrt(mm2)

  mu2 = musq
  LogMM = log(mm2/mu2)
  LogSM = Log(s12/mm2)
  Log1 = log(1. - s12/mm2)
  Li2SM = Li2(s12/mm2,img)

  if(abs(sq(q2))/mm2 > 1.0E-10) then
    ! Massify the result
    logz = log(sq(q2)/mm2)/2.

    delta = 1 + logMM**2/4. + logMM*(-0.25 + logz) - logz/2. + logz**2 + Pi**2/24.

    delta = delta*(2*mm2*s12 - 4*s12**2/3.)
  endif

  ! This is in HV

  m2ennl0 =  -6*mm2*s12 + 2*Li2SM*mm2*s12 + (5*LogMM*mm2*s12)/2. -   &
     (LogMM**2*mm2*s12)/2. + 2*LogSM*mm2*s12 + 2*Log1*LogSM*mm2*s12 -   &
     2*LogMM*LogSM*mm2*s12 - 2*LogSM**2*mm2*s12 -   &
     (5*mm2*Pi**2*s12)/12. + 4*s12**2 - (4*Li2SM*s12**2)/3. -   &
     (5*LogMM*s12**2)/3. + (LogMM**2*s12**2)/3. - 2*LogSM*s12**2 -   &
     (4*Log1*LogSM*s12**2)/3. + (4*LogMM*LogSM*s12**2)/3. +   &
     (4*LogSM**2*s12**2)/3. + (5*Pi**2*s12**2)/18.

  m2ennl0 = m2ennl0+delta

  M2ENNL0 = 4.*alpha/(2.*pi)*GF**2*M2ENNL0

  M2ENNL0 = 2.*M2ENNL0   !! from 2*Re(A0* A1)

  END FUNCTION M2ENNL0



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4)
  ! for massive electron
  FUNCTION M2ENNL(q1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: s12, mu2,me2, mm2, mex, mmx
  real (kind=prec) :: LogMM,LogME,LL,LM
  real (kind=prec) :: TS,TV1,TV2,TT11,TT12,TT22,TTg,wP,wM
  real (kind=prec) :: m2ennl

  s12 = s(q1,q2)

!  me2 = me**2
!  mm2 = mm**2
  mm2 = sq(q1)
  me2 = abs(sq(q2))
  mex = sqrt(me2)
  mmx = sqrt(mm2)

  mu2 = musq
  LogME = log(me2/mu2)
  LogMM = log(mm2/mu2)
  LM = Log(me2/mm2)

  wM = Sqrt(-2*mex*mmx + s12)
  wP = Sqrt(2*mex*mmx + s12)
  LL = Log(me2) + Log(mm2) - 2*Log(s12/2. + wP*wM/2.)

  TS = (-Pi**2/6. + LM**2/8. +   &
       Log((-wM + wP)/(wM + wP))*(2*Log((4*wM*wP)/(wM + wP)**2) -    &
       Log((-wM + wP)/(wM + wP))/2. + Log((mex*mmx)/mu2)) +    &
       Li2((-wM + wP)**2/(wM + wP)**2) +    &
       Li2(1 - (mex*(-wM + wP))/(mmx*(wM + wP))) +    &
       Li2(1 - (mmx*(-wM + wP))/(mex*(wM + wP))))/(wM*wP)

  TV1 = (2*LL*me2 - LL*s12 + wM*wP*LM)/(2.*(me2 + mm2 - s12)*wM*wP)
  TV2 = (2*LL*mm2 - LL*s12 - wM*wP*LM)/(2.*(me2 + mm2 - s12)*wM*wP)

  TT11 = (2*(me2 + mm2 - s12)*(4*me2*mm2 - s12**2) + LM*(-2*me2 + s12)*(4*me2*mm2 - s12**2) +    &
      LL*(2*me2**2 + s12**2 - 2*me2*(mm2 + s12))*wM*wP)/   &
      (4.*(me2 + mm2 - s12)**2*(-4*me2*mm2 + s12**2))
  TT12 =  -(2*(me2 + mm2 - s12)*(4*me2*mm2 - s12**2) - LM*mm2*(4*me2**2 + s12**2) +    &
        LM*me2*(4*mm2**2 + s12**2) + 2*LL*(-2*me2*mm2 + ((me2 + mm2)*s12)/2.)*wM*wP)/   &
      (4.*(me2 + mm2 - s12)**2*(-4*me2*mm2 + s12**2))
  TT22 =  (2*(me2 + mm2 - s12)*(4*me2*mm2 - s12**2) + LM*(2*mm2 - s12)*(4*me2*mm2 - s12**2) +    &
       LL*(2*mm2**2 + s12**2 - 2*mm2*(me2 + s12))*wM*wP)/   &
      (4.*(me2 + mm2 - s12)**2*(-4*me2*mm2 + s12**2))
  TTg =  (6*me2 + 6*mm2 - 6*s12 - LL*wM*wP + (-2*me2 + s12)*LogME +    &
        (-2*mm2 + s12)*LogMM)/(8.*(me2 + mm2 - s12))

  m2ennl =   8*me2*mm2 - 2*LogME*me2*mm2 - 2*LogMM*me2*mm2 - 6*me2*s12 + (3*LogME*me2*s12)/2. +    &
      (3*LogMM*me2*s12)/2. - 6*mm2*s12 + (3*LogME*mm2*s12)/2. + (3*LogMM*mm2*s12)/2. +    &
      4*s12**2 - LogME*s12**2 - LogMM*s12**2 - (8*me2*mm2*s12*TS)/3. + 2*me2*s12**2*TS +    &
      2*mm2*s12**2*TS - (4*s12**3*TS)/3. + 2*me2*mm2*s12*TT11 + 2*mm2**2*s12*TT11 -    &
      2*mm2*s12**2*TT11 + (8*me2**2*mm2*TT12)/3. + (8*me2*mm2**2*TT12)/3. -    &
      (8*me2*mm2*s12*TT12)/3. + (4*me2*s12**2*TT12)/3. + (4*mm2*s12**2*TT12)/3. -    &
      (4*s12**3*TT12)/3. + 2*me2**2*s12*TT22 + 2*me2*mm2*s12*TT22 - 2*me2*s12**2*TT22 -    &
      (16*me2*mm2*TTg)/3. + 4*me2*s12*TTg + 4*mm2*s12*TTg - (8*s12**2*TTg)/3. +    &
      4*me2**2*mm2*TV1 + 4*me2*mm2**2*TV1 - (10*me2*mm2*s12*TV1)/3. - 2*mm2**2*s12*TV1 -    &
      2*me2*s12**2*TV1 + (4*s12**3*TV1)/3. + 4*me2**2*mm2*TV2 + 4*me2*mm2**2*TV2 -    &
      2*me2**2*s12*TV2 - (10*me2*mm2*s12*TV2)/3. - 2*mm2*s12**2*TV2 + (4*s12**3*TV2)/3.

  M2ENNL = 4.*alpha/(2.*pi)*GF**2*M2ENNL

  M2ENNL = 2.*M2ENNL   !! from 2*Re(A0* A1)

  END FUNCTION M2ENNL


  FUNCTION PM2ENNLav(q1, n1, q2, q3, q4, pole,cdr)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! average over neutrino tensor taken
    !! for massive electron
  real (kind=prec), intent(in) :: q1(4),q2(4), n1(4), q3(4), q4(4)
  real (kind=prec) :: pm2ennlav
  real (kind=prec), optional :: pole, cdr
  real (kind=prec) :: s12, s2nm, me2, mm2, me, mm, sing
  real (kind=prec) :: disk, lam, born, logz, logmu, lin

  mm2 = sq(q1) ; mm = sqrt(mm2)
  me2 = sq(q2) ; me = sqrt(me2)
  s12 = s(q1, q2) ; s2nm = mm * s(n1,q2)

  lam = s12**2 - 4*me2*mm2
  disk = DiscB(me2+mm2-s12, me, mm)
  logZ = log(me2/mm2) ; logMu = log(musq / mm2)
  born = pm2ennav(q1, n1, q2,q3, q4, lin)/(4*GF**2)
  lin = lin/(4*GF**2)

  sing = -born + (born*disk*(me2 + mm2 - s12)*s12)/lam

  if (present(pole)) then
    pole = sing
    pole = 8.*GF**2*pole / pi * alpha
    pole = 0.5*pole  ! spin average
  endif
  pm2ennlav = -(born*logmu) + (2*(me2*(20*mm2 - 9*s12) + s12*(-9&
           &*mm2 + 4*s12)))/9. + (logz*((3*mm2 - 4*s12)*s12 + &
           &me2*(-8*mm2 + 9*s12)))/6. - (disk*(me2 + mm2 - s12&
           &)*(144*me2**2*mm2 + 9*born*(-2*logmu + logz)*s12 +&
           & 4*s12**3 + 16*me2*(9*mm2**2 - 10*mm2*s12)))/(18.*&
           &lam) + born*s12*ScalarC0IR6(me2 + mm2 - s12,me,mm)
  pm2ennlav = pm2ennlav - s2nm * 2./9.*(-9*me2 + Mm2 + 4*s12)
  pm2ennlav = pm2ennlav - s2nm * 1./6.*( 9*me2 + Mm2 - 4*s12) * logz
  pm2ennlav = pm2ennlav - s2nm * 2./9.*(me2+mm2-s12)*(12*me2*mm2 - s12*(4*mm2 + s12))*disk/lam

  ! CDR -> FDH
  if (.not.(present(cdr))) then
  pm2ennlav = pm2ennlav - lin * sing / born
  endif

  pm2ennlav = 8.*GF**2*pm2ennlav / pi * alpha
  pm2ennlav = 0.5*pm2ennlav  ! spin average


  END FUNCTION PM2ENNLav


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4)
  ! for massive (and massless) electron
  FUNCTION PM2ENN(q1,n1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: s12,s13,s14,s23,s24,s34,s3n
  real (kind=prec) :: pm2enn, Mmsq, Mesq, MmLoc

  s12 = s(q1,q2); s13 = s(q1,q3); s14 = s(q1,q4);
  s23 = s(q2,q3); s24 = s(q2,q4);
  s34 = s(q3,q4); s3n = s(q3,n1);

  Mmsq = 0.5*s(q1,q1)
  Mesq = 0.5*s(q2,q2)
  MmLoc = Sqrt(Mmsq)

  pm2enn = 2*s13*s24 - 2*MmLoc*s24*s3n

  pm2enn = 8.*GF**2*pm2enn

!  pm2enn = pm2enn  ! NO spin average

  END FUNCTION PM2ENN


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(p5)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(p5)
  ! for massive (and massless) electron
  FUNCTION M2ENNG(q1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4)
  real (kind=prec) :: s12,s13,s14,s15,s23,s24,s25,s34,s35,s45
  real (kind=prec) :: m2enng, Mmsq, Mesq

  s12 = s(q1,q2); s13 = s(q1,q3); s14 = s(q1,q4); s15 = s(q1,q5);
  s23 = s(q2,q3); s24 = s(q2,q4); s25 = s(q2,q5);
  s34 = s(q3,q4); s35 = s(q3,q5);
  s45 = s(q4,q5);

  Mmsq = 0.5*s(q1,q1)
  Mesq = 0.5*s(q2,q2)

  m2enng = (-16*Mmsq*s13*s24 + 16*Mmsq*s24*s35 + 8*s15*s24*s35)/(-s15)**2 + &
           (-16*Mesq*s13*s24 - 16*Mesq*s13*s45 + 8*s13*s25*s45)/(s25)**2 + &
           2.*(-8*s12*s13*s24 - 4*s13*s15*s24 - 4*s15*s23*s24 + 4*s13*s14*s25 + &
                4*s13*s24*s25 + 4*s12*s24*s35 - 4*s12*s13*s45)/(-s15*s25)

  m2enng = 8.*GF**2*(4.*pi*alpha)*m2enng

  m2enng = 0.5*m2enng  ! spin average

  END FUNCTION M2ENNG




  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(p5)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(p5)
  ! for massive (and massless) electron
  FUNCTION PM2ENNG(q1,n1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),n1(4),q2(4),q3(4),q4(4),q5(4)
  real (kind=prec) :: s12,s13,s14,s15,s23,s24,s25,s34,s35,s45
  real (kind=prec) :: s2n,s3n,s4n,s5n
  real (kind=prec) :: pm2enng,den1,den2
  real (kind=prec) :: if11,if22,if12
  real (kind=prec) :: Mm2, MmLoc, Me2

  Mm2 = sq(q1) ; Me2 = sq(q2)
  MmLoc = sqrt(Mm2)

  s12 = s(q1,q2); s13 = s(q1,q3); s14 = s(q1,q4); s15 = s(q1,q5);
  s23 = s(q2,q3); s24 = s(q2,q4); s25 = s(q2,q5);
  s34 = s(q3,q4); s35 = s(q3,q5);
  s45 = s(q4,q5);

  s2n = s(q2,n1); s3n = s(q3,n1); s4n = s(q4,n1); s5n = s(q5,n1);

  den1 = -s15
  den2 = s25

  if11 = -8*Mm2*s13*s24 + 8*Mm2*s24*s35 + 4*s15*s24*s35 + 8*Mm2*Mmloc*s24*s3n &
       - 4*Mmloc*s15*s24*s3n + 4*Mmloc*s13*s24*s5n - 4*Mmloc*s24*s35*s5n

  if22 = -8*Me2*s13*s24 + 8*Me2*Mmloc*s24*s3n - 8*Me2*s13*s45 &
       + 4*s13*s25*s45 + 8*Me2*Mmloc*s3n*s45 - 4*Mmloc*s25*s3n*s45

  if12 = -8*s12*s13*s24 - 4*s13*s15*s24 - 4*s15*s23*s24 + 4*s13*s14*s25 &
       + 4*s13*s24*s25 + 4*s12*s24*s35 - 4*Mmloc*s24*s2n*s35 + 8*Mmloc*s12*s24*s3n &
       + 4*Mmloc*s15*s24*s3n - 4*Mmloc*s14*s25*s3n - 4*Mmloc*s24*s25*s3n &
       - 4*s12*s13*s45 + 4*Mmloc*s12*s3n*s45 + 4*Mmloc*s23*s24*s5n

  pm2enng = if11/den1**2 + if22/den2**2 + if12/(den1*den2)

  pm2enng = 8.*GF**2*(4.*pi*alpha)*pm2enng  ! no spin average

  END FUNCTION PM2ENNG



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(p5)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(p5)
  ! average over neutrino tensor taken
  ! for massive (and massless) electron
  FUNCTION M2ENNGav(q1,q2,q3, q4, q5)

  real (kind=prec) :: q1(4),q2(4),q5(4), q3(4), q4(4)
  real (kind=prec) :: s12,s15,s25
  real (kind=prec) :: m2enngav, Mmsq, Mesq, i11, i22, i12

  s12 = s(q1,q2);  s15 = s(q1,q5);  s25 = s(q2,q5);

  Mmsq = 0.5*s(q1,q1)
  Mesq = 0.5*s(q2,q2)

  i11 = (8*Mesq*MMsq**2)/3. - 2*Mesq*MMsq*s12 - 2*MMsq**2*s12 + (4*MMsq*s12**2)/3. &
      - (8*Mesq*MMsq*s15)/3. + 2*MMsq*s12*s15 - (2*Mesq*s15**2)/3. + (s12*s15**2)/3. &
      + 2*Mesq*MMsq*s25 + 2*MMsq**2*s25 - (8*MMsq*s12*s25)/3. + Mesq*s15*s25 - (5*MMsq*s15*s25)/3. &
      - (2*s12*s15*s25)/3. - (2*s15**2*s25)/3. + (4*MMsq*s25**2)/3. + (2*s15*s25**2)/3.

  i22 = (8*Mesq**2*MMsq)/3. - 2*Mesq**2*s12 - 2*Mesq*MMsq*s12 + (4*Mesq*s12**2)/3. &
      - 2*Mesq**2*s15 - 2*Mesq*MMsq*s15 + (8*Mesq*s12*s15)/3. + (4*Mesq*s15**2)/3. &
     + (8*Mesq*MMsq*s25)/3. - 2*Mesq*s12*s25 - (5*Mesq*s15*s25)/3. + MMsq*s15*s25 - (2*s12*s15*s25)/3. &
     - (2*s15**2*s25)/3. - (2*MMsq*s25**2)/3. + (s12*s25**2)/3. + (2*s15*s25**2)/3.

  i12 =  (4*Mesq*MMsq*s12)/3. - Mesq*s12**2 - MMsq*s12**2 + (2*s12**3)/3. - Mesq**2*s15  &
       + (Mesq*MMsq*s15)/3. - (2*Mesq*s12*s15)/3. - MMsq*s12*s15 + (4*s12**2*s15)/3. &
      + (2*s12*s15**2)/3. - (Mesq*MMsq*s25)/3. + MMsq**2*s25 + Mesq*s12*s25 + (2*MMsq*s12*s25)/3. &
      - (4*s12**2*s25)/3. - (2*Mesq*s15*s25)/3. - (2*MMsq*s15*s25)/3. - s12*s15*s25 + (2*s12*s25**2)/3.

  m2enngav = 4*i11/(-s15)**2 + 4*i22/(s25)**2 + 8.*i12/(-s15*s25)

  m2enngav = 8.*GF**2*(4.*pi*alpha)*m2enngav

  m2enngav = 0.5*m2enngav  ! spin average

  END FUNCTION M2ENNGav










#ifdef RADMUDECCDR

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) y(q5) + ( \nu_e \bar{\nu}_\mu )
  ! mu-(p1) -> e-(p2) y(q5) + ( \bar{\nu}_e \nu_\mu )
  ! for massive electron
  ! average over neutrino tensor taken
  FUNCTION PM2ENNGLavcdr(q1,n,q2,q5, singlepole)


    ! The muon is understood to be polarised, and the polarisation vector is n.
    ! Polarisation is obtained by the substitution (see Okun)
    ! \sum u_{q,m}*\bar{u}_{q1,m} ---> (\slash{q}+I*m)*(1/2*(I+\gamma^5*\slash{n}));
    !
    ! The output is: 2*Re[One-Loop*Tree-level].
    !
    ! Regularisation: Conventional Dimensional Regularisation (CDR), naif anticommuting $\gamma^5$ is adopted.
    !
    ! Renormalisation: On-shell is adopted for external massive particles and internal massive propagators.
    !
    ! The result is averaged over the momenta of the neutrinos.

    ! In the subroutine the following shift is applied:
    ! q1=p1
    ! q2=p1-p2
    ! q3=p2-p3
    ! q4=p3-p4
    ! q5=p4


    ! Functions are in radiative/
    use mudec_T1BOXAC
    use mudec_T2BOXAC
    use mudec_T1BOXBC
    use mudec_T1BOXAD
    use mudec_T2BOXAC
    use mudec_T2BOXBC
    use mudec_T2BOXAD
    use mudec_T1TRIAC
    use mudec_T1TRIBC
    use mudec_T1TRIAD
    use mudec_T2TRIAC
    use mudec_T2TRIBC
    use mudec_T2TRIAD
    use mudec_T3TRIAC
    use mudec_T3TRIBC
    use mudec_T3TRIAD
    use mudec_T4TRIAC
    use mudec_T4TRIBC
    use mudec_T4TRIAD
    use mudec_T1BUBAC
    use mudec_T1BUBBC
    use mudec_T1BUBAD
    use mudec_CTOSAC
    use mudec_CTOSBC
    use mudec_CTOSAD
    use mudec_CTTBAC
    use mudec_CTTBBC
    use mudec_CTTBAD
    use mudec_TLEPS0
    use mudec_TLEPS1
#ifdef COLLIER
    use collier
#endif

  real (kind=prec), intent(out), optional :: singlepole
  real (kind=prec) :: q1(4),q2(4),q5(4), n(4)
  real (kind=prec) :: el, mm2, me2

  real (kind=prec) :: pm2ennglavcdr
  real (kind=prec) :: boxes, triangles, bubbles, renorm
  real (kind=prec) :: pole, finite, born, borneps
  real (kind=prec) :: boxes_pole, triangles_pole, bubbles_pole, renorm_pole


  real (kind=prec), dimension(4) :: p1, p2, p4
  real (kind=prec) :: p1p2, p1p4, p2p4,  np2, np3, np4, asym

  p1 = q1
  p2 = q1-q2
  p4 = q5

  el = sqrt(4*pi*alpha)

  mm2 = sq(q1)
  me2 = abs(sq(q2))

  p1p2 = s(p1, p2) / 2._prec
  p1p4 = s(p1, p4) / 2._prec
  p2p4 = s(p2, p4) / 2._prec

  np2 = s(n, p2) / 2._prec
  np4 = s(n, p4) / 2._prec
  np3 = 0._prec ! The result does not depend on np3

#ifdef COLLIER
  call initcachesystem_cll(1,4)
#endif

  asym =  -n(4)*p1(3)*p2(2)*p4(1) + n(3)*p1(4)*p2(2)*p4(1) + n(4)*p1(2)*p2(3)*p4(1) - &
           n(2)*p1(4)*p2(3)*p4(1) - n(3)*p1(2)*p2(4)*p4(1) + n(2)*p1(3)*p2(4)*p4(1) + &
           n(4)*p1(3)*p2(1)*p4(2) - n(3)*p1(4)*p2(1)*p4(2) - n(4)*p1(1)*p2(3)*p4(2) + &
           n(1)*p1(4)*p2(3)*p4(2) + n(3)*p1(1)*p2(4)*p4(2) - n(1)*p1(3)*p2(4)*p4(2) - &
           n(4)*p1(2)*p2(1)*p4(3) + n(2)*p1(4)*p2(1)*p4(3) + n(4)*p1(1)*p2(2)*p4(3) - &
           n(1)*p1(4)*p2(2)*p4(3) - n(2)*p1(1)*p2(4)*p4(3) + n(1)*p1(2)*p2(4)*p4(3) + &
           n(3)*p1(2)*p2(1)*p4(4) - n(2)*p1(3)*p2(1)*p4(4) - n(3)*p1(1)*p2(2)*p4(4) + &
           n(1)*p1(3)*p2(2)*p4(4) + n(2)*p1(1)*p2(3)*p4(4) - n(1)*p1(2)*p2(3)*p4(4)

  boxes     = T1BOXAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  ! Box 1
            + T1BOXBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  !
            + T2BOXAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  ! Box 2
            + T2BOXBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)     !

  bubbles   = T1BUBAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  !
            + T1BUBBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)     !

  triangles = T1TRIAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  ! Triangle 1
            + T1TRIBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  !
            + T2TRIAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  ! Triangle 2
            + T2TRIBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  !
            + T3TRIAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  ! Triangle 3
            + T3TRIBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  !
            + T4TRIAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)  &  ! Triangle 4
            + T4TRIBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)     !

  ! Renormalisation
  renorm = CTOSAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) & ! Counter terms
         + CTOSBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)

  renorm_pole = CTOSAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)   ! Counter terms

! Do not renormalise charge and photon WF
!  renorm = renorm + CTTBAD(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) & ! Charge and photon WF
!                  + CTTBBC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)
!  renorm_pole = renorm_pole + CTTBAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) & ! Charge and photon WF


  boxes_pole     = T1BOXAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) & ! Box 1
                 + T2BOXAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)   ! Box 2

  bubbles_pole   = T1BUBAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)

  triangles_pole = T1TRIAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) & ! Triangle 1
                 + T2TRIAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) & ! Triangle 2
                 + T3TRIAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) & ! Triangle 3
                 + T4TRIAC(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)   ! Triangle 4

  pole = boxes_pole + bubbles_pole + triangles_pole + renorm_pole
  finite = boxes + bubbles + triangles + renorm

  ! Convert to FDH
  born    = TLEPS0(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)
  borneps = TLEPS1(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq)

  if (present(singlepole)) then
    singlepole = pole
  endif
  pm2ennglavcdr = finite - borneps*pole/born


  END FUNCTION PM2ENNGLavcdr

#endif



  FUNCTION PM2ENNGav(q1,n,q2,q3,q4,q5, linear)
    !! mu+(p1) -> e+(p2) y(q5) + ( \nu_e \bar{\nu}_\mu )
    !! mu-(p1) -> e-(p2) y(q5) + ( \bar{\nu}_e \nu_\mu )
    !! for massive electron
    !! average over neutrino tensor taken

    ! The muon is understood to be polarised, and the polarisation vector is n.
    ! Polarisation is obtained by the substitution (see Okun)
    ! \sum u_{q,m}*\bar{u}_{q1,m} ---> (\slash{q}+I*m)*(1/2*(I+\gamma^5*\slash{n}));
    !
    ! The result is averaged over the momenta of the neutrinos.

    ! In the subroutine the following shift is applied:
    ! q1=p1
    ! q2=p1-p2
    ! q3=p2-p3
    ! q4=p3-p4
    ! q5=p4
  ! Functions are in radiative/
  use mudec_TLEPS0
  use mudec_TLEPS1
  real (kind=prec), intent(out), optional :: linear
  real (kind=prec) :: q1(4),q2(4),q5(4), n(4), q3(4), q4(4)
  real (kind=prec) :: el, mm2, me2

  real (kind=prec) :: pm2enngav



  real (kind=prec), dimension(4) :: p1, p2, p4
  real (kind=prec) :: p1p2, p1p4, p2p4, asym, np2, np3, np4

  p1 = q1
  p2 = q1-q2
  p4 = q5

  el = sqrt(4*pi*alpha)

  mm2 = sq(q1)
  me2 = abs(sq(q2))

  p1p2 = s(p1, p2) / 2._prec
  p1p4 = s(p1, p4) / 2._prec
  p2p4 = s(p2, p4) / 2._prec

  np2 = s(n, p2) / 2._prec
  np4 = s(n, p4) / 2._prec
  np3 = 0._prec ! The result does not depend on np3


  PM2ENNGav = TLEPS0(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) / 2.

  if (present(linear)) then
    linear = TLEPS1(el,GF,me2,mm2,p1p2,p1p4,p2p4,asym,np2,np3,np4,musq) / 2.
  endif

  END FUNCTION PM2ENNGav






          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!             NNLO                   !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION M2ENNeik(p1, p2, xicut, pole, lin)
  real(kind=prec) :: m2enneik
  real(kind=prec), intent(in) :: p1(4), p2(4), xicut
  real(kind=prec), intent(out), optional :: pole, lin
  real(kind=prec) :: Epart
  Epart = sqrt(sq(p1))

  m2enneik = + alpha/(2.*pi)*(2*Ireg(xicut,Epart,p1,p2) &
            - Ireg(xicut,Epart,p1)- Ireg(xicut,Epart,p2))

  if(present(pole)) then
    pole     =  + alpha/(2.*pi)*(2*Isin(xicut,Epart,p1,p2) &
             - Isin(xicut,Epart,p1)- Isin(xicut,Epart,p2))
  endif
  if (present(lin)) then ! TODO
    lin      =  + alpha/(2.*pi)*(2*Ilin(xicut,Epart,p1,p2) &
             - Ilin(xicut,Epart,p1)- Ilin(xicut,Epart,p2))
  endif
  END FUNCTION M2ENNeik

  FUNCTION M2ENNeikz(p1, p2, xicut, pole)
  real(kind=prec) :: m2enneikz
  real(kind=prec) :: x, z, m2, log1mx, logz, beta
  real(kind=prec), parameter :: z2 = 1.64493406684822643647241516665_prec
  real(kind=prec), intent(in) :: p1(4), p2(4), xicut
  real(kind=prec), intent(out), optional :: pole

  if (sum(abs(p1(1:3)) ) > zero) call crash('m2enneikz')

  m2 = sq(p1)
  x = sq(p1-p2) / m2
  z = sqrt(sq(p2) / m2)
  log1mx = log(1-x)
  logz = log(z)
  if(present(pole)) then
    pole = (1-log1mx + logz) / pi
  endif

  beta = sqrt(1 - 2*x + x**2 - 2*z**2 - 2*x*z**2 + z**4)/(1 - x + z**2)
  m2enneikz = (1 - z2 - log1mx**2 - logz - logz**2 + log1mx*(1 + 2*logz) &
          + log(musq/M2)*(1-log1mx+logz))/pi
  m2enneikz = m2enneikz + (-2*beta + Log((1 + beta)/(1 - beta)))/ beta / pi * log(xicut)
  END FUNCTION M2ENNeikz



  FUNCTION PM2ENNLav2(p1, n1, p2, p3, p4, pole, lin)
  real(kind=prec) :: pm2ennlav2
  real(kind=prec), intent(in) :: p1(4), n1(4), p2(4), p3(4), p4(4)
  real(kind=prec), intent(out), optional :: pole, lin
  real(kind=prec) :: Mm2, z, chi
  real(kind=prec) :: l(-1:1), l1, l2, l3, l4, l5, l6, l4m, l5m, l6m
  real(kind=prec) :: s2nM, s12
  real(kind=prec) :: F(1:2,-1:1), G(1:2, -1:1), ans(-1:1)
  real(kind=prec), parameter :: z2 = 1.64493406684822643647241516665_prec
  real(kind=prec), parameter :: z3 = 1.20205690315959428539973816151_prec
  Mm2 = sq(p1)
  s12 = s(p1, p2)
  s2nM = s(n1, p2) * sqrt(Mm2)
  z = sqrt(sq(p2) / Mm2)
  chi = (s12 - sqrt(s(p1,p2)**2 - 4* Mm2**2*z**2))/(2*Mm2*z)

  l1 = (chi**2-1) / (chi*(chi-z))
  l2 = z * chi * l1
  l3 = (chi**2+1)/(chi**2-1)
  l4 = 3 * l3+2*chi/(chi**2-1)
  l4m= (3 - 2*chi + 3*chi**2)/(chi**2-1)
  l5 = -(1+z)*(1-chi)*chi / (z-chi) / (1+chi) / (z*chi-1)
  l5m= -(1-z)*(1+chi)*chi / (z-chi) / (1-chi) / (z*chi-1)
  l6 = 2*(z-1) * chi / (z-chi) / (z*chi-1)
  l6m= 2*(-z-1) * chi / (z-chi) / (z*chi-1)

  L(-1) = log(1-l2) - log(1-l1)
  L( 0) = log(1-l1)**2/4 - log(1-l2)**2/4 + Li2(l1)-Li2(l2) - log(1-l1)*log(z)
  L( 1) = Li3(l1) - Li3(l2) &
   + (Li3(1/(1-l1)) - log(1-l1)**3/6 + log(1-l1)**2*log(-l1)/2 + z2*log(1-l1)- z3)/2 &
   - (Li3(1/(1-l2)) - log(1-l2)**3/6 + log(1-l2)**2*log(-l2)/2 + z2*log(1-l2)- z3)/2 &
   - log(1-l1)**3 / 24 + log(1-l2)**3 / 24 &
   + log(z) * (Li2(l1) + log(1-l1)**2/4) &
   - log(z)**2 * log(1-l1)/2


  F(1,-1) = -2 + l3 * L(-1)
  F(1, 0) = -4 - 2*l3*L(0) + 2*log(z) + l4 * L(-1)/2
  F(1,+1) = -8 - 2*z2 + l3*z2*L(-1) + (7 + 2*chi + 7*chi**2) / (chi**2-1) / 2 * L(-1) - l4*L(0) + 4*l3*L(1) + 4*log(z) -2*log(z)**2

  F(2,-1) = 0._prec
  F(2, 0) = l5 * L(-1) - l6 * log(z)
  F(2,+1) = 3*l5*L(-1) - 2*l5*L(0) - 3*l6*log(z) + l6*log(z)**2

  G(1,-1) = -2 + l3 * L(-1)
  G(1, 0) = -4 - 2*l3*L(0) + 2*log(z) + l4m * L(-1)/2
  G(1,+1) = -8 - 2*z2 + l3*z2*L(-1) + (7-2*chi+7*chi**2) / (chi**2-1) / 2 * L(-1) - l4m*L(0) + 4*l3*L(1) + 4*log(z) -2*log(z)**2

  G(2,-1) = 0._prec
  G(2, 0) = l5m* L(-1) - l6m * log(z)
  G(2,+1) = 3*l5m*L(-1) - 2*l5m*L(0) - 3*l6m*log(z) + l6m*log(z)**2


  F = F * alpha / 2 / pi
  G = G * alpha / 2 / pi

  ans = F(1,:) * (4*s12*(s2nm-s12) - 4*Mm2**2*z*(3 + 2*z + 3*z**2) + 2*Mm2*(3*s12*(1 + z)**2 - s2nm*(1 + 3*z**2))) &
      + F(2,:) * (s2nm + 3*s2nm*z - 3*s12*(1 + z) + 6*Mm2*z*(1 + z))*(-s12 + Mm2*(1 + z**2)) &
      + G(1,:) * (4*s12*(-s12 + s2nm) + 4*Mm2**2*z*(3 - 2*z + 3*z**2) + 2*Mm2*(3*s12*(-1 + z)**2 - s2nm*(1 + 3*z**2))) &
      + G(2,:) * (3*s12*(-1 + z) + s2nm*(1 - 3*z) + 6*Mm2*(-1 + z)*z)*(-s12 + Mm2*(1 + z**2))
  ans = ans * 2 * GF**2 / 3

  ans(1) = ans(1) - ans( 0) * log(mm2/musq) + ans(-1) * log(mm2/musq)**2/2
  ans(0) = ans(0) - ans(-1) * log(mm2/musq)

  if (present(pole)) pole = ans(-1)
  pm2ennlav2 = ans(0)
  if (present(lin )) lin  = ans(+1)
  END FUNCTION PM2ENNLav2

  FUNCTION PM2ENNlavz(p1, n1, p2, p3, p4, pole, lin)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massified electrons
    !! average over neutrino tensor taken
  use mudec_h2lff
  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: pm2ennlavz, ans(-1:1)
  real(kind=prec) :: f1(-1:1),f2(-1:1), x, s2nm, z, Mm2, me2, lm
  real(kind=prec), intent(out), optional :: pole, lin

  Mm2 = sq(p1) ; me2 = sq(p2) ; z = sqrt(me2/mm2)

  s2nm = s(p2, n1) / sqrt(Mm2)
  x = 1. + z**2 - s(p1, p2) / Mm2

  call init_hpls(x, z)

  lm = log(musq/mm2)
  f1 = [ff1a1es(), ff1a1ef(), ff1a1el()] / 2 / pi
  f2 = [0._prec  , ff2a1ef(), ff2a1el()] / 2 / pi

  ans = 8*GF**2*alpha*Mm2**2*(F2*x*(3*x-3 + s2nm) - 2*F1*(2*x**2-x-1 + s2nm*(-1 + 2*x)) )/3

  ans(1) = ans(1) - ans( 0) * log(mm2/musq) + ans(-1) * log(mm2/musq)**2/2
  ans(0) = ans(0) - ans(-1) * log(mm2/musq)

  if (present(pole)) pole = ans(-1)
  pm2ennlavz = ans(0)
  if (present(lin )) lin  = ans(+1)
  END FUNCTION PM2ENNlavz

  FUNCTION PM2ENNllavz(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massified electrons
    !! average over neutrino tensor taken
  use mudec_h2lff
  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: pm2ennllavz
  real(kind=prec) :: f11, f22, f12, x, s2nm, z, Mm2, me2, lm

  Mm2 = sq(p1) ; me2 = sq(p2) ; z = sqrt(me2/mm2)

  s2nm = s(p2, n1) / sqrt(Mm2)
  x = 1. + z**2 - s(p1, p2) / Mm2

  call init_hpls(x, z)

  lm = log(musq/mm2)
  f11 = ff1a1ef()**2 + 2*ff1a1el()*ff1a1es()                  &
        + Lm**2*(2*ff1a1es()**2 + 4*ff1a2ed()) + 2*ff1a2ef()  &
        + Lm*(4*ff1a1ef()*ff1a1es() + 4*ff1a2es())
  f22 = ff2a1ef()**2
  f12 = ff1a1ef()*ff2a1ef() + ff1a1es()*ff2a1el() + ff2a2ef() &
        + Lm*(2*ff1a1es()*ff2a1ef() + 2*ff2a2es())

  pm2ennllavz = f11*4.*(1.+x-2.*x**2) - f22*x*(-2.+x+x**2) + f12*12.*(x-1.)*x
  pm2ennllavz = pm2ennllavz + s2nm * (f11*4.*(1.-2*x) + 4.*f12*x + f22*x*(x-2.))

  pm2ennllavz = pm2ennllavz * GF**2*alpha**2 * Mm2**2 * (2./3.) / 8. / pi**2
  END FUNCTION PM2ENNllavz

  FUNCTION PM2ENNffavz(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massified electrons
    !! average over neutrino tensor taken
  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: pm2ennffavz, Mm2, x
  real(kind=prec) :: ctfin, ctpole, ollin, olfin, born
  real(kind=prec) :: ctfin1

  Mm2 = sq(p1)
  x = sq(p1-p2) / Mm2
#if 0
  ctfin = m2enneik(p1, p2, xieik2, ctpole)
  olfin = pm2ennlav(p1, n1, p2, p3, p4, lin=ollin)
  born  = pm2ennav(p1, n1, p2, p3, p4)

  pm2ennffavz = pm2ennllavz(p1, n1, p2, p3, p4)
  pm2ennffavz = pm2ennffavz + 0.5*(olfin * ctfin + ollin * ctpole)
  pm2ennffavz = pm2ennffavz + 0.5*ctfin**2 * born/2!
#else
  ctfin = m2enneikz(p1, p2, 1._prec, ctpole)
  olfin = pm2ennlavz(p1, n1, p2, p3, p4, lin=ollin)
  born  = 8*GF**2*Mm2**2*(1 + x - 2*x**2 - s(p2,n1) / sqrt(Mm2) *(2*x-1)) / 3

  pm2ennffavz = 2*pm2ennllavz(p1, n1, p2, p3, p4)
  pm2ennffavz = pm2ennffavz + (olfin * ctfin + ollin * ctpole)
  pm2ennffavz = pm2ennffavz + ctfin**2 * born/2!

  ctfin1 = m2enneik(p1, p2, 1._prec)
  ctfin  = m2enneik(p1, p2, xieik2 )

  pm2ennffavz = pm2ennffavz + pm2ennav (p1, n1, p2, p3, p4)*(ctfin**2 - ctfin1**2)/2
  pm2ennffavz = pm2ennffavz + pm2ennlav(p1, n1, p2, p3, p4)*(ctfin    - ctfin1   )
#endif


  END FUNCTION PM2ENNffavz


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!         integrand function         !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 FUNCTION PM2ENNfav(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! for massive electron
    !! average over neutrino tensor taken
 real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4)
 real(kind=prec) :: pm2ennfav, mat0
 mat0 = pm2ennav(p1,pol1,p2,p3,p4)
 pm2ennfav = pm2ennlav(p1,pol1,p2,p3,p4)
 pm2ennfav = pm2ennfav + alpha/(2.*pi)*mat0*Ieik(xieik1,Mm,p1,p2)
 END FUNCTION

 FUNCTION PM2ENNGcav(p1, n1, p2, p3, p4, p5)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
    !! for massive electron
 real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), p5(4)
 real(kind=prec) :: pm2enngcav, mat0
 mat0 = pm2enngav(p1,pol1,p2,p3,p4,p5)
 pm2enngcav = alpha/(2.*pi)*mat0*Ieik(xieik1, Mm, p1, p2)
 END FUNCTION

  FUNCTION PM2EJf(p1,n1,p2,p3)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  real (kind=prec) :: p1(4), n1(4), p2(4), p3(4), pm2ejf
  real (kind=prec) :: mat0
  mat0 = pm2ej(p1,pol1,p2,p3)
  pm2ejf = pm2ejl(p1,pol1,p2,p3)
  pm2ejf = pm2ejf + alpha/(2.*pi)*mat0*Ieik(xieik1, Mm, p1, p2)
  END FUNCTION pM2EJf


                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MUDEC_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!

