
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MUDEC
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use mudec_mat_el

!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains



          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!        SINGULAR    LIMITS          !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! PHOTON EMISSION SOFT LIMIT
  FUNCTION PM2EJG_S(q1,n1,q2,q3)
  !! mu+(p1) -> e+(p2) J(p3) y(ksoft)
  !! mu-(p1) -> e-(p2) J(p3) y(ksoft)

  real (kind=prec) :: q1(4), n1(4), q2(4), q3(4)
  real (kind=prec) :: pm2ejg_s

  pm2ejg_s = 2*eik(q1,ksoft,q2)*pm2ej(q1,n1,q2,q3)
  pm2ejg_s = (4.*pi*alpha)*pm2ejg_s

  END FUNCTION PM2EJG_S





  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
  ! for massive (and massless) electron
  FUNCTION PM2ENNG_S(q1,n1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: pm2enng_s

  pm2enng_s = 2*eik(q1,ksoft,q2)*pm2enn(q1,n1,q2,q3,q4)
  pm2enng_s = (4.*pi*alpha)*pm2enng_s

  END FUNCTION PM2ENNG_S



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5)
  ! for massless electron
  FUNCTION PM2ENNG_C(q1,n1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2enng_c, zz

  zz = q2(4)/(q2(4)+q5(4))

  pm2enng_c =  1._prec/q5(4)/q2(4)*                      &
             (1.+zz**2)/(1.-zz)*pm2enn(q1,n1,q2/zz,q3,q4)
  pm2enng_c = (4.*pi*alpha)*pm2enng_c

  END FUNCTION PM2ENNG_C



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
  ! for massless electron
  FUNCTION PM2ENNG_SC(q1,n1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: pm2enng_sc, s12

  s12 = sq(q1)

  pm2enng_sc =  8._prec/s12*pm2enn(q1,n1,q2,q3,q4)
  pm2enng_sc = (4.*pi*alpha)*pm2enng_sc

  END FUNCTION PM2ENNG_SC



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(ksoft)
  ! for massive (and massless) electron
  FUNCTION PM2ENNGG_S(q1,n1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2enngg_s

  pm2enngg_s = 2*eik(q1,ksoft,q2)*pm2enng(q1,n1,q2,q3,q4,q5)
  pm2enngg_s = (4.*pi*alpha)*pm2enngg_s

  END FUNCTION PM2ENNGG_S

  FUNCTION PM2ENNGGav_S(q1,n1,q2,q3,q4,q5)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(ksoft)
    !! for massive (and massless) electron

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2ennggav_s

  pm2ennggav_s = 2*eik(q1,ksoft,q2)*pm2enngav(q1,n1,q2,q3,q4,q5)
  pm2ennggav_s = (4.*pi*alpha)*pm2ennggav_s

  END FUNCTION PM2ENNGGav_S


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(q6)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(q6)
  ! for massless electron
  FUNCTION PM2ENNGG_C(q1,n1,q2,q3,q4,q5,q6)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),q6(4),n1(4)
  real (kind=prec) :: pm2enngg_c, zz

  zz = q2(4)/(q2(4)+q6(4))

  pm2enngg_c =  1._prec/q6(4)/q2(4)*                      &
               (1.+zz**2)/(1.-zz)*pm2enng(q1,n1,q2/zz,q3,q4,q5)
  pm2enngg_c = (4.*pi*alpha)*pm2enngg_c

  END FUNCTION PM2ENNGG_C



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(ksoft)
  ! for massless electron
  FUNCTION PM2ENNGG_SC(q1,n1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2enngg_sc, s12

  s12 = sq(q1)

  pm2enngg_sc =  8._prec/s12*pm2enng(q1,n1,q2,q3,q4,q5)
  pm2enngg_sc = (4.*pi*alpha)*pm2enngg_sc

  END FUNCTION PM2ENNGG_SC



          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!             NNLO                   !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION PM2ENNGav_S(q1,n1,q2,q3,q4,lin)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken

  real (kind=prec) :: q1(4),q2(4),n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real (kind=prec),optional :: lin
  real (kind=prec) :: pm2enngav_s

  ! TODO
  if (present(lin)) then
    pm2enngav_s = 2*eik(q1,ksoft,q2)*pm2ennav(q1,n1,q2,q3,q4, lin)
    pm2enngav_s = (4.*pi*alpha)*pm2enngav_s

    lin = 4*eik(q1,ksoft,q2)*lin
    lin = (4.*pi*alpha)*lin
  else
    pm2enngav_s = 2*eik(q1,ksoft,q2)*pm2ennav(q1,n1,q2,q3,q4)
    pm2enngav_s = (4.*pi*alpha)*pm2enngav_s
  endif

  END FUNCTION PM2ENNGav_S

  FUNCTION PM2ENNGlav_S(q1,n1,q2,q3,q4, sing)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken

  real (kind=prec) :: q1(4),q2(4),n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real (kind=prec) :: pm2ennglav_s,pole
  real (kind=prec), optional :: sing

  ! TODO
  pm2ennglav_s = pm2ennlav(q1,n1,q2,q3,q4,pole)
  pm2ennglav_s = 2*eik(q1,ksoft,q2)*pm2ennglav_s
  pm2ennglav_s = (4.*pi*alpha)*pm2ennglav_s

  if (present(sing)) then
    sing = 2*eik(q1,ksoft,q2)*pole
    sing = (4.*pi*alpha)*sing
  endif
  END FUNCTION PM2ENNGlav_S


  FUNCTION PM2ENNGfav_S(q1,n1,q2,q3,q4)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken

  real (kind=prec) :: q1(4),q2(4),n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real (kind=prec) :: pm2enngfav_s, ctfin, mat0


  mat0 = pm2enngav_s(q1,n1,q2,q3,q4)
  ctfin = m2enneik(q1, q2, xieik1)

  pm2enngfav_s = pm2ennglav_s(q1,n1,q2,q3,q4)

  pm2enngfav_s = pm2enngfav_s+ctfin*mat0

  END FUNCTION PM2ENNGfav_S



  FUNCTION PM2ENNGfzav_S(q1,n1,q2,q3,q4)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massified electron
    !! average over neutrino tensor taken

  real (kind=prec) :: q1(4),q2(4),n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real (kind=prec) :: pm2enngfzav_s, mat0, ctfin, eikmz


  eikmz = 2*s(q1,q2)/(s(q1,ksoft)*s(ksoft,q2))     &
        - 2*sq(q1)/(s(ksoft,q1)**2)
  ! Calculate the pm2ennglav_s bit
  ! m2ennl0 takes all three momenta but it does not depend on q3 and q4
  pm2enngfzav_s = m2ennl0(q1,q2,q1,q2)
  pm2enngfzav_s = 2*eikmz*pm2enngfzav_s
  pm2enngfzav_s = (4.*pi*alpha)*pm2enngfzav_s


  mat0 = pm2enngav_s(q1,n1,q2,q3,q4)
  ctfin = m2enneik(q1, q2, xieik1)

  pm2enngfzav_s = pm2enngfzav_s+ctfin*mat0


  END FUNCTION PM2ENNGfzav_S



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(ksoft) y(p6)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(ksoft) y(p6)
  ! for massive electron
  FUNCTION PM2ENNGG_SH(p1, pol1, p2, p3, p4, p6)
  real(kind=prec) :: p1(4), pol1(4), p2(4), p3(4), p4(4), p6(4)
  real(kind=prec) :: pm2enngg_sh

  pm2enngg_sh = 2*eik(p1,ksoftA,p2)*pm2enng(p1,pol1,p2,p3,p4,p6)
  pm2enngg_sh = (4.*pi*alpha)*pm2enngg_sh

  END FUNCTION PM2ENNGG_SH

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(p5) y(ksoft)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(p5) y(ksoft)
  ! for massive electron
  FUNCTION PM2ENNGG_HS(p1, pol1, p2, p3, p4, p5)
  real(kind=prec) :: p1(4), pol1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: pm2enngg_hs

  pm2enngg_hs = 2*eik(p1,ksoftB,p2)*pm2enng(p1,pol1,p2,p3,p4,p5)
  pm2enngg_hs = (4.*pi*alpha)*pm2enngg_hs

  END FUNCTION PM2ENNGG_HS

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(ksoft) y(ksoft)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(ksoft) y(ksoft)
  ! for massive electron
  FUNCTION PM2ENNGG_SS(p1, pol1, p2, p3, p4)
  real(kind=prec) :: p1(4), pol1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: pm2enngg_ss

  pm2enngg_ss = (2*eik(p1,ksoftB,p2))*(2*eik(p1,ksoftA,p2))*pm2enn(p1,pol1,p2,p3,p4)
  pm2enngg_ss = (4.*pi*alpha)**2*pm2enngg_ss

  END FUNCTION PM2ENNGG_SS


  FUNCTION PM2ENNGGav_SH(p1, n1, p2,q3,q4, p6)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(ksoft) y(p6)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(ksoft) y(p6)
    !! for massive electron
    !! average over neutrino tensor taken
  real(kind=prec) :: p1(4), p2(4), p6(4), n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real(kind=prec) :: pm2ennggav_sh

  pm2ennggav_sh = 2*eik(p1,ksoftA,p2)*pm2enngav(p1,n1, p2,q3,q4,p6)
  pm2ennggav_sh = (4.*pi*alpha)*pm2ennggav_sh

  END FUNCTION PM2ENNGGav_SH

  FUNCTION PM2ENNGGav_HS(p1, n1, p2,q3,q4, p5)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(p5) y(ksoft)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(p5) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken
  real(kind=prec) :: p1(4), p2(4), p5(4), n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real(kind=prec) :: pm2ennggav_hs

  pm2ennggav_hs = 2*eik(p1,ksoftB,p2)*pm2enngav(p1,n1, p2,q3,q4,p5)
  pm2ennggav_hs = (4.*pi*alpha)*pm2ennggav_hs

  END FUNCTION PM2ENNGGav_HS

  FUNCTION PM2ENNGGav_SS(p1, n1, p2,q3,q4)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(ksoft) y(ksoft)
    !! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(ksoft) y(ksoft)
    !! for massive electron
    !! average over neutrino tensor taken
  real(kind=prec) :: p1(4), p2(4), n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real(kind=prec) :: pm2ennggav_ss

  pm2ennggav_ss = (2*eik(p1,ksoftB,p2))*(2*eik(p1,ksoftA,p2))*pm2ennav(p1,n1, p2,q3,q4)
  pm2ennggav_ss = (4.*pi*alpha)**2*pm2ennggav_ss

  END FUNCTION PM2ENNGGav_SS


                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MUDEC
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!

