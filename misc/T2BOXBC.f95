
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T2BOXBC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T2BOXBC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Ccache(28)
    complex(kind=prec) :: Dcache(5)

!
      real(kind=prec) T2BOXBC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb=-1._prec
!

!    
      call setlambda( & 
           -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Ccache(1) = C0i(cc0,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(2) = C0i(cc0,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(3) = C0i(cc0,-MIN2+MOU2+2*p1p2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,MOU2,MOU2)
    Ccache(4) = C0i(cc0,MOU2+2*p1p4-2*p2p4,MOU2,0._prec,MOU2,0._prec,MOU2)
    Ccache(5) = C0i(cc00,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(6) = C0i(cc00,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(7) = C0i(cc00,-MIN2+MOU2+2*p1p2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,MOU2,MOU2)
    Ccache(8) = C0i(cc00,MOU2+2*p1p4-2*p2p4,MOU2,0._prec,MOU2,0._prec,MOU2)
    Ccache(9) = C0i(cc1,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(10) = C0i(cc1,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(11) = C0i(cc1,-MIN2+MOU2+2*p1p2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,MOU2,MOU2)
    Ccache(12) = C0i(cc1,MOU2+2*p1p4-2*p2p4,MOU2,0._prec,MOU2,0._prec,MOU2)
    Ccache(13) = C0i(cc11,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(14) = C0i(cc11,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(15) = C0i(cc11,-MIN2+MOU2+2*p1p2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,MOU2,MOU2)
    Ccache(16) = C0i(cc11,MOU2+2*p1p4-2*p2p4,MOU2,0._prec,MOU2,0._prec,MOU2)
    Ccache(17) = C0i(cc12,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(18) = C0i(cc12,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(19) = C0i(cc12,-MIN2+MOU2+2*p1p2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,MOU2,MOU2)
    Ccache(20) = C0i(cc12,MOU2+2*p1p4-2*p2p4,MOU2,0._prec,MOU2,0._prec,MOU2)
    Ccache(21) = C0i(cc2,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(22) = C0i(cc2,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(23) = C0i(cc2,-MIN2+MOU2+2*p1p2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,MOU2,MOU2)
    Ccache(24) = C0i(cc2,MOU2+2*p1p4-2*p2p4,MOU2,0._prec,MOU2,0._prec,MOU2)
    Ccache(25) = C0i(cc22,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(26) = C0i(cc22,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(27) = C0i(cc22,-MIN2+MOU2+2*p1p2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,MOU2,MOU2)
    Ccache(28) = C0i(cc22,MOU2+2*p1p4-2*p2p4,MOU2,0._prec,MOU2,0._prec,MOU2)
    Dcache(1) = D0i(dd0,MIN2,MOU2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,-MIN2+MOU2+2*p1p2,MOU2+2*p1p4-2*p2p4,MIN2,0._prec,MOU2,MOU2)
    Dcache(2) = D0i(dd00,MIN2,MOU2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,-MIN2+MOU2+2*p1p2,MOU2+2*p1p4-2*p2p4,MIN2,0._prec,MOU2,MOU2)
    Dcache(3) = D0i(dd1,MIN2,MOU2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,-MIN2+MOU2+2*p1p2,MOU2+2*p1p4-2*p2p4,MIN2,0._prec,MOU2,MOU2)
    Dcache(4) = D0i(dd2,MIN2,MOU2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,-MIN2+MOU2+2*p1p2,MOU2+2*p1p4-2*p2p4,MIN2,0._prec,MOU2,MOU2)
    Dcache(5) = D0i(dd3,MIN2,MOU2,0._prec,-MIN2+MOU2+2*p1p2-2*p2p4,-MIN2+MOU2+2*p1p2,MOU2+2*p1p4-2*p2p4,MIN2,0._prec,MOU2,MOU2)

!
      T2BOXBC=real((EL**4*GF**2*(72*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           18*MIN2**3*p1p4  & 
           + 30*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           -  & 
           44*MIN2**2*MOU2*p1p4  & 
           + 62*MIN2*MOU2**2*p1p4  & 
           -  & 
           22*MIN2**2.5*np2*p1p4  & 
           +  & 
           4*MIN2**1.5*MOU2*np2*p1p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           -  & 
           60*MIN2**2.5*np4*p1p4  & 
           +  & 
           60*MIN2**1.5*MOU2*np4*p1p4  & 
           +  & 
           37*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           +  & 
           46*MIN2**2*p1p2*p1p4  & 
           + 132*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
           18*MOU2**2*p1p2*p1p4  & 
           +  & 
           36*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           204*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           52*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           12*MIN2*p1p2**2*p1p4  & 
           - 12*MOU2*p1p2**2*p1p4  & 
           +  & 
           16*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           144*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           -  & 
           16*p1p2**3*p1p4  & 
           - 91*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           +  & 
           60*MIN2**2*p1p4**2  & 
           - 60*MIN2*MOU2*p1p4**2  & 
           -  & 
           76*MIN2**1.5*np2*p1p4**2  & 
           +  & 
           76*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           128*MIN2*p1p2*p1p4**2  & 
           - 24*MOU2*p1p2*p1p4**2  & 
           +  & 
           128*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           16*p1p2**2*p1p4**2  & 
           - 9*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           -  & 
           26*MIN2**3*p2p4  & 
           - 51*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           +  & 
           60*MIN2**2*MOU2*p2p4  & 
           - 34*MIN2*MOU2**2*p2p4  & 
           +  & 
           6*MIN2**2.5*np2*p2p4  & 
           -  & 
           24*MIN2**1.5*MOU2*np2*p2p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p2p4  & 
           -  & 
           4*MIN2**2.5*np4*p2p4  & 
           +  & 
           4*MIN2**1.5*MOU2*np4*p2p4  & 
           -  & 
           26*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           106*MIN2**2*p1p2*p2p4  & 
           - 104*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
           18*MOU2**2*p1p2*p2p4  & 
           -  & 
           16*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           36*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           +  & 
           12*MIN2**1.5*np4*p1p2*p2p4  & 
           -  & 
           128*MIN2*p1p2**2*p2p4  & 
           - 36*MOU2*p1p2**2*p2p4  & 
           -  & 
           48*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           -  & 
           8*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           + 48*p1p2**3*p2p4  & 
           -  & 
           38*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           -  & 
           80*MIN2**2*p1p4*p2p4  & 
           - 112*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
           172*MIN2**1.5*np2*p1p4*p2p4  & 
           -  & 
           160*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           176*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           40*MIN2*p1p2*p1p4*p2p4  & 
           +  & 
           160*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           200*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           +  & 
           176*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           +  & 
           208*p1p2**2*p1p4*p2p4  & 
           + 128*MIN2*p1p4**2*p2p4  & 
           -  & 
           160*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           16*p1p2*p1p4**2*p2p4  & 
           +  & 
           118*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           -  & 
           68*MIN2**2*p2p4**2  & 
           + 112*MIN2*MOU2*p2p4**2  & 
           -  & 
           88*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           32*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           +  & 
           176*MIN2*p1p2*p2p4**2  & 
           - 32*MOU2*p1p2*p2p4**2  & 
           +  & 
           144*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           144*p1p2**2*p2p4**2  & 
           + 32*MIN2*p1p4*p2p4**2  & 
           +  & 
           192*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           -  & 
           192*p1p2*p1p4*p2p4**2  & 
           - 48*MIN2*p2p4**3  & 
           -  & 
           96*sqrt(MIN2)*np2*p2p4**3  & 
           + 96*p1p2*p2p4**3)* & 
         Ccache(1) & 
         )/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (EL**4*GF**2*( & 
           -4*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           +  & 
           14*MIN2**3*p1p4  & 
           + 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           -  & 
           28*MIN2**2*MOU2*p1p4  & 
           + 14*MIN2*MOU2**2*p1p4  & 
           +  & 
           14*MIN2**2.5*np2*p1p4  & 
           -  & 
           68*MIN2**1.5*MOU2*np2*p1p4  & 
           +  & 
           54*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           -  & 
           10*MIN2**2.5*np4*p1p4  & 
           +  & 
           28*MIN2**1.5*MOU2*np4*p1p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           -  & 
           81*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           26*MIN2**2*p1p2*p1p4  & 
           + 80*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
           54*MOU2**2*p1p2*p1p4  & 
           -  & 
           148*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           188*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           52*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           68*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           +  & 
           124*MIN2*p1p2**2*p1p4  & 
           - 188*MOU2*p1p2**2*p1p4  & 
           +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           32*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           -  & 
           112*p1p2**3*p1p4  & 
           + 199*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           +  & 
           22*MIN2**2*p1p4**2  & 
           - 40*MIN2*MOU2*p1p4**2  & 
           +  & 
           18*MOU2**2*p1p4**2  & 
           + 4*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           4*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           56*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           56*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           -  & 
           56*MIN2*p1p2*p1p4**2  & 
           + 72*MOU2*p1p2*p1p4**2  & 
           +  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           112*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           +  & 
           16*p1p2**2*p1p4**2  & 
           + 56*MIN2*p1p4**3  & 
           -  & 
           56*MOU2*p1p4**3  & 
           - 112*p1p2*p1p4**3  & 
           -  & 
           29*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           - 18*MIN2**3*p2p4  & 
           +  & 
           29*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           -  & 
           16*MIN2**2*MOU2*p2p4  & 
           + 34*MIN2*MOU2**2*p2p4  & 
           -  & 
           10*MIN2**2.5*np2*p2p4  & 
           +  & 
           28*MIN2**1.5*MOU2*np2*p2p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np2*p2p4  & 
           +  & 
           38*MIN2**2.5*np4*p2p4  & 
           -  & 
           20*MIN2**1.5*MOU2*np4*p2p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           +  & 
           25*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           22*MIN2**2*p1p2*p2p4  & 
           + 64*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
           18*MOU2**2*p1p2*p2p4  & 
           +  & 
           64*MIN2**1.5*np2*p1p2*p2p4  & 
           -  & 
           108*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           60*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           4*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           +  & 
           16*MIN2*p1p2**2*p2p4  & 
           + 108*MOU2*p1p2**2*p2p4  & 
           -  & 
           80*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           72*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           +  & 
           80*p1p2**3*p2p4  & 
           - 89*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           -  & 
           82*MIN2**2*p1p4*p2p4  & 
           + 64*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
           18*MOU2**2*p1p4*p2p4  & 
           +  & 
           24*MIN2**1.5*np2*p1p4*p2p4  & 
           -  & 
           64*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           +  & 
           64*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           48*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           +  & 
           92*MIN2*p1p2*p1p4*p2p4  & 
           + 60*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           40*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           192*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           32*p1p2**2*p1p4*p2p4  & 
           - 64*MIN2*p1p4**2*p2p4  & 
           +  & 
           48*MOU2*p1p4**2*p2p4  & 
           -  & 
           16*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           112*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           208*p1p2*p1p4**2*p2p4  & 
           + 112*p1p4**3*p2p4  & 
           -  & 
           54*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           + 24*MIN2**2*p2p4**2  & 
           -  & 
           128*MIN2*MOU2*p2p4**2  & 
           +  & 
           16*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           28*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           +  & 
           40*MIN2**1.5*np4*p2p4**2  & 
           +  & 
           16*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           -  & 
           184*MIN2*p1p2*p2p4**2  & 
           - 28*MOU2*p1p2*p2p4**2  & 
           +  & 
           32*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           72*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           -  & 
           32*p1p2**2*p2p4**2  & 
           - 104*MIN2*p1p4*p2p4**2  & 
           -  & 
           16*MOU2*p1p4*p2p4**2  & 
           -  & 
           72*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           224*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           +  & 
           144*p1p2*p1p4*p2p4**2  & 
           - 224*p1p4**2*p2p4**2  & 
           +  & 
           120*MIN2*p2p4**3  & 
           + 48*sqrt(MIN2)*np2*p2p4**3  & 
           -  & 
           48*p1p2*p2p4**3)* & 
         Ccache(2))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (EL**4*GF**2*( & 
           -10*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           12*MIN2**3*p1p4  & 
           + 10*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           +  & 
           60*MIN2**2*MOU2*p1p4  & 
           - 84*MIN2*MOU2**2*p1p4  & 
           +  & 
           36*MOU2**3*p1p4  & 
           - 20*MIN2**2.5*np2*p1p4  & 
           +  & 
           40*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           20*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           +  & 
           62*MIN2**2.5*np4*p1p4  & 
           -  & 
           84*MIN2**1.5*MOU2*np4*p1p4  & 
           +  & 
           22*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           +  & 
           26*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           +  & 
           84*MIN2**2*p1p2*p1p4  & 
           - 240*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
           156*MOU2**2*p1p2*p1p4  & 
           +  & 
           116*MIN2**1.5*np2*p1p2*p1p4  & 
           -  & 
           116*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           -  & 
           200*MIN2**1.5*np4*p1p2*p1p4  & 
           +  & 
           120*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           232*MIN2*p1p2**2*p1p4  & 
           + 280*MOU2*p1p2**2*p1p4  & 
           -  & 
           96*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           +  & 
           128*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           +  & 
           160*p1p2**3*p1p4  & 
           - 82*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           -  & 
           64*MIN2**2*p1p4**2  & 
           + 64*MIN2*MOU2*p1p4**2  & 
           -  & 
           24*MIN2**1.5*np2*p1p4**2  & 
           +  & 
           24*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           24*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           24*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           +  & 
           192*MIN2*p1p2*p1p4**2  & 
           - 64*MOU2*p1p2*p1p4**2  & 
           +  & 
           48*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           48*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           -  & 
           128*p1p2**2*p1p4**2  & 
           + 13*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           +  & 
           56*MIN2**3*p2p4  & 
           - 13*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           -  & 
           56*MIN2**2*MOU2*p2p4  & 
           + 72*MIN2**2.5*np2*p2p4  & 
           -  & 
           72*MIN2**1.5*MOU2*np2*p2p4  & 
           -  & 
           58*MIN2**2.5*np4*p2p4  & 
           +  & 
           20*MIN2**1.5*MOU2*np4*p2p4  & 
           +  & 
           38*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           -  & 
           252*MIN2**2*p1p2*p2p4  & 
           + 176*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
           36*MOU2**2*p1p2*p2p4  & 
           -  & 
           220*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           92*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           +  & 
           136*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           40*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           +  & 
           384*MIN2*p1p2**2*p2p4  & 
           - 192*MOU2*p1p2**2*p2p4  & 
           +  & 
           224*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           -  & 
           128*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           -  & 
           288*p1p2**3*p2p4  & 
           - 11*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           10*MIN2**2*p1p4*p2p4  & 
           + 96*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
           106*MOU2**2*p1p4*p2p4  & 
           -  & 
           16*MIN2**1.5*np2*p1p4*p2p4  & 
           +  & 
           16*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           +  & 
           200*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           120*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           +  & 
           72*MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           248*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           208*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           72*MIN2*p1p4**2*p2p4  & 
           - 56*MOU2*p1p4**2*p2p4  & 
           -  & 
           48*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           48*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           16*p1p2*p1p4**2*p2p4  & 
           +  & 
           77*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           +  & 
           182*MIN2**2*p2p4**2  & 
           - 52*MIN2*MOU2*p2p4**2  & 
           -  & 
           18*MOU2**2*p2p4**2  & 
           + 144*MIN2**1.5*np2*p2p4**2  & 
           -  & 
           16*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           -  & 
           184*MIN2**1.5*np4*p2p4**2  & 
           +  & 
           8*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           -  & 
           416*MIN2*p1p2*p2p4**2  & 
           + 64*MOU2*p1p2*p2p4**2  & 
           -  & 
           272*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           +  & 
           272*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           +  & 
           384*p1p2**2*p2p4**2  & 
           - 48*MIN2*p1p4*p2p4**2  & 
           +  & 
           176*MOU2*p1p4*p2p4**2  & 
           +  & 
           128*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           80*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           -  & 
           64*p1p2*p1p4*p2p4**2  & 
           + 112*p1p4**2*p2p4**2  & 
           +  & 
           160*MIN2*p2p4**3  & 
           - 32*MOU2*p2p4**3  & 
           +  & 
           48*sqrt(MIN2)*np2*p2p4**3  & 
           -  & 
           144*sqrt(MIN2)*np4*p2p4**3  & 
           - 192*p1p2*p2p4**3  & 
           -  & 
           96*p1p4*p2p4**3  & 
           + 96*p2p4**4)* & 
         Ccache(3))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (EL**4*GF**2*( & 
           -18*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           +  & 
           56*MIN2**3*p1p4  & 
           - 84*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           -  & 
           68*MIN2**2*MOU2*p1p4  & 
           + 48*MIN2*MOU2**2*p1p4  & 
           -  & 
           36*MOU2**3*p1p4  & 
           + 36*MIN2**2.5*np2*p1p4  & 
           +  & 
           44*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           80*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           -  & 
           60*MIN2**1.5*MOU2*np4*p1p4  & 
           +  & 
           60*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           +  & 
           46*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           240*MIN2**2*p1p2*p1p4  & 
           + 136*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
           56*MOU2**2*p1p2*p1p4  & 
           -  & 
           100*MIN2**1.5*np2*p1p2*p1p4  & 
           -  & 
           60*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           88*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           +  & 
           312*MIN2*p1p2**2*p1p4  & 
           - 104*MOU2*p1p2**2*p1p4  & 
           +  & 
           64*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           128*p1p2**3*p1p4  & 
           - 150*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           -  & 
           10*MIN2**2*p1p4**2  & 
           + 92*MIN2*MOU2*p1p4**2  & 
           -  & 
           82*MOU2**2*p1p4**2  & 
           + 40*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           40*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           32*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           32*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           -  & 
           8*MIN2*p1p2*p1p4**2  & 
           - 128*MOU2*p1p2*p1p4**2  & 
           -  & 
           48*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           64*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           +  & 
           56*MIN2*p1p4**3  & 
           - 56*MOU2*p1p4**3  & 
           -  & 
           112*p1p2*p1p4**3  & 
           + 17*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           -  & 
           56*MIN2**3*p2p4  & 
           + 43*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           +  & 
           56*MIN2**2*MOU2*p2p4  & 
           - 36*MIN2**2.5*np2*p2p4  & 
           +  & 
           36*sqrt(MIN2)*MOU2**2*np2*p2p4  & 
           +  & 
           40*MIN2**1.5*MOU2*np4*p2p4  & 
           -  & 
           40*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           -  & 
           45*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           240*MIN2**2*p1p2*p2p4  & 
           - 128*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
           100*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           28*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           92*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           -  & 
           312*MIN2*p1p2**2*p2p4  & 
           + 72*MOU2*p1p2**2*p2p4  & 
           -  & 
           64*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           128*p1p2**3*p2p4  & 
           +  & 
           284*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           192*MIN2**2*p1p4*p2p4  & 
           - 108*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
           108*MOU2**2*p1p4*p2p4  & 
           +  & 
           64*MIN2**1.5*np2*p1p4*p2p4  & 
           +  & 
           100*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           32*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           56*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           -  & 
           416*MIN2*p1p2*p1p4*p2p4  & 
           +  & 
           184*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           64*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           16*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           +  & 
           224*p1p2**2*p1p4*p2p4  & 
           - 96*MIN2*p1p4**2*p2p4  & 
           +  & 
           232*MOU2*p1p4**2*p2p4  & 
           +  & 
           48*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           64*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           208*p1p2*p1p4**2*p2p4  & 
           + 112*p1p4**3*p2p4  & 
           -  & 
           135*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           -  & 
           182*MIN2**2*p2p4**2  & 
           + 24*MIN2*MOU2*p2p4**2  & 
           +  & 
           18*MOU2**2*p2p4**2  & 
           - 104*MIN2**1.5*np2*p2p4**2  & 
           -  & 
           24*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           +  & 
           64*MIN2**1.5*np4*p2p4**2  & 
           +  & 
           24*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           +  & 
           424*MIN2*p1p2*p2p4**2  & 
           - 24*MOU2*p1p2*p2p4**2  & 
           +  & 
           112*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           48*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           -  & 
           224*p1p2**2*p2p4**2  & 
           + 200*MIN2*p1p4*p2p4**2  & 
           -  & 
           208*MOU2*p1p4*p2p4**2  & 
           +  & 
           16*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           -  & 
           288*p1p2*p1p4*p2p4**2  & 
           - 208*p1p4**2*p2p4**2  & 
           -  & 
           160*MIN2*p2p4**3  & 
           + 32*MOU2*p2p4**3  & 
           -  & 
           48*sqrt(MIN2)*np2*p2p4**3  & 
           +  & 
           48*sqrt(MIN2)*np4*p2p4**3  & 
           + 192*p1p2*p2p4**3  & 
           +  & 
           192*p1p4*p2p4**3  & 
           - 96*p2p4**4)* & 
         Ccache(4))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (32*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4  & 
           +  & 
           6*MIN2**2*p1p4  & 
           - 6*MIN2*MOU2*p1p4  & 
           +  & 
           MIN2**1.5*np4*p1p4  & 
           - sqrt(MIN2)*MOU2*np4*p1p4  & 
           -  & 
           14*MIN2*p1p2*p1p4  & 
           + 2*MOU2*p1p2*p1p4  & 
           -  & 
           sqrt(MIN2)*np4*p1p2*p1p4  & 
           + 8*p1p2**2*p1p4  & 
           -  & 
           MIN2*p1p4**2  & 
           + MOU2*p1p4**2  & 
           - 3*MIN2**2*p2p4  & 
           +  & 
           3*MIN2*MOU2*p2p4  & 
           - 2*MIN2**1.5*np4*p2p4  & 
           +  & 
           sqrt(MIN2)*MOU2*np4*p2p4  & 
           + 7*MIN2*p1p2*p2p4  & 
           -  & 
           MOU2*p1p2*p2p4  & 
           + 2*sqrt(MIN2)*np4*p1p2*p2p4  & 
           -  & 
           4*p1p2**2*p2p4  & 
           + 9*MIN2*p1p4*p2p4  & 
           -  & 
           MOU2*p1p4*p2p4  & 
           - 8*p1p2*p1p4*p2p4  & 
           -  & 
           4*MIN2*p2p4**2  & 
           + 4*p1p2*p2p4**2  & 
           +  & 
           sqrt(MIN2)*np2* & 
            (2*MIN2*p1p4  & 
           - 2*MOU2*p1p4  & 
           - 8*p1p2*p1p4  & 
           +  & 
              p1p4**2  & 
           + MOU2*p2p4  & 
           + 4*p1p2*p2p4  & 
           +  & 
              6*p1p4*p2p4  & 
           - 4*p2p4**2))* & 
         Ccache(5))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (16*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4  & 
           - p2p4)  & 
           +  & 
           2*( & 
           -3*MIN2**2*p1p4  & 
           + 3*MIN2*MOU2*p1p4  & 
           +  & 
              7*MIN2*p1p2*p1p4  & 
           - MOU2*p1p2*p1p4  & 
           -  & 
              4*p1p2**2*p1p4  & 
           + MIN2*p1p4**2  & 
           - MOU2*p1p4**2  & 
           +  & 
              sqrt(MIN2)*np4*( & 
           -MIN2  & 
           + MOU2  & 
           + p1p2)* & 
               (p1p4  & 
           - p2p4)  & 
           + 4*MIN2**2*p2p4  & 
           -  & 
              4*MIN2*MOU2*p2p4  & 
           - 10*MIN2*p1p2*p2p4  & 
           +  & 
              MOU2*p1p2*p2p4  & 
           + 4*p1p2**2*p2p4  & 
           -  & 
              5*MIN2*p1p4*p2p4  & 
           + MOU2*p1p4*p2p4  & 
           +  & 
              4*p1p2*p1p4*p2p4  & 
           + 8*MIN2*p2p4**2  & 
           -  & 
              4*p1p2*p2p4**2  & 
           -  & 
              sqrt(MIN2)*np2*(p1p4  & 
           - p2p4)* & 
               (MIN2  & 
           - MOU2  & 
           - 4*p1p2  & 
           + p1p4  & 
           + 4*p2p4)))* & 
         Ccache(6))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (64*EL**4*GF**2*MIN2*p2p4* & 
         (MIN2  & 
           - MOU2  & 
           - 2*p1p2  & 
           + 2*p2p4)* & 
         Ccache(7))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (16*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)*(3*p1p4  & 
           - p2p4)  & 
           +  & 
           2*((MIN2  & 
           - p1p2)* & 
               (3*MIN2*p1p4  & 
           - 4*p1p2*p1p4  & 
           - MIN2*p2p4  & 
           -  & 
                 sqrt(MIN2)*np4*p2p4  & 
           + 4*p1p4*p2p4)  & 
           +  & 
              MOU2*(p1p2*p1p4  & 
           + MIN2*( & 
           -3*p1p4  & 
           + p2p4))  & 
           +  & 
              sqrt(MIN2)*np2* & 
               ( & 
           -(MOU2*p1p4)  & 
           - 4*p1p2*p1p4  & 
           + 3*p1p4*p2p4  & 
           +  & 
                 MIN2*(p1p4  & 
           + p2p4))))* & 
         Ccache(8)) & 
        /(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (2*EL**4*GF**2*(25*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           62*MIN2**3*p1p4  & 
           + 14*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           +  & 
           48*MIN2**2*MOU2*p1p4  & 
           + 14*MIN2*MOU2**2*p1p4  & 
           +  & 
           10*MIN2**2.5*np2*p1p4  & 
           -  & 
           10*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           18*MIN2**2.5*np4*p1p4  & 
           +  & 
           18*MIN2**1.5*MOU2*np4*p1p4  & 
           -  & 
           9*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           +  & 
           136*MIN2**2*p1p2*p1p4  & 
           + 16*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
           6*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           2*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           62*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           14*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           50*MIN2*p1p2**2*p1p4  & 
           - 2*MOU2*p1p2**2*p1p4  & 
           +  & 
           24*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           44*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           -  & 
           24*p1p2**3*p1p4  & 
           + 46*MIN2**2*p1p4**2  & 
           -  & 
           46*MIN2*MOU2*p1p4**2  & 
           - 46*MIN2**1.5*np2*p1p4**2  & 
           +  & 
           26*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           72*MIN2*p1p2*p1p4**2  & 
           - 12*MOU2*p1p2*p1p4**2  & 
           +  & 
           44*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           7*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           + 24*MIN2**3*p2p4  & 
           -  & 
           20*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           -  & 
           6*MIN2**2*MOU2*p2p4  & 
           - 18*MIN2*MOU2**2*p2p4  & 
           -  & 
           34*MIN2**2.5*np2*p2p4  & 
           +  & 
           14*MIN2**1.5*MOU2*np2*p2p4  & 
           +  & 
           22*MIN2**2.5*np4*p2p4  & 
           -  & 
           2*MIN2**1.5*MOU2*np4*p2p4  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           -  & 
           12*MIN2**2*p1p2*p2p4  & 
           - 64*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
           20*MIN2**1.5*np2*p1p2*p2p4  & 
           -  & 
           6*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           30*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           4*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           -  & 
           52*MIN2*p1p2**2*p2p4  & 
           + 6*MOU2*p1p2**2*p2p4  & 
           -  & 
           40*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           + 40*p1p2**3*p2p4  & 
           -  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           -  & 
           192*MIN2**2*p1p4*p2p4  & 
           + 52*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
           78*MIN2**1.5*np2*p1p4*p2p4  & 
           -  & 
           20*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           56*MIN2**1.5*np4*p1p4*p2p4  & 
           +  & 
           212*MIN2*p1p2*p1p4*p2p4  & 
           +  & 
           16*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           +  & 
           56*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           +  & 
           24*p1p2**2*p1p4*p2p4  & 
           + 56*MIN2*p1p4**2*p2p4  & 
           -  & 
           56*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           +  & 
           36*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           + 34*MIN2**2*p2p4**2  & 
           +  & 
           12*MIN2*MOU2*p2p4**2  & 
           - 44*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           24*MIN2**1.5*np4*p2p4**2  & 
           - 12*MIN2*p1p2*p2p4**2  & 
           +  & 
           40*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           -  & 
           40*p1p2**2*p2p4**2  & 
           - 80*MIN2*p1p4*p2p4**2  & 
           +  & 
           24*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           + 24*MIN2*p2p4**3)* & 
         Ccache(9) & 
         )/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (2*EL**4*GF**2*( & 
           -10*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           +  & 
           24*MIN2**3*p1p4  & 
           + 10*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           -  & 
           16*MIN2**2*MOU2*p1p4  & 
           - 8*MIN2*MOU2**2*p1p4  & 
           -  & 
           28*MIN2**2.5*np2*p1p4  & 
           +  & 
           28*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           8*MIN2**2.5*np4*p1p4  & 
           +  & 
           8*MIN2**1.5*MOU2*np4*p1p4  & 
           -  & 
           27*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           10*MIN2**2*p1p2*p1p4  & 
           - 54*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
           18*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           2*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           28*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           12*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           38*MIN2*p1p2**2*p1p4  & 
           - 2*MOU2*p1p2**2*p1p4  & 
           -  & 
           24*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           8*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           + 24*p1p2**3*p1p4  & 
           +  & 
           47*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           - 42*MIN2**2*p1p4**2  & 
           +  & 
           42*MIN2*MOU2*p1p4**2  & 
           + 58*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           38*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           16*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           16*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           +  & 
           14*MIN2*p1p2*p1p4**2  & 
           + 50*MOU2*p1p2*p1p4**2  & 
           -  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           16*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           +  & 
           40*p1p2**2*p1p4**2  & 
           + 16*MIN2*p1p4**3  & 
           -  & 
           16*MOU2*p1p4**3  & 
           - 16*sqrt(MIN2)*np2*p1p4**3  & 
           -  & 
           12*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           - 58*MIN2**3*p2p4  & 
           +  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           +  & 
           40*MIN2**2*MOU2*p2p4  & 
           + 18*MIN2*MOU2**2*p2p4  & 
           +  & 
           30*MIN2**2.5*np2*p2p4  & 
           -  & 
           30*MIN2**1.5*MOU2*np2*p2p4  & 
           +  & 
           22*MIN2**2.5*np4*p2p4  & 
           -  & 
           22*MIN2**1.5*MOU2*np4*p2p4  & 
           +  & 
           20*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           88*MIN2**2*p1p2*p2p4  & 
           + 80*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
           60*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           6*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           32*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           +  & 
           92*MIN2*p1p2**2*p2p4  & 
           - 6*MOU2*p1p2**2*p2p4  & 
           +  & 
           40*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           28*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           -  & 
           40*p1p2**3*p2p4  & 
           - 16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           108*MIN2**2*p1p4*p2p4  & 
           - 44*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
           72*MIN2**1.5*np2*p1p4*p2p4  & 
           +  & 
           14*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           12*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           104*MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           26*MOU2*p1p2*p1p4*p2p4  & 
           +  & 
           20*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           12*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           48*p1p2**2*p1p4*p2p4  & 
           - 44*MIN2*p1p4**2*p2p4  & 
           +  & 
           52*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           40*p1p2*p1p4**2*p2p4  & 
           -  & 
           24*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           -  & 
           144*MIN2**2*p2p4**2  & 
           - 24*MIN2*MOU2*p2p4**2  & 
           +  & 
           72*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           16*MIN2**1.5*np4*p2p4**2  & 
           - 44*MIN2*p1p2*p2p4**2  & 
           -  & 
           40*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           +  & 
           40*p1p2**2*p2p4**2  & 
           + 132*MIN2*p1p4*p2p4**2  & 
           +  & 
           24*p1p2*p1p4*p2p4**2  & 
           - 24*MIN2*p2p4**3)* & 
         Ccache(10))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (2*EL**4*GF**2*(9*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           9*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           +  & 
           32*MIN2**2.5*np2*p1p4  & 
           -  & 
           64*MIN2**1.5*MOU2*np2*p1p4  & 
           +  & 
           32*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           -  & 
           18*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           32*MIN2**2*p1p2*p1p4  & 
           + 64*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
           32*MOU2**2*p1p2*p1p4  & 
           -  & 
           96*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           96*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           -  & 
           12*MIN2**1.5*np4*p1p2*p1p4  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           +  & 
           96*MIN2*p1p2**2*p1p4  & 
           - 96*MOU2*p1p2**2*p1p4  & 
           +  & 
           64*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           +  & 
           24*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           -  & 
           64*p1p2**3*p1p4  & 
           + 12*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           12*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           24*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           -  & 
           17*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           +  & 
           17*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           -  & 
           18*MIN2**2.5*np2*p2p4  & 
           +  & 
           36*MIN2**1.5*MOU2*np2*p2p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np2*p2p4  & 
           +  & 
           14*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           6*MIN2**2*p1p2*p2p4  & 
           - 24*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
           18*MOU2**2*p1p2*p2p4  & 
           +  & 
           92*MIN2**1.5*np2*p1p2*p2p4  & 
           -  & 
           92*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           +  & 
           10*MIN2**1.5*np4*p1p2*p2p4  & 
           -  & 
           10*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           -  & 
           68*MIN2*p1p2**2*p2p4  & 
           + 92*MOU2*p1p2**2*p2p4  & 
           -  & 
           144*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           +  & 
           144*p1p2**3*p2p4  & 
           + 51*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           22*MIN2**1.5*np2*p1p4*p2p4  & 
           -  & 
           22*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           32*MIN2*p1p2*p1p4*p2p4  & 
           + 32*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           8*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           +  & 
           24*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           41*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           -  & 
           20*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           20*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           -  & 
           4*MIN2*p1p2*p2p4**2  & 
           - 20*MOU2*p1p2*p2p4**2  & 
           +  & 
           144*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           8*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           -  & 
           144*p1p2**2*p2p4**2  & 
           -  & 
           56*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           64*p1p2*p1p4*p2p4**2)* & 
         Ccache(11))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (2*EL**4*GF**2*(18*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           28*MIN2**3*p1p4  & 
           - 57*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           +  & 
           100*MIN2**2*MOU2*p1p4  & 
           - 72*MIN2*MOU2**2*p1p4  & 
           -  & 
           36*MIN2**2.5*np2*p1p4  & 
           +  & 
           106*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           70*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           +  & 
           6*MIN2**2.5*np4*p1p4  & 
           -  & 
           58*MIN2**1.5*MOU2*np4*p1p4  & 
           +  & 
           52*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           -  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           +  & 
           120*MIN2**2*p1p2*p1p4  & 
           - 278*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
           70*MOU2**2*p1p2*p1p4  & 
           +  & 
           100*MIN2**1.5*np2*p1p2*p1p4  & 
           -  & 
           160*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           -  & 
           26*MIN2**1.5*np4*p1p2*p1p4  & 
           +  & 
           106*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           156*MIN2*p1p2**2*p1p4  & 
           + 160*MOU2*p1p2**2*p1p4  & 
           -  & 
           64*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           +  & 
           20*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           +  & 
           64*p1p2**3*p1p4  & 
           - 81*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           +  & 
           36*MIN2**2*p1p4**2  & 
           + 16*MIN2*MOU2*p1p4**2  & 
           -  & 
           52*MOU2**2*p1p4**2  & 
           + 36*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           36*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           32*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           32*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           -  & 
           94*MIN2*p1p2*p1p4**2  & 
           - 70*MOU2*p1p2*p1p4**2  & 
           -  & 
           60*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           64*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           +  & 
           40*p1p2**2*p1p4**2  & 
           + 32*MIN2*p1p4**3  & 
           -  & 
           32*MOU2*p1p4**3  & 
           - 64*p1p2*p1p4**3  & 
           -  & 
           18*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           + 28*MIN2**3*p2p4  & 
           +  & 
           31*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           -  & 
           46*MIN2**2*MOU2*p2p4  & 
           + 18*MIN2*MOU2**2*p2p4  & 
           +  & 
           36*MIN2**2.5*np2*p2p4  & 
           -  & 
           34*MIN2**1.5*MOU2*np2*p2p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p2p4  & 
           -  & 
           6*MIN2**2.5*np4*p2p4  & 
           -  & 
           6*MIN2**1.5*MOU2*np4*p2p4  & 
           -  & 
           8*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           -  & 
           120*MIN2**2*p1p2*p2p4  & 
           + 94*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
           18*MOU2**2*p1p2*p2p4  & 
           -  & 
           100*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           48*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           +  & 
           26*MIN2**1.5*np4*p1p2*p2p4  & 
           -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           +  & 
           156*MIN2*p1p2**2*p2p4  & 
           - 48*MOU2*p1p2**2*p2p4  & 
           +  & 
           64*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           -  & 
           20*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           -  & 
           64*p1p2**3*p2p4  & 
           + 151*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           -  & 
           124*MIN2**2*p1p4*p2p4  & 
           + 192*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
           8*MOU2**2*p1p4*p2p4  & 
           -  & 
           112*MIN2**1.5*np2*p1p4*p2p4  & 
           +  & 
           176*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           +  & 
           52*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           124*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           +  & 
           270*MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           168*MOU2*p1p2*p1p4*p2p4  & 
           +  & 
           148*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           96*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           128*p1p2**2*p1p4*p2p4  & 
           + 28*MIN2*p1p4**2*p2p4  & 
           +  & 
           124*MOU2*p1p4**2*p2p4  & 
           +  & 
           72*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           64*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           24*p1p2*p1p4**2*p2p4  & 
           + 64*p1p4**3*p2p4  & 
           -  & 
           70*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           + 88*MIN2**2*p2p4**2  & 
           -  & 
           30*MIN2*MOU2*p2p4**2  & 
           + 76*MIN2**1.5*np2*p2p4**2  & 
           -  & 
           30*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           -  & 
           20*MIN2**1.5*np4*p2p4**2  & 
           -  & 
           4*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           -  & 
           176*MIN2*p1p2*p2p4**2  & 
           + 30*MOU2*p1p2*p2p4**2  & 
           -  & 
           88*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           +  & 
           32*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           +  & 
           88*p1p2**2*p2p4**2  & 
           - 84*MIN2*p1p4*p2p4**2  & 
           +  & 
           4*MOU2*p1p4*p2p4**2  & 
           -  & 
           96*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           64*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           +  & 
           64*p1p2*p1p4*p2p4**2  & 
           - 64*p1p4**2*p2p4**2  & 
           +  & 
           24*MIN2*p2p4**3  & 
           + 24*sqrt(MIN2)*np2*p2p4**3  & 
           -  & 
           24*p1p2*p2p4**3)* & 
         Ccache(12))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (16*EL**4*GF**2*MIN2* & 
         (CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4  & 
           - p2p4)  & 
           +  & 
           2*(MIN2**2*(4*p1p4  & 
           - 3*p2p4)  & 
           +  & 
              MIN2**1.5*(np2  & 
           - np4)*p2p4  & 
           +  & 
              MOU2*( & 
           -4*MIN2*p1p4  & 
           + 2*p1p2*p1p4  & 
           + p1p4**2  & 
           +  & 
                 3*MIN2*p2p4  & 
           - p1p2*p2p4  & 
           - p1p4*p2p4)  & 
           -  & 
              MIN2*(10*p1p2*p1p4  & 
           + p1p4**2  & 
           - 7*p1p2*p2p4  & 
           -  & 
                 8*p1p4*p2p4  & 
           + 4*p2p4**2)  & 
           +  & 
              p1p2*(6*p1p2*p1p4  & 
           + p1p4**2  & 
           - 4*p1p2*p2p4  & 
           -  & 
                 8*p1p4*p2p4  & 
           + 4*p2p4**2)  & 
           +  & 
              sqrt(MIN2)* & 
               (np4*p1p2*p2p4  & 
           +  & 
                 np2*( & 
           -2*p1p2*p1p4  & 
           + p1p4**2  & 
           + p1p2*p2p4  & 
           -  & 
                    p1p4*p2p4))))* & 
         Ccache(13))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (32*EL**4*GF**2*MIN2* & 
         (p1p2*(p1p2*(3*p1p4  & 
           - 4*p2p4)  & 
           +  & 
              (p1p4  & 
           - 2*p2p4)**2)  & 
           +  & 
           MOU2*( & 
           -2*MIN2*(p1p4  & 
           - 2*p2p4)  & 
           +  & 
              (p1p2  & 
           + p1p4)*(p1p4  & 
           - p2p4))  & 
           +  & 
           2*MIN2**2*(p1p4  & 
           - 2*p2p4)  & 
           -  & 
           sqrt(MIN2)*np2*(p1p2  & 
           - p1p4)*(p1p4  & 
           - p2p4)  & 
           -  & 
           MIN2*(p1p4**2  & 
           + 5*p1p2*(p1p4  & 
           - 2*p2p4)  & 
           -  & 
              4*p1p4*p2p4  & 
           + 8*p2p4**2))* & 
         Ccache(14))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (64*EL**4*GF**2*(sqrt(MIN2)*np2  & 
           - p1p2)*p1p2* & 
         ( & 
           -MIN2  & 
           + MOU2  & 
           + 2*p1p2  & 
           - 2*p2p4)*p2p4* & 
         Ccache(15))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (16*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4  & 
           - p2p4)* & 
            (2*MIN2  & 
           - MOU2  & 
           - 2*p1p2  & 
           + p1p4  & 
           + p2p4)  & 
           -  & 
           2*( & 
           -2*MIN2**2*MOU2*p1p4  & 
           + 2*MIN2*MOU2**2*p1p4  & 
           +  & 
              5*MIN2*MOU2*p1p2*p1p4  & 
           - MOU2**2*p1p2*p1p4  & 
           -  & 
              3*MOU2*p1p2**2*p1p4  & 
           - 3*MIN2**2*p1p4**2  & 
           +  & 
              2*MIN2*MOU2*p1p4**2  & 
           + MOU2**2*p1p4**2  & 
           +  & 
              7*MIN2*p1p2*p1p4**2  & 
           + MOU2*p1p2*p1p4**2  & 
           -  & 
              4*p1p2**2*p1p4**2  & 
           - 2*MIN2*p1p4**3  & 
           +  & 
              2*MOU2*p1p4**3  & 
           + 4*p1p2*p1p4**3  & 
           +  & 
              MIN2*MOU2*p1p2*p2p4  & 
           - MOU2*p1p2**2*p2p4  & 
           +  & 
              4*MIN2**2*p1p4*p2p4  & 
           - 8*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
              8*MIN2*p1p2*p1p4*p2p4  & 
           +  & 
              7*MOU2*p1p2*p1p4*p2p4  & 
           + 4*p1p2**2*p1p4*p2p4  & 
           -  & 
              2*MIN2*p1p4**2*p2p4  & 
           - 6*MOU2*p1p4**2*p2p4  & 
           -  & 
              4*p1p4**3*p2p4  & 
           - MIN2**2*p2p4**2  & 
           +  & 
              MIN2*p1p2*p2p4**2  & 
           + 4*MIN2*p1p4*p2p4**2  & 
           -  & 
              4*p1p2*p1p4*p2p4**2  & 
           + 4*p1p4**2*p2p4**2  & 
           +  & 
              sqrt(MIN2)*np2* & 
               (MOU2**2*p1p4  & 
           -  & 
                 p1p4*(p1p4  & 
           - p2p4)* & 
                  (MIN2  & 
           - 3*p1p2  & 
           + 3*p2p4)  & 
           +  & 
                 MOU2*(3*p1p2*p1p4  & 
           + p1p4**2  & 
           + p1p2*p2p4  & 
           -  & 
                    5*p1p4*p2p4  & 
           - MIN2*(p1p4  & 
           + p2p4)))  & 
           -  & 
              sqrt(MIN2)*np4* & 
               (MOU2**2*p1p4  & 
           +  & 
                 2*MOU2* & 
                  (p1p4**2  & 
           - MIN2*p2p4  & 
           - 3*p1p4*p2p4  & 
           +  & 
                    p1p2*(p1p4  & 
           + p2p4))  & 
           -  & 
                 (p1p4  & 
           - p2p4)* & 
                  (MIN2**2  & 
           + (p1p2  & 
           - 4*p1p4)*(p1p2  & 
           - p2p4)  & 
           +  & 
                    MIN2*( & 
           -2*p1p2  & 
           + 2*p1p4  & 
           + p2p4)))))* & 
         Ccache(16)) & 
        /(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (32*EL**4*GF**2*(2*MIN2**3*p1p4  & 
           +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           10*MIN2*p1p2**2*p1p4  & 
           + 8*p1p2**3*p1p4  & 
           -  & 
           MIN2**2*p1p4**2  & 
           + MIN2*p1p2*p1p4**2  & 
           +  & 
           MIN2*MOU2**2*(2*p1p4  & 
           - p2p4)  & 
           -  & 
           CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           - 3*MIN2**3*p2p4  & 
           -  & 
           MIN2**2.5*np4*p2p4  & 
           + 4*MIN2**2*p1p2*p2p4  & 
           +  & 
           3*MIN2*p1p2**2*p2p4  & 
           +  & 
           sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           - 4*p1p2**3*p2p4  & 
           +  & 
           7*MIN2**2*p1p4*p2p4  & 
           + MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           8*p1p2**2*p1p4*p2p4  & 
           - 4*MIN2**2*p2p4**2  & 
           +  & 
           4*p1p2**2*p2p4**2  & 
           +  & 
           sqrt(MIN2)*np2* & 
            ( & 
           -2*MIN2**2*(p1p4  & 
           - p2p4)  & 
           +  & 
              p1p2*( & 
           -8*p1p2*p1p4  & 
           + 4*p1p2*p2p4  & 
           +  & 
                 7*p1p4*p2p4  & 
           - 4*p2p4**2)  & 
           +  & 
              MIN2*(6*p1p2*p1p4  & 
           + p1p4**2  & 
           - 2*p1p2*p2p4  & 
           -  & 
                 7*p1p4*p2p4  & 
           + 3*p2p4**2))  & 
           +  & 
           MOU2*( & 
           -4*MIN2**2*(p1p4  & 
           - p2p4)  & 
           +  & 
              MIN2**1.5*np4*p2p4  & 
           +  & 
              p1p2*(2*p1p2*p1p4  & 
           + p1p4**2  & 
           - p1p2*p2p4  & 
           -  & 
                 p1p4*p2p4)  & 
           +  & 
              MIN2*(p1p4**2  & 
           - 7*p1p4*p2p4  & 
           + 3*p2p4**2)  & 
           +  & 
              sqrt(MIN2)*np2* & 
               (2*MIN2*(p1p4  & 
           - p2p4)  & 
           +  & 
                 p1p2*( & 
           -2*p1p4  & 
           + p2p4)  & 
           + p1p4*( & 
           -p1p4  & 
           + p2p4)) & 
              ))*Ccache(17))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (16*EL**4*GF**2*(2*MIN2**3*p1p4  & 
           +  & 
           4*MIN2**2.5*np4*p1p4  & 
           -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           10*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           10*MIN2*p1p2**2*p1p4  & 
           +  & 
           6*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           + 8*p1p2**3*p1p4  & 
           +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           - 10*MIN2**2*p1p4**2  & 
           -  & 
           2*MIN2**1.5*np4*p1p4**2  & 
           + 20*MIN2*p1p2*p1p4**2  & 
           +  & 
           2*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           -  & 
           8*p1p2**2*p1p4**2  & 
           + 2*MIN2*p1p4**3  & 
           +  & 
           2*MIN2*MOU2**2*(p1p4  & 
           - p2p4)  & 
           - 4*MIN2**3*p2p4  & 
           -  & 
           8*MIN2**2.5*np4*p2p4  & 
           +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           18*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           20*MIN2*p1p2**2*p2p4  & 
           -  & 
           6*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           - 8*p1p2**3*p2p4  & 
           -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           24*MIN2**2*p1p4*p2p4  & 
           +  & 
           10*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           38*MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           10*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           18*MIN2*p1p4**2*p2p4  & 
           + 8*p1p2*p1p4**2*p2p4  & 
           -  & 
           12*MIN2**2*p2p4**2  & 
           - 16*MIN2**1.5*np4*p2p4**2  & 
           -  & 
           8*MIN2*p1p2*p2p4**2  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           +  & 
           8*p1p2**2*p2p4**2  & 
           + 40*MIN2*p1p4*p2p4**2  & 
           -  & 
           8*p1p2*p1p4*p2p4**2  & 
           - 8*MIN2*p2p4**3  & 
           +  & 
           2*MOU2*( & 
           -2*MIN2**2*p1p4  & 
           + p1p2**2*p1p4  & 
           +  & 
              5*MIN2*p1p4**2  & 
           - p1p4**3  & 
           +  & 
              sqrt(MIN2)*np2* & 
               (MIN2*(p1p4  & 
           - 3*p2p4)  & 
           -  & 
                 (p1p2  & 
           + p1p4)*(p1p4  & 
           - p2p4))  & 
           +  & 
              sqrt(MIN2)*np4* & 
               ( & 
           -2*MIN2*(p1p4  & 
           - 2*p2p4)  & 
           +  & 
                 (p1p2  & 
           + p1p4)*(p1p4  & 
           - p2p4))  & 
           +  & 
              3*MIN2**2*p2p4  & 
           + 2*MIN2*p1p2*p2p4  & 
           -  & 
              p1p2**2*p2p4  & 
           - 12*MIN2*p1p4*p2p4  & 
           +  & 
              p1p4**2*p2p4  & 
           + 4*MIN2*p2p4**2)  & 
           -  & 
           2*sqrt(MIN2)*np2* & 
            (MIN2**2*(p1p4  & 
           - 3*p2p4)  & 
           -  & 
              MIN2*(3*p1p2*p1p4  & 
           + p1p4**2  & 
           - 7*p1p2*p2p4  & 
           -  & 
                 3*p1p4*p2p4  & 
           + 6*p2p4**2)  & 
           +  & 
              (p1p4  & 
           - p2p4)* & 
               (4*p1p2**2  & 
           + p1p4**2  & 
           - p1p2*(p1p4  & 
           + 4*p2p4)))) & 
          *Ccache(18))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (64*EL**4*GF**2*( & 
           -(p1p2* & 
              (sqrt(MIN2)*np4  & 
           + 2*p1p2  & 
           - 2*p1p4))  & 
           +  & 
           sqrt(MIN2)*np2*(2*p1p2  & 
           - p1p4))* & 
         ( & 
           -MIN2  & 
           + MOU2  & 
           + 2*p1p2  & 
           - 2*p2p4)*p2p4* & 
         Ccache(19))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (16*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (2*MOU2*p1p4  & 
           -  & 
              (p1p4  & 
           - p2p4)*(2*MIN2  & 
           - 2*p1p2  & 
           + p1p4  & 
           + p2p4)) & 
           - 2*( & 
           -(MOU2**2*p1p4**2)  & 
           -  & 
              MOU2*p1p4* & 
               (p1p2*p1p4  & 
           + 4*p1p4**2  & 
           +  & 
                 2*MIN2*(p1p4  & 
           - 2*p2p4)  & 
           +  & 
                 sqrt(MIN2)*np2*(p1p4  & 
           - 2*p2p4)  & 
           +  & 
                 3*p1p2*p2p4  & 
           - 8*p1p4*p2p4)  & 
           +  & 
              (p1p4  & 
           - p2p4)* & 
               (4*(p1p2  & 
           - 2*p1p4)*p1p4*(p1p2  & 
           - p2p4)  & 
           +  & 
                 MIN2**2*(3*p1p4  & 
           - p2p4)  & 
           +  & 
                 sqrt(MIN2)*np2*p1p4* & 
                  (MIN2  & 
           - 3*p1p2  & 
           + 3*p2p4)  & 
           +  & 
                 MIN2*(p1p2*( & 
           -7*p1p4  & 
           + p2p4)  & 
           +  & 
                    4*p1p4*(p1p4  & 
           + p2p4)))  & 
           +  & 
              sqrt(MIN2)*np4* & 
               (MOU2**2*p1p4  & 
           +  & 
                 MOU2*(4*p1p4**2  & 
           - MIN2*p2p4  & 
           - 8*p1p4*p2p4  & 
           +  & 
                    p1p2*(2*p1p4  & 
           + p2p4))  & 
           -  & 
                 (p1p4  & 
           - p2p4)* & 
                  (MIN2**2  & 
           + (p1p2  & 
           - 8*p1p4)*(p1p2  & 
           - p2p4)  & 
           +  & 
                    MIN2*( & 
           -2*p1p2  & 
           + 4*p1p4  & 
           + p2p4)))))* & 
         Ccache(20)) & 
        /(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (2*EL**4*GF**2*(11*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           +  & 
           4*MIN2**3*p1p4  & 
           - 11*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           -  & 
           8*MIN2**2*MOU2*p1p4  & 
           + 4*MIN2*MOU2**2*p1p4  & 
           +  & 
           12*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           12*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           -  & 
           14*MIN2**2.5*np4*p1p4  & 
           +  & 
           28*MIN2**1.5*MOU2*np4*p1p4  & 
           -  & 
           14*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           +  & 
           5*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           52*MIN2**2*p1p2*p1p4  & 
           + 40*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
           12*MOU2**2*p1p2*p1p4  & 
           -  & 
           40*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           16*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           54*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           54*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           +  & 
           128*MIN2*p1p2**2*p1p4  & 
           - 16*MOU2*p1p2**2*p1p4  & 
           +  & 
           80*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           40*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           -  & 
           80*p1p2**3*p1p4  & 
           + 26*MIN2**2*p1p4**2  & 
           -  & 
           40*MIN2*MOU2*p1p4**2  & 
           + 14*MOU2**2*p1p4**2  & 
           -  & 
           26*MIN2**1.5*np2*p1p4**2  & 
           +  & 
           26*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           52*MIN2*p1p2*p1p4**2  & 
           + 28*MOU2*p1p2*p1p4**2  & 
           +  & 
           40*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           6*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           - 8*MIN2**3*p2p4  & 
           +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           +  & 
           16*MIN2**2*MOU2*p2p4  & 
           - 8*MIN2*MOU2**2*p2p4  & 
           -  & 
           10*MIN2**2.5*np2*p2p4  & 
           -  & 
           2*MIN2**1.5*MOU2*np2*p2p4  & 
           +  & 
           12*sqrt(MIN2)*MOU2**2*np2*p2p4  & 
           +  & 
           18*MIN2**2.5*np4*p2p4  & 
           -  & 
           22*MIN2**1.5*MOU2*np4*p2p4  & 
           +  & 
           4*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           +  & 
           5*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           76*MIN2**2*p1p2*p2p4  & 
           - 64*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
           12*MOU2**2*p1p2*p2p4  & 
           +  & 
           20*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           4*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           22*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           14*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           -  & 
           132*MIN2*p1p2**2*p2p4  & 
           - 4*MOU2*p1p2**2*p2p4  & 
           -  & 
           64*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           + 64*p1p2**3*p2p4  & 
           -  & 
           20*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           -  & 
           112*MIN2**2*p1p4*p2p4  & 
           + 116*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
           4*MOU2**2*p1p4*p2p4  & 
           +  & 
           98*MIN2**1.5*np2*p1p4*p2p4  & 
           -  & 
           34*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           56*MIN2**1.5*np4*p1p4*p2p4  & 
           +  & 
           56*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           +  & 
           76*MIN2*p1p2*p1p4*p2p4  & 
           + 20*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           84*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           +  & 
           56*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           +  & 
           80*p1p2**2*p1p4*p2p4  & 
           + 56*MIN2*p1p4**2*p2p4  & 
           -  & 
           56*MOU2*p1p4**2*p2p4  & 
           -  & 
           56*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           +  & 
           24*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           + 6*MIN2**2*p2p4**2  & 
           -  & 
           6*MIN2*MOU2*p2p4**2  & 
           - 52*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           8*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           +  & 
           24*MIN2**1.5*np4*p2p4**2  & 
           -  & 
           24*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           +  & 
           40*MIN2*p1p2*p2p4**2  & 
           - 8*MOU2*p1p2*p2p4**2  & 
           +  & 
           64*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           -  & 
           64*p1p2**2*p2p4**2  & 
           - 80*MIN2*p1p4*p2p4**2  & 
           +  & 
           24*MOU2*p1p4*p2p4**2  & 
           +  & 
           24*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           + 24*MIN2*p2p4**3)* & 
         Ccache(21) & 
         )/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (2*EL**4*GF**2*(39*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           6*MIN2**3*p1p4  & 
           - 39*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           +  & 
           12*MIN2**2*MOU2*p1p4  & 
           - 6*MIN2*MOU2**2*p1p4  & 
           +  & 
           10*MIN2**2.5*np2*p1p4  & 
           -  & 
           20*MIN2**1.5*MOU2*np2*p1p4  & 
           +  & 
           10*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           +  & 
           20*MIN2**1.5*MOU2*np4*p1p4  & 
           -  & 
           20*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           -  & 
           86*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           +  & 
           42*MIN2**2*p1p2*p1p4  & 
           - 32*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
           10*MOU2**2*p1p2*p1p4  & 
           -  & 
           28*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           28*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           4*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           44*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           52*MIN2*p1p2**2*p1p4  & 
           - 28*MOU2*p1p2**2*p1p4  & 
           -  & 
           16*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           + 16*p1p2**3*p1p4  & 
           -  & 
           12*MIN2**2*p1p4**2  & 
           - 8*MIN2*MOU2*p1p4**2  & 
           +  & 
           20*MOU2**2*p1p4**2  & 
           + 52*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           52*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           32*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           32*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           -  & 
           32*MIN2*p1p2*p1p4**2  & 
           + 96*MOU2*p1p2*p1p4**2  & 
           -  & 
           88*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           64*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           +  & 
           80*p1p2**2*p1p4**2  & 
           + 32*MIN2*p1p4**3  & 
           -  & 
           32*MOU2*p1p4**3  & 
           - 64*p1p2*p1p4**3  & 
           -  & 
           22*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           - 4*MIN2**3*p2p4  & 
           +  & 
           22*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           +  & 
           8*MIN2**2*MOU2*p2p4  & 
           - 4*MIN2*MOU2**2*p2p4  & 
           +  & 
           14*MIN2**2.5*np2*p2p4  & 
           -  & 
           2*MIN2**1.5*MOU2*np2*p2p4  & 
           -  & 
           12*sqrt(MIN2)*MOU2**2*np2*p2p4  & 
           -  & 
           10*MIN2**2.5*np4*p2p4  & 
           -  & 
           20*MIN2**1.5*MOU2*np4*p2p4  & 
           +  & 
           30*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           +  & 
           52*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           -  & 
           46*MIN2**2*p1p2*p2p4  & 
           + 34*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
           12*MOU2**2*p1p2*p2p4  & 
           -  & 
           20*MIN2**1.5*np2*p1p2*p2p4  & 
           -  & 
           32*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           4*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           84*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           +  & 
           116*MIN2*p1p2**2*p2p4  & 
           + 32*MOU2*p1p2**2*p2p4  & 
           -  & 
           16*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           64*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           +  & 
           16*p1p2**3*p2p4  & 
           + 86*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           42*MIN2**2*p1p4*p2p4  & 
           - 12*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
           30*MOU2**2*p1p4*p2p4  & 
           -  & 
           60*MIN2**1.5*np2*p1p4*p2p4  & 
           +  & 
           60*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           +  & 
           48*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           +  & 
           24*MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           144*MOU2*p1p2*p1p4*p2p4  & 
           +  & 
           192*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           136*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           256*p1p2**2*p1p4*p2p4  & 
           - 72*MIN2*p1p4**2*p2p4  & 
           +  & 
           8*MOU2*p1p4**2*p2p4  & 
           +  & 
           88*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           64*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           48*p1p2*p1p4**2*p2p4  & 
           + 64*p1p4**3*p2p4  & 
           -  & 
           52*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           - 16*MIN2**2*p2p4**2  & 
           +  & 
           16*MIN2*MOU2*p2p4**2  & 
           + 60*MIN2**1.5*np2*p2p4**2  & 
           -  & 
           8*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           -  & 
           20*MIN2**1.5*np4*p2p4**2  & 
           -  & 
           60*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           -  & 
           140*MIN2*p1p2*p2p4**2  & 
           + 8*MOU2*p1p2*p2p4**2  & 
           -  & 
           48*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           64*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           +  & 
           48*p1p2**2*p2p4**2  & 
           + 140*MIN2*p1p4*p2p4**2  & 
           +  & 
           60*MOU2*p1p4*p2p4**2  & 
           -  & 
           176*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           128*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           +  & 
           240*p1p2*p1p4*p2p4**2  & 
           - 128*p1p4**2*p2p4**2  & 
           -  & 
           16*MIN2*p2p4**3  & 
           + 64*sqrt(MIN2)*np2*p2p4**3  & 
           -  & 
           64*p1p2*p2p4**3)* & 
         Ccache(22))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (2*EL**4*GF**2*((CMPLX(aa,bb)*asym*sqrt(MIN2)*(5*p1p4  & 
           - 7*p2p4)* & 
               ( & 
           -MIN2  & 
           + MOU2  & 
           + 2*p1p2  & 
           - 2*p2p4)  & 
           +  & 
              2*(sqrt(MIN2)*np2* & 
                  (4*MIN2**2*p1p4  & 
           - 8*MIN2*MOU2*p1p4  & 
           +  & 
                    4*MOU2**2*p1p4  & 
           - 12*MIN2*p1p2*p1p4  & 
           +  & 
                    12*MOU2*p1p2*p1p4  & 
           + 8*p1p2**2*p1p4  & 
           +  & 
                    2*MIN2*p1p4**2  & 
           - 2*MOU2*p1p4**2  & 
           -  & 
                    4*p1p2*p1p4**2  & 
           - 2*MIN2**2*p2p4  & 
           +  & 
                    4*MIN2*MOU2*p2p4  & 
           - 2*MOU2**2*p2p4  & 
           +  & 
                    10*MIN2*p1p2*p2p4  & 
           - 10*MOU2*p1p2*p2p4  & 
           -  & 
                    16*p1p2**2*p2p4  & 
           + 2*MIN2*p1p4*p2p4  & 
           -  & 
                    2*MOU2*p1p4*p2p4  & 
           + 4*p1p2*p1p4*p2p4  & 
           +  & 
                    4*p1p4**2*p2p4  & 
           - 2*MIN2*p2p4**2  & 
           +  & 
                    2*MOU2*p2p4**2  & 
           + 16*p1p2*p2p4**2  & 
           -  & 
                    12*p1p4*p2p4**2)  & 
           +  & 
                 (p1p2  & 
           - p1p4)* & 
                  ( & 
           -4*MIN2**2*p1p4  & 
           + 8*MIN2*MOU2*p1p4  & 
           -  & 
                    4*MOU2**2*p1p4  & 
           + 12*MIN2*p1p2*p1p4  & 
           -  & 
                    12*MOU2*p1p2*p1p4  & 
           - 8*p1p2**2*p1p4  & 
           -  & 
                    2*MIN2*MOU2*p2p4  & 
           + 2*MOU2**2*p2p4  & 
           -  & 
                    6*MIN2*p1p2*p2p4  & 
           + 10*MOU2*p1p2*p2p4  & 
           +  & 
                    16*p1p2**2*p2p4  & 
           - 4*MIN2*p1p4*p2p4  & 
           +  & 
                    4*MOU2*p1p4*p2p4  & 
           - 2*MIN2*p2p4**2  & 
           -  & 
                    2*MOU2*p2p4**2  & 
           - 16*p1p2*p2p4**2  & 
           +  & 
                    8*p1p4*p2p4**2)  & 
           +  & 
                 sqrt(MIN2)*np4* & 
                  ( & 
           -4*MIN2**2*p1p4  & 
           + 8*MIN2*MOU2*p1p4  & 
           -  & 
                    4*MOU2**2*p1p4  & 
           + 10*MIN2*p1p2*p1p4  & 
           -  & 
                    10*MOU2*p1p2*p1p4  & 
           - 4*p1p2**2*p1p4  & 
           +  & 
                    2*MIN2**2*p2p4  & 
           - 4*MIN2*MOU2*p2p4  & 
           +  & 
                    2*MOU2**2*p2p4  & 
           - 8*MIN2*p1p2*p2p4  & 
           +  & 
                    8*MOU2*p1p2*p2p4  & 
           + 12*p1p2**2*p2p4  & 
           -  & 
                    4*MIN2*p1p4*p2p4  & 
           + 4*MOU2*p1p4*p2p4  & 
           -  & 
                    4*p1p2*p1p4*p2p4  & 
           + 2*MIN2*p2p4**2  & 
           -  & 
                    2*MOU2*p2p4**2  & 
           - 12*p1p2*p2p4**2  & 
           +  & 
                    8*p1p4*p2p4**2)))/3.  & 
           +  & 
           (8*CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
               ( & 
           -MIN2  & 
           + MOU2  & 
           + 2*p1p2  & 
           - 2*p2p4)*p2p4  & 
           -  & 
              2*(sqrt(MIN2)*np4* & 
                  (4*MIN2**2*p1p4  & 
           - 8*MIN2*MOU2*p1p4  & 
           +  & 
                    4*MOU2**2*p1p4  & 
           - 12*MIN2*p1p2*p1p4  & 
           +  & 
                    12*MOU2*p1p2*p1p4  & 
           + 8*p1p2**2*p1p4  & 
           -  & 
                    3*MIN2**2*p2p4  & 
           + 6*MIN2*MOU2*p2p4  & 
           -  & 
                    3*MOU2**2*p2p4  & 
           + 12*MIN2*p1p2*p2p4  & 
           -  & 
                    12*MOU2*p1p2*p2p4  & 
           - 16*p1p2**2*p2p4  & 
           +  & 
                    4*MIN2*p1p4*p2p4  & 
           - 4*MOU2*p1p4*p2p4  & 
           -  & 
                    4*MIN2*p2p4**2  & 
           + 4*MOU2*p2p4**2  & 
           +  & 
                    16*p1p2*p2p4**2  & 
           - 8*p1p4*p2p4**2)  & 
           +  & 
                 (p1p2  & 
           - p1p4)* & 
                  (4*MIN2**2*p1p4  & 
           - 8*MIN2*MOU2*p1p4  & 
           +  & 
                    4*MOU2**2*p1p4  & 
           - 12*MIN2*p1p2*p1p4  & 
           +  & 
                    12*MOU2*p1p2*p1p4  & 
           + 8*p1p2**2*p1p4  & 
           -  & 
                    3*MIN2**2*p2p4  & 
           + 6*MIN2*MOU2*p2p4  & 
           -  & 
                    3*MOU2**2*p2p4  & 
           + 16*MIN2*p1p2*p2p4  & 
           -  & 
                    16*MOU2*p1p2*p2p4  & 
           - 24*p1p2**2*p2p4  & 
           +  & 
                    4*MIN2*p1p4*p2p4  & 
           - 4*MOU2*p1p4*p2p4  & 
           -  & 
                    4*MIN2*p2p4**2  & 
           + 4*MOU2*p2p4**2  & 
           +  & 
                    24*p1p2*p2p4**2  & 
           - 8*p1p4*p2p4**2)  & 
           +  & 
                 sqrt(MIN2)*np2* & 
                  ( & 
           -4*MIN2**2*p1p4  & 
           + 8*MIN2*MOU2*p1p4  & 
           -  & 
                    4*MOU2**2*p1p4  & 
           + 12*MIN2*p1p2*p1p4  & 
           -  & 
                    12*MOU2*p1p2*p1p4  & 
           - 8*p1p2**2*p1p4  & 
           +  & 
                    3*MIN2**2*p2p4  & 
           - 6*MIN2*MOU2*p2p4  & 
           +  & 
                    3*MOU2**2*p2p4  & 
           - 16*MIN2*p1p2*p2p4  & 
           +  & 
                    16*MOU2*p1p2*p2p4  & 
           + 24*p1p2**2*p2p4  & 
           -  & 
                    8*p1p2*p1p4*p2p4  & 
           + 4*MIN2*p2p4**2  & 
           -  & 
                    4*MOU2*p2p4**2  & 
           - 24*p1p2*p2p4**2  & 
           +  & 
                    16*p1p4*p2p4**2)))/9.)* & 
         Ccache(23))/ & 
       (p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (2*EL**4*GF**2*((CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
               ( & 
           -4*MIN2*p1p4  & 
           + 4*MOU2*p1p4  & 
           + 21*p1p4**2  & 
           +  & 
                 4*MIN2*p2p4  & 
           - 42*p1p4*p2p4  & 
           + 21*p2p4**2)  & 
           -  & 
              2*(sqrt(MIN2)*np4* & 
                  ( & 
           -8*MIN2*MOU2*p1p4  & 
           + 6*MOU2**2*p1p4  & 
           +  & 
                    14*MOU2*p1p2*p1p4  & 
           + 4*MOU2*p1p4**2  & 
           +  & 
                    4*MIN2*MOU2*p2p4  & 
           - 2*MOU2**2*p2p4  & 
           -  & 
                    6*MOU2*p1p2*p2p4  & 
           - 14*MOU2*p1p4*p2p4  & 
           +  & 
                    2*MOU2*p2p4**2  & 
           +  & 
                    (p1p4  & 
           - p2p4)* & 
                     (2*MIN2**2  & 
           - 6*MIN2*p1p2  & 
           + 4*p1p2**2  & 
           -  & 
                       4*MIN2*p1p4  & 
           + 8*p1p2*p1p4  & 
           +  & 
                       2*MIN2*p2p4  & 
           - 4*p1p2*p2p4  & 
           -  & 
                       8*p1p4*p2p4))  & 
           +  & 
                 p1p4*(6*MIN2*MOU2*p1p4  & 
           - 6*MOU2**2*p1p4  & 
           -  & 
                    4*sqrt(MIN2)*MOU2*np2*p1p4  & 
           -  & 
                    10*MOU2*p1p2*p1p4  & 
           - 4*MOU2*p1p4**2  & 
           -  & 
                    2*MIN2*MOU2*p2p4  & 
           + 2*MOU2**2*p2p4  & 
           +  & 
                    4*sqrt(MIN2)*MOU2*np2*p2p4  & 
           +  & 
                    2*MOU2*p1p2*p2p4  & 
           + 14*MOU2*p1p4*p2p4  & 
           -  & 
                    2*MOU2*p2p4**2  & 
           +  & 
                    (p1p4  & 
           - p2p4)* & 
                     (4*MIN2**1.5*np2  & 
           - 2*MIN2*p1p2  & 
           -  & 
                       4*sqrt(MIN2)*np2*p1p2  & 
           + 4*MIN2*p1p4  & 
           -  & 
                       8*p1p2*p1p4  & 
           + 2*MIN2*p2p4  & 
           +  & 
                       4*sqrt(MIN2)*np2*p2p4  & 
           + 8*p1p4*p2p4))) & 
              )/3.  & 
           + ( & 
           -(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
                 ( & 
           -6*MIN2*p1p4  & 
           + 6*MOU2*p1p4  & 
           + 8*p1p2*p1p4  & 
           +  & 
                   6*MIN2*p2p4  & 
           - 6*MOU2*p2p4  & 
           - 8*p1p2*p2p4  & 
           -  & 
                   8*p1p4*p2p4  & 
           + 8*p2p4**2))  & 
           +  & 
              2*(p1p4*( & 
           -10*MIN2*MOU2*p1p4  & 
           + 7*MOU2**2*p1p4  & 
           -  & 
                    sqrt(MIN2)*MOU2*np2*p1p4  & 
           +  & 
                    16*MOU2*p1p2*p1p4  & 
           + 4*MOU2*p1p4**2  & 
           +  & 
                    6*MIN2*MOU2*p2p4  & 
           - 3*MOU2**2*p2p4  & 
           +  & 
                    sqrt(MIN2)*MOU2*np2*p2p4  & 
           -  & 
                    8*MOU2*p1p2*p2p4  & 
           - 16*MOU2*p1p4*p2p4  & 
           +  & 
                    4*MOU2*p2p4**2  & 
           +  & 
                    (p1p4  & 
           - p2p4)* & 
                     (3*MIN2**2  & 
           + MIN2**1.5*np2  & 
           -  & 
                       8*MIN2*p1p2  & 
           - 4*sqrt(MIN2)*np2*p1p2  & 
           +  & 
                       8*p1p2**2  & 
           - 4*MIN2*p1p4  & 
           +  & 
                       8*p1p2*p1p4  & 
           + 4*MIN2*p2p4  & 
           +  & 
                       4*sqrt(MIN2)*np2*p2p4  & 
           - 8*p1p2*p2p4  & 
           -  & 
                       8*p1p4*p2p4))  & 
           +  & 
                 sqrt(MIN2)*np4* & 
                  (10*MIN2*MOU2*p1p4  & 
           - 7*MOU2**2*p1p4  & 
           -  & 
                    15*MOU2*p1p2*p1p4  & 
           - 4*MOU2*p1p4**2  & 
           -  & 
                    6*MIN2*MOU2*p2p4  & 
           + 3*MOU2**2*p2p4  & 
           +  & 
                    7*MOU2*p1p2*p2p4  & 
           + 16*MOU2*p1p4*p2p4  & 
           -  & 
                    4*MOU2*p2p4**2  & 
           +  & 
                    (p1p4  & 
           - p2p4)* & 
                     ( & 
           -3*MIN2**2  & 
           + 7*MIN2*p1p2  & 
           - 4*p1p2**2  & 
           +  & 
                       4*MIN2*p1p4  & 
           - 8*p1p2*p1p4  & 
           -  & 
                       4*MIN2*p2p4  & 
           + 4*p1p2*p2p4  & 
           +  & 
                       8*p1p4*p2p4))))/9.)* & 
         Ccache(24))/ & 
       (p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (16*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (2*p1p2*p1p4  & 
           - MIN2*(p1p4  & 
           + p2p4)  & 
           +  & 
              MOU2*(p1p4  & 
           + p2p4))  & 
           -  & 
           2*(MOU2**2*(2*MIN2*p1p4  & 
           +  & 
                 p1p2*( & 
           -2*p1p4  & 
           + p2p4))  & 
           +  & 
              (MIN2  & 
           - p1p2)* & 
               (2*MIN2**2*p1p4  & 
           + sqrt(MIN2)*np4*p1p2*p2p4  & 
           -  & 
                 4*p1p2*(p1p2  & 
           - p2p4)*( & 
           -2*p1p4  & 
           + p2p4)  & 
           +  & 
                 MIN2*( & 
           -8*p1p2*p1p4  & 
           + 3*p1p2*p2p4  & 
           +  & 
                    p1p4*p2p4))  & 
           +  & 
              sqrt(MIN2)*np2* & 
               (MIN2**2*(2*p1p4  & 
           - p2p4)  & 
           +  & 
                 MOU2**2*(2*p1p4  & 
           - p2p4)  & 
           +  & 
                 MIN2*( & 
           -8*p1p2*p1p4  & 
           + 3*p1p2*p2p4  & 
           +  & 
                    6*p1p4*p2p4  & 
           - 3*p2p4**2)  & 
           +  & 
                 MOU2*( & 
           -4*MIN2*p1p4  & 
           + 8*p1p2*p1p4  & 
           +  & 
                    2*MIN2*p2p4  & 
           - 3*p1p2*p2p4  & 
           -  & 
                    6*p1p4*p2p4  & 
           + 3*p2p4**2)  & 
           +  & 
                 p1p2*(8*p1p2*p1p4  & 
           - 4*p1p2*p2p4  & 
           -  & 
                    7*p1p4*p2p4  & 
           + 4*p2p4**2))  & 
           -  & 
              MOU2*(4*MIN2**2*p1p4  & 
           +  & 
                 sqrt(MIN2)*np4*p1p2*p2p4  & 
           +  & 
                 p1p2*(8*p1p2*p1p4  & 
           - 3*p1p2*p2p4  & 
           -  & 
                    7*p1p4*p2p4  & 
           + 3*p2p4**2)  & 
           +  & 
                 MIN2*(p1p4*p2p4  & 
           + 4*p1p2*( & 
           -3*p1p4  & 
           + p2p4)))) & 
           )*Ccache(25))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (16*EL**4*GF**2*( & 
           -MIN2  & 
           + MOU2  & 
           + 2*p1p2  & 
           - 2*p2p4)* & 
         (CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4  & 
           - p2p4)  & 
           +  & 
           2*( & 
           -(MIN2**2*p1p4)  & 
           + MIN2*MOU2*p1p4  & 
           +  & 
              3*MIN2*p1p2*p1p4  & 
           - MOU2*p1p2*p1p4  & 
           -  & 
              2*p1p2**2*p1p4  & 
           - MIN2*p1p4**2  & 
           + MOU2*p1p4**2  & 
           +  & 
              2*p1p4**3  & 
           - 2*MIN2*p1p2*p2p4  & 
           +  & 
              MOU2*p1p2*p2p4  & 
           + MIN2*p1p4*p2p4  & 
           -  & 
              MOU2*p1p4*p2p4  & 
           + 6*p1p2*p1p4*p2p4  & 
           -  & 
              6*p1p4**2*p2p4  & 
           - 2*p1p2*p2p4**2  & 
           +  & 
              2*p1p4*p2p4**2  & 
           -  & 
              sqrt(MIN2)*np4* & 
               ( & 
           -(MIN2*p1p4)  & 
           + MOU2*p1p4  & 
           + p1p2*p1p4  & 
           +  & 
                 2*p1p4**2  & 
           + MIN2*p2p4  & 
           - MOU2*p2p4  & 
           +  & 
                 p1p2*p2p4  & 
           - 6*p1p4*p2p4  & 
           + 2*p2p4**2)  & 
           +  & 
              sqrt(MIN2)*np2* & 
               ( & 
           -(MIN2*p1p4)  & 
           + 2*p1p2*p1p4  & 
           + p1p4**2  & 
           +  & 
                 MOU2*(p1p4  & 
           - p2p4)  & 
           + MIN2*p2p4  & 
           -  & 
                 5*p1p4*p2p4  & 
           + 2*p2p4**2)))* & 
         Ccache(26))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (64*EL**4*GF**2*(p1p2  & 
           - p1p4)* & 
         (sqrt(MIN2)*np2  & 
           - sqrt(MIN2)*np4  & 
           - p1p2  & 
           + p1p4)* & 
         ( & 
           -MIN2  & 
           + MOU2  & 
           + 2*p1p2  & 
           - 2*p2p4)*p2p4* & 
         Ccache(27))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (64*EL**4*GF**2*(sqrt(MIN2)*np4  & 
           - p1p4)* & 
         ( & 
           -MIN2  & 
           + MOU2  & 
           + 2*p1p2  & 
           - 2*p2p4)* & 
         Ccache(28)) & 
        /(9.*Pi**2)  & 
           + (2*EL**4*GF**2* & 
         ( & 
           -38*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4  & 
           + 56*MIN2**4*p1p4  & 
           +  & 
           32*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
           -  & 
           112*MIN2**3*MOU2*p1p4  & 
           +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4  & 
           +  & 
           56*MIN2**2*MOU2**2*p1p4  & 
           + 72*MIN2**3.5*np2*p1p4  & 
           -  & 
           144*MIN2**2.5*MOU2*np2*p1p4  & 
           +  & 
           72*MIN2**1.5*MOU2**2*np2*p1p4  & 
           -  & 
           36*MIN2**3.5*np4*p1p4  & 
           +  & 
           72*MIN2**2.5*MOU2*np4*p1p4  & 
           -  & 
           36*MIN2**1.5*MOU2**2*np4*p1p4  & 
           +  & 
           112*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4  & 
           -  & 
           296*MIN2**3*p1p2*p1p4  & 
           -  & 
           20*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
           +  & 
           424*MIN2**2*MOU2*p1p2*p1p4  & 
           -  & 
           128*MIN2*MOU2**2*p1p2*p1p4  & 
           -  & 
           272*MIN2**2.5*np2*p1p2*p1p4  & 
           +  & 
           344*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
           -  & 
           72*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4  & 
           +  & 
           136*MIN2**2.5*np4*p1p2*p1p4  & 
           -  & 
           172*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
           +  & 
           36*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4  & 
           -  & 
           72*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4  & 
           +  & 
           552*MIN2**2*p1p2**2*p1p4  & 
           -  & 
           512*MIN2*MOU2*p1p2**2*p1p4  & 
           +  & 
           72*MOU2**2*p1p2**2*p1p4  & 
           +  & 
           328*MIN2**1.5*np2*p1p2**2*p1p4  & 
           -  & 
           200*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4  & 
           -  & 
           164*MIN2**1.5*np4*p1p2**2*p1p4  & 
           +  & 
           100*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
           -  & 
           440*MIN2*p1p2**3*p1p4  & 
           + 200*MOU2*p1p2**3*p1p4  & 
           -  & 
           128*sqrt(MIN2)*np2*p1p2**3*p1p4  & 
           +  & 
           64*sqrt(MIN2)*np4*p1p2**3*p1p4  & 
           +  & 
           128*p1p2**4*p1p4  & 
           + 2*MIN2**3*p1p4**2  & 
           -  & 
           30*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
           -  & 
           4*MIN2**2*MOU2*p1p4**2  & 
           + 2*MIN2*MOU2**2*p1p4**2  & 
           -  & 
           2*MIN2**2.5*np2*p1p4**2  & 
           +  & 
           20*MIN2**1.5*MOU2*np2*p1p4**2  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4**2  & 
           -  & 
           32*MIN2**2.5*np4*p1p4**2  & 
           +  & 
           32*MIN2**1.5*MOU2*np4*p1p4**2  & 
           -  & 
           36*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
           -  & 
           18*MIN2**2*p1p2*p1p4**2  & 
           +  & 
           36*MIN2*MOU2*p1p2*p1p4**2  & 
           -  & 
           18*MOU2**2*p1p2*p1p4**2  & 
           +  & 
           20*MIN2**1.5*np2*p1p2*p1p4**2  & 
           -  & 
           52*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
           +  & 
           96*MIN2**1.5*np4*p1p2*p1p4**2  & 
           -  & 
           32*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
           +  & 
           48*MIN2*p1p2**2*p1p4**2  & 
           -  & 
           48*MOU2*p1p2**2*p1p4**2  & 
           -  & 
           32*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
           -  & 
           64*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
           -  & 
           32*p1p2**3*p1p4**2  & 
           - 12*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
           +  & 
           56*MIN2**2*p1p4**3  & 
           - 56*MIN2*MOU2*p1p4**3  & 
           +  & 
           24*MIN2**1.5*np2*p1p4**3  & 
           -  & 
           24*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
           -  & 
           168*MIN2*p1p2*p1p4**3  & 
           + 56*MOU2*p1p2*p1p4**3  & 
           -  & 
           48*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
           +  & 
           112*p1p2**2*p1p4**3  & 
           + 31*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4  & 
           -  & 
           56*MIN2**4*p2p4  & 
           - 37*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
           +  & 
           56*MIN2**3*MOU2*p2p4  & 
           +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p2p4  & 
           -  & 
           72*MIN2**3.5*np2*p2p4  & 
           +  & 
           72*MIN2**2.5*MOU2*np2*p2p4  & 
           +  & 
           36*MIN2**3.5*np4*p2p4  & 
           -  & 
           36*MIN2**1.5*MOU2**2*np4*p2p4  & 
           -  & 
           94*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4  & 
           +  & 
           296*MIN2**3*p1p2*p2p4  & 
           +  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4  & 
           -  & 
           184*MIN2**2*MOU2*p1p2*p2p4  & 
           +  & 
           272*MIN2**2.5*np2*p1p2*p2p4  & 
           -  & 
           144*MIN2**1.5*MOU2*np2*p1p2*p2p4  & 
           -  & 
           136*MIN2**2.5*np4*p1p2*p2p4  & 
           -  & 
           28*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
           +  & 
           36*sqrt(MIN2)*MOU2**2*np4*p1p2*p2p4  & 
           +  & 
           61*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p2p4  & 
           -  & 
           552*MIN2**2*p1p2**2*p2p4  & 
           +  & 
           200*MIN2*MOU2*p1p2**2*p2p4  & 
           -  & 
           328*MIN2**1.5*np2*p1p2**2*p2p4  & 
           +  & 
           72*sqrt(MIN2)*MOU2*np2*p1p2**2*p2p4  & 
           +  & 
           164*MIN2**1.5*np4*p1p2**2*p2p4  & 
           +  & 
           28*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4  & 
           +  & 
           440*MIN2*p1p2**3*p2p4  & 
           - 72*MOU2*p1p2**3*p2p4  & 
           +  & 
           128*sqrt(MIN2)*np2*p1p2**3*p2p4  & 
           -  & 
           64*sqrt(MIN2)*np4*p1p2**3*p2p4  & 
           -  & 
           128*p1p2**4*p2p4  & 
           - 111*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
           +  & 
           180*MIN2**3*p1p4*p2p4  & 
           +  & 
           104*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
           -  & 
           248*MIN2**2*MOU2*p1p4*p2p4  & 
           +  & 
           68*MIN2*MOU2**2*p1p4*p2p4  & 
           +  & 
           168*MIN2**2.5*np2*p1p4*p2p4  & 
           -  & 
           168*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
           -  & 
           72*MIN2**2.5*np4*p1p4*p2p4  & 
           +  & 
           72*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
           +  & 
           222*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
           -  & 
           588*MIN2**2*p1p2*p1p4*p2p4  & 
           +  & 
           512*MIN2*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           36*MOU2**2*p1p2*p1p4*p2p4  & 
           -  & 
           396*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
           +  & 
           204*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4  & 
           +  & 
           120*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
           -  & 
           72*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
           +  & 
           600*MIN2*p1p2**2*p1p4*p2p4  & 
           -  & 
           232*MOU2*p1p2**2*p1p4*p2p4  & 
           +  & 
           256*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4  & 
           -  & 
           48*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
           -  & 
           192*p1p2**3*p1p4*p2p4  & 
           +  & 
           35*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
           -  & 
           104*MIN2**2*p1p4**2*p2p4  & 
           +  & 
           128*MIN2*MOU2*p1p4**2*p2p4  & 
           -  & 
           112*MIN2**1.5*np2*p1p4**2*p2p4  & 
           +  & 
           144*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
           -  & 
           64*MIN2**1.5*np4*p1p4**2*p2p4  & 
           +  & 
           280*MIN2*p1p2*p1p4**2*p2p4  & 
           -  & 
           72*MOU2*p1p2*p1p4**2*p2p4  & 
           +  & 
           224*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
           +  & 
           64*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
           -  & 
           176*p1p2**2*p1p4**2*p2p4  & 
           +  & 
           112*MIN2*p1p4**3*p2p4  & 
           +  & 
           48*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
           -  & 
           112*p1p2*p1p4**3*p2p4  & 
           +  & 
           70*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
           - 182*MIN2**3*p2p4**2  & 
           -  & 
           73*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2  & 
           +  & 
           68*MIN2**2*MOU2*p2p4**2  & 
           +  & 
           2*MIN2*MOU2**2*p2p4**2  & 
           -  & 
           166*MIN2**2.5*np2*p2p4**2  & 
           +  & 
           20*MIN2**1.5*MOU2*np2*p2p4**2  & 
           +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p2p4**2  & 
           +  & 
           104*MIN2**2.5*np4*p2p4**2  & 
           +  & 
           24*MIN2**1.5*MOU2*np4*p2p4**2  & 
           -  & 
           120*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2  & 
           +  & 
           606*MIN2**2*p1p2*p2p4**2  & 
           -  & 
           108*MIN2*MOU2*p1p2*p2p4**2  & 
           -  & 
           18*MOU2**2*p1p2*p2p4**2  & 
           +  & 
           376*MIN2**1.5*np2*p1p2*p2p4**2  & 
           -  & 
           24*sqrt(MIN2)*MOU2*np2*p1p2*p2p4**2  & 
           -  & 
           216*MIN2**1.5*np4*p1p2*p2p4**2  & 
           -  & 
           24*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2  & 
           -  & 
           648*MIN2*p1p2**2*p2p4**2  & 
           +  & 
           24*MOU2*p1p2**2*p2p4**2  & 
           -  & 
           224*sqrt(MIN2)*np2*p1p2**2*p2p4**2  & 
           +  & 
           112*sqrt(MIN2)*np4*p1p2**2*p2p4**2  & 
           +  & 
           224*p1p2**3*p2p4**2  & 
           -  & 
           208*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
           +  & 
           208*MIN2**2*p1p4*p2p4**2  & 
           -  & 
           288*MIN2*MOU2*p1p4*p2p4**2  & 
           +  & 
           216*MIN2**1.5*np2*p1p4*p2p4**2  & 
           -  & 
           152*sqrt(MIN2)*MOU2*np2*p1p4*p2p4**2  & 
           +  & 
           16*MIN2**1.5*np4*p1p4*p2p4**2  & 
           -  & 
           464*MIN2*p1p2*p1p4*p2p4**2  & 
           +  & 
           176*MOU2*p1p2*p1p4*p2p4**2  & 
           -  & 
           368*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2  & 
           -  & 
           16*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
           +  & 
           256*p1p2**2*p1p4*p2p4**2  & 
           -  & 
           208*MIN2*p1p4**2*p2p4**2  & 
           -  & 
           192*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
           +  & 
           208*p1p2*p1p4**2*p2p4**2  & 
           +  & 
           142*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3  & 
           -  & 
           160*MIN2**2*p2p4**3  & 
           + 88*MIN2*MOU2*p2p4**3  & 
           -  & 
           128*MIN2**1.5*np2*p2p4**3  & 
           +  & 
           32*sqrt(MIN2)*MOU2*np2*p2p4**3  & 
           +  & 
           48*MIN2**1.5*np4*p2p4**3  & 
           +  & 
           352*MIN2*p1p2*p2p4**3  & 
           - 32*MOU2*p1p2*p2p4**3  & 
           +  & 
           192*sqrt(MIN2)*np2*p1p2*p2p4**3  & 
           -  & 
           48*sqrt(MIN2)*np4*p1p2*p2p4**3  & 
           -  & 
           192*p1p2**2*p2p4**3  & 
           + 192*MIN2*p1p4*p2p4**3  & 
           +  & 
           240*sqrt(MIN2)*np2*p1p4*p2p4**3  & 
           -  & 
           192*p1p2*p1p4*p2p4**3  & 
           - 96*MIN2*p2p4**4  & 
           -  & 
           96*sqrt(MIN2)*np2*p2p4**4  & 
           + 96*p1p2*p2p4**4)* & 
         Dcache(1))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (4*CMPLX(aa,bb)*asym*EL**4*GF**2*sqrt(MIN2)* & 
         (MIN2*p1p4  & 
           - MOU2*p1p4  & 
           - 5*p1p2*p1p4  & 
           - 5*p1p4**2  & 
           -  & 
           5*MIN2*p2p4  & 
           + 5*MOU2*p2p4  & 
           + 12*p1p2*p2p4  & 
           +  & 
           3*p1p4*p2p4  & 
           - 3*p2p4**2)* & 
         Dcache(2))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (2*CMPLX(aa,bb)*asym*EL**4*GF**2*sqrt(MIN2)* & 
         (50*MIN2**2*p1p4  & 
           - 44*MIN2*MOU2*p1p4  & 
           -  & 
           6*MOU2**2*p1p4  & 
           - 140*MIN2*p1p2*p1p4  & 
           +  & 
           38*MOU2*p1p2*p1p4  & 
           + 88*p1p2**2*p1p4  & 
           -  & 
           14*MIN2*p1p4**2  & 
           + 22*MOU2*p1p4**2  & 
           -  & 
           11*p1p2*p1p4**2  & 
           + 103*p1p4**3  & 
           - 43*MIN2**2*p2p4  & 
           +  & 
           55*MIN2*MOU2*p2p4  & 
           - 12*MOU2**2*p2p4  & 
           +  & 
           122*MIN2*p1p2*p2p4  & 
           - 62*MOU2*p1p2*p2p4  & 
           -  & 
           77*p1p2**2*p2p4  & 
           + 148*MIN2*p1p4*p2p4  & 
           -  & 
           103*MOU2*p1p4*p2p4  & 
           - 157*p1p2*p1p4*p2p4  & 
           -  & 
           152*p1p4**2*p2p4  & 
           - 93*MIN2*p2p4**2  & 
           +  & 
           86*MOU2*p2p4**2  & 
           + 102*p1p2*p2p4**2  & 
           +  & 
           188*p1p4*p2p4**2  & 
           - 96*p2p4**3)* & 
         Dcache(3))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (2*EL**4*GF**2*sqrt(MIN2)* & 
         (4*CMPLX(aa,bb)*asym*MIN2**2*p1p4  & 
           - 12*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4  & 
           +  & 
           8*CMPLX(aa,bb)*asym*MOU2**2*p1p4  & 
           + 36*MIN2**3*np2*p1p4  & 
           -  & 
           108*MIN2**2*MOU2*np2*p1p4  & 
           +  & 
           108*MIN2*MOU2**2*np2*p1p4  & 
           - 36*MOU2**3*np2*p1p4  & 
           +  & 
           28*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4  & 
           - 20*CMPLX(aa,bb)*asym*MOU2*p1p2*p1p4  & 
           -  & 
           136*MIN2**2*np2*p1p2*p1p4  & 
           +  & 
           272*MIN2*MOU2*np2*p1p2*p1p4  & 
           -  & 
           136*MOU2**2*np2*p1p2*p1p4  & 
           -  & 
           30*CMPLX(aa,bb)*asym*p1p2**2*p1p4  & 
           +  & 
           164*MIN2*np2*p1p2**2*p1p4  & 
           -  & 
           164*MOU2*np2*p1p2**2*p1p4  & 
           - 64*np2*p1p2**3*p1p4  & 
           -  & 
           30*CMPLX(aa,bb)*asym*MIN2*p1p4**2  & 
           + 30*CMPLX(aa,bb)*asym*MOU2*p1p4**2  & 
           +  & 
           2*MIN2**2*np2*p1p4**2  & 
           +  & 
           20*MIN2*MOU2*np2*p1p4**2  & 
           -  & 
           22*MOU2**2*np2*p1p4**2  & 
           + 24*CMPLX(aa,bb)*asym*p1p2*p1p4**2  & 
           +  & 
           32*MIN2*np2*p1p2*p1p4**2  & 
           -  & 
           80*MOU2*np2*p1p2*p1p4**2  & 
           -  & 
           48*np2*p1p2**2*p1p4**2  & 
           + 24*MIN2*np2*p1p4**3  & 
           -  & 
           24*MOU2*np2*p1p4**3  & 
           - 48*np2*p1p2*p1p4**3  & 
           -  & 
           10*CMPLX(aa,bb)*asym*MIN2**2*p2p4  & 
           - 2*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4  & 
           +  & 
           12*CMPLX(aa,bb)*asym*MOU2**2*p2p4  & 
           - 36*MIN2**3*np2*p2p4  & 
           +  & 
           72*MIN2**2*MOU2*np2*p2p4  & 
           -  & 
           36*MIN2*MOU2**2*np2*p2p4  & 
           -  & 
           12*CMPLX(aa,bb)*asym*MIN2*p1p2*p2p4  & 
           + 40*CMPLX(aa,bb)*asym*MOU2*p1p2*p2p4  & 
           +  & 
           136*MIN2**2*np2*p1p2*p2p4  & 
           -  & 
           172*MIN2*MOU2*np2*p1p2*p2p4  & 
           +  & 
           36*MOU2**2*np2*p1p2*p2p4  & 
           + 20*CMPLX(aa,bb)*asym*p1p2**2*p2p4  & 
           -  & 
           164*MIN2*np2*p1p2**2*p2p4  & 
           +  & 
           100*MOU2*np2*p1p2**2*p2p4  & 
           + 64*np2*p1p2**3*p2p4  & 
           -  & 
           51*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4  & 
           - 13*CMPLX(aa,bb)*asym*MOU2*p1p4*p2p4  & 
           +  & 
           60*MIN2**2*np2*p1p4*p2p4  & 
           -  & 
           128*MIN2*MOU2*np2*p1p4*p2p4  & 
           +  & 
           68*MOU2**2*np2*p1p4*p2p4  & 
           +  & 
           108*CMPLX(aa,bb)*asym*p1p2*p1p4*p2p4  & 
           -  & 
           192*MIN2*np2*p1p2*p1p4*p2p4  & 
           +  & 
           192*MOU2*np2*p1p2*p1p4*p2p4  & 
           +  & 
           160*np2*p1p2**2*p1p4*p2p4  & 
           -  & 
           91*CMPLX(aa,bb)*asym*p1p4**2*p2p4  & 
           -  & 
           128*MIN2*np2*p1p4**2*p2p4  & 
           +  & 
           176*MOU2*np2*p1p4**2*p2p4  & 
           +  & 
           240*np2*p1p2*p1p4**2*p2p4  & 
           + 48*np2*p1p4**3*p2p4  & 
           +  & 
           41*CMPLX(aa,bb)*asym*MIN2*p2p4**2  & 
           - 33*CMPLX(aa,bb)*asym*MOU2*p2p4**2  & 
           -  & 
           62*MIN2**2*np2*p2p4**2  & 
           +  & 
           44*MIN2*MOU2*np2*p2p4**2  & 
           +  & 
           18*MOU2**2*np2*p2p4**2  & 
           - 67*CMPLX(aa,bb)*asym*p1p2*p2p4**2  & 
           +  & 
           160*MIN2*np2*p1p2*p2p4**2  & 
           -  & 
           48*MOU2*np2*p1p2*p2p4**2  & 
           -  & 
           112*np2*p1p2**2*p2p4**2  & 
           - 28*CMPLX(aa,bb)*asym*p1p4*p2p4**2  & 
           +  & 
           184*MIN2*np2*p1p4*p2p4**2  & 
           -  & 
           184*MOU2*np2*p1p4*p2p4**2  & 
           -  & 
           336*np2*p1p2*p1p4*p2p4**2  & 
           -  & 
           192*np2*p1p4**2*p2p4**2  & 
           + 76*CMPLX(aa,bb)*asym*p2p4**3  & 
           -  & 
           80*MIN2*np2*p2p4**3  & 
           + 32*MOU2*np2*p2p4**3  & 
           +  & 
           144*np2*p1p2*p2p4**3  & 
           + 240*np2*p1p4*p2p4**3  & 
           -  & 
           96*np2*p2p4**4)* & 
         Dcache(4))/(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (2*EL**4*GF**2*sqrt(MIN2)* & 
         (( & 
           -22*CMPLX(aa,bb)*asym*MIN2**2*p1p4  & 
           + 42*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4  & 
           -  & 
              20*CMPLX(aa,bb)*asym*MOU2**2*p1p4  & 
           +  & 
              60*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4  & 
           -  & 
              56*CMPLX(aa,bb)*asym*MOU2*p1p2*p1p4  & 
           -  & 
              34*CMPLX(aa,bb)*asym*p1p2**2*p1p4  & 
           - 2*CMPLX(aa,bb)*asym*MIN2*p1p4**2  & 
           +  & 
              2*CMPLX(aa,bb)*asym*MOU2*p1p4**2  & 
           - 2*CMPLX(aa,bb)*asym*p1p2*p1p4**2  & 
           +  & 
              4*CMPLX(aa,bb)*asym*p1p4**3  & 
           + 16*CMPLX(aa,bb)*asym*MIN2**2*p2p4  & 
           -  & 
              25*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4  & 
           + 9*CMPLX(aa,bb)*asym*MOU2**2*p2p4  & 
           -  & 
              40*CMPLX(aa,bb)*asym*MIN2*p1p2*p2p4  & 
           +  & 
              26*CMPLX(aa,bb)*asym*MOU2*p1p2*p2p4  & 
           +  & 
              20*CMPLX(aa,bb)*asym*p1p2**2*p2p4  & 
           -  & 
              14*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4  & 
           +  & 
              10*CMPLX(aa,bb)*asym*MOU2*p1p4*p2p4  & 
           -  & 
              4*CMPLX(aa,bb)*asym*p1p2*p1p4*p2p4  & 
           + 8*CMPLX(aa,bb)*asym*p1p4**2*p2p4  & 
           +  & 
              7*CMPLX(aa,bb)*asym*MIN2*p2p4**2  & 
           + 7*CMPLX(aa,bb)*asym*MOU2*p2p4**2  & 
           +  & 
              8*CMPLX(aa,bb)*asym*p1p2*p2p4**2  & 
           + 16*CMPLX(aa,bb)*asym*p1p4*p2p4**2  & 
           -  & 
              22*CMPLX(aa,bb)*asym*p2p4**3  & 
           -  & 
              2*(np2  & 
           - np4)* & 
               ( & 
           -18*MIN2**2*MOU2*p1p4  & 
           - 6*MOU2**3*p1p4  & 
           +  & 
                 40*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
                 22*MOU2*p1p2**2*p1p4  & 
           -  & 
                 2*MIN2*MOU2*p1p4**2  & 
           + 2*MOU2*p1p2*p1p4**2  & 
           +  & 
                 12*MIN2**2*MOU2*p2p4  & 
           -  & 
                 26*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
                 14*MOU2*p1p2**2*p2p4  & 
           -  & 
                 10*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
                 12*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
                 2*MOU2*p1p4**2*p2p4  & 
           + 4*MIN2*MOU2*p2p4**2  & 
           -  & 
                 6*MOU2*p1p2*p2p4**2  & 
           + 4*MOU2*p1p4*p2p4**2  & 
           -  & 
                 2*MOU2*p2p4**3  & 
           +  & 
                 MOU2**2* & 
                  (18*MIN2*p1p4  & 
           - 20*p1p2*p1p4  & 
           + p1p4**2  & 
           -  & 
                    6*MIN2*p2p4  & 
           + 6*p1p2*p2p4  & 
           +  & 
                    4*p1p4*p2p4  & 
           + 3*p2p4**2)  & 
           +  & 
                 (p1p4  & 
           - p2p4)* & 
                  (6*MIN2**3  & 
           - 20*MIN2**2*p1p2  & 
           +  & 
                    22*MIN2*p1p2**2  & 
           - 8*p1p2**3  & 
           +  & 
                    MIN2**2*p1p4  & 
           - 2*MIN2*p1p2*p1p4  & 
           +  & 
                    7*MIN2**2*p2p4  & 
           - 14*MIN2*p1p2*p2p4  & 
           +  & 
                    8*p1p2**2*p2p4  & 
           + 2*MIN2*p1p4*p2p4  & 
           -  & 
                    2*MIN2*p2p4**2)))/9.  & 
           +  & 
           (18*CMPLX(aa,bb)*asym*MIN2**2*p1p4  & 
           - 26*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4  & 
           +  & 
              8*CMPLX(aa,bb)*asym*MOU2**2*p1p4  & 
           - 52*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4  & 
           +  & 
              32*CMPLX(aa,bb)*asym*MOU2*p1p2*p1p4  & 
           +  & 
              32*CMPLX(aa,bb)*asym*p1p2**2*p1p4  & 
           - 2*CMPLX(aa,bb)*asym*MIN2*p1p4**2  & 
           +  & 
              2*CMPLX(aa,bb)*asym*MOU2*p1p4**2  & 
           - 6*CMPLX(aa,bb)*asym*p1p2*p1p4**2  & 
           +  & 
              6*CMPLX(aa,bb)*asym*p1p4**3  & 
           - 14*CMPLX(aa,bb)*asym*MIN2**2*p2p4  & 
           +  & 
              21*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4  & 
           - 7*CMPLX(aa,bb)*asym*MOU2**2*p2p4  & 
           +  & 
              40*CMPLX(aa,bb)*asym*MIN2*p1p2*p2p4  & 
           -  & 
              26*CMPLX(aa,bb)*asym*MOU2*p1p2*p2p4  & 
           -  & 
              24*CMPLX(aa,bb)*asym*p1p2**2*p2p4  & 
           +  & 
              56*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4  & 
           -  & 
              36*CMPLX(aa,bb)*asym*MOU2*p1p4*p2p4  & 
           -  & 
              56*CMPLX(aa,bb)*asym*p1p2*p1p4*p2p4  & 
           +  & 
              30*CMPLX(aa,bb)*asym*p1p4**2*p2p4  & 
           - 38*CMPLX(aa,bb)*asym*MIN2*p2p4**2  & 
           +  & 
              24*CMPLX(aa,bb)*asym*MOU2*p2p4**2  & 
           + 40*CMPLX(aa,bb)*asym*p1p2*p2p4**2  & 
           -  & 
              26*CMPLX(aa,bb)*asym*p1p4*p2p4**2  & 
           + 2*CMPLX(aa,bb)*asym*p2p4**3  & 
           +  & 
              2*(np2  & 
           - np4)* & 
               (12*MIN2**2*MOU2*p1p4  & 
           + 4*MOU2**3*p1p4  & 
           -  & 
                 32*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
                 20*MOU2*p1p2**2*p1p4  & 
           -  & 
                 4*MIN2*MOU2*p1p4**2  & 
           +  & 
                 14*MOU2*p1p2*p1p4**2  & 
           + 4*MOU2*p1p4**3  & 
           -  & 
                 8*MIN2**2*MOU2*p2p4  & 
           +  & 
                 20*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
                 12*MOU2*p1p2**2*p2p4  & 
           +  & 
                 18*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
                 28*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
                 30*MOU2*p1p4**2*p2p4  & 
           -  & 
                 6*MIN2*MOU2*p2p4**2  & 
           + 6*MOU2*p1p2*p2p4**2  & 
           +  & 
                 32*MOU2*p1p4*p2p4**2  & 
           - 6*MOU2*p2p4**3  & 
           +  & 
                 MOU2**2* & 
                  ( & 
           -12*MIN2*p1p4  & 
           + 16*p1p2*p1p4  & 
           +  & 
                    4*p1p4**2  & 
           + 4*MIN2*p2p4  & 
           - 4*p1p2*p2p4  & 
           -  & 
                    10*p1p4*p2p4  & 
           - 2*p2p4**2)  & 
           +  & 
                 (p1p4  & 
           - p2p4)* & 
                  ( & 
           -4*MIN2**3  & 
           + 16*MIN2**2*p1p2  & 
           -  & 
                    20*MIN2*p1p2**2  & 
           + 8*p1p2**3  & 
           -  & 
                    6*MIN2*p1p2*p1p4  & 
           + 8*p1p2**2*p1p4  & 
           -  & 
                    4*MIN2*p1p4**2  & 
           + 8*p1p2*p1p4**2  & 
           -  & 
                    8*MIN2**2*p2p4  & 
           + 22*MIN2*p1p2*p2p4  & 
           -  & 
                    16*p1p2**2*p2p4  & 
           + 18*MIN2*p1p4*p2p4  & 
           -  & 
                    32*p1p2*p1p4*p2p4  & 
           - 8*p1p4**2*p2p4  & 
           -  & 
                    14*MIN2*p2p4**2  & 
           + 24*p1p2*p2p4**2  & 
           +  & 
                    24*p1p4*p2p4**2  & 
           - 16*p2p4**3)))/3.)* & 
         Dcache(5))/(p1p4*(p1p4  & 
           - p2p4)*Pi**2))
!
!	  T1BOXAC=T1BOXAC
!     ^       +real(
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T2BOXBC
                          !!!!!!!!!!!!!!!!!!!!!!
