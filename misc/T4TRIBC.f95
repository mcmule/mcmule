
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T4TRIBC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T4TRIBC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Ccache(6)
    complex(kind=prec) :: Bcache(1)

!
      real(kind=prec) T4TRIBC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb= & 
           -1._prec
!

!    
      call setlambda( & 
           -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Bcache(1) = B0i(bb0,0._prec,MOU2,MOU2)
    Ccache(1) = C0i(cc0,0._prec,MOU2,MOU2+2*p1p4-2*p2p4,MOU2,MOU2,0._prec)
    Ccache(2) = C0i(cc00,0._prec,MOU2,MOU2+2*p1p4-2*p2p4,MOU2,MOU2,0._prec)
    Ccache(3) = C0i(cc1,0._prec,MOU2,MOU2+2*p1p4-2*p2p4,MOU2,MOU2,0._prec)
    Ccache(4) = C0i(cc12,0._prec,MOU2,MOU2+2*p1p4-2*p2p4,MOU2,MOU2,0._prec)
    Ccache(5) = C0i(cc2,0._prec,MOU2,MOU2+2*p1p4-2*p2p4,MOU2,MOU2,0._prec)
    Ccache(6) = C0i(cc22,0._prec,MOU2,MOU2+2*p1p4-2*p2p4,MOU2,MOU2,0._prec)

!
      T4TRIBC=real((4*EL**4*GF**2*(6*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           +  & 
           10*MIN2**3*p1p4  & 
           - 6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           -  & 
           20*MIN2**2*MOU2*p1p4  & 
           + 10*MIN2*MOU2**2*p1p4  & 
           +  & 
           18*MIN2**2.5*np2*p1p4  & 
           -  & 
           36*MIN2**1.5*MOU2*np2*p1p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           -  & 
           9*MIN2**2.5*np4*p1p4  & 
           +  & 
           18*MIN2**1.5*MOU2*np4*p1p4  & 
           -  & 
           9*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           48*MIN2**2*p1p2*p1p4  & 
           + 66*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
           18*MOU2**2*p1p2*p1p4  & 
           -  & 
           46*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           46*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           23*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           23*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           +  & 
           66*MIN2*p1p2**2*p1p4  & 
           - 46*MOU2*p1p2**2*p1p4  & 
           +  & 
           28*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           14*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           -  & 
           28*p1p2**3*p1p4  & 
           - 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           -  & 
           MIN2**2*p1p4**2  & 
           - 8*MIN2*MOU2*p1p4**2  & 
           +  & 
           9*MOU2**2*p1p4**2  & 
           - 13*MIN2**1.5*np2*p1p4**2  & 
           +  & 
           13*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           2*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           +  & 
           10*MIN2*p1p2*p1p4**2  & 
           + 10*MOU2*p1p2*p1p4**2  & 
           +  & 
           14*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           + 2*MIN2*p1p4**3  & 
           -  & 
           2*MOU2*p1p4**3  & 
           + 4*sqrt(MIN2)*np2*p1p4**3  & 
           -  & 
           12*p1p2*p1p4**3  & 
           - 6*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           -  & 
           10*MIN2**3*p2p4  & 
           + 6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           +  & 
           10*MIN2**2*MOU2*p2p4  & 
           - 18*MIN2**2.5*np2*p2p4  & 
           +  & 
           18*MIN2**1.5*MOU2*np2*p2p4  & 
           +  & 
           9*MIN2**2.5*np4*p2p4  & 
           -  & 
           9*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           +  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           48*MIN2**2*p1p2*p2p4  & 
           - 28*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
           46*MIN2**1.5*np2*p1p2*p2p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           23*MIN2**1.5*np4*p1p2*p2p4  & 
           -  & 
           5*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           -  & 
           66*MIN2*p1p2**2*p2p4  & 
           + 18*MOU2*p1p2**2*p2p4  & 
           -  & 
           28*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           14*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           +  & 
           28*p1p2**3*p2p4  & 
           + 12*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           40*MIN2**2*p1p4*p2p4  & 
           - 49*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
           9*MOU2**2*p1p4*p2p4  & 
           +  & 
           63*MIN2**1.5*np2*p1p4*p2p4  & 
           -  & 
           59*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           16*MIN2**1.5*np4*p1p4*p2p4  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           -  & 
           114*MIN2*p1p2*p1p4*p2p4  & 
           +  & 
           64*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           70*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           +  & 
           7*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           +  & 
           56*p1p2**2*p1p4*p2p4  & 
           - 8*MIN2*p1p4**2*p2p4  & 
           -  & 
           12*MOU2*p1p4**2*p2p4  & 
           -  & 
           19*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           12*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           12*p1p2*p1p4**2*p2p4  & 
           + 12*p1p4**3*p2p4  & 
           -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           - 39*MIN2**2*p2p4**2  & 
           +  & 
           19*MIN2*MOU2*p2p4**2  & 
           - 50*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           18*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           +  & 
           18*MIN2**1.5*np4*p2p4**2  & 
           +  & 
           14*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           +  & 
           104*MIN2*p1p2*p2p4**2  & 
           - 18*MOU2*p1p2*p2p4**2  & 
           +  & 
           56*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           15*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           -  & 
           56*p1p2**2*p2p4**2  & 
           + 41*MIN2*p1p4*p2p4**2  & 
           -  & 
           14*MOU2*p1p4*p2p4**2  & 
           +  & 
           43*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           12*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           -  & 
           28*p1p2*p1p4*p2p4**2  & 
           - 12*p1p4**2*p2p4**2  & 
           -  & 
           35*MIN2*p2p4**3  & 
           - 28*sqrt(MIN2)*np2*p2p4**3  & 
           +  & 
           28*p1p2*p2p4**3)*Bcache(1))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)**2*Pi**2)  & 
           -  & 
      (4*EL**4*GF**2*(9*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
           +  & 
           20*MIN2**3*MOU2*p1p4  & 
           -  & 
           9*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4  & 
           -  & 
           40*MIN2**2*MOU2**2*p1p4  & 
           + 20*MIN2*MOU2**3*p1p4  & 
           +  & 
           36*MIN2**2.5*MOU2*np2*p1p4  & 
           -  & 
           72*MIN2**1.5*MOU2**2*np2*p1p4  & 
           +  & 
           36*sqrt(MIN2)*MOU2**3*np2*p1p4  & 
           -  & 
           18*MIN2**2.5*MOU2*np4*p1p4  & 
           +  & 
           36*MIN2**1.5*MOU2**2*np4*p1p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**3*np4*p1p4  & 
           -  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
           -  & 
           96*MIN2**2*MOU2*p1p2*p1p4  & 
           +  & 
           132*MIN2*MOU2**2*p1p2*p1p4  & 
           -  & 
           36*MOU2**3*p1p2*p1p4  & 
           -  & 
           92*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
           +  & 
           92*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4  & 
           +  & 
           46*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
           -  & 
           46*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4  & 
           +  & 
           132*MIN2*MOU2*p1p2**2*p1p4  & 
           -  & 
           92*MOU2**2*p1p2**2*p1p4  & 
           +  & 
           56*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4  & 
           -  & 
           28*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
           -  & 
           56*MOU2*p1p2**3*p1p4  & 
           +  & 
           12*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
           + 20*MIN2**3*p1p4**2  & 
           -  & 
           20*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
           -  & 
           42*MIN2**2*MOU2*p1p4**2  & 
           +  & 
           4*MIN2*MOU2**2*p1p4**2  & 
           + 18*MOU2**3*p1p4**2  & 
           +  & 
           36*MIN2**2.5*np2*p1p4**2  & 
           -  & 
           98*MIN2**1.5*MOU2*np2*p1p4**2  & 
           +  & 
           62*sqrt(MIN2)*MOU2**2*np2*p1p4**2  & 
           -  & 
           18*MIN2**2.5*np4*p1p4**2  & 
           +  & 
           33*MIN2**1.5*MOU2*np4*p1p4**2  & 
           -  & 
           15*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
           -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
           -  & 
           96*MIN2**2*p1p2*p1p4**2  & 
           +  & 
           152*MIN2*MOU2*p1p2*p1p4**2  & 
           -  & 
           16*MOU2**2*p1p2*p1p4**2  & 
           -  & 
           92*MIN2**1.5*np2*p1p2*p1p4**2  & 
           +  & 
           120*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
           +  & 
           46*MIN2**1.5*np4*p1p2*p1p4**2  & 
           -  & 
           32*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
           +  & 
           132*MIN2*p1p2**2*p1p4**2  & 
           -  & 
           92*MOU2*p1p2**2*p1p4**2  & 
           +  & 
           56*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
           -  & 
           28*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
           -  & 
           56*p1p2**3*p1p4**2  & 
           - 8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
           -  & 
           2*MIN2**2*p1p4**3  & 
           - 13*MIN2*MOU2*p1p4**3  & 
           +  & 
           15*MOU2**2*p1p4**3  & 
           - 26*MIN2**1.5*np2*p1p4**3  & 
           +  & 
           34*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
           -  & 
           4*MIN2**1.5*np4*p1p4**3  & 
           +  & 
           4*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
           +  & 
           20*MIN2*p1p2*p1p4**3  & 
           - 2*MOU2*p1p2*p1p4**3  & 
           +  & 
           28*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
           +  & 
           16*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
           + 4*MIN2*p1p4**4  & 
           -  & 
           4*MOU2*p1p4**4  & 
           + 8*sqrt(MIN2)*np2*p1p4**4  & 
           -  & 
           24*p1p2*p1p4**4  & 
           - 9*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
           -  & 
           20*MIN2**3*MOU2*p2p4  & 
           +  & 
           9*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p2p4  & 
           +  & 
           20*MIN2**2*MOU2**2*p2p4  & 
           -  & 
           36*MIN2**2.5*MOU2*np2*p2p4  & 
           +  & 
           36*MIN2**1.5*MOU2**2*np2*p2p4  & 
           +  & 
           18*MIN2**2.5*MOU2*np4*p2p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**3*np4*p2p4  & 
           +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4  & 
           +  & 
           96*MIN2**2*MOU2*p1p2*p2p4  & 
           -  & 
           56*MIN2*MOU2**2*p1p2*p2p4  & 
           +  & 
           92*MIN2**1.5*MOU2*np2*p1p2*p2p4  & 
           -  & 
           36*sqrt(MIN2)*MOU2**2*np2*p1p2*p2p4  & 
           -  & 
           46*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
           -  & 
           10*sqrt(MIN2)*MOU2**2*np4*p1p2*p2p4  & 
           -  & 
           132*MIN2*MOU2*p1p2**2*p2p4  & 
           +  & 
           36*MOU2**2*p1p2**2*p2p4  & 
           -  & 
           56*sqrt(MIN2)*MOU2*np2*p1p2**2*p2p4  & 
           +  & 
           28*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4  & 
           +  & 
           56*MOU2*p1p2**3*p2p4  & 
           -  & 
           24*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
           -  & 
           40*MIN2**3*p1p4*p2p4  & 
           +  & 
           45*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
           +  & 
           140*MIN2**2*MOU2*p1p4*p2p4  & 
           -  & 
           118*MIN2*MOU2**2*p1p4*p2p4  & 
           +  & 
           18*MOU2**3*p1p4*p2p4  & 
           -  & 
           72*MIN2**2.5*np2*p1p4*p2p4  & 
           +  & 
           234*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
           -  & 
           154*sqrt(MIN2)*MOU2**2*np2*p1p4*p2p4  & 
           +  & 
           36*MIN2**2.5*np4*p1p4*p2p4  & 
           -  & 
           78*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
           +  & 
           34*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4  & 
           +  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
           +  & 
           192*MIN2**2*p1p2*p1p4*p2p4  & 
           -  & 
           416*MIN2*MOU2*p1p2*p1p4*p2p4  & 
           +  & 
           164*MOU2**2*p1p2*p1p4*p2p4  & 
           +  & 
           184*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
           -  & 
           268*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4  & 
           -  & 
           92*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
           +  & 
           69*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
           -  & 
           264*MIN2*p1p2**2*p1p4*p2p4  & 
           +  & 
           240*MOU2*p1p2**2*p1p4*p2p4  & 
           -  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4  & 
           +  & 
           56*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
           +  & 
           112*p1p2**3*p1p4*p2p4  & 
           +  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
           +  & 
           82*MIN2**2*p1p4**2*p2p4  & 
           -  & 
           88*MIN2*MOU2*p1p4**2*p2p4  & 
           -  & 
           34*MOU2**2*p1p4**2*p2p4  & 
           +  & 
           152*MIN2**1.5*np2*p1p4**2*p2p4  & 
           -  & 
           181*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
           -  & 
           28*MIN2**1.5*np4*p1p4**2*p2p4  & 
           -  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
           -  & 
           248*MIN2*p1p2*p1p4**2*p2p4  & 
           +  & 
           112*MOU2*p1p2*p1p4**2*p2p4  & 
           -  & 
           168*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
           -  & 
           2*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
           +  & 
           112*p1p2**2*p1p4**2*p2p4  & 
           - 20*MIN2*p1p4**3*p2p4  & 
           +  & 
           2*MOU2*p1p4**3*p2p4  & 
           -  & 
           46*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
           +  & 
           48*p1p2*p1p4**3*p2p4  & 
           + 24*p1p4**4*p2p4  & 
           +  & 
           12*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
           + 20*MIN2**3*p2p4**2  & 
           -  & 
           25*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2  & 
           -  & 
           98*MIN2**2*MOU2*p2p4**2  & 
           +  & 
           38*MIN2*MOU2**2*p2p4**2  & 
           +  & 
           36*MIN2**2.5*np2*p2p4**2  & 
           -  & 
           136*MIN2**1.5*MOU2*np2*p2p4**2  & 
           +  & 
           36*sqrt(MIN2)*MOU2**2*np2*p2p4**2  & 
           -  & 
           18*MIN2**2.5*np4*p2p4**2  & 
           +  & 
           45*MIN2**1.5*MOU2*np4*p2p4**2  & 
           +  & 
           37*sqrt(MIN2)*MOU2**2*np4*p2p4**2  & 
           -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2  & 
           -  & 
           96*MIN2**2*p1p2*p2p4**2  & 
           +  & 
           264*MIN2*MOU2*p1p2*p2p4**2  & 
           -  & 
           36*MOU2**2*p1p2*p2p4**2  & 
           -  & 
           92*MIN2**1.5*np2*p1p2*p2p4**2  & 
           +  & 
           148*sqrt(MIN2)*MOU2*np2*p1p2*p2p4**2  & 
           +  & 
           46*MIN2**1.5*np4*p1p2*p2p4**2  & 
           -  & 
           37*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2  & 
           +  & 
           132*MIN2*p1p2**2*p2p4**2  & 
           -  & 
           148*MOU2*p1p2**2*p2p4**2  & 
           +  & 
           56*sqrt(MIN2)*np2*p1p2**2*p2p4**2  & 
           -  & 
           28*sqrt(MIN2)*np4*p1p2**2*p2p4**2  & 
           -  & 
           56*p1p2**3*p2p4**2  & 
           -  & 
           40*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
           -  & 
           158*MIN2**2*p1p4*p2p4**2  & 
           +  & 
           208*MIN2*MOU2*p1p4*p2p4**2  & 
           -  & 
           37*MOU2**2*p1p4*p2p4**2  & 
           -  & 
           226*MIN2**1.5*np2*p1p4*p2p4**2  & 
           +  & 
           239*sqrt(MIN2)*MOU2*np2*p1p4*p2p4**2  & 
           +  & 
           68*MIN2**1.5*np4*p1p4*p2p4**2  & 
           +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
           +  & 
           436*MIN2*p1p2*p1p4*p2p4**2  & 
           -  & 
           202*MOU2*p1p2*p1p4*p2p4**2  & 
           +  & 
           252*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2  & 
           -  & 
           44*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
           -  & 
           224*p1p2**2*p1p4*p2p4**2  & 
           +  & 
           98*MIN2*p1p4**2*p2p4**2  & 
           -  & 
           8*MOU2*p1p4**2*p2p4**2  & 
           +  & 
           124*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
           +  & 
           48*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
           -  & 
           80*p1p2*p1p4**2*p2p4**2  & 
           - 48*p1p4**3*p2p4**2  & 
           +  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3  & 
           + 78*MIN2**2*p2p4**3  & 
           -  & 
           107*MIN2*MOU2*p2p4**3  & 
           +  & 
           100*MIN2**1.5*np2*p2p4**3  & 
           -  & 
           92*sqrt(MIN2)*MOU2*np2*p2p4**3  & 
           -  & 
           36*MIN2**1.5*np4*p2p4**3  & 
           -  & 
           10*sqrt(MIN2)*MOU2*np4*p2p4**3  & 
           -  & 
           208*MIN2*p1p2*p2p4**3  & 
           + 92*MOU2*p1p2*p2p4**3  & 
           -  & 
           112*sqrt(MIN2)*np2*p1p2*p2p4**3  & 
           +  & 
           30*sqrt(MIN2)*np4*p1p2*p2p4**3  & 
           +  & 
           112*p1p2**2*p2p4**3  & 
           - 152*MIN2*p1p4*p2p4**3  & 
           +  & 
           10*MOU2*p1p4*p2p4**3  & 
           -  & 
           142*sqrt(MIN2)*np2*p1p4*p2p4**3  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
           +  & 
           112*p1p2*p1p4*p2p4**3  & 
           + 24*p1p4**2*p2p4**3  & 
           +  & 
           70*MIN2*p2p4**4  & 
           + 56*sqrt(MIN2)*np2*p2p4**4  & 
           -  & 
           56*p1p2*p2p4**4)* & 
         Ccache(1))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)**2*Pi**2)  & 
           +  & 
      (8*EL**4*GF**2*( & 
           -6*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           10*MIN2**3*p1p4  & 
           + 6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           +  & 
           20*MIN2**2*MOU2*p1p4  & 
           - 10*MIN2*MOU2**2*p1p4  & 
           -  & 
           18*MIN2**2.5*np2*p1p4  & 
           +  & 
           36*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           +  & 
           9*MIN2**2.5*np4*p1p4  & 
           -  & 
           18*MIN2**1.5*MOU2*np4*p1p4  & 
           +  & 
           9*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           +  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           +  & 
           48*MIN2**2*p1p2*p1p4  & 
           - 66*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
           18*MOU2**2*p1p2*p1p4  & 
           +  & 
           46*MIN2**1.5*np2*p1p2*p1p4  & 
           -  & 
           46*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           -  & 
           23*MIN2**1.5*np4*p1p2*p1p4  & 
           +  & 
           23*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           66*MIN2*p1p2**2*p1p4  & 
           + 46*MOU2*p1p2**2*p1p4  & 
           -  & 
           28*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           +  & 
           14*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           +  & 
           28*p1p2**3*p1p4  & 
           + 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           +  & 
           MIN2**2*p1p4**2  & 
           + 8*MIN2*MOU2*p1p4**2  & 
           -  & 
           9*MOU2**2*p1p4**2  & 
           + 13*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           13*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           12*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           -  & 
           10*MIN2*p1p2*p1p4**2  & 
           - 10*MOU2*p1p2*p1p4**2  & 
           -  & 
           14*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           20*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           +  & 
           12*MIN2*p1p4**3  & 
           - 12*MOU2*p1p4**3  & 
           -  & 
           4*sqrt(MIN2)*np2*p1p4**3  & 
           - 16*p1p2*p1p4**3  & 
           +  & 
           6*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           + 10*MIN2**3*p2p4  & 
           -  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           -  & 
           10*MIN2**2*MOU2*p2p4  & 
           + 18*MIN2**2.5*np2*p2p4  & 
           -  & 
           18*MIN2**1.5*MOU2*np2*p2p4  & 
           -  & 
           9*MIN2**2.5*np4*p2p4  & 
           +  & 
           9*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           -  & 
           48*MIN2**2*p1p2*p2p4  & 
           + 28*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
           46*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           +  & 
           23*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           5*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           +  & 
           66*MIN2*p1p2**2*p2p4  & 
           - 18*MOU2*p1p2**2*p2p4  & 
           +  & 
           28*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           -  & 
           14*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           -  & 
           28*p1p2**3*p2p4  & 
           - 12*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           -  & 
           40*MIN2**2*p1p4*p2p4  & 
           + 49*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
           9*MOU2**2*p1p4*p2p4  & 
           -  & 
           63*MIN2**1.5*np2*p1p4*p2p4  & 
           +  & 
           59*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           +  & 
           24*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           20*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           +  & 
           114*MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           64*MOU2*p1p2*p1p4*p2p4  & 
           +  & 
           70*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           30*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           56*p1p2**2*p1p4*p2p4  & 
           + 20*MOU2*p1p4**2*p2p4  & 
           +  & 
           26*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           16*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           4*p1p2*p1p4**2*p2p4  & 
           + 16*p1p4**3*p2p4  & 
           +  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           + 39*MIN2**2*p2p4**2  & 
           -  & 
           19*MIN2*MOU2*p2p4**2  & 
           + 50*MIN2**1.5*np2*p2p4**2  & 
           -  & 
           18*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           -  & 
           12*MIN2**1.5*np4*p2p4**2  & 
           -  & 
           20*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           -  & 
           104*MIN2*p1p2*p2p4**2  & 
           + 18*MOU2*p1p2*p2p4**2  & 
           -  & 
           56*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           +  & 
           10*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           +  & 
           56*p1p2**2*p2p4**2  & 
           - 54*MIN2*p1p4*p2p4**2  & 
           +  & 
           20*MOU2*p1p4*p2p4**2  & 
           -  & 
           50*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           4*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           +  & 
           40*p1p2*p1p4*p2p4**2  & 
           - 4*p1p4**2*p2p4**2  & 
           +  & 
           42*MIN2*p2p4**3  & 
           + 28*sqrt(MIN2)*np2*p2p4**3  & 
           +  & 
           12*sqrt(MIN2)*np4*p2p4**3  & 
           - 28*p1p2*p2p4**3  & 
           -  & 
           12*p1p4*p2p4**3)* & 
         Ccache(2)) & 
        /(9.*p1p4*(p1p4  & 
           - p2p4)**2*Pi**2)  & 
           -  & 
      (8*EL**4*GF**2*(6*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           +  & 
           10*MIN2**3*p1p4  & 
           - 6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           -  & 
           20*MIN2**2*MOU2*p1p4  & 
           + 10*MIN2*MOU2**2*p1p4  & 
           +  & 
           18*MIN2**2.5*np2*p1p4  & 
           -  & 
           36*MIN2**1.5*MOU2*np2*p1p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           -  & 
           9*MIN2**2.5*np4*p1p4  & 
           +  & 
           18*MIN2**1.5*MOU2*np4*p1p4  & 
           -  & 
           9*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           -  & 
           48*MIN2**2*p1p2*p1p4  & 
           + 66*MIN2*MOU2*p1p2*p1p4  & 
           -  & 
           18*MOU2**2*p1p2*p1p4  & 
           -  & 
           46*MIN2**1.5*np2*p1p2*p1p4  & 
           +  & 
           46*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           +  & 
           23*MIN2**1.5*np4*p1p2*p1p4  & 
           -  & 
           23*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           +  & 
           66*MIN2*p1p2**2*p1p4  & 
           - 46*MOU2*p1p2**2*p1p4  & 
           +  & 
           28*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           -  & 
           14*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           -  & 
           28*p1p2**3*p1p4  & 
           - 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           -  & 
           MIN2**2*p1p4**2  & 
           - 8*MIN2*MOU2*p1p4**2  & 
           +  & 
           9*MOU2**2*p1p4**2  & 
           - 13*MIN2**1.5*np2*p1p4**2  & 
           +  & 
           13*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           2*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           +  & 
           10*MIN2*p1p2*p1p4**2  & 
           + 10*MOU2*p1p2*p1p4**2  & 
           +  & 
           14*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           + 2*MIN2*p1p4**3  & 
           -  & 
           2*MOU2*p1p4**3  & 
           + 4*sqrt(MIN2)*np2*p1p4**3  & 
           -  & 
           12*p1p2*p1p4**3  & 
           - 6*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           -  & 
           10*MIN2**3*p2p4  & 
           + 6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           +  & 
           10*MIN2**2*MOU2*p2p4  & 
           - 18*MIN2**2.5*np2*p2p4  & 
           +  & 
           18*MIN2**1.5*MOU2*np2*p2p4  & 
           +  & 
           9*MIN2**2.5*np4*p2p4  & 
           -  & 
           9*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           +  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           +  & 
           48*MIN2**2*p1p2*p2p4  & 
           - 28*MIN2*MOU2*p1p2*p2p4  & 
           +  & 
           46*MIN2**1.5*np2*p1p2*p2p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           -  & 
           23*MIN2**1.5*np4*p1p2*p2p4  & 
           -  & 
           5*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           -  & 
           66*MIN2*p1p2**2*p2p4  & 
           + 18*MOU2*p1p2**2*p2p4  & 
           -  & 
           28*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           +  & 
           14*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           +  & 
           28*p1p2**3*p2p4  & 
           + 12*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           +  & 
           40*MIN2**2*p1p4*p2p4  & 
           - 49*MIN2*MOU2*p1p4*p2p4  & 
           +  & 
           9*MOU2**2*p1p4*p2p4  & 
           +  & 
           63*MIN2**1.5*np2*p1p4*p2p4  & 
           -  & 
           59*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           -  & 
           16*MIN2**1.5*np4*p1p4*p2p4  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           -  & 
           114*MIN2*p1p2*p1p4*p2p4  & 
           +  & 
           64*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           70*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           +  & 
           7*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           +  & 
           56*p1p2**2*p1p4*p2p4  & 
           - 8*MIN2*p1p4**2*p2p4  & 
           -  & 
           12*MOU2*p1p4**2*p2p4  & 
           -  & 
           19*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           12*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           +  & 
           12*p1p2*p1p4**2*p2p4  & 
           + 12*p1p4**3*p2p4  & 
           -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           - 39*MIN2**2*p2p4**2  & 
           +  & 
           19*MIN2*MOU2*p2p4**2  & 
           - 50*MIN2**1.5*np2*p2p4**2  & 
           +  & 
           18*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           +  & 
           18*MIN2**1.5*np4*p2p4**2  & 
           +  & 
           14*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           +  & 
           104*MIN2*p1p2*p2p4**2  & 
           - 18*MOU2*p1p2*p2p4**2  & 
           +  & 
           56*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           15*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           -  & 
           56*p1p2**2*p2p4**2  & 
           + 41*MIN2*p1p4*p2p4**2  & 
           -  & 
           14*MOU2*p1p4*p2p4**2  & 
           +  & 
           43*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           12*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           -  & 
           28*p1p2*p1p4*p2p4**2  & 
           - 12*p1p4**2*p2p4**2  & 
           -  & 
           35*MIN2*p2p4**3  & 
           - 28*sqrt(MIN2)*np2*p2p4**3  & 
           +  & 
           28*p1p2*p2p4**3)* & 
         Ccache(3))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           +  & 
      (8*EL**4*GF**2*( & 
           -4*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
           -  & 
           10*MIN2**3*p1p4  & 
           + 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
           +  & 
           20*MIN2**2*MOU2*p1p4  & 
           - 10*MIN2*MOU2**2*p1p4  & 
           -  & 
           18*MIN2**2.5*np2*p1p4  & 
           +  & 
           36*MIN2**1.5*MOU2*np2*p1p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
           +  & 
           4*MIN2**2.5*np4*p1p4  & 
           -  & 
           8*MIN2**1.5*MOU2*np4*p1p4  & 
           +  & 
           4*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
           +  & 
           48*MIN2**2*p1p2*p1p4  & 
           - 66*MIN2*MOU2*p1p2*p1p4  & 
           +  & 
           18*MOU2**2*p1p2*p1p4  & 
           +  & 
           46*MIN2**1.5*np2*p1p2*p1p4  & 
           -  & 
           46*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
           -  & 
           8*MIN2**1.5*np4*p1p2*p1p4  & 
           +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
           -  & 
           66*MIN2*p1p2**2*p1p4  & 
           + 46*MOU2*p1p2**2*p1p4  & 
           -  & 
           28*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
           + 28*p1p2**3*p1p4  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
           + 6*MIN2**2*p1p4**2  & 
           -  & 
           2*MIN2*MOU2*p1p4**2  & 
           - 4*MOU2**2*p1p4**2  & 
           +  & 
           22*MIN2**1.5*np2*p1p4**2  & 
           -  & 
           22*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
           -  & 
           12*MIN2**1.5*np4*p1p4**2  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
           -  & 
           34*MIN2*p1p2*p1p4**2  & 
           + 14*MOU2*p1p2*p1p4**2  & 
           -  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
           +  & 
           20*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
           +  & 
           28*p1p2**2*p1p4**2  & 
           + 12*MIN2*p1p4**3  & 
           -  & 
           12*MOU2*p1p4**3  & 
           - 4*sqrt(MIN2)*np2*p1p4**3  & 
           -  & 
           16*p1p2*p1p4**3  & 
           + 4*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
           +  & 
           10*MIN2**3*p2p4  & 
           - 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
           -  & 
           10*MIN2**2*MOU2*p2p4  & 
           + 18*MIN2**2.5*np2*p2p4  & 
           -  & 
           18*MIN2**1.5*MOU2*np2*p2p4  & 
           -  & 
           4*MIN2**2.5*np4*p2p4  & 
           -  & 
           5*MIN2**1.5*MOU2*np4*p2p4  & 
           +  & 
           9*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
           -  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
           -  & 
           48*MIN2**2*p1p2*p2p4  & 
           + 28*MIN2*MOU2*p1p2*p2p4  & 
           -  & 
           46*MIN2**1.5*np2*p1p2*p2p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
           +  & 
           8*MIN2**1.5*np4*p1p2*p2p4  & 
           +  & 
           10*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
           +  & 
           66*MIN2*p1p2**2*p2p4  & 
           - 18*MOU2*p1p2**2*p2p4  & 
           +  & 
           28*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
           -  & 
           4*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
           - 28*p1p2**3*p2p4  & 
           -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
           -  & 
           40*MIN2**2*p1p4*p2p4  & 
           + 49*MIN2*MOU2*p1p4*p2p4  & 
           -  & 
           9*MOU2**2*p1p4*p2p4  & 
           -  & 
           68*MIN2**1.5*np2*p1p4*p2p4  & 
           +  & 
           64*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
           +  & 
           10*MIN2**1.5*np4*p1p4*p2p4  & 
           -  & 
           6*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
           +  & 
           124*MIN2*p1p2*p1p4*p2p4  & 
           -  & 
           74*MOU2*p1p2*p1p4*p2p4  & 
           +  & 
           88*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
           -  & 
           16*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
           -  & 
           84*p1p2**2*p1p4*p2p4  & 
           + 14*MIN2*p1p4**2*p2p4  & 
           +  & 
           6*MOU2*p1p4**2*p2p4  & 
           +  & 
           40*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
           -  & 
           16*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
           -  & 
           24*p1p2*p1p4**2*p2p4  & 
           + 16*p1p4**3*p2p4  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
           + 34*MIN2**2*p2p4**2  & 
           -  & 
           14*MIN2*MOU2*p2p4**2  & 
           + 46*MIN2**1.5*np2*p2p4**2  & 
           -  & 
           18*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
           +  & 
           2*MIN2**1.5*np4*p2p4**2  & 
           -  & 
           20*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
           -  & 
           90*MIN2*p1p2*p2p4**2  & 
           + 18*MOU2*p1p2*p2p4**2  & 
           -  & 
           56*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
           -  & 
           4*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
           +  & 
           56*p1p2**2*p2p4**2  & 
           - 54*MIN2*p1p4*p2p4**2  & 
           +  & 
           20*MOU2*p1p4*p2p4**2  & 
           -  & 
           64*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
           +  & 
           4*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
           +  & 
           68*p1p2*p1p4*p2p4**2  & 
           - 4*p1p4**2*p2p4**2  & 
           +  & 
           28*MIN2*p2p4**3  & 
           + 28*sqrt(MIN2)*np2*p2p4**3  & 
           +  & 
           12*sqrt(MIN2)*np4*p2p4**3  & 
           - 28*p1p2*p2p4**3  & 
           -  & 
           12*p1p4*p2p4**3)* & 
         Ccache(4)) & 
        /(9.*p1p4*(p1p4  & 
           - p2p4)*Pi**2)  & 
           -  & 
      (4*EL**4*GF**2*(29*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
           +  & 
           54*MIN2**3*MOU2*p1p4  & 
           -  & 
           29*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4  & 
           -  & 
           108*MIN2**2*MOU2**2*p1p4  & 
           + 54*MIN2*MOU2**3*p1p4  & 
           +  & 
           90*MIN2**2.5*MOU2*np2*p1p4  & 
           -  & 
           180*MIN2**1.5*MOU2**2*np2*p1p4  & 
           +  & 
           90*sqrt(MIN2)*MOU2**3*np2*p1p4  & 
           -  & 
           45*MIN2**2.5*MOU2*np4*p1p4  & 
           +  & 
           90*MIN2**1.5*MOU2**2*np4*p1p4  & 
           -  & 
           45*sqrt(MIN2)*MOU2**3*np4*p1p4  & 
           -  & 
           40*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
           -  & 
           252*MIN2**2*MOU2*p1p2*p1p4  & 
           +  & 
           342*MIN2*MOU2**2*p1p2*p1p4  & 
           -  & 
           90*MOU2**3*p1p2*p1p4  & 
           -  & 
           234*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
           +  & 
           234*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4  & 
           +  & 
           117*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
           -  & 
           117*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4  & 
           +  & 
           342*MIN2*MOU2*p1p2**2*p1p4  & 
           -  & 
           234*MOU2**2*p1p2**2*p1p4  & 
           +  & 
           144*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4  & 
           -  & 
           72*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
           -  & 
           144*MOU2*p1p2**3*p1p4  & 
           +  & 
           24*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
           + 40*MIN2**3*p1p4**2  & 
           -  & 
           42*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
           -  & 
           89*MIN2**2*MOU2*p1p4**2  & 
           +  & 
           4*MIN2*MOU2**2*p1p4**2  & 
           + 45*MOU2**3*p1p4**2  & 
           +  & 
           72*MIN2**2.5*np2*p1p4**2  & 
           -  & 
           234*MIN2**1.5*MOU2*np2*p1p4**2  & 
           +  & 
           162*sqrt(MIN2)*MOU2**2*np2*p1p4**2  & 
           -  & 
           33*MIN2**2.5*np4*p1p4**2  & 
           +  & 
           83*MIN2**1.5*MOU2*np4*p1p4**2  & 
           -  & 
           50*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
           -  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
           -  & 
           192*MIN2**2*p1p2*p1p4**2  & 
           +  & 
           345*MIN2*MOU2*p1p2*p1p4**2  & 
           -  & 
           45*MOU2**2*p1p2*p1p4**2  & 
           -  & 
           184*MIN2**1.5*np2*p1p2*p1p4**2  & 
           +  & 
           310*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
           +  & 
           83*MIN2**1.5*np4*p1p2*p1p4**2  & 
           -  & 
           99*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
           +  & 
           264*MIN2*p1p2**2*p1p4**2  & 
           -  & 
           238*MOU2*p1p2**2*p1p4**2  & 
           +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
           -  & 
           50*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
           -  & 
           112*p1p2**3*p1p4**2  & 
           -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
           - 7*MIN2**2*p1p4**3  & 
           -  & 
           43*MIN2*MOU2*p1p4**3  & 
           + 50*MOU2**2*p1p4**3  & 
           -  & 
           61*MIN2**1.5*np2*p1p4**3  & 
           +  & 
           79*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
           +  & 
           4*MIN2**1.5*np4*p1p4**3  & 
           -  & 
           4*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
           +  & 
           58*MIN2*p1p2*p1p4**3  & 
           + 20*MOU2*p1p2*p1p4**3  & 
           +  & 
           74*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
           -  & 
           24*p1p2**2*p1p4**3  & 
           - 4*MIN2*p1p4**4  & 
           +  & 
           4*MOU2*p1p4**4  & 
           + 16*sqrt(MIN2)*np2*p1p4**4  & 
           -  & 
           24*p1p2*p1p4**4  & 
           - 29*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
           -  & 
           54*MIN2**3*MOU2*p2p4  & 
           +  & 
           29*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p2p4  & 
           +  & 
           54*MIN2**2*MOU2**2*p2p4  & 
           -  & 
           90*MIN2**2.5*MOU2*np2*p2p4  & 
           +  & 
           90*MIN2**1.5*MOU2**2*np2*p2p4  & 
           +  & 
           45*MIN2**2.5*MOU2*np4*p2p4  & 
           -  & 
           45*sqrt(MIN2)*MOU2**3*np4*p2p4  & 
           +  & 
           40*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4  & 
           +  & 
           252*MIN2**2*MOU2*p1p2*p2p4  & 
           -  & 
           144*MIN2*MOU2**2*p1p2*p2p4  & 
           +  & 
           234*MIN2**1.5*MOU2*np2*p1p2*p2p4  & 
           -  & 
           90*sqrt(MIN2)*MOU2**2*np2*p1p2*p2p4  & 
           -  & 
           117*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
           -  & 
           27*sqrt(MIN2)*MOU2**2*np4*p1p2*p2p4  & 
           -  & 
           342*MIN2*MOU2*p1p2**2*p2p4  & 
           +  & 
           90*MOU2**2*p1p2**2*p2p4  & 
           -  & 
           144*sqrt(MIN2)*MOU2*np2*p1p2**2*p2p4  & 
           +  & 
           72*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4  & 
           +  & 
           144*MOU2*p1p2**3*p2p4  & 
           -  & 
           48*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
           -  & 
           80*MIN2**3*p1p4*p2p4  & 
           +  & 
           113*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
           +  & 
           319*MIN2**2*MOU2*p1p4*p2p4  & 
           -  & 
           284*MIN2*MOU2**2*p1p4*p2p4  & 
           +  & 
           45*MOU2**3*p1p4*p2p4  & 
           -  & 
           144*MIN2**2.5*np2*p1p4*p2p4  & 
           +  & 
           558*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
           -  & 
           396*sqrt(MIN2)*MOU2**2*np2*p1p4*p2p4  & 
           +  & 
           66*MIN2**2.5*np4*p1p4*p2p4  & 
           -  & 
           170*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
           +  & 
           86*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4  & 
           +  & 
           64*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
           +  & 
           384*MIN2**2*p1p2*p1p4*p2p4  & 
           -  & 
           963*MIN2*MOU2*p1p2*p1p4*p2p4  & 
           +  & 
           423*MOU2**2*p1p2*p1p4*p2p4  & 
           +  & 
           368*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
           -  & 
           680*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4  & 
           -  & 
           166*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
           +  & 
           161*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
           -  & 
           528*MIN2*p1p2**2*p1p4*p2p4  & 
           +  & 
           608*MOU2*p1p2**2*p1p4*p2p4  & 
           -  & 
           224*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4  & 
           +  & 
           100*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
           +  & 
           224*p1p2**3*p1p4*p2p4  & 
           +  & 
           64*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
           +  & 
           167*MIN2**2*p1p4**2*p2p4  & 
           -  & 
           189*MIN2*MOU2*p1p4**2*p2p4  & 
           -  & 
           86*MOU2**2*p1p4**2*p2p4  & 
           +  & 
           316*MIN2**1.5*np2*p1p4**2*p2p4  & 
           -  & 
           463*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
           -  & 
           68*MIN2**1.5*np4*p1p4**2*p2p4  & 
           +  & 
           66*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
           -  & 
           520*MIN2*p1p2*p1p4**2*p2p4  & 
           +  & 
           302*MOU2*p1p2*p1p4**2*p2p4  & 
           -  & 
           372*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
           +  & 
           38*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
           +  & 
           272*p1p2**2*p1p4**2*p2p4  & 
           - 28*MIN2*p1p4**3*p2p4  & 
           -  & 
           66*MOU2*p1p4**3*p2p4  & 
           -  & 
           110*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
           +  & 
           72*p1p2*p1p4**3*p2p4  & 
           + 24*p1p4**4*p2p4  & 
           +  & 
           24*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
           + 40*MIN2**3*p2p4**2  & 
           -  & 
           71*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2  & 
           -  & 
           230*MIN2**2*MOU2*p2p4**2  & 
           +  & 
           82*MIN2*MOU2**2*p2p4**2  & 
           +  & 
           72*MIN2**2.5*np2*p2p4**2  & 
           -  & 
           324*MIN2**1.5*MOU2*np2*p2p4**2  & 
           +  & 
           90*sqrt(MIN2)*MOU2**2*np2*p2p4**2  & 
           -  & 
           33*MIN2**2.5*np4*p2p4**2  & 
           +  & 
           87*MIN2**1.5*MOU2*np4*p2p4**2  & 
           +  & 
           108*sqrt(MIN2)*MOU2**2*np4*p2p4**2  & 
           -  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2  & 
           -  & 
           192*MIN2**2*p1p2*p2p4**2  & 
           +  & 
           618*MIN2*MOU2*p1p2*p2p4**2  & 
           -  & 
           90*MOU2**2*p1p2*p2p4**2  & 
           -  & 
           184*MIN2**1.5*np2*p1p2*p2p4**2  & 
           +  & 
           370*sqrt(MIN2)*MOU2*np2*p1p2*p2p4**2  & 
           +  & 
           83*MIN2**1.5*np4*p1p2*p2p4**2  & 
           -  & 
           62*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2  & 
           +  & 
           264*MIN2*p1p2**2*p2p4**2  & 
           -  & 
           370*MOU2*p1p2**2*p2p4**2  & 
           +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p2p4**2  & 
           -  & 
           50*sqrt(MIN2)*np4*p1p2**2*p2p4**2  & 
           -  & 
           112*p1p2**3*p2p4**2  & 
           -  & 
           80*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
           -  & 
           313*MIN2**2*p1p4*p2p4**2  & 
           +  & 
           454*MIN2*MOU2*p1p4*p2p4**2  & 
           -  & 
           108*MOU2**2*p1p4*p2p4**2  & 
           -  & 
           449*MIN2**1.5*np2*p1p4*p2p4**2  & 
           +  & 
           610*sqrt(MIN2)*MOU2*np2*p1p4*p2p4**2  & 
           +  & 
           124*MIN2**1.5*np4*p1p4*p2p4**2  & 
           -  & 
           16*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
           +  & 
           866*MIN2*p1p2*p1p4*p2p4**2  & 
           -  & 
           548*MOU2*p1p2*p1p4*p2p4**2  & 
           +  & 
           522*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2  & 
           -  & 
           100*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
           -  & 
           472*p1p2**2*p1p4*p2p4**2  & 
           +  & 
           202*MIN2*p1p4**2*p2p4**2  & 
           +  & 
           16*MOU2*p1p4**2*p2p4**2  & 
           +  & 
           284*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
           +  & 
           48*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
           -  & 
           184*p1p2*p1p4**2*p2p4**2  & 
           - 48*p1p4**3*p2p4**2  & 
           +  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3  & 
           +  & 
           153*MIN2**2*p2p4**3  & 
           - 222*MIN2*MOU2*p2p4**3  & 
           +  & 
           194*MIN2**1.5*np2*p2p4**3  & 
           -  & 
           226*sqrt(MIN2)*MOU2*np2*p2p4**3  & 
           -  & 
           60*MIN2**1.5*np4*p2p4**3  & 
           -  & 
           46*sqrt(MIN2)*MOU2*np4*p2p4**3  & 
           -  & 
           404*MIN2*p1p2*p2p4**3  & 
           + 226*MOU2*p1p2*p2p4**3  & 
           -  & 
           224*sqrt(MIN2)*np2*p1p2*p2p4**3  & 
           +  & 
           54*sqrt(MIN2)*np4*p1p2*p2p4**3  & 
           +  & 
           224*p1p2**2*p2p4**3  & 
           - 304*MIN2*p1p4*p2p4**3  & 
           +  & 
           46*MOU2*p1p4*p2p4**3  & 
           -  & 
           302*sqrt(MIN2)*np2*p1p4*p2p4**3  & 
           -  & 
           24*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
           +  & 
           248*p1p2*p1p4*p2p4**3  & 
           + 24*p1p4**2*p2p4**3  & 
           +  & 
           134*MIN2*p2p4**4  & 
           + 112*sqrt(MIN2)*np2*p2p4**4  & 
           -  & 
           112*p1p2*p2p4**4)* & 
         Ccache(5))/ & 
       (9.*p1p4*(p1p4  & 
           - p2p4)**2*Pi**2)  & 
           +  & 
      (8*EL**4*GF**2*( & 
           -4*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
           -  & 
           10*MIN2**3*MOU2*p1p4  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4  & 
           +  & 
           20*MIN2**2*MOU2**2*p1p4  & 
           - 10*MIN2*MOU2**3*p1p4  & 
           -  & 
           18*MIN2**2.5*MOU2*np2*p1p4  & 
           +  & 
           36*MIN2**1.5*MOU2**2*np2*p1p4  & 
           -  & 
           18*sqrt(MIN2)*MOU2**3*np2*p1p4  & 
           +  & 
           9*MIN2**2.5*MOU2*np4*p1p4  & 
           -  & 
           18*MIN2**1.5*MOU2**2*np4*p1p4  & 
           +  & 
           9*sqrt(MIN2)*MOU2**3*np4*p1p4  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
           +  & 
           48*MIN2**2*MOU2*p1p2*p1p4  & 
           -  & 
           66*MIN2*MOU2**2*p1p2*p1p4  & 
           +  & 
           18*MOU2**3*p1p2*p1p4  & 
           +  & 
           46*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
           -  & 
           46*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4  & 
           -  & 
           23*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
           +  & 
           23*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4  & 
           -  & 
           66*MIN2*MOU2*p1p2**2*p1p4  & 
           +  & 
           46*MOU2**2*p1p2**2*p1p4  & 
           -  & 
           28*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4  & 
           +  & 
           14*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
           +  & 
           28*MOU2*p1p2**3*p1p4  & 
           - 4*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
           -  & 
           10*MIN2**3*p1p4**2  & 
           +  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
           +  & 
           21*MIN2**2*MOU2*p1p4**2  & 
           -  & 
           2*MIN2*MOU2**2*p1p4**2  & 
           - 9*MOU2**3*p1p4**2  & 
           -  & 
           18*MIN2**2.5*np2*p1p4**2  & 
           +  & 
           58*MIN2**1.5*MOU2*np2*p1p4**2  & 
           -  & 
           40*sqrt(MIN2)*MOU2**2*np2*p1p4**2  & 
           +  & 
           4*MIN2**2.5*np4*p1p4**2  & 
           -  & 
           21*MIN2**1.5*MOU2*np4*p1p4**2  & 
           +  & 
           17*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
           +  & 
           48*MIN2**2*p1p2*p1p4**2  & 
           -  & 
           85*MIN2*MOU2*p1p2*p1p4**2  & 
           +  & 
           17*MOU2**2*p1p2*p1p4**2  & 
           +  & 
           46*MIN2**1.5*np2*p1p2*p1p4**2  & 
           -  & 
           78*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
           -  & 
           8*MIN2**1.5*np4*p1p2*p1p4**2  & 
           +  & 
           30*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
           -  & 
           66*MIN2*p1p2**2*p1p4**2  & 
           +  & 
           64*MOU2*p1p2**2*p1p4**2  & 
           -  & 
           28*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
           +  & 
           28*p1p2**3*p1p4**2  & 
           + 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
           +  & 
           6*MIN2**2*p1p4**3  & 
           + 11*MIN2*MOU2*p1p4**3  & 
           -  & 
           17*MOU2**2*p1p4**3  & 
           + 22*MIN2**1.5*np2*p1p4**3  & 
           -  & 
           26*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
           -  & 
           12*MIN2**1.5*np4*p1p4**3  & 
           +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
           -  & 
           34*MIN2*p1p2*p1p4**3  & 
           - 4*MOU2*p1p2*p1p4**3  & 
           -  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
           +  & 
           20*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
           +  & 
           28*p1p2**2*p1p4**3  & 
           + 12*MIN2*p1p4**4  & 
           -  & 
           12*MOU2*p1p4**4  & 
           - 4*sqrt(MIN2)*np2*p1p4**4  & 
           -  & 
           16*p1p2*p1p4**4  & 
           + 4*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
           +  & 
           10*MIN2**3*MOU2*p2p4  & 
           -  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p2p4  & 
           -  & 
           10*MIN2**2*MOU2**2*p2p4  & 
           +  & 
           18*MIN2**2.5*MOU2*np2*p2p4  & 
           -  & 
           18*MIN2**1.5*MOU2**2*np2*p2p4  & 
           -  & 
           9*MIN2**2.5*MOU2*np4*p2p4  & 
           +  & 
           9*sqrt(MIN2)*MOU2**3*np4*p2p4  & 
           -  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4  & 
           -  & 
           48*MIN2**2*MOU2*p1p2*p2p4  & 
           +  & 
           28*MIN2*MOU2**2*p1p2*p2p4  & 
           -  & 
           46*MIN2**1.5*MOU2*np2*p1p2*p2p4  & 
           +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p2*p2p4  & 
           +  & 
           23*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
           +  & 
           5*sqrt(MIN2)*MOU2**2*np4*p1p2*p2p4  & 
           +  & 
           66*MIN2*MOU2*p1p2**2*p2p4  & 
           -  & 
           18*MOU2**2*p1p2**2*p2p4  & 
           +  & 
           28*sqrt(MIN2)*MOU2*np2*p1p2**2*p2p4  & 
           -  & 
           14*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4  & 
           -  & 
           28*MOU2*p1p2**3*p2p4  & 
           +  & 
           8*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
           +  & 
           20*MIN2**3*p1p4*p2p4  & 
           -  & 
           18*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
           -  & 
           65*MIN2**2*MOU2*p1p4*p2p4  & 
           +  & 
           54*MIN2*MOU2**2*p1p4*p2p4  & 
           - 9*MOU2**3*p1p4*p2p4  & 
           +  & 
           36*MIN2**2.5*np2*p1p4*p2p4  & 
           -  & 
           126*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
           +  & 
           86*sqrt(MIN2)*MOU2**2*np2*p1p4*p2p4  & 
           -  & 
           8*MIN2**2.5*np4*p1p4*p2p4  & 
           +  & 
           29*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
           -  & 
           17*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4  & 
           -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
           -  & 
           96*MIN2**2*p1p2*p1p4*p2p4  & 
           +  & 
           207*MIN2*MOU2*p1p2*p1p4*p2p4  & 
           -  & 
           91*MOU2**2*p1p2*p1p4*p2p4  & 
           -  & 
           92*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
           +  & 
           156*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4  & 
           +  & 
           16*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
           -  & 
           32*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
           +  & 
           132*MIN2*p1p2**2*p1p4*p2p4  & 
           -  & 
           142*MOU2*p1p2**2*p1p4*p2p4  & 
           +  & 
           56*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4  & 
           -  & 
           8*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
           -  & 
           56*p1p2**3*p1p4*p2p4  & 
           -  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
           -  & 
           46*MIN2**2*p1p4**2*p2p4  & 
           +  & 
           49*MIN2*MOU2*p1p4**2*p2p4  & 
           +  & 
           17*MOU2**2*p1p4**2*p2p4  & 
           -  & 
           90*MIN2**1.5*np2*p1p4**2*p2p4  & 
           +  & 
           130*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
           +  & 
           22*MIN2**1.5*np4*p1p4**2*p2p4  & 
           -  & 
           40*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
           +  & 
           158*MIN2*p1p2*p1p4**2*p2p4  & 
           -  & 
           98*MOU2*p1p2*p1p4**2*p2p4  & 
           +  & 
           120*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
           -  & 
           36*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
           -  & 
           112*p1p2**2*p1p4**2*p2p4  & 
           + 2*MIN2*p1p4**3*p2p4  & 
           +  & 
           40*MOU2*p1p4**3*p2p4  & 
           +  & 
           44*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
           -  & 
           16*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
           -  & 
           8*p1p2*p1p4**3*p2p4  & 
           + 16*p1p4**4*p2p4  & 
           -  & 
           4*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
           - 10*MIN2**3*p2p4**2  & 
           +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2  & 
           +  & 
           44*MIN2**2*MOU2*p2p4**2  & 
           -  & 
           14*MIN2*MOU2**2*p2p4**2  & 
           -  & 
           18*MIN2**2.5*np2*p2p4**2  & 
           +  & 
           68*MIN2**1.5*MOU2*np2*p2p4**2  & 
           -  & 
           18*sqrt(MIN2)*MOU2**2*np2*p2p4**2  & 
           +  & 
           4*MIN2**2.5*np4*p2p4**2  & 
           -  & 
           8*MIN2**1.5*MOU2*np4*p2p4**2  & 
           -  & 
           28*sqrt(MIN2)*MOU2**2*np4*p2p4**2  & 
           +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2  & 
           +  & 
           48*MIN2**2*p1p2*p2p4**2  & 
           -  & 
           122*MIN2*MOU2*p1p2*p2p4**2  & 
           +  & 
           18*MOU2**2*p1p2*p2p4**2  & 
           +  & 
           46*MIN2**1.5*np2*p1p2*p2p4**2  & 
           -  & 
           78*sqrt(MIN2)*MOU2*np2*p1p2*p2p4**2  & 
           -  & 
           8*MIN2**1.5*np4*p1p2*p2p4**2  & 
           +  & 
           2*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2  & 
           -  & 
           66*MIN2*p1p2**2*p2p4**2  & 
           +  & 
           78*MOU2*p1p2**2*p2p4**2  & 
           -  & 
           28*sqrt(MIN2)*np2*p1p2**2*p2p4**2  & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p2p4**2  & 
           +  & 
           28*p1p2**3*p2p4**2  & 
           +  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
           +  & 
           74*MIN2**2*p1p4*p2p4**2  & 
           -  & 
           102*MIN2*MOU2*p1p4*p2p4**2  & 
           +  & 
           28*MOU2**2*p1p4*p2p4**2  & 
           +  & 
           114*MIN2**1.5*np2*p1p4*p2p4**2  & 
           -  & 
           154*sqrt(MIN2)*MOU2*np2*p1p4*p2p4**2  & 
           -  & 
           8*MIN2**1.5*np4*p1p4*p2p4**2  & 
           +  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
           -  & 
           214*MIN2*p1p2*p1p4*p2p4**2  & 
           +  & 
           152*MOU2*p1p2*p1p4*p2p4**2  & 
           -  & 
           144*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2  & 
           +  & 
           12*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
           +  & 
           140*p1p2**2*p1p4*p2p4**2  & 
           -  & 
           68*MIN2*p1p4**2*p2p4**2  & 
           -  & 
           2*MOU2*p1p4**2*p2p4**2  & 
           -  & 
           104*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
           +  & 
           20*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
           +  & 
           92*p1p2*p1p4**2*p2p4**2  & 
           - 20*p1p4**3*p2p4**2  & 
           -  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3  & 
           - 34*MIN2**2*p2p4**3  & 
           +  & 
           42*MIN2*MOU2*p2p4**3  & 
           - 46*MIN2**1.5*np2*p2p4**3  & 
           +  & 
           50*sqrt(MIN2)*MOU2*np2*p2p4**3  & 
           -  & 
           2*MIN2**1.5*np4*p2p4**3  & 
           +  & 
           26*sqrt(MIN2)*MOU2*np4*p2p4**3  & 
           +  & 
           90*MIN2*p1p2*p2p4**3  & 
           - 50*MOU2*p1p2*p2p4**3  & 
           +  & 
           56*sqrt(MIN2)*np2*p1p2*p2p4**3  & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2*p2p4**3  & 
           -  & 
           56*p1p2**2*p2p4**3  & 
           + 82*MIN2*p1p4*p2p4**3  & 
           -  & 
           26*MOU2*p1p4*p2p4**3  & 
           +  & 
           92*sqrt(MIN2)*np2*p1p4*p2p4**3  & 
           +  & 
           8*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
           -  & 
           96*p1p2*p1p4*p2p4**3  & 
           - 8*p1p4**2*p2p4**3  & 
           -  & 
           28*MIN2*p2p4**4  & 
           - 28*sqrt(MIN2)*np2*p2p4**4  & 
           -  & 
           12*sqrt(MIN2)*np4*p2p4**4  & 
           + 28*p1p2*p2p4**4  & 
           +  & 
           12*p1p4*p2p4**4)* & 
         Ccache(6)) & 
        /(9.*p1p4*(p1p4  & 
           - p2p4)**2*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T4TRIBC
                          !!!!!!!!!!!!!!!!!!!!!!
