
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T3TRIAC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T3TRIAC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Ccache(7)
    complex(kind=prec) :: Bcache(1)

!
      real(kind=prec) T3TRIAC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb= & 
          -1._prec
!

!    
      call setlambda( & 
          -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec*10._prec**( & 
          -10._prec))
!     
    Bcache(1) = B0i(bb0,MIN2,0._prec,MIN2)
    Ccache(1) = C0i(cc0,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(2) = C0i(cc00,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(3) = C0i(cc1,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(4) = C0i(cc11,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(5) = C0i(cc12,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(6) = C0i(cc2,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(7) = C0i(cc22,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)

!
      T3TRIAC=real(( & 
          -2*EL**4*GF**2*( & 
          -2*MIN2**2*p1p2*p1p4  & 
          +  & 
           2*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
           6*MIN2**1.5*np2*p1p2*p1p4  & 
          +  & 
           6*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
          +  & 
           10*MIN2*p1p2**2*p1p4  & 
          - 6*MOU2*p1p2**2*p1p4  & 
          +  & 
           8*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
          - 8*p1p2**3*p1p4  & 
          -  & 
           MIN2**2*p1p4**2  & 
          + 4*MIN2*MOU2*p1p4**2  & 
          -  & 
           3*MOU2**2*p1p4**2  & 
          + 5*MIN2**1.5*np2*p1p4**2  & 
          -  & 
           5*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
          -  & 
           2*MIN2*p1p2*p1p4**2  & 
          - 2*MOU2*p1p2*p1p4**2  & 
          -  & 
           4*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
          + 2*MIN2*p1p4**3  & 
          -  & 
           2*MOU2*p1p4**3  & 
          - 2*sqrt(MIN2)*np2*p1p4**3  & 
          +  & 
           2*MIN2**3*p2p4  & 
          - 2*MIN2**2*MOU2*p2p4  & 
          +  & 
           6*MIN2**2.5*np2*p2p4  & 
          -  & 
           6*MIN2**1.5*MOU2*np2*p2p4  & 
          -  & 
           10*MIN2**2*p1p2*p2p4  & 
          + 6*MIN2*MOU2*p1p2*p2p4  & 
          -  & 
           8*MIN2**1.5*np2*p1p2*p2p4  & 
          + 8*MIN2*p1p2**2*p2p4  & 
          +  & 
           MIN2**2*p1p4*p2p4  & 
          - MIN2*MOU2*p1p4*p2p4  & 
          -  & 
           2*MIN2**1.5*np2*p1p4*p2p4  & 
          -  & 
           8*MIN2*p1p2*p1p4*p2p4  & 
          + 6*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           8*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
          +  & 
           16*p1p2**2*p1p4*p2p4  & 
          + 4*MOU2*p1p4**2*p2p4  & 
          +  & 
           6*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
          +  & 
           10*MIN2**2*p2p4**2  & 
          - 6*MIN2*MOU2*p2p4**2  & 
          +  & 
           8*MIN2**1.5*np2*p2p4**2  & 
          - 16*MIN2*p1p2*p2p4**2  & 
          -  & 
           2*MIN2*p1p4*p2p4**2  & 
          - 8*p1p2*p1p4*p2p4**2  & 
          +  & 
           8*MIN2*p2p4**3  & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            ( & 
          -(MIN2*p1p4)  & 
          + MOU2*p1p4  & 
          + 3*p1p2*p1p4  & 
          -  & 
              p1p4**2  & 
          + 3*MIN2*p2p4  & 
          - 3*MOU2*p2p4  & 
          -  & 
              4*p1p2*p2p4  & 
          - 3*p1p4*p2p4  & 
          + 4*p2p4**2)  & 
          -  & 
           sqrt(MIN2)*np4* & 
            ( & 
          -3*MIN2**2*p1p4  & 
          + 6*MIN2*MOU2*p1p4  & 
          -  & 
              3*MOU2**2*p1p4  & 
          + 7*MIN2*p1p2*p1p4  & 
          -  & 
              7*MOU2*p1p2*p1p4  & 
          - 4*p1p2**2*p1p4  & 
          +  & 
              2*MIN2*p1p4**2  & 
          - 2*MOU2*p1p4**2  & 
          -  & 
              2*p1p2*p1p4**2  & 
          + 6*MIN2**2*p2p4  & 
          -  & 
              6*MIN2*MOU2*p2p4  & 
          - 14*MIN2*p1p2*p2p4  & 
          +  & 
              6*MOU2*p1p2*p2p4  & 
          + 8*p1p2**2*p2p4  & 
          -  & 
              6*MIN2*p1p4*p2p4  & 
          + 4*MOU2*p1p4*p2p4  & 
          +  & 
              6*p1p2*p1p4*p2p4  & 
          + 8*MIN2*p2p4**2  & 
          -  & 
              8*p1p2*p2p4**2))*Bcache(1))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (2*EL**4*GF**2*( & 
          -4*MIN2**3*p1p2*p1p4  & 
          +  & 
           4*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
           12*MIN2**2.5*np2*p1p2*p1p4  & 
          +  & 
           12*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           20*MIN2**2*p1p2**2*p1p4  & 
          -  & 
           12*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
           16*MIN2**1.5*np2*p1p2**2*p1p4  & 
          -  & 
           16*MIN2*p1p2**3*p1p4  & 
          + 4*MIN2**3*p1p4**2  & 
          -  & 
           4*MIN2**2*MOU2*p1p4**2  & 
          +  & 
           12*MIN2**2.5*np2*p1p4**2  & 
          -  & 
           12*MIN2**1.5*MOU2*np2*p1p4**2  & 
          -  & 
           26*MIN2**2*p1p2*p1p4**2  & 
          +  & 
           18*MIN2*MOU2*p1p2*p1p4**2  & 
          -  & 
           24*MIN2**1.5*np2*p1p2*p1p4**2  & 
          +  & 
           12*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           32*MIN2*p1p2**2*p1p4**2  & 
          -  & 
           12*MOU2*p1p2**2*p1p4**2  & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           16*p1p2**3*p1p4**2  & 
          + 12*MIN2**2*p1p4**3  & 
          -  & 
           12*MIN2*MOU2*p1p4**3  & 
          + 12*MIN2**1.5*np2*p1p4**3  & 
          -  & 
           16*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          -  & 
           32*MIN2*p1p2*p1p4**3  & 
          + 16*MOU2*p1p2*p1p4**3  & 
          -  & 
           20*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          +  & 
           16*p1p2**2*p1p4**3  & 
          - 4*MIN2*p1p4**4  & 
          +  & 
           4*MOU2*p1p4**4  & 
          - 4*sqrt(MIN2)*np2*p1p4**4  & 
          +  & 
           16*p1p2*p1p4**4  & 
          + 4*MIN2**4*p2p4  & 
          -  & 
           4*MIN2**3*MOU2*p2p4  & 
          + 12*MIN2**3.5*np2*p2p4  & 
          -  & 
           12*MIN2**2.5*MOU2*np2*p2p4  & 
          -  & 
           20*MIN2**3*p1p2*p2p4  & 
          +  & 
           12*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
           16*MIN2**2.5*np2*p1p2*p2p4  & 
          +  & 
           16*MIN2**2*p1p2**2*p2p4  & 
          + 4*MIN2**3*p1p4*p2p4  & 
          -  & 
           4*MIN2**2*MOU2*p1p4*p2p4  & 
          +  & 
           2*MIN2**2.5*np2*p1p4*p2p4  & 
          -  & 
           6*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          -  & 
           26*MIN2**2*p1p2*p1p4*p2p4  & 
          +  & 
           18*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           24*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          +  & 
           40*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           10*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           2*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
           6*MIN2**1.5*np2*p1p4**2*p2p4  & 
          +  & 
           6*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          -  & 
           8*MIN2*p1p2*p1p4**2*p2p4  & 
          -  & 
           8*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          +  & 
           16*p1p2**2*p1p4**2*p2p4  & 
          + 28*MIN2*p1p4**3*p2p4  & 
          -  & 
           16*MOU2*p1p4**3*p2p4  & 
          +  & 
           28*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           48*p1p2*p1p4**3*p2p4  & 
          - 16*p1p4**4*p2p4  & 
          +  & 
           20*MIN2**3*p2p4**2  & 
          - 12*MIN2**2*MOU2*p2p4**2  & 
          +  & 
           16*MIN2**2.5*np2*p2p4**2  & 
          -  & 
           32*MIN2**2*p1p2*p2p4**2  & 
          +  & 
           2*MIN2**2*p1p4*p2p4**2  & 
          -  & 
           6*MIN2*MOU2*p1p4*p2p4**2  & 
          +  & 
           8*MIN2**1.5*np2*p1p4*p2p4**2  & 
          -  & 
           32*MIN2*p1p2*p1p4*p2p4**2  & 
          -  & 
           16*MIN2*p1p4**2*p2p4**2  & 
          +  & 
           12*MOU2*p1p4**2*p2p4**2  & 
          -  & 
           8*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           16*p1p2*p1p4**2*p2p4**2  & 
          + 32*p1p4**3*p2p4**2  & 
          +  & 
           16*MIN2**2*p2p4**3  & 
          + 8*MIN2*p1p4*p2p4**3  & 
          -  & 
           16*p1p4**2*p2p4**3  & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            ( & 
          -6*MIN2**2*p1p4  & 
          + 6*MIN2*MOU2*p1p4  & 
          +  & 
              8*MIN2*p1p2*p1p4  & 
          + 8*MIN2*p1p4**2  & 
          -  & 
              4*MOU2*p1p4**2  & 
          - 10*p1p2*p1p4**2  & 
          + 2*p1p4**3  & 
          -  & 
              8*MIN2*p1p4*p2p4  & 
          + 6*p1p4**2*p2p4)  & 
          +  & 
           sqrt(MIN2)*np4* & 
            (6*MIN2**3*p1p4  & 
          - 12*MIN2**2*MOU2*p1p4  & 
          +  & 
              6*MIN2*MOU2**2*p1p4  & 
          + 14*MIN2*MOU2*p1p2*p1p4  & 
          +  & 
              8*MIN2*p1p2**2*p1p4  & 
          + 8*MIN2*MOU2*p1p4**2  & 
          +  & 
              12*MIN2*p1p2*p1p4**2  & 
          + 4*p1p2**2*p1p4**2  & 
          +  & 
              4*MIN2*p1p4**3  & 
          - 4*MOU2*p1p4**3  & 
          -  & 
              12*p1p2*p1p4**3  & 
          - 12*MIN2**3*p2p4  & 
          +  & 
              12*MIN2**2*MOU2*p2p4  & 
          -  & 
              12*MIN2*MOU2*p1p2*p2p4  & 
          -  & 
              16*MIN2*p1p2**2*p2p4  & 
          -  & 
              18*MIN2*MOU2*p1p4*p2p4  & 
          -  & 
              26*MIN2*p1p2*p1p4*p2p4  & 
          -  & 
              6*MOU2*p1p2*p1p4*p2p4  & 
          - 8*p1p2**2*p1p4*p2p4  & 
          -  & 
              24*MIN2*p1p4**2*p2p4  & 
          + 16*MOU2*p1p4**2*p2p4  & 
          +  & 
              20*p1p2*p1p4**2*p2p4  & 
          + 16*p1p4**3*p2p4  & 
          +  & 
              12*MIN2*MOU2*p2p4**2  & 
          + 32*MIN2*p1p2*p2p4**2  & 
          +  & 
              36*MIN2*p1p4*p2p4**2  & 
          - 12*MOU2*p1p4*p2p4**2  & 
          -  & 
              8*p1p2*p1p4*p2p4**2  & 
          - 32*p1p4**2*p2p4**2  & 
          -  & 
              16*MIN2*p2p4**3  & 
          + 16*p1p4*p2p4**3  & 
          +  & 
              MIN2**2*( & 
          -14*p1p2*p1p4  & 
          - 8*p1p4**2  & 
          +  & 
                 28*p1p2*p2p4  & 
          + 22*p1p4*p2p4  & 
          - 28*p2p4**2)))* & 
         Ccache(1))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (4*EL**4*GF**2*( & 
          -2*MIN2**2*p1p2*p1p4  & 
          +  & 
           2*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
           6*MIN2**1.5*np2*p1p2*p1p4  & 
          +  & 
           6*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
          +  & 
           10*MIN2*p1p2**2*p1p4  & 
          - 6*MOU2*p1p2**2*p1p4  & 
          +  & 
           8*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
          - 8*p1p2**3*p1p4  & 
          -  & 
           MIN2**2*p1p4**2  & 
          + 4*MIN2*MOU2*p1p4**2  & 
          -  & 
           3*MOU2**2*p1p4**2  & 
          + 5*MIN2**1.5*np2*p1p4**2  & 
          -  & 
           5*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
          -  & 
           2*MIN2*p1p2*p1p4**2  & 
          - 2*MOU2*p1p2*p1p4**2  & 
          -  & 
           4*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
          - 2*MIN2*p1p4**3  & 
          +  & 
           2*MOU2*p1p4**3  & 
          - 2*sqrt(MIN2)*np2*p1p4**3  & 
          +  & 
           8*p1p2*p1p4**3  & 
          + 2*MIN2**3*p2p4  & 
          -  & 
           2*MIN2**2*MOU2*p2p4  & 
          + 6*MIN2**2.5*np2*p2p4  & 
          -  & 
           6*MIN2**1.5*MOU2*np2*p2p4  & 
          -  & 
           10*MIN2**2*p1p2*p2p4  & 
          + 6*MIN2*MOU2*p1p2*p2p4  & 
          -  & 
           8*MIN2**1.5*np2*p1p2*p2p4  & 
          + 8*MIN2*p1p2**2*p2p4  & 
          +  & 
           MIN2**2*p1p4*p2p4  & 
          - MIN2*MOU2*p1p4*p2p4  & 
          -  & 
           2*MIN2**1.5*np2*p1p4*p2p4  & 
          -  & 
           8*MIN2*p1p2*p1p4*p2p4  & 
          + 6*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           8*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
          +  & 
           16*p1p2**2*p1p4*p2p4  & 
          + 8*MIN2*p1p4**2*p2p4  & 
          -  & 
           4*MOU2*p1p4**2*p2p4  & 
          +  & 
           8*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
          -  & 
           16*p1p2*p1p4**2*p2p4  & 
          - 8*p1p4**3*p2p4  & 
          +  & 
           10*MIN2**2*p2p4**2  & 
          - 6*MIN2*MOU2*p2p4**2  & 
          +  & 
           8*MIN2**1.5*np2*p2p4**2  & 
          - 16*MIN2*p1p2*p2p4**2  & 
          -  & 
           6*MIN2*p1p4*p2p4**2  & 
          + 6*MOU2*p1p4*p2p4**2  & 
          +  & 
           16*p1p4**2*p2p4**2  & 
          + 8*MIN2*p2p4**3  & 
          -  & 
           8*p1p4*p2p4**3  & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            ( & 
          -5*MIN2*p1p4  & 
          + 5*MOU2*p1p4  & 
          + 10*p1p2*p1p4  & 
          +  & 
              6*MIN2*p2p4  & 
          - 6*MOU2*p2p4  & 
          - 8*p1p2*p2p4  & 
          -  & 
              10*p1p4*p2p4  & 
          + 8*p2p4**2)  & 
          -  & 
           sqrt(MIN2)*np4* & 
            ( & 
          -3*MIN2**2*p1p4  & 
          + 6*MIN2*MOU2*p1p4  & 
          -  & 
              3*MOU2**2*p1p4  & 
          + 7*MIN2*p1p2*p1p4  & 
          -  & 
              7*MOU2*p1p2*p1p4  & 
          - 4*p1p2**2*p1p4  & 
          -  & 
              2*MIN2*p1p4**2  & 
          + 2*MOU2*p1p4**2  & 
          +  & 
              6*p1p2*p1p4**2  & 
          + 6*MIN2**2*p2p4  & 
          -  & 
              6*MIN2*MOU2*p2p4  & 
          - 14*MIN2*p1p2*p2p4  & 
          +  & 
              6*MOU2*p1p2*p2p4  & 
          + 8*p1p2**2*p2p4  & 
          +  & 
              2*MIN2*p1p4*p2p4  & 
          - 4*MOU2*p1p4*p2p4  & 
          -  & 
              8*p1p2*p1p4*p2p4  & 
          - 8*p1p4**2*p2p4  & 
          +  & 
              2*MIN2*p2p4**2  & 
          + 6*MOU2*p2p4**2  & 
          +  & 
              16*p1p4*p2p4**2  & 
          - 8*p2p4**3))* & 
         Ccache(2))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (2*EL**4*GF**2*(4*MIN2**2*p1p2*p1p4**2  & 
          -  & 
           4*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
           18*MIN2**1.5*np2*p1p2*p1p4**2  & 
          -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          -  & 
           20*MIN2*p1p2**2*p1p4**2  & 
          +  & 
           12*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           24*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          +  & 
           16*p1p2**3*p1p4**2  & 
          - 18*MIN2**2*p1p4**3  & 
          +  & 
           18*MIN2*MOU2*p1p4**3  & 
          - 18*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          +  & 
           52*MIN2*p1p2*p1p4**3  & 
          - 16*MOU2*p1p2*p1p4**3  & 
          +  & 
           20*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          -  & 
           16*p1p2**2*p1p4**3  & 
          + 4*MIN2*p1p4**4  & 
          -  & 
           4*MOU2*p1p4**4  & 
          + 4*sqrt(MIN2)*np2*p1p4**4  & 
          -  & 
           16*p1p2*p1p4**4  & 
          - 4*MIN2**3*p1p4*p2p4  & 
          +  & 
           4*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           18*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           18*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          +  & 
           20*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
           12*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           24*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          -  & 
           16*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           24*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           24*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
           16*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           6*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          -  & 
           48*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           16*p1p2**2*p1p4**2*p2p4  & 
          - 52*MIN2*p1p4**3*p2p4  & 
          +  & 
           16*MOU2*p1p4**3*p2p4  & 
          -  & 
           28*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          +  & 
           48*p1p2*p1p4**3*p2p4  & 
          + 16*p1p4**4*p2p4  & 
          -  & 
           26*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           24*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           24*MIN2**1.5*np2*p1p4*p2p4**2  & 
          +  & 
           48*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
           64*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           12*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           8*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          -  & 
           16*p1p2*p1p4**2*p2p4**2  & 
          - 32*p1p4**3*p2p4**2  & 
          -  & 
           32*MIN2*p1p4*p2p4**3  & 
          + 16*p1p4**2*p2p4**3  & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (6*MIN2**2*p1p4  & 
          - 6*MIN2*MOU2*p1p4  & 
          -  & 
              4*MIN2*p1p2*p1p4  & 
          - 6*MOU2*p1p2*p1p4  & 
          -  & 
              8*p1p2**2*p1p4  & 
          - 8*MIN2*p1p4**2  & 
          +  & 
              6*MOU2*p1p4**2  & 
          + 10*p1p2*p1p4**2  & 
          - 2*p1p4**3  & 
          -  & 
              6*MIN2**2*p2p4  & 
          + 6*MIN2*MOU2*p2p4  & 
          +  & 
              8*MIN2*p1p2*p2p4  & 
          + 8*MIN2*p1p4*p2p4  & 
          +  & 
              8*p1p2*p1p4*p2p4  & 
          - 6*p1p4**2*p2p4  & 
          -  & 
              8*MIN2*p2p4**2)  & 
          +  & 
           sqrt(MIN2)*np4* & 
            ( & 
          -6*MIN2**3*p1p4  & 
          + 12*MIN2**2*MOU2*p1p4  & 
          -  & 
              6*MIN2*MOU2**2*p1p4  & 
          - 20*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
              22*MIN2*p1p2**2*p1p4  & 
          + 6*MOU2*p1p2**2*p1p4  & 
          +  & 
              8*p1p2**3*p1p4  & 
          - 10*MIN2*MOU2*p1p4**2  & 
          -  & 
              18*MIN2*p1p2*p1p4**2  & 
          - 2*MOU2*p1p2*p1p4**2  & 
          -  & 
              4*p1p2**2*p1p4**2  & 
          - 4*MIN2*p1p4**3  & 
          +  & 
              4*MOU2*p1p4**3  & 
          + 12*p1p2*p1p4**3  & 
          +  & 
              26*MIN2*MOU2*p1p4*p2p4  & 
          +  & 
              44*MIN2*p1p2*p1p4*p2p4  & 
          +  & 
              6*MOU2*p1p2*p1p4*p2p4  & 
          + 32*MIN2*p1p4**2*p2p4  & 
          -  & 
              16*MOU2*p1p4**2*p2p4  & 
          - 20*p1p2*p1p4**2*p2p4  & 
          -  & 
              16*p1p4**3*p2p4  & 
          - 18*MIN2*MOU2*p2p4**2  & 
          -  & 
              24*MIN2*p1p2*p2p4**2  & 
          - 52*MIN2*p1p4*p2p4**2  & 
          +  & 
              12*MOU2*p1p4*p2p4**2  & 
          + 8*p1p2*p1p4*p2p4**2  & 
          +  & 
              32*p1p4**2*p2p4**2  & 
          + 24*MIN2*p2p4**3  & 
          -  & 
              16*p1p4*p2p4**3  & 
          +  & 
              MIN2**2*(20*p1p2*p1p4  & 
          + 10*p1p4**2  & 
          -  & 
                 26*p1p4*p2p4  & 
          + 18*p2p4**2)))* & 
         Ccache(3))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (2*EL**4*GF**2*sqrt(MIN2)* & 
         ( & 
          -(CMPLX(aa,bb)*asym*(8*p1p2*p1p4*( & 
          -p1p2  & 
          + p2p4)  & 
          -  & 
                MIN2**2*(4*p1p4  & 
          + 3*p2p4)  & 
          +  & 
                MIN2*(11*p1p2*p1p4  & 
          + p1p4**2  & 
          + 4*p1p2*p2p4  & 
          -  & 
                   7*p1p4*p2p4  & 
          - 4*p2p4**2)  & 
          +  & 
                MOU2*(2*p1p4*( & 
          -3*p1p2  & 
          + p1p4)  & 
          +  & 
                   MIN2*(4*p1p4  & 
          + 3*p2p4))))  & 
          -  & 
           2*( & 
          -6*MIN2**2*np2*p1p2*p1p4  & 
          +  & 
              6*MIN2*MOU2*np2*p1p2*p1p4  & 
          +  & 
              8*MIN2*np2*p1p2**2*p1p4  & 
          +  & 
              6*MIN2**2*np2*p1p4**2  & 
          -  & 
              6*MIN2*MOU2*np2*p1p4**2  & 
          -  & 
              3*MIN2*np2*p1p2*p1p4**2  & 
          -  & 
              3*MOU2*np2*p1p2*p1p4**2  & 
          -  & 
              4*np2*p1p2**2*p1p4**2  & 
          - 3*MIN2*np2*p1p4**3  & 
          +  & 
              MOU2*np2*p1p4**3  & 
          + 6*MIN2**3*np2*p2p4  & 
          -  & 
              6*MIN2**2*MOU2*np2*p2p4  & 
          -  & 
              8*MIN2**2*np2*p1p2*p2p4  & 
          -  & 
              8*MIN2**2*np2*p1p4*p2p4  & 
          +  & 
              6*MIN2*MOU2*np2*p1p4*p2p4  & 
          +  & 
              10*MIN2*np2*p1p4**2*p2p4  & 
          +  & 
              4*np2*p1p2*p1p4**2*p2p4  & 
          +  & 
              8*MIN2**2*np2*p2p4**2  & 
          -  & 
              8*MIN2*np2*p1p4*p2p4**2  & 
          +  & 
              np4*(MIN2  & 
          - p1p2)* & 
               (3*MIN2*p1p2*p1p4  & 
          - 3*MOU2*p1p2*p1p4  & 
          -  & 
                 4*p1p2**2*p1p4  & 
          - MIN2*p1p4**2  & 
          +  & 
                 MOU2*p1p4**2  & 
          - 6*MIN2**2*p2p4  & 
          +  & 
                 6*MIN2*MOU2*p2p4  & 
          + 8*MIN2*p1p2*p2p4  & 
          +  & 
                 2*MIN2*p1p4*p2p4  & 
          + 4*p1p2*p1p4*p2p4  & 
          -  & 
                 8*MIN2*p2p4**2)  & 
          +  & 
              sqrt(MIN2)* & 
               (2*MIN2*MOU2*p1p2*p1p4  & 
          +  & 
                 10*MIN2*p1p2**2*p1p4  & 
          -  & 
                 6*MOU2*p1p2**2*p1p4  & 
          - 8*p1p2**3*p1p4  & 
          -  & 
                 2*MIN2*MOU2*p1p4**2  & 
          -  & 
                 11*MIN2*p1p2*p1p4**2  & 
          +  & 
                 7*MOU2*p1p2*p1p4**2  & 
          + 6*p1p2**2*p1p4**2  & 
          -  & 
                 MIN2*p1p4**3  & 
          + MOU2*p1p4**3  & 
          +  & 
                 6*p1p2*p1p4**3  & 
          + 2*MIN2**3*p2p4  & 
          -  & 
                 2*MIN2**2*MOU2*p2p4  & 
          +  & 
                 6*MIN2*MOU2*p1p2*p2p4  & 
          +  & 
                 8*MIN2*p1p2**2*p2p4  & 
          -  & 
                 3*MIN2*p1p2*p1p4*p2p4  & 
          +  & 
                 3*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
                 12*p1p2**2*p1p4*p2p4  & 
          +  & 
                 13*MIN2*p1p4**2*p2p4  & 
          -  & 
                 9*MOU2*p1p4**2*p2p4  & 
          -  & 
                 20*p1p2*p1p4**2*p2p4  & 
          - 8*p1p4**3*p2p4  & 
          -  & 
                 6*MIN2*MOU2*p2p4**2  & 
          -  & 
                 16*MIN2*p1p2*p2p4**2  & 
          -  & 
                 10*MIN2*p1p4*p2p4**2  & 
          +  & 
                 6*MOU2*p1p4*p2p4**2  & 
          + 4*p1p2*p1p4*p2p4**2  & 
          +  & 
                 16*p1p4**2*p2p4**2  & 
          + 8*MIN2*p2p4**3  & 
          -  & 
                 8*p1p4*p2p4**3  & 
          +  & 
                 2*MIN2**2* & 
                  ( & 
          -(p1p2*p1p4)  & 
          + p1p4**2  & 
          - 5*p1p2*p2p4  & 
          +  & 
                    5*p2p4**2))))* & 
         Ccache(4))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (4*EL**4*GF**2*(4*MIN2**3*p1p2*p1p4  & 
          -  & 
           4*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
           6*MIN2**2.5*np4*p1p2*p1p4  & 
          +  & 
           6*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           20*MIN2**2*p1p2**2*p1p4  & 
          +  & 
           12*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
           14*MIN2**1.5*np4*p1p2**2*p1p4  & 
          -  & 
           6*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
          +  & 
           16*MIN2*p1p2**3*p1p4  & 
          -  & 
           8*sqrt(MIN2)*np4*p1p2**3*p1p4  & 
          -  & 
           4*MIN2**3*p1p4**2  & 
          + 4*MIN2**2*MOU2*p1p4**2  & 
          +  & 
           2*MIN2**2.5*np4*p1p4**2  & 
          -  & 
           2*MIN2**1.5*MOU2*np4*p1p4**2  & 
          +  & 
           20*MIN2**2*p1p2*p1p4**2  & 
          -  & 
           12*MIN2*MOU2*p1p2*p1p4**2  & 
          -  & 
           2*MIN2*p1p2**2*p1p4**2  & 
          - 6*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           2*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
          -  & 
           8*p1p2**3*p1p4**2  & 
          + 4*MIN2**2*p1p4**3  & 
          -  & 
           4*MIN2*MOU2*p1p4**3  & 
          + 2*MIN2**1.5*np4*p1p4**3  & 
          -  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          -  & 
           24*MIN2*p1p2*p1p4**3  & 
          + 8*MOU2*p1p2*p1p4**3  & 
          -  & 
           6*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
          +  & 
           8*p1p2**2*p1p4**3  & 
          - 2*MIN2*p1p4**4  & 
          +  & 
           2*MOU2*p1p4**4  & 
          + 8*p1p2*p1p4**4  & 
          -  & 
           4*MIN2**4*p2p4  & 
          + 4*MIN2**3*MOU2*p2p4  & 
          +  & 
           12*MIN2**3.5*np4*p2p4  & 
          -  & 
           12*MIN2**2.5*MOU2*np4*p2p4  & 
          +  & 
           20*MIN2**3*p1p2*p2p4  & 
          -  & 
           12*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
           28*MIN2**2.5*np4*p1p2*p2p4  & 
          +  & 
           12*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          -  & 
           16*MIN2**2*p1p2**2*p2p4  & 
          +  & 
           16*MIN2**1.5*np4*p1p2**2*p2p4  & 
          +  & 
           2*MIN2**3*p1p4*p2p4  & 
          - 2*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           9*MIN2**2.5*np4*p1p4*p2p4  & 
          +  & 
           5*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          -  & 
           4*MIN2**2*p1p2*p1p4*p2p4  & 
          +  & 
           5*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          -  & 
           3*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          -  & 
           16*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
          -  & 
           25*MIN2**2*p1p4**2*p2p4  & 
          +  & 
           17*MIN2*MOU2*p1p4**2*p2p4  & 
          -  & 
           6*MIN2**1.5*np4*p1p4**2*p2p4  & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          +  & 
           38*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
           14*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          +  & 
           8*p1p2**2*p1p4**2*p2p4  & 
          + 28*MIN2*p1p4**3*p2p4  & 
          -  & 
           8*MOU2*p1p4**3*p2p4  & 
          +  & 
           8*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          -  & 
           24*p1p2*p1p4**3*p2p4  & 
          - 8*p1p4**4*p2p4  & 
          -  & 
           20*MIN2**3*p2p4**2  & 
          + 12*MIN2**2*MOU2*p2p4**2  & 
          +  & 
           16*MIN2**2.5*np4*p2p4**2  & 
          +  & 
           32*MIN2**2*p1p2*p2p4**2  & 
          -  & 
           16*MIN2**1.5*np4*p1p2*p2p4**2  & 
          +  & 
           27*MIN2**2*p1p4*p2p4**2  & 
          -  & 
           15*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           6*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          -  & 
           20*MIN2*p1p2*p1p4*p2p4**2  & 
          -  & 
           4*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          -  & 
           38*MIN2*p1p4**2*p2p4**2  & 
          +  & 
           6*MOU2*p1p4**2*p2p4**2  & 
          -  & 
           16*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          +  & 
           8*p1p2*p1p4**2*p2p4**2  & 
          + 16*p1p4**3*p2p4**2  & 
          -  & 
           16*MIN2**2*p2p4**3  & 
          + 20*MIN2*p1p4*p2p4**3  & 
          +  & 
           8*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          -  & 
           8*p1p4**2*p2p4**3  & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            ( & 
          -4*MIN2**2*p1p4  & 
          + 4*MIN2*MOU2*p1p4  & 
          +  & 
              11*MIN2*p1p2*p1p4  & 
          - 6*MOU2*p1p2*p1p4  & 
          -  & 
              8*p1p2**2*p1p4  & 
          + MIN2*p1p4**2  & 
          +  & 
              2*MOU2*p1p4**2  & 
          + p1p2*p1p4**2  & 
          - p1p4**3  & 
          -  & 
              3*MIN2**2*p2p4  & 
          + 3*MIN2*MOU2*p2p4  & 
          +  & 
              4*MIN2*p1p2*p2p4  & 
          - 7*MIN2*p1p4*p2p4  & 
          +  & 
              8*p1p2*p1p4*p2p4  & 
          + p1p4**2*p2p4  & 
          -  & 
              4*MIN2*p2p4**2)  & 
          -  & 
           sqrt(MIN2)*np2* & 
            ( & 
          -12*MIN2**2*p1p2*p1p4  & 
          +  & 
              12*MIN2*MOU2*p1p2*p1p4  & 
          +  & 
              16*MIN2*p1p2**2*p1p4  & 
          + 12*MIN2**2*p1p4**2  & 
          -  & 
              12*MIN2*MOU2*p1p4**2  & 
          - 12*MOU2*p1p2*p1p4**2  & 
          -  & 
              16*p1p2**2*p1p4**2  & 
          - 12*MIN2*p1p4**3  & 
          +  & 
              8*MOU2*p1p4**3  & 
          + 6*p1p2*p1p4**3  & 
          + 2*p1p4**4  & 
          +  & 
              12*MIN2**3*p2p4  & 
          - 12*MIN2**2*MOU2*p2p4  & 
          -  & 
              16*MIN2**2*p1p2*p2p4  & 
          - 22*MIN2**2*p1p4*p2p4  & 
          +  & 
              18*MIN2*MOU2*p1p4*p2p4  & 
          +  & 
              8*MIN2*p1p2*p1p4*p2p4  & 
          + 25*MIN2*p1p4**2*p2p4  & 
          -  & 
              3*MOU2*p1p4**2*p2p4  & 
          + 12*p1p2*p1p4**2*p2p4  & 
          -  & 
              10*p1p4**3*p2p4  & 
          + 16*MIN2**2*p2p4**2  & 
          -  & 
              24*MIN2*p1p4*p2p4**2  & 
          + 4*p1p4**2*p2p4**2))* & 
         Ccache(5))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (2*EL**4*GF**2*(8*MIN2**2*p1p2*p1p4**2  & 
          -  & 
           8*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
           30*MIN2**1.5*np2*p1p2*p1p4**2  & 
          -  & 
           30*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          -  & 
           40*MIN2*p1p2**2*p1p4**2  & 
          +  & 
           24*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           40*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          +  & 
           32*p1p2**3*p1p4**2  & 
          - 22*MIN2**2*p1p4**3  & 
          +  & 
           22*MIN2*MOU2*p1p4**3  & 
          - 30*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           30*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          +  & 
           76*MIN2*p1p2*p1p4**3  & 
          - 32*MOU2*p1p2*p1p4**3  & 
          +  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          -  & 
           32*p1p2**2*p1p4**3  & 
          + 8*MIN2*p1p4**4  & 
          -  & 
           8*MOU2*p1p4**4  & 
          + 8*sqrt(MIN2)*np2*p1p4**4  & 
          -  & 
           32*p1p2*p1p4**4  & 
          - 8*MIN2**3*p1p4*p2p4  & 
          +  & 
           8*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           30*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           30*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          +  & 
           40*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
           24*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           40*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          -  & 
           32*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           22*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           22*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
           26*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           12*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          -  & 
           44*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
           24*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           32*p1p2**2*p1p4**2*p2p4  & 
          - 76*MIN2*p1p4**3*p2p4  & 
          +  & 
           32*MOU2*p1p4**3*p2p4  & 
          -  & 
           48*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          +  & 
           96*p1p2*p1p4**3*p2p4  & 
          + 32*p1p4**4*p2p4  & 
          -  & 
           40*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           30*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           40*MIN2**1.5*np2*p1p4*p2p4**2  & 
          +  & 
           72*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
           76*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           24*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           16*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          -  & 
           32*p1p2*p1p4**2*p2p4**2  & 
          - 64*p1p4**3*p2p4**2  & 
          -  & 
           40*MIN2*p1p4*p2p4**3  & 
          + 32*p1p4**2*p2p4**3  & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (6*MIN2**2*p1p4  & 
          - 6*MIN2*MOU2*p1p4  & 
          -  & 
              4*MIN2*p1p2*p1p4  & 
          - 6*MOU2*p1p2*p1p4  & 
          -  & 
              8*p1p2**2*p1p4  & 
          - 8*MIN2*p1p4**2  & 
          +  & 
              6*MOU2*p1p4**2  & 
          + 12*p1p2*p1p4**2  & 
          - 4*p1p4**3  & 
          -  & 
              6*MIN2**2*p2p4  & 
          + 6*MIN2*MOU2*p2p4  & 
          +  & 
              8*MIN2*p1p2*p2p4  & 
          + 8*MIN2*p1p4*p2p4  & 
          +  & 
              8*p1p2*p1p4*p2p4  & 
          - 4*p1p4**2*p2p4  & 
          -  & 
              8*MIN2*p2p4**2)  & 
          +  & 
           sqrt(MIN2)*np4* & 
            ( & 
          -6*MIN2**3*p1p4  & 
          + 12*MIN2**2*MOU2*p1p4  & 
          -  & 
              6*MIN2*MOU2**2*p1p4  & 
          - 20*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
              22*MIN2*p1p2**2*p1p4  & 
          + 6*MOU2*p1p2**2*p1p4  & 
          +  & 
              8*p1p2**3*p1p4  & 
          - 10*MIN2*MOU2*p1p4**2  & 
          -  & 
              22*MIN2*p1p2*p1p4**2  & 
          + 2*MOU2*p1p2*p1p4**2  & 
          -  & 
              8*MIN2*p1p4**3  & 
          + 8*MOU2*p1p4**3  & 
          +  & 
              24*p1p2*p1p4**3  & 
          + 16*MIN2*MOU2*p1p4*p2p4  & 
          +  & 
              26*MIN2*p1p2*p1p4*p2p4  & 
          +  & 
              12*MOU2*p1p2*p1p4*p2p4  & 
          + 8*p1p2**2*p1p4*p2p4  & 
          +  & 
              44*MIN2*p1p4**2*p2p4  & 
          - 32*MOU2*p1p4**2*p2p4  & 
          -  & 
              48*p1p2*p1p4**2*p2p4  & 
          - 32*p1p4**3*p2p4  & 
          -  & 
              18*MIN2*MOU2*p2p4**2  & 
          - 24*MIN2*p1p2*p2p4**2  & 
          -  & 
              52*MIN2*p1p4*p2p4**2  & 
          + 24*MOU2*p1p4*p2p4**2  & 
          +  & 
              16*p1p2*p1p4*p2p4**2  & 
          + 64*p1p4**2*p2p4**2  & 
          +  & 
              24*MIN2*p2p4**3  & 
          - 32*p1p4*p2p4**3  & 
          +  & 
              MIN2**2*(20*p1p2*p1p4  & 
          + 10*p1p4**2  & 
          -  & 
                 16*p1p4*p2p4  & 
          + 18*p2p4**2)))* & 
         Ccache(6))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (2*EL**4*GF**2*( & 
          -(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
              ( & 
          -4*MIN2**2*p1p4  & 
          + 4*MIN2*MOU2*p1p4  & 
          +  & 
                11*MIN2*p1p2*p1p4  & 
          - 6*MOU2*p1p2*p1p4  & 
          -  & 
                8*p1p2**2*p1p4  & 
          + MIN2*p1p4**2  & 
          +  & 
                2*MOU2*p1p4**2  & 
          + 2*p1p2*p1p4**2  & 
          -  & 
                2*p1p4**3  & 
          - 3*MIN2**2*p2p4  & 
          +  & 
                3*MIN2*MOU2*p2p4  & 
          + 4*MIN2*p1p2*p2p4  & 
          -  & 
                7*MIN2*p1p4*p2p4  & 
          + 8*p1p2*p1p4*p2p4  & 
          +  & 
                2*p1p4**2*p2p4  & 
          - 4*MIN2*p2p4**2))  & 
          -  & 
           2*( & 
          -2*MIN2**3*p1p2*p1p4  & 
          +  & 
              2*MIN2**2*MOU2*p1p2*p1p4  & 
          +  & 
              10*MIN2**2*p1p2**2*p1p4  & 
          -  & 
              6*MIN2*MOU2*p1p2**2*p1p4  & 
          -  & 
              8*MIN2*p1p2**3*p1p4  & 
          + 2*MIN2**3*p1p4**2  & 
          -  & 
              2*MIN2**2*MOU2*p1p4**2  & 
          -  & 
              9*MIN2**2*p1p2*p1p4**2  & 
          +  & 
              5*MIN2*MOU2*p1p2*p1p4**2  & 
          -  & 
              4*MIN2*p1p2**2*p1p4**2  & 
          +  & 
              6*MOU2*p1p2**2*p1p4**2  & 
          + 8*p1p2**3*p1p4**2  & 
          -  & 
              3*MIN2**2*p1p4**3  & 
          + 3*MIN2*MOU2*p1p4**3  & 
          +  & 
              18*MIN2*p1p2*p1p4**3  & 
          - 8*MOU2*p1p2*p1p4**3  & 
          -  & 
              8*p1p2**2*p1p4**3  & 
          + 2*MIN2*p1p4**4  & 
          -  & 
              2*MOU2*p1p4**4  & 
          - 8*p1p2*p1p4**4  & 
          +  & 
              2*MIN2**4*p2p4  & 
          - 2*MIN2**3*MOU2*p2p4  & 
          -  & 
              10*MIN2**3*p1p2*p2p4  & 
          +  & 
              6*MIN2**2*MOU2*p1p2*p2p4  & 
          +  & 
              8*MIN2**2*p1p2**2*p2p4  & 
          - 2*MIN2**3*p1p4*p2p4  & 
          +  & 
              2*MIN2**2*MOU2*p1p4*p2p4  & 
          +  & 
              7*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
              3*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
              4*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
              12*MIN2**2*p1p4**2*p2p4  & 
          -  & 
              8*MIN2*MOU2*p1p4**2*p2p4  & 
          -  & 
              18*MIN2*p1p2*p1p4**2*p2p4  & 
          -  & 
              8*p1p2**2*p1p4**2*p2p4  & 
          -  & 
              20*MIN2*p1p4**3*p2p4  & 
          + 8*MOU2*p1p4**3*p2p4  & 
          +  & 
              24*p1p2*p1p4**3*p2p4  & 
          + 8*p1p4**4*p2p4  & 
          +  & 
              10*MIN2**3*p2p4**2  & 
          - 6*MIN2**2*MOU2*p2p4**2  & 
          -  & 
              16*MIN2**2*p1p2*p2p4**2  & 
          -  & 
              17*MIN2**2*p1p4*p2p4**2  & 
          +  & 
              9*MIN2*MOU2*p1p4*p2p4**2  & 
          +  & 
              16*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
              22*MIN2*p1p4**2*p2p4**2  & 
          -  & 
              6*MOU2*p1p4**2*p2p4**2  & 
          -  & 
              8*p1p2*p1p4**2*p2p4**2  & 
          - 16*p1p4**3*p2p4**2  & 
          +  & 
              8*MIN2**2*p2p4**3  & 
          - 12*MIN2*p1p4*p2p4**3  & 
          +  & 
              8*p1p4**2*p2p4**3  & 
          +  & 
              sqrt(MIN2)*np2* & 
               ( & 
          -6*MIN2**2*p1p2*p1p4  & 
          +  & 
                 6*MIN2*MOU2*p1p2*p1p4  & 
          +  & 
                 8*MIN2*p1p2**2*p1p4  & 
          + 6*MIN2**2*p1p4**2  & 
          -  & 
                 6*MIN2*MOU2*p1p4**2  & 
          + 3*MIN2*p1p2*p1p4**2  & 
          -  & 
                 9*MOU2*p1p2*p1p4**2  & 
          - 12*p1p2**2*p1p4**2  & 
          -  & 
                 9*MIN2*p1p4**3  & 
          + 7*MOU2*p1p4**3  & 
          +  & 
                 6*p1p2*p1p4**3  & 
          + 2*p1p4**4  & 
          +  & 
                 6*MIN2**3*p2p4  & 
          - 6*MIN2**2*MOU2*p2p4  & 
          -  & 
                 8*MIN2**2*p1p2*p2p4  & 
          -  & 
                 14*MIN2**2*p1p4*p2p4  & 
          +  & 
                 12*MIN2*MOU2*p1p4*p2p4  & 
          +  & 
                 8*MIN2*p1p2*p1p4*p2p4  & 
          +  & 
                 15*MIN2*p1p4**2*p2p4  & 
          -  & 
                 3*MOU2*p1p4**2*p2p4  & 
          + 8*p1p2*p1p4**2*p2p4  & 
          -  & 
                 10*p1p4**3*p2p4  & 
          + 8*MIN2**2*p2p4**2  & 
          -  & 
                 16*MIN2*p1p4*p2p4**2  & 
          + 4*p1p4**2*p2p4**2)  & 
          +  & 
              sqrt(MIN2)*np4* & 
               ( & 
          -3*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
                 7*MIN2*p1p2**2*p1p4  & 
          + 3*MOU2*p1p2**2*p1p4  & 
          +  & 
                 4*p1p2**3*p1p4  & 
          + MIN2*MOU2*p1p4**2  & 
          -  & 
                 MIN2*p1p2*p1p4**2  & 
          + MOU2*p1p2*p1p4**2  & 
          +  & 
                 2*p1p2**2*p1p4**2  & 
          - 2*MIN2*p1p4**3  & 
          +  & 
                 2*MOU2*p1p4**3  & 
          + 6*p1p2*p1p4**3  & 
          -  & 
                 6*MIN2**3*p2p4  & 
          + 6*MIN2**2*MOU2*p2p4  & 
          -  & 
                 6*MIN2*MOU2*p1p2*p2p4  & 
          -  & 
                 8*MIN2*p1p2**2*p2p4  & 
          -  & 
                 5*MIN2*MOU2*p1p4*p2p4  & 
          -  & 
                 7*MIN2*p1p2*p1p4*p2p4  & 
          +  & 
                 3*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
                 6*MIN2*p1p4**2*p2p4  & 
          - 8*MOU2*p1p4**2*p2p4  & 
          -  & 
                 14*p1p2*p1p4**2*p2p4  & 
          - 8*p1p4**3*p2p4  & 
          +  & 
                 8*MIN2*p1p2*p2p4**2  & 
          + 6*MOU2*p1p4*p2p4**2  & 
          +  & 
                 4*p1p2*p1p4*p2p4**2  & 
          + 16*p1p4**2*p2p4**2  & 
          -  & 
                 8*p1p4*p2p4**3  & 
          +  & 
                 MIN2**2* & 
                  (3*p1p2*p1p4  & 
          - p1p4**2  & 
          + 14*p1p2*p2p4  & 
          +  & 
                    7*p1p4*p2p4  & 
          - 8*p2p4**2))))* & 
         Ccache(7))/ & 
       (3.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T3TRIAC
                          !!!!!!!!!!!!!!!!!!!!!!
