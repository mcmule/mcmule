
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T1BOXAC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T1BOXAC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Ccache(28)
    complex(kind=prec) :: Dcache(5)

!
      real(kind=prec) T1BOXAC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb=  & 
          -1._prec
!

!    
      call setlambda(  & 
          -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec*10._prec**(  & 
          -10._prec))
!     
    Ccache(1) = C0i(cc0,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(2) = C0i(cc0,MIN2,MIN2-2*p1p4,0._prec,MIN2,0._prec,MIN2)
    Ccache(3) = C0i(cc0,-MIN2+MOU2+2*p1p2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,MIN2,MOU2,MIN2)
    Ccache(4) = C0i(cc0,MIN2-2*p1p4,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(5) = C0i(cc00,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(6) = C0i(cc00,MIN2,MIN2-2*p1p4,0._prec,MIN2,0._prec,MIN2)
    Ccache(7) = C0i(cc00,-MIN2+MOU2+2*p1p2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,MIN2,MOU2,MIN2)
    Ccache(8) = C0i(cc00,MIN2-2*p1p4,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(9) = C0i(cc1,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(10) = C0i(cc1,MIN2,MIN2-2*p1p4,0._prec,MIN2,0._prec,MIN2)
    Ccache(11) = C0i(cc1,-MIN2+MOU2+2*p1p2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,MIN2,MOU2,MIN2)
    Ccache(12) = C0i(cc1,MIN2-2*p1p4,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(13) = C0i(cc11,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(14) = C0i(cc11,MIN2,MIN2-2*p1p4,0._prec,MIN2,0._prec,MIN2)
    Ccache(15) = C0i(cc11,-MIN2+MOU2+2*p1p2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,MIN2,MOU2,MIN2)
    Ccache(16) = C0i(cc11,MIN2-2*p1p4,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(17) = C0i(cc12,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(18) = C0i(cc12,MIN2,MIN2-2*p1p4,0._prec,MIN2,0._prec,MIN2)
    Ccache(19) = C0i(cc12,-MIN2+MOU2+2*p1p2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,MIN2,MOU2,MIN2)
    Ccache(20) = C0i(cc12,MIN2-2*p1p4,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(21) = C0i(cc2,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(22) = C0i(cc2,MIN2,MIN2-2*p1p4,0._prec,MIN2,0._prec,MIN2)
    Ccache(23) = C0i(cc2,-MIN2+MOU2+2*p1p2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,MIN2,MOU2,MIN2)
    Ccache(24) = C0i(cc2,MIN2-2*p1p4,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(25) = C0i(cc22,MIN2,MOU2,-MIN2+MOU2+2*p1p2,MIN2,0._prec,MOU2)
    Ccache(26) = C0i(cc22,MIN2,MIN2-2*p1p4,0._prec,MIN2,0._prec,MIN2)
    Ccache(27) = C0i(cc22,-MIN2+MOU2+2*p1p2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,MIN2,MOU2,MIN2)
    Ccache(28) = C0i(cc22,MIN2-2*p1p4,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Dcache(1) = D0i(dd0,MIN2,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,-MIN2+MOU2+2*p1p2,MIN2-2*p1p4,MIN2,0._prec,MOU2,MIN2)
    Dcache(2) = D0i(dd00,MIN2,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,-MIN2+MOU2+2*p1p2,MIN2-2*p1p4,MIN2,0._prec,MOU2,MIN2)
    Dcache(3) = D0i(dd1,MIN2,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,-MIN2+MOU2+2*p1p2,MIN2-2*p1p4,MIN2,0._prec,MOU2,MIN2)
    Dcache(4) = D0i(dd2,MIN2,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,-MIN2+MOU2+2*p1p2,MIN2-2*p1p4,MIN2,0._prec,MOU2,MIN2)
    Dcache(5) = D0i(dd3,MIN2,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,0._prec,-MIN2+MOU2+2*p1p2,MIN2-2*p1p4,MIN2,0._prec,MOU2,MIN2)

!
      T1BOXAC=real((EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (23*MIN2*p1p4   & 
          - 31*MOU2*p1p4   & 
          - 57*p1p2*p1p4   & 
          +  & 
              3*p1p4**2   & 
          + 7*MOU2*p2p4   & 
          + 19*p1p2*p2p4   & 
          +  & 
              36*p1p4*p2p4   & 
          - 14*p2p4**2)   & 
          -  & 
           2*(  & 
          -3*MIN2**3*p1p4   & 
          + 10*MIN2**2*MOU2*p1p4   & 
          -  & 
              7*MIN2*MOU2**2*p1p4   & 
          - 10*MIN2**2.5*np4*p1p4   & 
          +  & 
              10*MIN2**1.5*MOU2*np4*p1p4   & 
          +  & 
              9*MIN2**2*p1p2*p1p4   & 
          - 20*MIN2*MOU2*p1p2*p1p4   & 
          +  & 
              3*MOU2**2*p1p2*p1p4   & 
          +  & 
              26*MIN2**1.5*np4*p1p2*p1p4   & 
          -  & 
              10*sqrt(MIN2)*MOU2*np4*p1p2*p1p4   & 
          -  & 
              14*MIN2*p1p2**2*p1p4   & 
          + 10*MOU2*p1p2**2*p1p4   & 
          -  & 
              16*sqrt(MIN2)*np4*p1p2**2*p1p4   & 
          +  & 
              8*p1p2**3*p1p4   & 
          + 10*MIN2**2*p1p4**2   & 
          -  & 
              10*MIN2*MOU2*p1p4**2   & 
          - 20*MIN2*p1p2*p1p4**2   & 
          +  & 
              4*MOU2*p1p2*p1p4**2   & 
          + 8*p1p2**2*p1p4**2   & 
          -  & 
              6*MIN2**2*MOU2*p2p4   & 
          + 6*MIN2*MOU2**2*p2p4   & 
          +  & 
              10*MIN2**2.5*np4*p2p4   & 
          -  & 
              10*MIN2**1.5*MOU2*np4*p2p4   & 
          -  & 
              4*MIN2**2*p1p2*p2p4   & 
          + 16*MIN2*MOU2*p1p2*p2p4   & 
          -  & 
              26*MIN2**1.5*np4*p1p2*p2p4   & 
          +  & 
              4*sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
          +  & 
              12*MIN2*p1p2**2*p2p4   & 
          - 6*MOU2*p1p2**2*p2p4   & 
          +  & 
              16*sqrt(MIN2)*np4*p1p2**2*p2p4   & 
          -  & 
              8*p1p2**3*p2p4   & 
          - 10*MIN2**2*p1p4*p2p4   & 
          +  & 
              22*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
              16*MIN2**1.5*np4*p1p4*p2p4   & 
          +  & 
              40*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              12*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
              16*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
          -  & 
              24*p1p2**2*p1p4*p2p4   & 
          + 16*MIN2*p1p4**2*p2p4   & 
          -  & 
              8*p1p2*p1p4**2*p2p4   & 
          + 2*MIN2**2*p2p4**2   & 
          -  & 
              16*MIN2*MOU2*p2p4**2   & 
          +  & 
              16*MIN2**1.5*np4*p2p4**2   & 
          -  & 
              22*MIN2*p1p2*p2p4**2   & 
          + 6*MOU2*p1p2*p2p4**2   & 
          -  & 
              16*sqrt(MIN2)*np4*p1p2*p2p4**2   & 
          +  & 
              16*p1p2**2*p2p4**2   & 
          - 32*MIN2*p1p4*p2p4**2   & 
          +  & 
              16*p1p2*p1p4*p2p4**2   & 
          + 16*MIN2*p2p4**3   & 
          -  & 
              8*p1p2*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (  & 
          -(MIN2**2*p1p4)   & 
          + 4*MIN2*MOU2*p1p4   & 
          -  & 
                 3*MOU2**2*p1p4   & 
          + 10*MIN2*p1p2*p1p4   & 
          -  & 
                 10*MOU2*p1p2*p1p4   & 
          - 8*p1p2**2*p1p4   & 
          -  & 
                 6*MIN2*p1p4**2   & 
          + 6*MOU2*p1p4**2   & 
          +  & 
                 8*p1p2*p1p4**2   & 
          + 4*MIN2**2*p2p4   & 
          -  & 
                 4*MIN2*MOU2*p2p4   & 
          - 12*MIN2*p1p2*p2p4   & 
          +  & 
                 6*MOU2*p1p2*p2p4   & 
          + 8*p1p2**2*p2p4   & 
          -  & 
                 14*MIN2*p1p4*p2p4   & 
          + 8*MOU2*p1p4*p2p4   & 
          +  & 
                 8*p1p2*p1p4*p2p4   & 
          - 8*p1p4**2*p2p4   & 
          +  & 
                 18*MIN2*p2p4**2   & 
          - 6*MOU2*p2p4**2   & 
          -  & 
                 16*p1p2*p2p4**2   & 
          + 8*p2p4**3)))* & 
         Ccache(1) & 
         )/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -2*MIN2*p1p4   & 
          + 10*MOU2*p1p4   & 
          + 9*p1p2*p1p4   & 
          -  & 
              5*p1p4**2   & 
          - 11*MIN2*p2p4   & 
          + 4*MOU2*p2p4   & 
          +  & 
              10*p1p2*p2p4   & 
          - p1p4*p2p4   & 
          - 9*p2p4**2)   & 
          -  & 
           2*(2*MIN2**3*p1p4   & 
          - 8*MIN2**2*MOU2*p1p4   & 
          +  & 
              6*MIN2*MOU2**2*p1p4   & 
          - 2*MIN2**2.5*np2*p1p4   & 
          +  & 
              2*MIN2**1.5*MOU2*np2*p1p4   & 
          +  & 
              8*MIN2*MOU2*p1p2*p1p4   & 
          +  & 
              4*MIN2**1.5*np2*p1p2*p1p4   & 
          -  & 
              12*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          -  & 
              4*MIN2*p1p2**2*p1p4   & 
          + 12*MOU2*p1p2**2*p1p4   & 
          -  & 
              16*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          +  & 
              16*p1p2**3*p1p4   & 
          - 3*MIN2**2*p1p4**2   & 
          +  & 
              3*MOU2**2*p1p4**2   & 
          + 8*MIN2**1.5*np2*p1p4**2   & 
          -  & 
              8*sqrt(MIN2)*MOU2*np2*p1p4**2   & 
          -  & 
              4*MIN2*p1p2*p1p4**2   & 
          + 12*MOU2*p1p2*p1p4**2   & 
          -  & 
              16*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          +  & 
              16*p1p2**2*p1p4**2   & 
          - 4*MIN2*p1p4**3   & 
          +  & 
              4*MOU2*p1p4**3   & 
          + 8*p1p2*p1p4**3   & 
          -  & 
              4*MIN2**2*p1p4*p2p4   & 
          - 8*MIN2*MOU2*p1p4*p2p4   & 
          +  & 
              6*sqrt(MIN2)*MOU2*np2*p1p4*p2p4   & 
          +  & 
              4*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              12*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
              24*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          -  & 
              32*p1p2**2*p1p4*p2p4   & 
          + 4*MIN2*p1p4**2*p2p4   & 
          -  & 
              12*MOU2*p1p4**2*p2p4   & 
          +  & 
              16*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          -  & 
              32*p1p2*p1p4**2*p2p4   & 
          - 8*p1p4**3*p2p4   & 
          +  & 
              2*MIN2**2*p2p4**2   & 
          - 2*MIN2*p1p4*p2p4**2   & 
          +  & 
              6*MOU2*p1p4*p2p4**2   & 
          -  & 
              8*sqrt(MIN2)*np2*p1p4*p2p4**2   & 
          +  & 
              24*p1p2*p1p4*p2p4**2   & 
          + 16*p1p4**2*p2p4**2   & 
          -  & 
              8*p1p4*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np4* & 
               (3*MIN2**2*p1p4   & 
          - 3*MOU2**2*p1p4   & 
          -  & 
                 4*MIN2*p1p2*p1p4   & 
          - 4*MOU2*p1p2*p1p4   & 
          +  & 
                 4*MIN2*p1p4**2   & 
          - 4*MOU2*p1p4**2   & 
          -  & 
                 8*p1p2*p1p4**2   & 
          - 4*MIN2*p1p2*p2p4   & 
          +  & 
                 6*MOU2*p1p2*p2p4   & 
          + 8*p1p2**2*p2p4   & 
          -  & 
                 4*MIN2*p1p4*p2p4   & 
          + 12*MOU2*p1p4*p2p4   & 
          +  & 
                 16*p1p2*p1p4*p2p4   & 
          + 8*p1p4**2*p2p4   & 
          +  & 
                 6*MIN2*p2p4**2   & 
          - 6*MOU2*p2p4**2   & 
          -  & 
                 16*p1p2*p2p4**2   & 
          - 16*p1p4*p2p4**2   & 
          +  & 
                 8*p2p4**3)))* & 
         Ccache(2))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -4*MIN2*p1p4   & 
          + 4*MOU2*p1p4   & 
          + p1p2*p1p4   & 
          +  & 
              7*p1p4**2   & 
          + 14*MIN2*p2p4   & 
          - 14*MOU2*p2p4   & 
          -  & 
              11*p1p2*p2p4   & 
          - 25*p1p4*p2p4   & 
          + 17*p2p4**2)   & 
          +  & 
           2*(6*MIN2**2*p1p2*p1p4   & 
          - 12*MIN2*MOU2*p1p2*p1p4   & 
          +  & 
              6*MOU2**2*p1p2*p1p4   & 
          +  & 
              10*MIN2**1.5*np2*p1p2*p1p4   & 
          -  & 
              10*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          -  & 
              24*MIN2*p1p2**2*p1p4   & 
          + 24*MOU2*p1p2**2*p1p4   & 
          -  & 
              24*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          +  & 
              32*p1p2**3*p1p4   & 
          + 8*MIN2**2*p1p4**2   & 
          -  & 
              8*MIN2*MOU2*p1p4**2   & 
          - 24*MIN2*p1p2*p1p4**2   & 
          +  & 
              8*MOU2*p1p2*p1p4**2   & 
          + 16*p1p2**2*p1p4**2   & 
          -  & 
              4*MIN2**3*p2p4   & 
          + 4*MIN2**2*MOU2*p2p4   & 
          -  & 
              12*MIN2**2.5*np2*p2p4   & 
          +  & 
              12*MIN2**1.5*MOU2*np2*p2p4   & 
          +  & 
              24*MIN2**2*p1p2*p2p4   & 
          -  & 
              16*MIN2*MOU2*p1p2*p2p4   & 
          +  & 
              28*MIN2**1.5*np2*p1p2*p2p4   & 
          -  & 
              12*sqrt(MIN2)*MOU2*np2*p1p2*p2p4   & 
          -  & 
              36*MIN2*p1p2**2*p2p4   & 
          + 12*MOU2*p1p2**2*p2p4   & 
          -  & 
              16*sqrt(MIN2)*np2*p1p2**2*p2p4   & 
          +  & 
              16*p1p2**3*p2p4   & 
          - 7*MIN2**2*p1p4*p2p4   & 
          +  & 
              4*MIN2*MOU2*p1p4*p2p4   & 
          + 3*MOU2**2*p1p4*p2p4   & 
          +  & 
              8*MIN2**1.5*np2*p1p4*p2p4   & 
          -  & 
              8*sqrt(MIN2)*MOU2*np2*p1p4*p2p4   & 
          +  & 
              20*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              4*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
              8*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          -  & 
              32*p1p2**2*p1p4*p2p4   & 
          + 12*MIN2*p1p4**2*p2p4   & 
          +  & 
              4*MOU2*p1p4**2*p2p4   & 
          - 8*p1p2*p1p4**2*p2p4   & 
          -  & 
              14*MIN2**2*p2p4**2   & 
          + 6*MIN2*MOU2*p2p4**2   & 
          -  & 
              22*MIN2**1.5*np2*p2p4**2   & 
          +  & 
              6*sqrt(MIN2)*MOU2*np2*p2p4**2   & 
          +  & 
              44*MIN2*p1p2*p2p4**2   & 
          - 12*MOU2*p1p2*p2p4**2   & 
          +  & 
              24*sqrt(MIN2)*np2*p1p2*p2p4**2   & 
          -  & 
              32*p1p2**2*p2p4**2   & 
          - 4*MIN2*p1p4*p2p4**2   & 
          -  & 
              12*MOU2*p1p4*p2p4**2   & 
          +  & 
              16*sqrt(MIN2)*np2*p1p4*p2p4**2   & 
          -  & 
              16*p1p2*p1p4*p2p4**2   & 
          - 8*p1p4**2*p2p4**2   & 
          -  & 
              10*MIN2*p2p4**3   & 
          + 6*MOU2*p2p4**3   & 
          -  & 
              8*sqrt(MIN2)*np2*p2p4**3   & 
          + 24*p1p2*p2p4**3   & 
          +  & 
              16*p1p4*p2p4**3   & 
          - 8*p2p4**4   & 
          +  & 
              sqrt(MIN2)*np4* & 
               (  & 
          -7*MIN2**2*p1p4   & 
          + 6*MIN2*MOU2*p1p4   & 
          +  & 
                 MOU2**2*p1p4   & 
          + 22*MIN2*p1p2*p1p4   & 
          -  & 
                 6*MOU2*p1p2*p1p4   & 
          - 16*p1p2**2*p1p4   & 
          +  & 
                 10*MIN2**2*p2p4   & 
          - 4*MIN2*MOU2*p2p4   & 
          -  & 
                 6*MOU2**2*p2p4   & 
          - 30*MIN2*p1p2*p2p4   & 
          +  & 
                 6*MOU2*p1p2*p2p4   & 
          + 24*p1p2**2*p2p4   & 
          -  & 
                 14*MIN2*p1p4*p2p4   & 
          - 2*MOU2*p1p4*p2p4   & 
          +  & 
                 16*p1p2*p1p4*p2p4   & 
          + 20*MIN2*p2p4**2   & 
          +  & 
                 4*MOU2*p2p4**2   & 
          - 24*p1p2*p2p4**2)))* & 
         Ccache(3))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -15*MIN2*p1p4   & 
          + 15*MOU2*p1p4   & 
          + 31*p1p2*p1p4   & 
          -  & 
              p1p4**2   & 
          - 5*MIN2*p2p4   & 
          + 5*MOU2*p2p4   & 
          -  & 
              2*p1p2*p2p4   & 
          - 20*p1p4*p2p4   & 
          + 6*p2p4**2)   & 
          -  & 
           2*(  & 
          -(MIN2**3*p1p4)   & 
          + 2*MIN2**2*MOU2*p1p4   & 
          -  & 
              MIN2*MOU2**2*p1p4   & 
          + 11*MIN2**2*p1p2*p1p4   & 
          -  & 
              20*MIN2*MOU2*p1p2*p1p4   & 
          + 9*MOU2**2*p1p2*p1p4   & 
          -  & 
              34*MIN2*p1p2**2*p1p4   & 
          + 22*MOU2*p1p2**2*p1p4   & 
          +  & 
              24*p1p2**3*p1p4   & 
          - MIN2**2*p1p4**2   & 
          +  & 
              6*MIN2*MOU2*p1p4**2   & 
          - 5*MOU2**2*p1p4**2   & 
          +  & 
              8*MIN2*p1p2*p1p4**2   & 
          - 16*MOU2*p1p2*p1p4**2   & 
          -  & 
              8*p1p2**2*p1p4**2   & 
          - 4*MIN2*p1p4**3   & 
          +  & 
              4*MOU2*p1p4**3   & 
          + 8*p1p2*p1p4**3   & 
          -  & 
              2*MIN2**3*p2p4   & 
          + 2*MIN2**2*MOU2*p2p4   & 
          +  & 
              14*MIN2**2*p1p2*p2p4   & 
          -  & 
              10*MIN2*MOU2*p1p2*p2p4   & 
          -  & 
              20*MIN2*p1p2**2*p2p4   & 
          + 6*MOU2*p1p2**2*p2p4   & 
          +  & 
              8*p1p2**3*p2p4   & 
          - 3*MIN2**2*p1p4*p2p4   & 
          +  & 
              6*MIN2*MOU2*p1p4*p2p4   & 
          - 3*MOU2**2*p1p4*p2p4   & 
          +  & 
              22*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              6*MOU2*p1p2*p1p4*p2p4   & 
          - 16*p1p2**2*p1p4*p2p4   & 
          +  & 
              8*MOU2*p1p4**2*p2p4   & 
          - 8*p1p4**3*p2p4   & 
          -  & 
              12*MIN2**2*p2p4**2   & 
          + 8*MIN2*MOU2*p2p4**2   & 
          +  & 
              34*MIN2*p1p2*p2p4**2   & 
          - 12*MOU2*p1p2*p2p4**2   & 
          -  & 
              24*p1p2**2*p2p4**2   & 
          + 2*MIN2*p1p4*p2p4**2   & 
          -  & 
              2*MOU2*p1p4*p2p4**2   & 
          - 16*p1p2*p1p4*p2p4**2   & 
          +  & 
              8*p1p4**2*p2p4**2   & 
          - 14*MIN2*p2p4**3   & 
          +  & 
              6*MOU2*p2p4**3   & 
          + 24*p1p2*p2p4**3   & 
          +  & 
              8*p1p4*p2p4**3   & 
          - 8*p2p4**4   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (  & 
          -3*MIN2**2*p1p4   & 
          + 6*MIN2*MOU2*p1p4   & 
          -  & 
                 3*MOU2**2*p1p4   & 
          + 16*MIN2*p1p2*p1p4   & 
          -  & 
                 8*MOU2*p1p2*p1p4   & 
          - 16*p1p2**2*p1p4   & 
          -  & 
                 10*MIN2**2*p2p4   & 
          + 10*MIN2*MOU2*p2p4   & 
          +  & 
                 20*MIN2*p1p2*p2p4   & 
          - 6*MOU2*p1p2*p2p4   & 
          -  & 
                 8*p1p2**2*p2p4   & 
          - 2*MIN2*p1p4*p2p4   & 
          -  & 
                 6*MOU2*p1p4*p2p4   & 
          + 8*p1p2*p1p4*p2p4   & 
          -  & 
                 20*MIN2*p2p4**2   & 
          + 6*MOU2*p2p4**2   & 
          +  & 
                 16*p1p2*p2p4**2   & 
          + 8*p1p4*p2p4**2   & 
          - 8*p2p4**3 & 
                 )   & 
          + sqrt(MIN2)*np4* & 
               (2*MIN2**2*p1p4   & 
          - 8*MIN2*MOU2*p1p4   & 
          +  & 
                 6*MOU2**2*p1p4   & 
          - 10*MIN2*p1p2*p1p4   & 
          +  & 
                 18*MOU2*p1p2*p1p4   & 
          + 8*p1p2**2*p1p4   & 
          +  & 
                 4*MIN2*p1p4**2   & 
          - 4*MOU2*p1p4**2   & 
          -  & 
                 8*p1p2*p1p4**2   & 
          + 6*MIN2**2*p2p4   & 
          -  & 
                 6*MIN2*MOU2*p2p4   & 
          - 14*MIN2*p1p2*p2p4   & 
          +  & 
                 6*MOU2*p1p2*p2p4   & 
          + 8*p1p2**2*p2p4   & 
          -  & 
                 2*MIN2*p1p4*p2p4   & 
          - 6*MOU2*p1p4*p2p4   & 
          +  & 
                 8*p1p2*p1p4*p2p4   & 
          + 8*p1p4**2*p2p4   & 
          +  & 
                 14*MIN2*p2p4**2   & 
          - 6*MOU2*p2p4**2   & 
          -  & 
                 16*p1p2*p2p4**2   & 
          - 16*p1p4*p2p4**2   & 
          +  & 
                 8*p2p4**3)))* & 
         Ccache(4))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (8*EL**4*GF**2*(6*MIN2**2*p1p4   & 
          - 6*MIN2*MOU2*p1p4   & 
          +  & 
           MIN2**1.5*np4*p1p4   & 
          - sqrt(MIN2)*MOU2*np4*p1p4   & 
          -  & 
           14*MIN2*p1p2*p1p4   & 
          + 2*MOU2*p1p2*p1p4   & 
          -  & 
           sqrt(MIN2)*np4*p1p2*p1p4   & 
          + 8*p1p2**2*p1p4   & 
          -  & 
           MIN2*p1p4**2   & 
          + MOU2*p1p4**2   & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4   & 
          - 2*p2p4)   & 
          -  & 
           3*MIN2**2*p2p4   & 
          + 3*MIN2*MOU2*p2p4   & 
          -  & 
           2*MIN2**1.5*np4*p2p4   & 
          + sqrt(MIN2)*MOU2*np4*p2p4   & 
          +  & 
           7*MIN2*p1p2*p2p4   & 
          - MOU2*p1p2*p2p4   & 
          +  & 
           2*sqrt(MIN2)*np4*p1p2*p2p4   & 
          - 4*p1p2**2*p2p4   & 
          +  & 
           9*MIN2*p1p4*p2p4   & 
          - MOU2*p1p4*p2p4   & 
          -  & 
           8*p1p2*p1p4*p2p4   & 
          - 4*MIN2*p2p4**2   & 
          +  & 
           4*p1p2*p2p4**2   & 
          +  & 
           sqrt(MIN2)*np2* & 
            (2*MIN2*p1p4   & 
          - 2*MOU2*p1p4   & 
          - 8*p1p2*p1p4   & 
          +  & 
              p1p4**2   & 
          + MOU2*p2p4   & 
          + 4*p1p2*p2p4   & 
          +  & 
              6*p1p4*p2p4   & 
          - 4*p2p4**2))* & 
         Ccache(5))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (4*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)*(3*p1p4   & 
          - 4*p2p4)   & 
          +  & 
           2*(3*MIN2**2*p1p4   & 
          - 3*MIN2*MOU2*p1p4   & 
          -  & 
              7*MIN2*p1p2*p1p4   & 
          + MOU2*p1p2*p1p4   & 
          +  & 
              4*p1p2**2*p1p4   & 
          + MIN2**2*p2p4   & 
          -  & 
              MIN2*MOU2*p2p4   & 
          - MIN2**1.5*np4*p2p4   & 
          -  & 
              3*MIN2*p1p2*p2p4   & 
          + sqrt(MIN2)*np4*p1p2*p2p4   & 
          +  & 
              4*MIN2*p1p4*p2p4   & 
          - 4*p1p2*p1p4*p2p4   & 
          +  & 
              4*MIN2*p2p4**2   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (  & 
          -(MOU2*p1p4)   & 
          - 4*p1p2*p1p4   & 
          + 3*p1p4*p2p4   & 
          +  & 
                 MIN2*(p1p4   & 
          + p2p4))))* & 
         Ccache(6))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (16*EL**4*GF**2*MIN2*p2p4* & 
         (MIN2   & 
          - MOU2   & 
          - 2*p1p2   & 
          + 2*p2p4)* & 
         Ccache(7))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4   & 
          +  & 
           2*(  & 
          -3*MIN2**2*p1p4   & 
          + 3*MIN2*MOU2*p1p4   & 
          +  & 
              7*MIN2*p1p2*p1p4   & 
          - MOU2*p1p2*p1p4   & 
          -  & 
              4*p1p2**2*p1p4   & 
          + MIN2*p1p4**2   & 
          - MOU2*p1p4**2   & 
          +  & 
              sqrt(MIN2)*np4*(  & 
          -MIN2   & 
          + MOU2   & 
          + p1p2)* & 
               (p1p4   & 
          - p2p4)   & 
          + 2*MIN2**2*p2p4   & 
          -  & 
              2*MIN2*MOU2*p2p4   & 
          - 6*MIN2*p1p2*p2p4   & 
          +  & 
              MOU2*p1p2*p2p4   & 
          + 4*p1p2**2*p2p4   & 
          -  & 
              5*MIN2*p1p4*p2p4   & 
          + MOU2*p1p4*p2p4   & 
          +  & 
              4*p1p2*p1p4*p2p4   & 
          + 4*MIN2*p2p4**2   & 
          -  & 
              4*p1p2*p2p4**2   & 
          -  & 
              sqrt(MIN2)*np2*(p1p4   & 
          - p2p4)* & 
               (MIN2   & 
          - MOU2   & 
          - 4*p1p2   & 
          + p1p4   & 
          + 4*p2p4)))* & 
         Ccache(8))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (5*MIN2*p1p4   & 
          - 6*MOU2*p1p4   & 
          - 20*p1p2*p1p4   & 
          +  & 
              2*MIN2*p2p4   & 
          + 2*MOU2*p2p4   & 
          + 14*p1p2*p2p4   & 
          +  & 
              8*p1p4*p2p4   & 
          - 8*p2p4**2)   & 
          +  & 
           2*(  & 
          -4*MIN2**3*p1p4   & 
          + 4*MIN2*MOU2**2*p1p4   & 
          +  & 
              MIN2**2.5*np4*p1p4   & 
          + MIN2**2*p1p2*p1p4   & 
          -  & 
              3*MIN2**1.5*np4*p1p2*p1p4   & 
          +  & 
              15*MIN2*p1p2**2*p1p4   & 
          +  & 
              2*sqrt(MIN2)*np4*p1p2**2*p1p4   & 
          -  & 
              12*p1p2**3*p1p4   & 
          + MIN2**2*p1p4**2   & 
          +  & 
              7*MIN2**3*p2p4   & 
          - 3*MIN2*MOU2**2*p2p4   & 
          -  & 
              11*MIN2**2*p1p2*p2p4   & 
          +  & 
              4*MIN2**1.5*np4*p1p2*p2p4   & 
          -  & 
              4*sqrt(MIN2)*np4*p1p2**2*p2p4   & 
          +  & 
              4*p1p2**3*p2p4   & 
          - 9*MIN2**2*p1p4*p2p4   & 
          +  & 
              4*MIN2**1.5*np4*p1p4*p2p4   & 
          -  & 
              6*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              4*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
          +  & 
              12*p1p2**2*p1p4*p2p4   & 
          - 4*MIN2*p1p4**2*p2p4   & 
          +  & 
              8*MIN2**2*p2p4**2   & 
          - 4*MIN2**1.5*np4*p2p4**2   & 
          -  & 
              2*MIN2*p1p2*p2p4**2   & 
          +  & 
              4*sqrt(MIN2)*np4*p1p2*p2p4**2   & 
          -  & 
              4*p1p2**2*p2p4**2   & 
          + 8*MIN2*p1p4*p2p4**2   & 
          -  & 
              4*MIN2*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (8*MIN2**2*p1p4   & 
          - 17*MIN2*p1p2*p1p4   & 
          +  & 
                 12*p1p2**2*p1p4   & 
          - MIN2*p1p4**2   & 
          -  & 
                 2*p1p2*p1p4**2   & 
          - 6*MIN2**2*p2p4   & 
          +  & 
                 6*MIN2*p1p2*p2p4   & 
          - 4*p1p2**2*p2p4   & 
          +  & 
                 16*MIN2*p1p4*p2p4   & 
          - 8*p1p2*p1p4*p2p4   & 
          +  & 
                 4*p1p4**2*p2p4   & 
          - 6*MIN2*p2p4**2   & 
          +  & 
                 4*p1p2*p2p4**2   & 
          - 4*p1p4*p2p4**2)   & 
          +  & 
              MOU2*(15*MIN2*p1p2*p1p4   & 
          - 9*p1p2**2*p1p4   & 
          -  & 
                 MIN2*p1p4**2   & 
          + 2*p1p2*p1p4**2   & 
          -  & 
                 4*MIN2**2*p2p4   & 
          - 5*MIN2*p1p2*p2p4   & 
          +  & 
                 3*p1p2**2*p2p4   & 
          - 7*MIN2*p1p4*p2p4   & 
          +  & 
                 5*MIN2*p2p4**2   & 
          +  & 
                 sqrt(MIN2)*np4* & 
                  (  & 
          -(MIN2*p1p4)   & 
          + p1p2*p1p4   & 
          + 4*MIN2*p2p4   & 
          -  & 
                    p1p2*p2p4)   & 
          +  & 
                 sqrt(MIN2)*np2* & 
                  (  & 
          -8*MIN2*p1p4   & 
          + 9*p1p2*p1p4   & 
          - 3*p1p4**2   & 
          +  & 
                    2*MIN2*p2p4   & 
          - 3*p1p2*p2p4   & 
          + p1p4*p2p4)))) & 
          *Ccache(9))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (3*MIN2*p1p4   & 
          - 2*MOU2*p1p4   & 
          - 4*p1p2*p1p4   & 
          +  & 
              8*p1p4**2   & 
          - 10*MIN2*p2p4   & 
          + 6*MOU2*p2p4   & 
          +  & 
              8*p1p2*p2p4   & 
          + 2*p1p4*p2p4   & 
          - 8*p2p4**2)   & 
          +  & 
           2*(MIN2**3*p1p4   & 
          + 2*MIN2**2*MOU2*p1p4   & 
          -  & 
              3*MIN2*MOU2**2*p1p4   & 
          +  & 
              5*MIN2**1.5*MOU2*np2*p1p4   & 
          -  & 
              MIN2**2.5*np4*p1p4   & 
          + MIN2**1.5*MOU2*np4*p1p4   & 
          +  & 
              5*MIN2**2*p1p2*p1p4   & 
          - 13*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
              6*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          +  & 
              3*MIN2**1.5*np4*p1p2*p1p4   & 
          -  & 
              sqrt(MIN2)*MOU2*np4*p1p2*p1p4   & 
          -  & 
              18*MIN2*p1p2**2*p1p4   & 
          + 6*MOU2*p1p2**2*p1p4   & 
          -  & 
              2*sqrt(MIN2)*np4*p1p2**2*p1p4   & 
          +  & 
              8*p1p2**3*p1p4   & 
          + 4*MIN2**2*p1p4**2   & 
          -  & 
              4*MIN2*MOU2*p1p4**2   & 
          - 9*MIN2*p1p2*p1p4**2   & 
          +  & 
              MOU2*p1p2*p1p4**2   & 
          + 4*p1p2**2*p1p4**2   & 
          -  & 
              MIN2**3*p2p4   & 
          + MIN2**2*MOU2*p2p4   & 
          +  & 
              6*MIN2**1.5*MOU2*np2*p2p4   & 
          +  & 
              MIN2**2.5*np4*p2p4   & 
          -  & 
              5*MIN2**1.5*MOU2*np4*p2p4   & 
          +  & 
              2*MIN2**2*p1p2*p2p4   & 
          - 6*MIN2*MOU2*p1p2*p2p4   & 
          -  & 
              6*MIN2**1.5*np4*p1p2*p2p4   & 
          +  & 
              3*sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
          -  & 
              8*MIN2*p1p2**2*p2p4   & 
          +  & 
              4*sqrt(MIN2)*np4*p1p2**2*p2p4   & 
          +  & 
              MIN2**2*p1p4*p2p4   & 
          + 7*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
              4*MIN2**1.5*np4*p1p4*p2p4   & 
          +  & 
              18*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              3*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
              4*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
          -  & 
              12*p1p2**2*p1p4*p2p4   & 
          + 8*MIN2*p1p4**2*p2p4   & 
          -  & 
              4*p1p2*p1p4**2*p2p4   & 
          + 2*MIN2**2*p2p4**2   & 
          +  & 
              3*MIN2*MOU2*p2p4**2   & 
          +  & 
              4*MIN2**1.5*np4*p2p4**2   & 
          +  & 
              12*MIN2*p1p2*p2p4**2   & 
          -  & 
              4*sqrt(MIN2)*np4*p1p2*p2p4**2   & 
          -  & 
              4*MIN2*p1p4*p2p4**2   & 
          + 4*p1p2*p1p4*p2p4**2   & 
          -  & 
              4*MIN2*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (  & 
          -5*MIN2**2*p1p4   & 
          + 14*MIN2*p1p2*p1p4   & 
          -  & 
                 8*p1p2**2*p1p4   & 
          - 2*p1p2*p1p4**2   & 
          -  & 
                 2*MIN2**2*p2p4   & 
          + 8*MIN2*p1p2*p2p4   & 
          -  & 
                 10*MIN2*p1p4*p2p4   & 
          + 8*p1p2*p1p4*p2p4   & 
          -  & 
                 8*MIN2*p2p4**2)))* & 
         Ccache(10))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -(MIN2*p1p4)   & 
          + MOU2*p1p4   & 
          + 2*p1p2*p1p4   & 
          -  & 
              4*p1p2*p2p4   & 
          - p1p4*p2p4   & 
          + 2*p2p4**2)   & 
          -  & 
           2*(  & 
          -4*MIN2*p1p2**2*p1p4   & 
          + 4*MOU2*p1p2**2*p1p4   & 
          +  & 
              8*p1p2**3*p1p4   & 
          + 3*MIN2**2*p1p2*p2p4   & 
          -  & 
              6*MIN2*MOU2*p1p2*p2p4   & 
          + 3*MOU2**2*p1p2*p2p4   & 
          +  & 
              MIN2**1.5*np4*p1p2*p2p4   & 
          -  & 
              sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
          -  & 
              12*MIN2*p1p2**2*p2p4   & 
          + 12*MOU2*p1p2**2*p2p4   & 
          +  & 
              16*p1p2**3*p2p4   & 
          - 4*MIN2*p1p2*p1p4*p2p4   & 
          +  & 
              4*MOU2*p1p2*p1p4*p2p4   & 
          + 8*MIN2*p1p2*p2p4**2   & 
          -  & 
              8*MOU2*p1p2*p2p4**2   & 
          - 24*p1p2**2*p2p4**2   & 
          -  & 
              8*p1p2*p1p4*p2p4**2   & 
          + 8*p1p2*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (4*MIN2*p1p2*p1p4   & 
          - 4*MOU2*p1p2*p1p4   & 
          -  & 
                 8*p1p2**2*p1p4   & 
          - 3*MIN2**2*p2p4   & 
          +  & 
                 6*MIN2*MOU2*p2p4   & 
          - 3*MOU2**2*p2p4   & 
          +  & 
                 12*MIN2*p1p2*p2p4   & 
          - 12*MOU2*p1p2*p2p4   & 
          -  & 
                 16*p1p2**2*p2p4   & 
          + 3*MIN2*p1p4*p2p4   & 
          -  & 
                 3*MOU2*p1p4*p2p4   & 
          - 8*MIN2*p2p4**2   & 
          +  & 
                 8*MOU2*p2p4**2   & 
          + 24*p1p2*p2p4**2   & 
          +  & 
                 8*p1p4*p2p4**2   & 
          - 8*p2p4**3)))* & 
         Ccache(11))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (4*MIN2*p1p4   & 
          - 4*MOU2*p1p4   & 
          - 3*p1p2*p1p4   & 
          -  & 
              7*p1p4**2   & 
          - 4*p1p2*p2p4   & 
          + 7*p1p4*p2p4   & 
          +  & 
              4*p2p4**2)   & 
          -  & 
           2*(  & 
          -4*MIN2**2*MOU2*p1p4   & 
          + 4*MIN2**2.5*np2*p1p4   & 
          -  & 
              4*MIN2**1.5*MOU2*np2*p1p4   & 
          -  & 
              5*MIN2**2*p1p2*p1p4   & 
          + 13*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
              7*MIN2**1.5*np2*p1p2*p1p4   & 
          +  & 
              5*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          +  & 
              9*MIN2*p1p2**2*p1p4   & 
          - 5*MOU2*p1p2**2*p1p4   & 
          +  & 
              4*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          -  & 
              4*p1p2**3*p1p4   & 
          +  & 
              4*MOU2**2*(MIN2   & 
          - p1p4)*p1p4   & 
          +  & 
              5*MIN2**2*p1p4**2   & 
          - MIN2*MOU2*p1p4**2   & 
          -  & 
              5*MIN2**1.5*np2*p1p4**2   & 
          +  & 
              sqrt(MIN2)*MOU2*np2*p1p4**2   & 
          -  & 
              5*MIN2*p1p2*p1p4**2   & 
          - 9*MOU2*p1p2*p1p4**2   & 
          +  & 
              6*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          -  & 
              4*p1p2**2*p1p4**2   & 
          - 4*MIN2*p1p4**3   & 
          +  & 
              4*MOU2*p1p4**3   & 
          + 8*p1p2*p1p4**3   & 
          +  & 
              4*MIN2**3*p2p4   & 
          - 4*MIN2**2*MOU2*p2p4   & 
          -  & 
              2*MIN2**2.5*np2*p2p4   & 
          +  & 
              2*MIN2**1.5*MOU2*np2*p2p4   & 
          -  & 
              10*MIN2**2*p1p2*p2p4   & 
          +  & 
              4*MIN2**1.5*np2*p1p2*p2p4   & 
          -  & 
              3*sqrt(MIN2)*MOU2*np2*p1p2*p2p4   & 
          +  & 
              2*MIN2*p1p2**2*p2p4   & 
          + 3*MOU2*p1p2**2*p2p4   & 
          -  & 
              4*sqrt(MIN2)*np2*p1p2**2*p2p4   & 
          +  & 
              4*p1p2**3*p2p4   & 
          - 9*MIN2**2*p1p4*p2p4   & 
          +  & 
              MIN2*MOU2*p1p4*p2p4   & 
          +  & 
              9*MIN2**1.5*np2*p1p4*p2p4   & 
          -  & 
              6*sqrt(MIN2)*MOU2*np2*p1p4*p2p4   & 
          +  & 
              17*MIN2*p1p2*p1p4*p2p4   & 
          +  & 
              4*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
              10*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          +  & 
              4*p1p2**2*p1p4*p2p4   & 
          + 12*MIN2*p1p4**2*p2p4   & 
          +  & 
              2*MOU2*p1p4**2*p2p4   & 
          -  & 
              6*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          -  & 
              12*p1p2*p1p4**2*p2p4   & 
          - 8*p1p4**3*p2p4   & 
          +  & 
              12*MIN2**2*p2p4**2   & 
          - 2*MIN2*MOU2*p2p4**2   & 
          -  & 
              4*MIN2**1.5*np2*p2p4**2   & 
          +  & 
              3*sqrt(MIN2)*MOU2*np2*p2p4**2   & 
          -  & 
              12*MIN2*p1p2*p2p4**2   & 
          - 3*MOU2*p1p2*p2p4**2   & 
          +  & 
              8*sqrt(MIN2)*np2*p1p2*p2p4**2   & 
          -  & 
              8*p1p2**2*p2p4**2   & 
          - 26*MIN2*p1p4*p2p4**2   & 
          +  & 
              2*MOU2*p1p4*p2p4**2   & 
          +  & 
              6*sqrt(MIN2)*np2*p1p4*p2p4**2   & 
          +  & 
              8*p1p2*p1p4*p2p4**2   & 
          + 16*p1p4**2*p2p4**2   & 
          +  & 
              10*MIN2*p2p4**3   & 
          - 4*sqrt(MIN2)*np2*p2p4**3   & 
          +  & 
              4*p1p2*p2p4**3   & 
          - 8*p1p4*p2p4**3   & 
          -  & 
              2*sqrt(MIN2)*np4* & 
               (MIN2**2*p1p4   & 
          + MIN2*MOU2*p1p4   & 
          -  & 
                 2*MOU2**2*p1p4   & 
          - 2*MIN2*p1p2*p1p4   & 
          -  & 
                 4*MOU2*p1p2*p1p4   & 
          + p1p2**2*p1p4   & 
          -  & 
                 2*MIN2*p1p4**2   & 
          + 2*MOU2*p1p4**2   & 
          +  & 
                 4*p1p2*p1p4**2   & 
          - 2*MIN2**2*p2p4   & 
          +  & 
                 2*MIN2*MOU2*p2p4   & 
          + 5*MIN2*p1p2*p2p4   & 
          -  & 
                 MOU2*p1p2*p2p4   & 
          - 3*p1p2**2*p2p4   & 
          +  & 
                 5*MIN2*p1p4*p2p4   & 
          + MOU2*p1p4*p2p4   & 
          -  & 
                 9*p1p2*p1p4*p2p4   & 
          - 4*p1p4**2*p2p4   & 
          -  & 
                 5*MIN2*p2p4**2   & 
          + MOU2*p2p4**2   & 
          +  & 
                 7*p1p2*p2p4**2   & 
          + 8*p1p4*p2p4**2   & 
          - 4*p2p4**3) & 
              ))*Ccache(12))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*MIN2* & 
         (CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4   & 
          - 2*p2p4)   & 
          +  & 
           2*(MIN2**2*(4*p1p4   & 
          - 3*p2p4)   & 
          +  & 
              MIN2**1.5*(np2   & 
          - np4)*p2p4   & 
          +  & 
              MOU2*(  & 
          -4*MIN2*p1p4   & 
          + 2*p1p2*p1p4   & 
          + p1p4**2   & 
          +  & 
                 3*MIN2*p2p4   & 
          - p1p2*p2p4   & 
          - p1p4*p2p4)   & 
          -  & 
              MIN2*(10*p1p2*p1p4   & 
          + p1p4**2   & 
          - 7*p1p2*p2p4   & 
          -  & 
                 8*p1p4*p2p4   & 
          + 4*p2p4**2)   & 
          +  & 
              p1p2*(6*p1p2*p1p4   & 
          + p1p4**2   & 
          - 4*p1p2*p2p4   & 
          -  & 
                 8*p1p4*p2p4   & 
          + 4*p2p4**2)   & 
          +  & 
              sqrt(MIN2)* & 
               (np4*p1p2*p2p4   & 
          +  & 
                 np2*(  & 
          -2*p1p2*p1p4   & 
          + p1p4**2   & 
          + p1p2*p2p4   & 
          -  & 
                    p1p4*p2p4))))* & 
         Ccache(13))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (4*EL**4*GF**2*MIN2* & 
         (CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4   & 
          - 2*p2p4)   & 
          +  & 
           2*(p1p2*p1p4*(3*p1p2   & 
          - 4*p2p4)   & 
          +  & 
              MIN2**1.5*(np2   & 
          - np4)*p2p4   & 
          +  & 
              MIN2**2*(2*p1p4   & 
          + p2p4)   & 
          +  & 
              sqrt(MIN2)* & 
               (  & 
          -(np2*p1p2*p1p4)   & 
          + np4*p1p2*p2p4)   & 
          +  & 
              MOU2*(p1p2*p1p4   & 
          - MIN2*(2*p1p4   & 
          + p2p4))   & 
          +  & 
              MIN2*(4*p2p4*(p1p4   & 
          + p2p4)   & 
          -  & 
                 p1p2*(5*p1p4   & 
          + 3*p2p4))))* & 
         Ccache(14))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (16*EL**4*GF**2*(sqrt(MIN2)*np2   & 
          - p1p2)*p1p2* & 
         (  & 
          -MIN2   & 
          + MOU2   & 
          + 2*p1p2   & 
          - 2*p2p4)*p2p4* & 
         Ccache(15))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (4*EL**4*GF**2*(4*MIN2**3*p1p4   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4   & 
          -  & 
           10*MIN2**2*p1p2*p1p4   & 
          -  & 
           2*MIN2**1.5*np2*p1p2*p1p4   & 
          + 6*MIN2*p1p2**2*p1p4   & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2   & 
          - 10*MIN2**2*p1p4**2   & 
          +  & 
           2*MIN2**1.5*np2*p1p4**2   & 
          + 20*MIN2*p1p2*p1p4**2   & 
          +  & 
           2*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          -  & 
           8*p1p2**2*p1p4**2   & 
          + 6*MIN2*p1p4**3   & 
          -  & 
           2*sqrt(MIN2)*np2*p1p4**3   & 
          - 8*p1p2*p1p4**3   & 
          -  & 
           4*MIN2**3*p2p4   & 
          + 12*MIN2**2*p1p2*p2p4   & 
          +  & 
           2*MIN2**1.5*np2*p1p2*p2p4   & 
          - 8*MIN2*p1p2**2*p2p4   & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4   & 
          +  & 
           18*MIN2**2*p1p4*p2p4   & 
          - 32*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
           2*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          +  & 
           8*p1p2**2*p1p4*p2p4   & 
          - 24*MIN2*p1p4**2*p2p4   & 
          +  & 
           24*p1p2*p1p4**2*p2p4   & 
          + 8*p1p4**3*p2p4   & 
          -  & 
           12*MIN2**2*p2p4**2   & 
          - 2*MIN2**1.5*np2*p2p4**2   & 
          +  & 
           16*MIN2*p1p2*p2p4**2   & 
          + 26*MIN2*p1p4*p2p4**2   & 
          +  & 
           2*sqrt(MIN2)*np2*p1p4*p2p4**2   & 
          -  & 
           16*p1p2*p1p4*p2p4**2   & 
          - 16*p1p4**2*p2p4**2   & 
          -  & 
           8*MIN2*p2p4**3   & 
          + 8*p1p4*p2p4**3   & 
          -  & 
           2*MOU2*(MIN2   & 
          - p1p4)*(p1p4   & 
          - p2p4)* & 
            (2*MIN2   & 
          - p1p2   & 
          - 3*p1p4   & 
          + p2p4)   & 
          +  & 
           2*sqrt(MIN2)*np4*(p1p4   & 
          - p2p4)* & 
            (2*MIN2**2   & 
          - 5*MIN2*p1p2   & 
          + 3*p1p2**2   & 
          -  & 
              3*MIN2*p1p4   & 
          + 5*p1p2*p1p4   & 
          +  & 
              MOU2*(  & 
          -2*MIN2   & 
          + p1p2   & 
          + 3*p1p4   & 
          - p2p4)   & 
          +  & 
              5*MIN2*p2p4   & 
          - 7*p1p2*p2p4   & 
          - 4*p1p4*p2p4   & 
          +  & 
              4*p2p4**2))* & 
         Ccache(16))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (8*EL**4*GF**2*(2*MIN2**3*p1p4   & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4   & 
          -  & 
           10*MIN2*p1p2**2*p1p4   & 
          + 8*p1p2**3*p1p4   & 
          -  & 
           MIN2**2*p1p4**2   & 
          + MIN2*p1p2*p1p4**2   & 
          +  & 
           MIN2*MOU2**2*(2*p1p4   & 
          - p2p4)   & 
          -  & 
           CMPLX(aa,bb)*asym*MIN2**1.5*p2p4   & 
          - 3*MIN2**3*p2p4   & 
          -  & 
           MIN2**2.5*np4*p2p4   & 
          - CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4   & 
          +  & 
           4*MIN2**2*p1p2*p2p4   & 
          + 3*MIN2*p1p2**2*p2p4   & 
          +  & 
           sqrt(MIN2)*np4*p1p2**2*p2p4   & 
          - 4*p1p2**3*p2p4   & 
          +  & 
           7*MIN2**2*p1p4*p2p4   & 
          + MIN2*p1p2*p1p4*p2p4   & 
          -  & 
           8*p1p2**2*p1p4*p2p4   & 
          - 4*MIN2**2*p2p4**2   & 
          +  & 
           4*p1p2**2*p2p4**2   & 
          +  & 
           sqrt(MIN2)*np2* & 
            (  & 
          -2*MIN2**2*(p1p4   & 
          - p2p4)   & 
          +  & 
              p1p2*(  & 
          -8*p1p2*p1p4   & 
          + 4*p1p2*p2p4   & 
          +  & 
                 7*p1p4*p2p4   & 
          - 4*p2p4**2)   & 
          +  & 
              MIN2*(6*p1p2*p1p4   & 
          + p1p4**2   & 
          - 2*p1p2*p2p4   & 
          -  & 
                 7*p1p4*p2p4   & 
          + 3*p2p4**2))   & 
          +  & 
           MOU2*(  & 
          -4*MIN2**2*(p1p4   & 
          - p2p4)   & 
          +  & 
              MIN2**1.5*np4*p2p4   & 
          +  & 
              p1p2*(2*p1p2*p1p4   & 
          + p1p4**2   & 
          - p1p2*p2p4   & 
          -  & 
                 p1p4*p2p4)   & 
          +  & 
              MIN2*(p1p4**2   & 
          - 7*p1p4*p2p4   & 
          + 3*p2p4**2)   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (2*MIN2*(p1p4   & 
          - p2p4)   & 
          +  & 
                 p1p2*(  & 
          -2*p1p4   & 
          + p2p4)   & 
          + p1p4*(  & 
          -p1p4   & 
          + p2p4)) & 
              ))*Ccache(17))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (4*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (2*p1p2*p1p4   & 
          + p1p4**2   & 
          - 2*MIN2*p2p4   & 
          -  & 
              2*p1p4*p2p4)   & 
          -  & 
           2*sqrt(MIN2)*np4* & 
            (p1p2*p1p4*(3*p1p2   & 
          - 4*p2p4)   & 
          +  & 
              MIN2**2*(2*p1p4   & 
          + p2p4)   & 
          -  & 
              MIN2*(p1p2   & 
          - p2p4)*(5*p1p4   & 
          + 3*p2p4)   & 
          +  & 
              MOU2*(p1p2*p1p4   & 
          - MIN2*(2*p1p4   & 
          + p2p4)))   & 
          +  & 
           2*(  & 
          -(sqrt(MIN2)*np2*p1p2*p1p4**2)   & 
          +  & 
              4*p1p2*p1p4**2*(p1p2   & 
          - p2p4)   & 
          +  & 
              MIN2**1.5*np2*p1p4*p2p4   & 
          +  & 
              MIN2**2*(4*p1p4**2   & 
          + p1p4*p2p4   & 
          + p2p4**2)   & 
          +  & 
              MIN2*p1p4* & 
               (  & 
          -9*p1p2*p1p4   & 
          - 5*p1p2*p2p4   & 
          + 8*p1p4*p2p4   & 
          +  & 
                 4*p2p4**2)   & 
          +  & 
              MOU2*p1p4*(p1p2*p1p4   & 
          - MIN2*(4*p1p4   & 
          + p2p4))))* & 
         Ccache(18))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (16*EL**4*GF**2*(sqrt(MIN2)*np4*p1p2   & 
          +  & 
           sqrt(MIN2)*np2*p1p4   & 
          - 2*p1p2*p1p4)* & 
         (  & 
          -MIN2   & 
          + MOU2   & 
          + 2*p1p2   & 
          - 2*p2p4)*p2p4* & 
         Ccache(19))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(  & 
          -(CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4* & 
              (MIN2   & 
          - MOU2   & 
          - 3*p1p2   & 
          + p1p4   & 
          + 3*p2p4))   & 
          -  & 
           2*(MIN2**3*p1p4   & 
          - MIN2**2.5*np2*p1p4   & 
          +  & 
              3*MIN2**1.5*np2*p1p2*p1p4   & 
          -  & 
              5*MIN2*p1p2**2*p1p4   & 
          -  & 
              4*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          +  & 
              4*p1p2**3*p1p4   & 
          - 6*MIN2**2*p1p4**2   & 
          +  & 
              2*MIN2**1.5*np2*p1p4**2   & 
          +  & 
              12*MIN2*p1p2*p1p4**2   & 
          -  & 
              sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          -  & 
              4*p1p2**2*p1p4**2   & 
          + 5*MIN2*p1p4**3   & 
          -  & 
              sqrt(MIN2)*np2*p1p4**3   & 
          - 8*p1p2*p1p4**3   & 
          +  & 
              MOU2**2*(MIN2   & 
          - p1p4)*(p1p4   & 
          - p2p4)   & 
          -  & 
              2*MIN2**3*p2p4   & 
          + MIN2**2.5*np2*p2p4   & 
          +  & 
              4*MIN2**2*p1p2*p2p4   & 
          -  & 
              3*MIN2**1.5*np2*p1p2*p2p4   & 
          +  & 
              2*MIN2*p1p2**2*p2p4   & 
          +  & 
              4*sqrt(MIN2)*np2*p1p2**2*p2p4   & 
          -  & 
              4*p1p2**3*p2p4   & 
          + 8*MIN2**2*p1p4*p2p4   & 
          -  & 
              5*MIN2**1.5*np2*p1p4*p2p4   & 
          -  & 
              10*MIN2*p1p2*p1p4*p2p4   & 
          +  & 
              9*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          -  & 
              4*p1p2**2*p1p4*p2p4   & 
          - 18*MIN2*p1p4**2*p2p4   & 
          +  & 
              2*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          +  & 
              20*p1p2*p1p4**2*p2p4   & 
          + 8*p1p4**3*p2p4   & 
          -  & 
              6*MIN2**2*p2p4**2   & 
          + 3*MIN2**1.5*np2*p2p4**2   & 
          +  & 
              2*MIN2*p1p2*p2p4**2   & 
          -  & 
              8*sqrt(MIN2)*np2*p1p2*p2p4**2   & 
          +  & 
              8*p1p2**2*p2p4**2   & 
          + 17*MIN2*p1p4*p2p4**2   & 
          -  & 
              5*sqrt(MIN2)*np2*p1p4*p2p4**2   & 
          -  & 
              8*p1p2*p1p4*p2p4**2   & 
          - 16*p1p4**2*p2p4**2   & 
          -  & 
              4*MIN2*p2p4**3   & 
          + 4*sqrt(MIN2)*np2*p2p4**3   & 
          -  & 
              4*p1p2*p2p4**3   & 
          + 8*p1p4*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np4*(p1p4   & 
          - p2p4)* & 
               (3*MIN2**2   & 
          + MOU2**2   & 
          + 5*p1p2**2   & 
          +  & 
                 9*p1p2*p1p4   & 
          +  & 
                 MOU2*(  & 
          -4*MIN2   & 
          + 4*p1p2   & 
          + 5*p1p4   & 
          - 5*p2p4)   & 
          -  & 
                 13*p1p2*p2p4   & 
          - 8*p1p4*p2p4   & 
          + 8*p2p4**2   & 
          +  & 
                 MIN2*(  & 
          -8*p1p2   & 
          - 5*p1p4   & 
          + 9*p2p4))   & 
          +  & 
              MOU2*(sqrt(MIN2)*np2*(p1p4   & 
          - p2p4)* & 
                  (MIN2   & 
          - p1p2   & 
          - 2*p1p4   & 
          + p2p4)   & 
          +  & 
                 MIN2**2*(  & 
          -2*p1p4   & 
          + 3*p2p4)   & 
          +  & 
                 MIN2*(7*p1p4**2   & 
          - 9*p1p4*p2p4   & 
          -  & 
                    2*(p1p2   & 
          - 2*p2p4)*p2p4)   & 
          +  & 
                 (p1p4   & 
          - p2p4)* & 
                  (p1p2**2   & 
          + 5*p1p4*(  & 
          -p1p4   & 
          + p2p4)   & 
          -  & 
                    p1p2*(2*p1p4   & 
          + p2p4)))))* & 
         Ccache(20))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -14*MIN2*p1p4   & 
          + 14*MOU2*p1p4   & 
          + 29*p1p2*p1p4   & 
          +  & 
              6*MIN2*p2p4   & 
          - 6*MOU2*p2p4   & 
          - 22*p1p2*p2p4   & 
          -  & 
              8*p1p4*p2p4   & 
          + 8*p2p4**2)   & 
          +  & 
           2*(  & 
          -3*MIN2**3*p1p4   & 
          + 6*MIN2**2*MOU2*p1p4   & 
          -  & 
              3*MIN2*MOU2**2*p1p4   & 
          + 17*MIN2**2*p1p2*p1p4   & 
          -  & 
              22*MIN2*MOU2*p1p2*p1p4   & 
          + 5*MOU2**2*p1p2*p1p4   & 
          -  & 
              30*MIN2*p1p2**2*p1p4   & 
          + 18*MOU2*p1p2**2*p1p4   & 
          +  & 
              16*p1p2**3*p1p4   & 
          + MIN2**2*p1p4**2   & 
          -  & 
              2*MIN2*MOU2*p1p4**2   & 
          + MOU2**2*p1p4**2   & 
          -  & 
              2*MIN2*p1p2*p1p4**2   & 
          + 2*MOU2*p1p2*p1p4**2   & 
          -  & 
              MIN2**3*p2p4   & 
          + 2*MIN2**2*MOU2*p2p4   & 
          -  & 
              MIN2*MOU2**2*p2p4   & 
          - 5*MIN2**2*p1p2*p2p4   & 
          +  & 
              5*MIN2*MOU2*p1p2*p2p4   & 
          + 14*MIN2*p1p2**2*p2p4   & 
          -  & 
              4*MOU2*p1p2**2*p2p4   & 
          - 8*p1p2**3*p2p4   & 
          +  & 
              MIN2**2*p1p4*p2p4   & 
          - MOU2**2*p1p4*p2p4   & 
          +  & 
              18*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              10*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
              16*p1p2**2*p1p4*p2p4   & 
          + 4*MIN2*p1p4**2*p2p4   & 
          -  & 
              4*MOU2*p1p4**2*p2p4   & 
          - 2*MIN2**2*p2p4**2   & 
          +  & 
              2*MIN2*MOU2*p2p4**2   & 
          - 8*MIN2*p1p2*p2p4**2   & 
          +  & 
              8*p1p2**2*p2p4**2   & 
          - 8*MIN2*p1p4*p2p4**2   & 
          +  & 
              4*MOU2*p1p4*p2p4**2   & 
          + 4*MIN2*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (  & 
          -5*MIN2**2*p1p4   & 
          + 10*MIN2*MOU2*p1p4   & 
          -  & 
                 5*MOU2**2*p1p4   & 
          + 18*MIN2*p1p2*p1p4   & 
          -  & 
                 18*MOU2*p1p2*p1p4   & 
          - 16*p1p2**2*p1p4   & 
          -  & 
                 MIN2*p1p4**2   & 
          + MOU2*p1p4**2   & 
          +  & 
                 2*p1p2*p1p4**2   & 
          + 4*MIN2**2*p2p4   & 
          -  & 
                 4*MIN2*MOU2*p2p4   & 
          - 8*MIN2*p1p2*p2p4   & 
          +  & 
                 4*MOU2*p1p2*p2p4   & 
          + 8*p1p2**2*p2p4   & 
          -  & 
                 16*MIN2*p1p4*p2p4   & 
          + 8*MOU2*p1p4*p2p4   & 
          +  & 
                 12*p1p2*p1p4*p2p4   & 
          - 4*p1p4**2*p2p4   & 
          +  & 
                 8*MIN2*p2p4**2   & 
          - 8*p1p2*p2p4**2   & 
          +  & 
                 4*p1p4*p2p4**2)   & 
          +  & 
              sqrt(MIN2)*np4* & 
               (2*MIN2*MOU2*p1p4   & 
          - MOU2**2*p1p4   & 
          -  & 
                 3*MOU2*p1p2*p1p4   & 
          - MIN2*MOU2*p2p4   & 
          +  & 
                 MOU2**2*p2p4   & 
          + 2*MOU2*p1p2*p2p4   & 
          +  & 
                 4*MOU2*p1p4*p2p4   & 
          - 4*MOU2*p2p4**2   & 
          -  & 
                 (MIN2   & 
          - p1p2)* & 
                  (MIN2*p1p4   & 
          - 2*p1p2*p1p4   & 
          + 4*p1p2*p2p4   & 
          +  & 
                    4*p1p4*p2p4   & 
          - 4*p2p4**2))))* & 
         Ccache(21) & 
         )/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -6*MIN2*p1p4   & 
          + 6*MOU2*p1p4   & 
          + 8*p1p2*p1p4   & 
          +  & 
              p1p4**2   & 
          - 10*p1p4*p2p4)   & 
          +  & 
           2*p1p4*(MIN2**2*p1p4   & 
          + 2*MIN2*MOU2*p1p4   & 
          -  & 
              3*MOU2**2*p1p4   & 
          - 5*MIN2**1.5*np2*p1p4   & 
          +  & 
              5*sqrt(MIN2)*MOU2*np2*p1p4   & 
          +  & 
              4*MIN2*p1p2*p1p4   & 
          - 12*MOU2*p1p2*p1p4   & 
          +  & 
              12*sqrt(MIN2)*np2*p1p2*p1p4   & 
          -  & 
              16*p1p2**2*p1p4   & 
          + 4*MIN2*p1p4**2   & 
          -  & 
              4*MOU2*p1p4**2   & 
          - 8*p1p2*p1p4**2   & 
          +  & 
              8*MOU2*p1p4*p2p4   & 
          -  & 
              12*sqrt(MIN2)*np2*p1p4*p2p4   & 
          +  & 
              24*p1p2*p1p4*p2p4   & 
          + 8*p1p4**2*p2p4   & 
          -  & 
              8*p1p4*p2p4**2   & 
          +  & 
              sqrt(MIN2)*np4* & 
               (  & 
          -MIN2**2   & 
          - 2*MIN2*MOU2   & 
          + 3*MOU2**2   & 
          +  & 
                 MIN2*p1p2   & 
          + 7*MOU2*p1p2   & 
          + 4*p1p2**2   & 
          -  & 
                 4*MIN2*p1p4   & 
          + 4*MOU2*p1p4   & 
          + 8*p1p2*p1p4   & 
          -  & 
                 8*MOU2*p2p4   & 
          - 12*p1p2*p2p4   & 
          - 8*p1p4*p2p4   & 
          +  & 
                 8*p2p4**2)))* & 
         Ccache(22))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (6*MIN2*p2p4   & 
          - 6*MOU2*p2p4   & 
          - 8*p1p2*p2p4   & 
          -  & 
              p1p4*p2p4   & 
          + 10*p2p4**2)   & 
          +  & 
           2*(  & 
          -4*MIN2*p1p2*p1p4**2   & 
          + 4*MOU2*p1p2*p1p4**2   & 
          +  & 
              8*p1p2**2*p1p4**2   & 
          + 3*MIN2**2*p1p4*p2p4   & 
          -  & 
              6*MIN2*MOU2*p1p4*p2p4   & 
          + 3*MOU2**2*p1p4*p2p4   & 
          +  & 
              5*MIN2**1.5*np2*p1p4*p2p4   & 
          -  & 
              5*sqrt(MIN2)*MOU2*np2*p1p4*p2p4   & 
          -  & 
              12*MIN2*p1p2*p1p4*p2p4   & 
          +  & 
              12*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
              12*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          +  & 
              16*p1p2**2*p1p4*p2p4   & 
          - 4*MIN2*p1p4**2*p2p4   & 
          +  & 
              4*MOU2*p1p4**2*p2p4   & 
          + 8*MIN2*p1p4*p2p4**2   & 
          -  & 
              8*MOU2*p1p4*p2p4**2   & 
          +  & 
              12*sqrt(MIN2)*np2*p1p4*p2p4**2   & 
          -  & 
              24*p1p2*p1p4*p2p4**2   & 
          - 8*p1p4**2*p2p4**2   & 
          +  & 
              8*p1p4*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np4* & 
               (4*MIN2*p1p2*p1p4   & 
          - 4*MOU2*p1p2*p1p4   & 
          -  & 
                 8*p1p2**2*p1p4   & 
          - 3*MIN2**2*p2p4   & 
          +  & 
                 6*MIN2*MOU2*p2p4   & 
          - 3*MOU2**2*p2p4   & 
          +  & 
                 7*MIN2*p1p2*p2p4   & 
          - 7*MOU2*p1p2*p2p4   & 
          -  & 
                 4*p1p2**2*p2p4   & 
          + 4*MIN2*p1p4*p2p4   & 
          -  & 
                 4*MOU2*p1p4*p2p4   & 
          - 8*MIN2*p2p4**2   & 
          +  & 
                 8*MOU2*p2p4**2   & 
          + 12*p1p2*p2p4**2   & 
          +  & 
                 8*p1p4*p2p4**2   & 
          - 8*p2p4**3)))* & 
         Ccache(23))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (3*MIN2*p1p4   & 
          - 3*MOU2*p1p4   & 
          - 4*p1p2*p1p4   & 
          -  & 
              4*MIN2*p2p4   & 
          + 4*MOU2*p2p4   & 
          + 8*p1p2*p2p4   & 
          +  & 
              4*p1p4*p2p4   & 
          - 8*p2p4**2)   & 
          +  & 
           2*(  & 
          -(MIN2**3*p1p4)   & 
          + 2*MIN2**2*MOU2*p1p4   & 
          -  & 
              MIN2*MOU2**2*p1p4   & 
          + MIN2**2*p1p2*p1p4   & 
          -  & 
              MOU2**2*p1p2*p1p4   & 
          + 2*MIN2**2*p1p4**2   & 
          -  & 
              2*MOU2**2*p1p4**2   & 
          + 2*MIN2*p1p2*p1p4**2   & 
          -  & 
              10*MOU2*p1p2*p1p4**2   & 
          - 8*p1p2**2*p1p4**2   & 
          -  & 
              4*MIN2*p1p4**3   & 
          + 4*MOU2*p1p4**3   & 
          +  & 
              8*p1p2*p1p4**3   & 
          + 2*MIN2**3*p2p4   & 
          -  & 
              4*MIN2**2*MOU2*p2p4   & 
          + 2*MIN2*MOU2**2*p2p4   & 
          -  & 
              2*MIN2**2*p1p2*p2p4   & 
          - MIN2*MOU2*p1p2*p2p4   & 
          +  & 
              3*MOU2**2*p1p2*p2p4   & 
          - 8*MIN2*p1p2**2*p2p4   & 
          +  & 
              8*MOU2*p1p2**2*p2p4   & 
          + 8*p1p2**3*p2p4   & 
          -  & 
              10*MIN2**2*p1p4*p2p4   & 
          +  & 
              12*MIN2*MOU2*p1p4*p2p4   & 
          - 2*MOU2**2*p1p4*p2p4   & 
          +  & 
              14*MIN2*p1p2*p1p4*p2p4   & 
          +  & 
              2*MOU2*p1p2*p1p4*p2p4   & 
          + 8*p1p2**2*p1p4*p2p4   & 
          +  & 
              10*MIN2*p1p4**2*p2p4   & 
          - 2*MOU2*p1p4**2*p2p4   & 
          -  & 
              8*p1p2*p1p4**2*p2p4   & 
          - 8*p1p4**3*p2p4   & 
          +  & 
              8*MIN2**2*p2p4**2   & 
          - 8*MIN2*MOU2*p2p4**2   & 
          -  & 
              8*MOU2*p1p2*p2p4**2   & 
          - 16*p1p2**2*p2p4**2   & 
          -  & 
              22*MIN2*p1p4*p2p4**2   & 
          + 6*MOU2*p1p4*p2p4**2   & 
          +  & 
              16*p1p4**2*p2p4**2   & 
          + 8*MIN2*p2p4**3   & 
          +  & 
              8*p1p2*p2p4**3   & 
          - 8*p1p4*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (MIN2**2*p1p4   & 
          - 2*MIN2*MOU2*p1p4   & 
          +  & 
                 MOU2**2*p1p4   & 
          - 6*MIN2*p1p4**2   & 
          +  & 
                 6*MOU2*p1p4**2   & 
          + 10*p1p2*p1p4**2   & 
          -  & 
                 4*MIN2**2*p2p4   & 
          + 7*MIN2*MOU2*p2p4   & 
          -  & 
                 3*MOU2**2*p2p4   & 
          + 10*MIN2*p1p2*p2p4   & 
          -  & 
                 8*MOU2*p1p2*p2p4   & 
          - 8*p1p2**2*p2p4   & 
          +  & 
                 8*MIN2*p1p4*p2p4   & 
          - 8*MOU2*p1p4*p2p4   & 
          -  & 
                 14*p1p2*p1p4*p2p4   & 
          - 10*p1p4**2*p2p4   & 
          -  & 
                 10*MIN2*p2p4**2   & 
          + 8*MOU2*p2p4**2   & 
          +  & 
                 16*p1p2*p2p4**2   & 
          + 14*p1p4*p2p4**2   & 
          -  & 
                 8*p2p4**3)   & 
          -  & 
              2*sqrt(MIN2)*np4* & 
               (MIN2**2*p1p4   & 
          - MOU2**2*p1p4   & 
          -  & 
                 2*MIN2*p1p2*p1p4   & 
          - 2*MOU2*p1p2*p1p4   & 
          +  & 
                 p1p2**2*p1p4   & 
          - 2*MIN2*p1p4**2   & 
          +  & 
                 2*MOU2*p1p4**2   & 
          + 4*p1p2*p1p4**2   & 
          -  & 
                 2*MIN2**2*p2p4   & 
          + 3*MIN2*MOU2*p2p4   & 
          -  & 
                 MOU2**2*p2p4   & 
          + 5*MIN2*p1p2*p2p4   & 
          -  & 
                 3*MOU2*p1p2*p2p4   & 
          - 3*p1p2**2*p2p4   & 
          +  & 
                 5*MIN2*p1p4*p2p4   & 
          - MOU2*p1p4*p2p4   & 
          -  & 
                 9*p1p2*p1p4*p2p4   & 
          - 4*p1p4**2*p2p4   & 
          -  & 
                 5*MIN2*p2p4**2   & 
          + 3*MOU2*p2p4**2   & 
          +  & 
                 7*p1p2*p2p4**2   & 
          + 8*p1p4*p2p4**2   & 
          - 4*p2p4**3) & 
              ))*Ccache(24))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -(MIN2*p1p4)   & 
          + MOU2*p1p4   & 
          + 2*p1p2*(p1p4   & 
          - p2p4)) & 
          - 2*(MOU2**2* & 
               (2*MIN2*p1p4   & 
          + p1p2*(  & 
          -2*p1p4   & 
          + p2p4))   & 
          +  & 
              (MIN2   & 
          - p1p2)* & 
               (2*MIN2**2*p1p4   & 
          + sqrt(MIN2)*np4*p1p2*p2p4   & 
          -  & 
                 4*p1p2*(p1p2   & 
          - p2p4)*(  & 
          -2*p1p4   & 
          + p2p4)   & 
          +  & 
                 MIN2*(  & 
          -8*p1p2*p1p4   & 
          + 3*p1p2*p2p4   & 
          +  & 
                    p1p4*p2p4))   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (MIN2**2*(2*p1p4   & 
          - p2p4)   & 
          +  & 
                 MOU2**2*(2*p1p4   & 
          - p2p4)   & 
          +  & 
                 MIN2*(  & 
          -8*p1p2*p1p4   & 
          + 3*p1p2*p2p4   & 
          +  & 
                    6*p1p4*p2p4   & 
          - 3*p2p4**2)   & 
          +  & 
                 MOU2*(  & 
          -4*MIN2*p1p4   & 
          + 8*p1p2*p1p4   & 
          +  & 
                    2*MIN2*p2p4   & 
          - 3*p1p2*p2p4   & 
          -  & 
                    6*p1p4*p2p4   & 
          + 3*p2p4**2)   & 
          +  & 
                 p1p2*(8*p1p2*p1p4   & 
          - 4*p1p2*p2p4   & 
          -  & 
                    7*p1p4*p2p4   & 
          + 4*p2p4**2))   & 
          -  & 
              MOU2*(4*MIN2**2*p1p4   & 
          +  & 
                 sqrt(MIN2)*np4*p1p2*p2p4   & 
          +  & 
                 p1p2*(8*p1p2*p1p4   & 
          - 3*p1p2*p2p4   & 
          -  & 
                    7*p1p4*p2p4   & 
          + 3*p2p4**2)   & 
          +  & 
                 MIN2*(p1p4*p2p4   & 
          + 4*p1p2*(  & 
          -3*p1p4   & 
          + p2p4)))) & 
           )*Ccache(25))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (16*EL**4*GF**2*(sqrt(MIN2)*np4   & 
          - p1p4)*p1p4* & 
         (MIN2   & 
          - MOU2   & 
          - 2*p1p2   & 
          + 2*p2p4)* & 
         Ccache(26))/ & 
       (3.*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (16*EL**4*GF**2*(sqrt(MIN2)*np4   & 
          - p1p4)* & 
         (  & 
          -MIN2   & 
          + MOU2   & 
          + 2*p1p2   & 
          - 2*p2p4)*p2p4* & 
         Ccache(27))/ & 
       (3.*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(  & 
          -MIN2   & 
          + MOU2   & 
          + 2*p1p2   & 
          - 2*p2p4)* & 
         (CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4   & 
          +  & 
           2*(  & 
          -(MIN2**2*p1p4)   & 
          + MIN2*MOU2*p1p4   & 
          +  & 
              3*MIN2*p1p2*p1p4   & 
          - MOU2*p1p2*p1p4   & 
          -  & 
              2*p1p2**2*p1p4   & 
          - MIN2*p1p4**2   & 
          + MOU2*p1p4**2   & 
          +  & 
              2*p1p4**3   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (  & 
          -MIN2   & 
          + MOU2   & 
          + 2*p1p2   & 
          + p1p4   & 
          - 2*p2p4)* & 
               (p1p4   & 
          - p2p4)   & 
          -  & 
              sqrt(MIN2)*np4* & 
               (  & 
          -MIN2   & 
          + MOU2   & 
          + p1p2   & 
          + 2*p1p4   & 
          - 2*p2p4)* & 
               (p1p4   & 
          - p2p4)   & 
          - 2*MIN2*p1p2*p2p4   & 
          +  & 
              MOU2*p1p2*p2p4   & 
          + 2*p1p2**2*p2p4   & 
          +  & 
              MIN2*p1p4*p2p4   & 
          - MOU2*p1p4*p2p4   & 
          +  & 
              2*p1p2*p1p4*p2p4   & 
          - 4*p1p4**2*p2p4   & 
          -  & 
              2*p1p2*p2p4**2   & 
          + 2*p1p4*p2p4**2))* & 
         Ccache(28))/ & 
       (3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (4*MIN2**2*p1p4   & 
          - 10*MIN2*MOU2*p1p4   & 
          +  & 
              6*MOU2**2*p1p4   & 
          - 11*MIN2*p1p2*p1p4   & 
          +  & 
              10*MOU2*p1p2*p1p4   & 
          + 7*p1p2**2*p1p4   & 
          -  & 
              3*MIN2*p1p4**2   & 
          + 5*p1p2*p1p4**2   & 
          -  & 
              16*MIN2**2*p2p4   & 
          + 16*MIN2*MOU2*p2p4   & 
          +  & 
              38*MIN2*p1p2*p2p4   & 
          - 16*MOU2*p1p2*p2p4   & 
          -  & 
              26*p1p2**2*p2p4   & 
          + 25*MIN2*p1p4*p2p4   & 
          -  & 
              20*MOU2*p1p4*p2p4   & 
          - 31*p1p2*p1p4*p2p4   & 
          -  & 
              29*MIN2*p2p4**2   & 
          + 12*MOU2*p2p4**2   & 
          +  & 
              41*p1p2*p2p4**2   & 
          + 16*p1p4*p2p4**2   & 
          - 16*p2p4**3) & 
          - 2*(4*MIN2**3*p1p2*p1p4   & 
          -  & 
              4*MIN2**2*MOU2*p1p2*p1p4   & 
          -  & 
              24*MIN2**2*p1p2**2*p1p4   & 
          +  & 
              16*MIN2*MOU2*p1p2**2*p1p4   & 
          +  & 
              36*MIN2*p1p2**3*p1p4   & 
          - 12*MOU2*p1p2**3*p1p4   & 
          -  & 
              16*p1p2**4*p1p4   & 
          + 7*MIN2**3*p1p4**2   & 
          -  & 
              14*MIN2**2*MOU2*p1p4**2   & 
          +  & 
              7*MIN2*MOU2**2*p1p4**2   & 
          -  & 
              27*MIN2**2*p1p2*p1p4**2   & 
          +  & 
              30*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
              3*MOU2**2*p1p2*p1p4**2   & 
          +  & 
              36*MIN2*p1p2**2*p1p4**2   & 
          -  & 
              12*MOU2*p1p2**2*p1p4**2   & 
          - 16*p1p2**3*p1p4**2   & 
          -  & 
              4*MIN2**2*p1p4**3   & 
          + 4*MIN2*MOU2*p1p4**3   & 
          +  & 
              12*MIN2*p1p2*p1p4**3   & 
          - 4*MOU2*p1p2*p1p4**3   & 
          -  & 
              8*p1p2**2*p1p4**3   & 
          - 4*MIN2**4*p2p4   & 
          +  & 
              4*MIN2**3*MOU2*p2p4   & 
          + 24*MIN2**3*p1p2*p2p4   & 
          -  & 
              16*MIN2**2*MOU2*p1p2*p2p4   & 
          -  & 
              36*MIN2**2*p1p2**2*p2p4   & 
          +  & 
              12*MIN2*MOU2*p1p2**2*p2p4   & 
          +  & 
              16*MIN2*p1p2**3*p2p4   & 
          +  & 
              20*MIN2**2*p1p2*p1p4*p2p4   & 
          -  & 
              12*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
              52*MIN2*p1p2**2*p1p4*p2p4   & 
          +  & 
              12*MOU2*p1p2**2*p1p4*p2p4   & 
          +  & 
              32*p1p2**3*p1p4*p2p4   & 
          +  & 
              20*MIN2**2*p1p4**2*p2p4   & 
          -  & 
              20*MIN2*MOU2*p1p4**2*p2p4   & 
          -  & 
              52*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
              12*MOU2*p1p2*p1p4**2*p2p4   & 
          +  & 
              32*p1p2**2*p1p4**2*p2p4   & 
          -  & 
              8*MIN2*p1p4**3*p2p4   & 
          + 8*p1p2*p1p4**3*p2p4   & 
          -  & 
              20*MIN2**3*p2p4**2   & 
          + 12*MIN2**2*MOU2*p2p4**2   & 
          +  & 
              52*MIN2**2*p1p2*p2p4**2   & 
          -  & 
              12*MIN2*MOU2*p1p2*p2p4**2   & 
          -  & 
              32*MIN2*p1p2**2*p2p4**2   & 
          -  & 
              2*MIN2**2*p1p4*p2p4**2   & 
          +  & 
              6*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
              26*MIN2*p1p2*p1p4*p2p4**2   & 
          -  & 
              6*MOU2*p1p2*p1p4*p2p4**2   & 
          -  & 
              24*p1p2**2*p1p4*p2p4**2   & 
          +  & 
              16*MIN2*p1p4**2*p2p4**2   & 
          -  & 
              16*p1p2*p1p4**2*p2p4**2   & 
          - 16*MIN2**2*p2p4**3   & 
          +  & 
              16*MIN2*p1p2*p2p4**3   & 
          - 8*MIN2*p1p4*p2p4**3   & 
          +  & 
              8*p1p2*p1p4*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np4*(MIN2   & 
          - p1p2)* & 
               (  & 
          -6*MIN2**2*p1p4   & 
          + 12*MIN2*MOU2*p1p4   & 
          -  & 
                 6*MOU2**2*p1p4   & 
          + 14*MIN2*p1p2*p1p4   & 
          -  & 
                 14*MOU2*p1p2*p1p4   & 
          - 8*p1p2**2*p1p4   & 
          +  & 
                 4*MIN2*p1p4**2   & 
          - 4*MOU2*p1p4**2   & 
          -  & 
                 8*p1p2*p1p4**2   & 
          + 12*MIN2**2*p2p4   & 
          -  & 
                 12*MIN2*MOU2*p2p4   & 
          - 28*MIN2*p1p2*p2p4   & 
          +  & 
                 12*MOU2*p1p2*p2p4   & 
          + 16*p1p2**2*p2p4   & 
          -  & 
                 22*MIN2*p1p4*p2p4   & 
          + 22*MOU2*p1p4*p2p4   & 
          +  & 
                 32*p1p2*p1p4*p2p4   & 
          + 8*p1p4**2*p2p4   & 
          +  & 
                 28*MIN2*p2p4**2   & 
          - 12*MOU2*p2p4**2   & 
          -  & 
                 32*p1p2*p2p4**2   & 
          - 24*p1p4*p2p4**2   & 
          +  & 
                 16*p2p4**3)   & 
          +  & 
              sqrt(MIN2)*np2* & 
               (  & 
          -12*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
                 28*MIN2*p1p2**2*p1p4   & 
          +  & 
                 12*MOU2*p1p2**2*p1p4   & 
          -  & 
                 2*MIN2*MOU2*p1p4**2   & 
          - 3*MOU2**2*p1p4**2   & 
          -  & 
                 14*MIN2*p1p2*p1p4**2   & 
          -  & 
                 2*MOU2*p1p2*p1p4**2   & 
          - 12*MIN2**3*p2p4   & 
          +  & 
                 12*MIN2**2*MOU2*p2p4   & 
          -  & 
                 12*MIN2*MOU2*p1p2*p2p4   & 
          -  & 
                 16*MIN2*p1p2**2*p2p4   & 
          +  & 
                 16*MIN2*p1p2*p1p4*p2p4   & 
          +  & 
                 6*MIN2*p1p4**2*p2p4   & 
          +  & 
                 10*MOU2*p1p4**2*p2p4   & 
          +  & 
                 16*MIN2*p1p2*p2p4**2   & 
          +  & 
                 6*MIN2*p1p4*p2p4**2   & 
          - 6*MOU2*p1p4*p2p4**2   & 
          +  & 
                 MIN2**2* & 
                  (12*p1p2*p1p4   & 
          + 5*p1p4**2   & 
          + 28*p1p2*p2p4   & 
          -  & 
                    16*p2p4**2)   & 
          +  & 
                 2*p1p4*(p1p2   & 
          - p2p4)* & 
                  (8*p1p2**2   & 
          + 4*p1p2*p1p4   & 
          + 4*p1p4*p2p4   & 
          -  & 
                    4*p2p4**2))))* & 
         Dcache(1))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (4*CMPLX(aa,bb)*asym*EL**4*GF**2*sqrt(MIN2)* & 
         (MIN2*p1p4   & 
          - MOU2*p1p4   & 
          - 2*p1p2*p1p4   & 
          - 2*p1p4**2   & 
          -  & 
           MIN2*p2p4   & 
          + MOU2*p2p4   & 
          + 4*p1p2*p2p4   & 
          +  & 
           6*p1p4*p2p4   & 
          - 4*p2p4**2)* & 
         Dcache(2))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*CMPLX(aa,bb)*asym*EL**4*GF**2*sqrt(MIN2)* & 
         (  & 
          -16*MIN2**2*p1p4   & 
          + 22*MIN2*MOU2*p1p4   & 
          -  & 
           6*MOU2**2*p1p4   & 
          + 39*MIN2*p1p2*p1p4   & 
          -  & 
           22*MOU2*p1p2*p1p4   & 
          - 23*p1p2**2*p1p4   & 
          +  & 
           20*MIN2*p1p4**2   & 
          - 19*MOU2*p1p4**2   & 
          -  & 
           30*p1p2*p1p4**2   & 
          + 3*p1p4**3   & 
          + 16*MIN2**2*p2p4   & 
          -  & 
           16*MIN2*MOU2*p2p4   & 
          - 38*MIN2*p1p2*p2p4   & 
          +  & 
           16*MOU2*p1p2*p2p4   & 
          + 26*p1p2**2*p2p4   & 
          -  & 
           37*MIN2*p1p4*p2p4   & 
          + 23*MOU2*p1p4*p2p4   & 
          +  & 
           38*p1p2*p1p4*p2p4   & 
          + 20*p1p4**2*p2p4   & 
          +  & 
           29*MIN2*p2p4**2   & 
          - 12*MOU2*p2p4**2   & 
          -  & 
           41*p1p2*p2p4**2   & 
          - 14*p1p4*p2p4**2   & 
          + 16*p2p4**3)* & 
         Dcache(3))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*sqrt(MIN2)* & 
         (10*CMPLX(aa,bb)*asym*MIN2**2*p1p4   & 
          - 10*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4   & 
          -  & 
           25*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4   & 
          + 5*CMPLX(aa,bb)*asym*MOU2*p1p2*p1p4   & 
          +  & 
           15*CMPLX(aa,bb)*asym*p1p2**2*p1p4   & 
          - 5*CMPLX(aa,bb)*asym*MIN2*p1p4**2   & 
          +  & 
           5*CMPLX(aa,bb)*asym*MOU2*p1p4**2   & 
          + 7*CMPLX(aa,bb)*asym*p1p2*p1p4**2   & 
          -  & 
           15*CMPLX(aa,bb)*asym*MIN2**2*p2p4   & 
          + 7*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4   & 
          +  & 
           8*CMPLX(aa,bb)*asym*MOU2**2*p2p4   & 
          + 40*CMPLX(aa,bb)*asym*MIN2*p1p2*p2p4   & 
          -  & 
           2*CMPLX(aa,bb)*asym*MOU2*p1p2*p2p4   & 
          - 29*CMPLX(aa,bb)*asym*p1p2**2*p2p4   & 
          +  & 
           10*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4   & 
          - 8*CMPLX(aa,bb)*asym*p1p2*p1p4*p2p4   & 
          -  & 
           3*CMPLX(aa,bb)*asym*p1p4**2*p2p4   & 
          - 33*CMPLX(aa,bb)*asym*MIN2*p2p4**2   & 
          +  & 
           2*CMPLX(aa,bb)*asym*MOU2*p2p4**2   & 
          + 50*CMPLX(aa,bb)*asym*p1p2*p2p4**2   & 
          -  & 
           4*CMPLX(aa,bb)*asym*p1p4*p2p4**2   & 
          - 18*CMPLX(aa,bb)*asym*p2p4**3   & 
          -  & 
           2*np2*(  & 
          -12*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
              14*MIN2*p1p2**2*p1p4   & 
          + 14*MOU2*p1p2**2*p1p4   & 
          -  & 
              2*MIN2*MOU2*p1p4**2   & 
          - 2*MIN2*p1p2*p1p4**2   & 
          +  & 
              2*MOU2*p1p2*p1p4**2   & 
          - 6*MIN2**3*p2p4   & 
          +  & 
              12*MIN2**2*MOU2*p2p4   & 
          -  & 
              14*MIN2*MOU2*p1p2*p2p4   & 
          - 8*MIN2*p1p2**2*p2p4   & 
          -  & 
              2*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              6*MOU2*p1p2*p1p4*p2p4   & 
          - 2*MIN2*p1p4**2*p2p4   & 
          +  & 
              2*MOU2*p1p4**2*p2p4   & 
          + 14*MIN2*MOU2*p2p4**2   & 
          +  & 
              22*MIN2*p1p2*p2p4**2   & 
          - 6*MOU2*p1p2*p2p4**2   & 
          +  & 
              16*MIN2*p1p4*p2p4**2   & 
          - 8*MOU2*p1p4*p2p4**2   & 
          -  & 
              14*MIN2*p2p4**3   & 
          + 6*MOU2*p2p4**3   & 
          +  & 
              MOU2**2*(6*p1p2*p1p4   & 
          + p1p4**2   & 
          - 6*MIN2*p2p4   & 
          -  & 
                 3*p1p4*p2p4)   & 
          +  & 
              MIN2**2*(6*p1p2*p1p4   & 
          + p1p4**2   & 
          +  & 
                 14*p1p2*p2p4   & 
          + 3*p1p4*p2p4   & 
          - 14*p2p4**2)   & 
          +  & 
              2*(p1p2   & 
          - p2p4)* & 
               (4*p1p2**2*p1p4   & 
          + 4*p1p2*p1p4*p2p4   & 
          +  & 
                 4*p1p4**2*p2p4   & 
          - 4*p1p2*p2p4**2   & 
          -  & 
                 8*p1p4*p2p4**2   & 
          + 4*p2p4**3)))* & 
         Dcache(4))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*EL**4*GF**2*sqrt(MIN2)* & 
         (  & 
          -12*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4   & 
          + 12*CMPLX(aa,bb)*asym*MOU2*p1p2*p1p4   & 
          +  & 
           16*CMPLX(aa,bb)*asym*p1p2**2*p1p4   & 
          - 4*CMPLX(aa,bb)*asym*MIN2*p1p4**2   & 
          +  & 
           4*CMPLX(aa,bb)*asym*MOU2*p1p4**2   & 
          + 3*CMPLX(aa,bb)*asym*p1p2*p1p4**2   & 
          +  & 
           7*CMPLX(aa,bb)*asym*p1p4**3   & 
          + 12*CMPLX(aa,bb)*asym*MIN2**2*p2p4   & 
          -  & 
           12*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4   & 
          - 16*CMPLX(aa,bb)*asym*MIN2*p1p2*p2p4   & 
          +  & 
           14*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4   & 
          - 4*CMPLX(aa,bb)*asym*MOU2*p1p4*p2p4   & 
          -  & 
           22*CMPLX(aa,bb)*asym*p1p2*p1p4*p2p4   & 
          - 30*CMPLX(aa,bb)*asym*p1p4**2*p2p4   & 
          +  & 
           5*CMPLX(aa,bb)*asym*MIN2*p2p4**2   & 
          + 4*CMPLX(aa,bb)*asym*MOU2*p2p4**2   & 
          +  & 
           10*CMPLX(aa,bb)*asym*p1p2*p2p4**2   & 
          + 16*CMPLX(aa,bb)*asym*p1p4*p2p4**2   & 
          -  & 
           9*CMPLX(aa,bb)*asym*p2p4**3   & 
          +  & 
           2*np4*(  & 
          -12*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
              14*MIN2*p1p2**2*p1p4   & 
          + 14*MOU2*p1p2**2*p1p4   & 
          -  & 
              2*MIN2*MOU2*p1p4**2   & 
          - 2*MIN2*p1p2*p1p4**2   & 
          +  & 
              2*MOU2*p1p2*p1p4**2   & 
          - 6*MIN2**3*p2p4   & 
          +  & 
              12*MIN2**2*MOU2*p2p4   & 
          -  & 
              14*MIN2*MOU2*p1p2*p2p4   & 
          - 8*MIN2*p1p2**2*p2p4   & 
          -  & 
              2*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              6*MOU2*p1p2*p1p4*p2p4   & 
          - 2*MIN2*p1p4**2*p2p4   & 
          +  & 
              2*MOU2*p1p4**2*p2p4   & 
          + 14*MIN2*MOU2*p2p4**2   & 
          +  & 
              22*MIN2*p1p2*p2p4**2   & 
          - 6*MOU2*p1p2*p2p4**2   & 
          +  & 
              16*MIN2*p1p4*p2p4**2   & 
          - 8*MOU2*p1p4*p2p4**2   & 
          -  & 
              14*MIN2*p2p4**3   & 
          + 6*MOU2*p2p4**3   & 
          +  & 
              MOU2**2*(6*p1p2*p1p4   & 
          + p1p4**2   & 
          - 6*MIN2*p2p4   & 
          -  & 
                 3*p1p4*p2p4)   & 
          +  & 
              MIN2**2*(6*p1p2*p1p4   & 
          + p1p4**2   & 
          +  & 
                 14*p1p2*p2p4   & 
          + 3*p1p4*p2p4   & 
          - 14*p2p4**2)   & 
          +  & 
              2*(p1p2   & 
          - p2p4)* & 
               (4*p1p2**2*p1p4   & 
          + 4*p1p2*p1p4*p2p4   & 
          +  & 
                 4*p1p4**2*p2p4   & 
          - 4*p1p2*p2p4**2   & 
          -  & 
                 8*p1p4*p2p4**2   & 
          + 4*p2p4**3)))* & 
         Dcache(5))/(3.*p1p4*(p1p4   & 
          - p2p4)*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T1BOXAC
                          !!!!!!!!!!!!!!!!!!!!!!
