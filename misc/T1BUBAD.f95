
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T1BUBAD
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T1BUBAD(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
#ifdef COLLIER
      use collier
#endif
      implicit none
!

#ifndef COLLIER
    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
#else
    complex(kind=prec) :: uvdump(2)
#endif
    complex(kind=prec) :: Bcache(4)

!
      real(kind=prec) T1BUBAD,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb= & 
          -1._prec
!     

#ifndef COLLIER
!     
      call setlambda(0._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Bcache(1) = B0i(bb0,MIN2-2*p1p4,MIN2,0._prec)
    Bcache(2) = B0i(bb0,MOU2+2*p1p4-2*p2p4,MOU2,0._prec)
    Bcache(3) = B0i(bb1,MIN2-2*p1p4,MIN2,0._prec)
    Bcache(4) = B0i(bb1,MOU2+2*p1p4-2*p2p4,MOU2,0._prec)

#else
    call B_cll(Bcache(1::2), uvdump, cmplx(MIN2-2*p1p4),cmplx(MIN2),cmplx(0.),1)
    call B_cll(Bcache(2::2), uvdump, cmplx(MOU2+2*p1p4-2*p2p4),cmplx(MOU2),cmplx(0._prec),1)
#endif
!
      T1BUBAD=real(        ( & 
          -4*EL**4*GF**2*sqrt(MIN2)* & 
         (3*CMPLX(aa,bb)*asym*MIN2**2*p1p4  & 
          - 3*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4  & 
          -  & 
           4*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4  & 
          + 2*MIN2**2.5*p1p2*p1p4  & 
          -  & 
           2*MIN2**1.5*MOU2*p1p2*p1p4  & 
          +  & 
           6*MIN2**2*np2*p1p2*p1p4  & 
          -  & 
           6*MIN2*MOU2*np2*p1p2*p1p4  & 
          -  & 
           10*MIN2**1.5*p1p2**2*p1p4  & 
          +  & 
           6*sqrt(MIN2)*MOU2*p1p2**2*p1p4  & 
          -  & 
           8*MIN2*np2*p1p2**2*p1p4  & 
          +  & 
           8*sqrt(MIN2)*p1p2**3*p1p4  & 
          - 4*CMPLX(aa,bb)*asym*MIN2*p1p4**2  & 
          -  & 
           2*MIN2**2.5*p1p4**2  & 
          + 2*CMPLX(aa,bb)*asym*MOU2*p1p4**2  & 
          +  & 
           2*MIN2**1.5*MOU2*p1p4**2  & 
          -  & 
           6*MIN2**2*np2*p1p4**2  & 
          + 6*MIN2*MOU2*np2*p1p4**2  & 
          +  & 
           4*CMPLX(aa,bb)*asym*p1p2*p1p4**2  & 
          + 11*MIN2**1.5*p1p2*p1p4**2  & 
          -  & 
           7*sqrt(MIN2)*MOU2*p1p2*p1p4**2  & 
          +  & 
           6*MIN2*np2*p1p2*p1p4**2  & 
          -  & 
           6*sqrt(MIN2)*p1p2**2*p1p4**2  & 
          -  & 
           4*MIN2**1.5*p1p4**3  & 
          + 4*sqrt(MIN2)*MOU2*p1p4**3  & 
          +  & 
           2*MOU2*np2*p1p4**3  & 
          + 4*sqrt(MIN2)*p1p2*p1p4**3  & 
          +  & 
           4*np2*p1p2*p1p4**3  & 
          - 2*MIN2**3.5*p2p4  & 
          +  & 
           2*MIN2**2.5*MOU2*p2p4  & 
          - 6*MIN2**3*np2*p2p4  & 
          +  & 
           6*MIN2**2*MOU2*np2*p2p4  & 
          +  & 
           10*MIN2**2.5*p1p2*p2p4  & 
          -  & 
           6*MIN2**1.5*MOU2*p1p2*p2p4  & 
          +  & 
           8*MIN2**2*np2*p1p2*p2p4  & 
          -  & 
           8*MIN2**1.5*p1p2**2*p2p4  & 
          +  & 
           4*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4  & 
          + 5*MIN2**2*np2*p1p4*p2p4  & 
          -  & 
           3*MIN2*MOU2*np2*p1p4*p2p4  & 
          +  & 
           3*MIN2**1.5*p1p2*p1p4*p2p4  & 
          -  & 
           3*sqrt(MIN2)*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           4*MIN2*np2*p1p2*p1p4*p2p4  & 
          -  & 
           12*sqrt(MIN2)*p1p2**2*p1p4*p2p4  & 
          -  & 
           4*CMPLX(aa,bb)*asym*p1p4**2*p2p4  & 
          - 4*MIN2**1.5*p1p4**2*p2p4  & 
          -  & 
           8*MIN2*np2*p1p4**2*p2p4  & 
          +  & 
           2*sqrt(MIN2)*p1p2*p1p4**2*p2p4  & 
          -  & 
           2*sqrt(MIN2)*p1p4**3*p2p4  & 
          - 4*np2*p1p4**3*p2p4  & 
          -  & 
           10*MIN2**2.5*p2p4**2  & 
          + 6*MIN2**1.5*MOU2*p2p4**2  & 
          -  & 
           8*MIN2**2*np2*p2p4**2  & 
          +  & 
           16*MIN2**1.5*p1p2*p2p4**2  & 
          +  & 
           6*MIN2**1.5*p1p4*p2p4**2  & 
          +  & 
           4*MIN2*np2*p1p4*p2p4**2  & 
          +  & 
           4*sqrt(MIN2)*p1p2*p1p4*p2p4**2  & 
          +  & 
           2*sqrt(MIN2)*p1p4**2*p2p4**2  & 
          -  & 
           8*MIN2**1.5*p2p4**3  & 
          +  & 
           np4*( & 
          -3*MIN2**3*p1p4  & 
          + 6*MIN2**2*MOU2*p1p4  & 
          -  & 
              3*MIN2*MOU2**2*p1p4  & 
          - 7*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
              4*MIN2*p1p2**2*p1p4  & 
          - 4*MIN2*MOU2*p1p4**2  & 
          -  & 
              4*MIN2*p1p2*p1p4**2  & 
          - 2*MOU2*p1p2*p1p4**2  & 
          -  & 
              4*p1p2*p1p4**2*(p1p2  & 
          - p2p4)  & 
          +  & 
              6*MIN2**3*p2p4  & 
          - 6*MIN2**2*MOU2*p2p4  & 
          +  & 
              6*MIN2*MOU2*p1p2*p2p4  & 
          + 8*MIN2*p1p2**2*p2p4  & 
          +  & 
              14*MIN2*MOU2*p1p4*p2p4  & 
          +  & 
              22*MIN2*p1p2*p1p4*p2p4  & 
          + 6*MIN2*p1p4**2*p2p4  & 
          -  & 
              6*MIN2*MOU2*p2p4**2  & 
          - 16*MIN2*p1p2*p2p4**2  & 
          -  & 
              18*MIN2*p1p4*p2p4**2  & 
          + 8*MIN2*p2p4**3  & 
          +  & 
              MIN2**2*(7*p1p2*p1p4  & 
          + 4*p1p4**2  & 
          -  & 
                 14*p1p2*p2p4  & 
          - 16*p1p4*p2p4  & 
          + 14*p2p4**2)))* & 
         Bcache(1))/ & 
       (3.*p1p4**3*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (4*EL**4*GF**2*MOU2* & 
         ( & 
          -2*MIN2**3*p1p4  & 
          + 4*MIN2**2*MOU2*p1p4  & 
          -  & 
           2*MIN2*MOU2**2*p1p4  & 
          + 3*MIN2**2.5*np4*p1p4  & 
          -  & 
           6*MIN2**1.5*MOU2*np4*p1p4  & 
          +  & 
           3*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
          +  & 
           12*MIN2**2*p1p2*p1p4  & 
          - 18*MIN2*MOU2*p1p2*p1p4  & 
          +  & 
           6*MOU2**2*p1p2*p1p4  & 
          - 7*MIN2**1.5*np4*p1p2*p1p4  & 
          +  & 
           7*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
          -  & 
           18*MIN2*p1p2**2*p1p4  & 
          + 14*MOU2*p1p2**2*p1p4  & 
          +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
          + 8*p1p2**3*p1p4  & 
          -  & 
           MIN2**2*p1p4**2  & 
          + 4*MIN2*MOU2*p1p4**2  & 
          -  & 
           3*MOU2**2*p1p4**2  & 
          - 5*MIN2*p1p2*p1p4**2  & 
          +  & 
           MOU2*p1p2*p1p4**2  & 
          -  & 
           2*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
          +  & 
           6*p1p2**2*p1p4**2  & 
          + 4*p1p2*p1p4**3  & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            ( & 
          -3*MIN2  & 
          + 3*MOU2  & 
          + 4*p1p2  & 
          + 2*p1p4  & 
          - 4*p2p4)* & 
            (p1p4  & 
          - p2p4)  & 
          + 2*MIN2**3*p2p4  & 
          -  & 
           2*MIN2**2*MOU2*p2p4  & 
          - 3*MIN2**2.5*np4*p2p4  & 
          +  & 
           3*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
          -  & 
           12*MIN2**2*p1p2*p2p4  & 
          + 8*MIN2*MOU2*p1p2*p2p4  & 
          +  & 
           7*MIN2**1.5*np4*p1p2*p2p4  & 
          +  & 
           sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
          +  & 
           18*MIN2*p1p2**2*p2p4  & 
          - 6*MOU2*p1p2**2*p2p4  & 
          -  & 
           4*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
          - 8*p1p2**3*p2p4  & 
          -  & 
           7*MIN2**2*p1p4*p2p4  & 
          + 10*MIN2*MOU2*p1p4*p2p4  & 
          -  & 
           3*MOU2**2*p1p4*p2p4  & 
          + 4*MIN2**1.5*np4*p1p4*p2p4  & 
          -  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
          +  & 
           31*MIN2*p1p2*p1p4*p2p4  & 
          - 23*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           24*p1p2**2*p1p4*p2p4  & 
          + 2*MIN2*p1p4**2*p2p4  & 
          +  & 
           2*MOU2*p1p4**2*p2p4  & 
          +  & 
           2*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
          -  & 
           14*p1p2*p1p4**2*p2p4  & 
          - 2*p1p4**3*p2p4  & 
          +  & 
           8*MIN2**2*p2p4**2  & 
          - 4*MIN2*MOU2*p2p4**2  & 
          -  & 
           4*MIN2**1.5*np4*p2p4**2  & 
          -  & 
           6*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
          -  & 
           26*MIN2*p1p2*p2p4**2  & 
          + 6*MOU2*p1p2*p2p4**2  & 
          +  & 
           2*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
          +  & 
           18*p1p2**2*p2p4**2  & 
          - 8*MIN2*p1p4*p2p4**2  & 
          +  & 
           6*MOU2*p1p4*p2p4**2  & 
          -  & 
           4*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
          +  & 
           20*p1p2*p1p4*p2p4**2  & 
          + 4*p1p4**2*p2p4**2  & 
          +  & 
           6*MIN2*p2p4**3  & 
          + 2*sqrt(MIN2)*np4*p2p4**3  & 
          -  & 
           10*p1p2*p2p4**3  & 
          - 2*p1p4*p2p4**3  & 
          +  & 
           2*sqrt(MIN2)*np2* & 
            (MOU2*p1p4  & 
          +  & 
              (p1p4  & 
          - p2p4)*( & 
          -MIN2  & 
          + p1p2  & 
          + p1p4  & 
          - p2p4))* & 
            (3*MIN2  & 
          - 3*MOU2  & 
          - 4*p1p2  & 
          - p1p4  & 
          + 5*p2p4))* & 
         Bcache(2))/ & 
       (3.*p1p4*(p1p4  & 
          - p2p4)**3*Pi**2)  & 
          +  & 
      (EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            ( & 
          -6*MIN2**2*p1p4  & 
          + 6*MIN2*MOU2*p1p4  & 
          +  & 
              8*MIN2*p1p2*p1p4  & 
          + 14*MIN2*p1p4**2  & 
          -  & 
              10*MOU2*p1p4**2  & 
          - 16*p1p2*p1p4**2  & 
          -  & 
              4*p1p4**3  & 
          - 8*MIN2*p1p4*p2p4  & 
          + 16*p1p4**2*p2p4) & 
          - 2*(2*MIN2**3*p1p2*p1p4  & 
          -  & 
              2*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
              10*MIN2**2*p1p2**2*p1p4  & 
          +  & 
              6*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
              8*MIN2*p1p2**3*p1p4  & 
          - 2*MIN2**3*p1p4**2  & 
          +  & 
              2*MIN2**2*MOU2*p1p4**2  & 
          +  & 
              9*MIN2**2*p1p2*p1p4**2  & 
          -  & 
              5*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
              4*MIN2*p1p2**2*p1p4**2  & 
          -  & 
              6*MOU2*p1p2**2*p1p4**2  & 
          - 8*p1p2**3*p1p4**2  & 
          -  & 
              5*MIN2**2*p1p4**3  & 
          + 8*MIN2*MOU2*p1p4**3  & 
          -  & 
              3*MOU2**2*p1p4**3  & 
          + 2*MIN2*p1p2*p1p4**3  & 
          -  & 
              2*MOU2*p1p2*p1p4**3  & 
          + 6*MIN2*p1p4**4  & 
          -  & 
              6*MOU2*p1p4**4  & 
          - 8*p1p2*p1p4**4  & 
          -  & 
              2*MIN2**4*p2p4  & 
          + 2*MIN2**3*MOU2*p2p4  & 
          +  & 
              10*MIN2**3*p1p2*p2p4  & 
          -  & 
              6*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
              8*MIN2**2*p1p2**2*p2p4  & 
          + 2*MIN2**3*p1p4*p2p4  & 
          -  & 
              2*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
              7*MIN2**2*p1p2*p1p4*p2p4  & 
          +  & 
              3*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
              4*MIN2*p1p2**2*p1p4*p2p4  & 
          -  & 
              3*MIN2**2*p1p4**2*p2p4  & 
          -  & 
              MIN2*MOU2*p1p4**2*p2p4  & 
          -  & 
              6*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
              6*MOU2*p1p2*p1p4**2*p2p4  & 
          +  & 
              16*p1p2**2*p1p4**2*p2p4  & 
          -  & 
              10*MIN2*p1p4**3*p2p4  & 
          + 12*MOU2*p1p4**3*p2p4  & 
          +  & 
              16*p1p2*p1p4**3*p2p4  & 
          + 8*p1p4**4*p2p4  & 
          -  & 
              10*MIN2**3*p2p4**2  & 
          + 6*MIN2**2*MOU2*p2p4**2  & 
          +  & 
              16*MIN2**2*p1p2*p2p4**2  & 
          +  & 
              16*MIN2**2*p1p4*p2p4**2  & 
          -  & 
              6*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
              12*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
              4*MIN2*p1p4**2*p2p4**2  & 
          -  & 
              6*MOU2*p1p4**2*p2p4**2  & 
          -  & 
              16*p1p2*p1p4**2*p2p4**2  & 
          - 16*p1p4**3*p2p4**2  & 
          -  & 
              8*MIN2**2*p2p4**3  & 
          + 8*MIN2*p1p4*p2p4**3  & 
          +  & 
              8*p1p4**2*p2p4**3  & 
          +  & 
              sqrt(MIN2)*np2* & 
               (6*MIN2**2*p1p2*p1p4  & 
          -  & 
                 6*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
                 8*MIN2*p1p2**2*p1p4  & 
          - 6*MIN2**2*p1p4**2  & 
          +  & 
                 6*MIN2*MOU2*p1p4**2  & 
          + 6*MOU2*p1p2*p1p4**2  & 
          +  & 
                 8*p1p2**2*p1p4**2  & 
          + 5*MIN2*p1p4**3  & 
          -  & 
                 3*MOU2*p1p4**3  & 
          - 2*p1p4**4  & 
          -  & 
                 6*MIN2**3*p2p4  & 
          + 6*MIN2**2*MOU2*p2p4  & 
          +  & 
                 8*MIN2**2*p1p2*p2p4  & 
          +  & 
                 11*MIN2**2*p1p4*p2p4  & 
          -  & 
                 9*MIN2*MOU2*p1p4*p2p4  & 
          -  & 
                 4*MIN2*p1p2*p1p4*p2p4  & 
          -  & 
                 10*MIN2*p1p4**2*p2p4  & 
          -  & 
                 8*p1p2*p1p4**2*p2p4  & 
          - 8*MIN2**2*p2p4**2  & 
          +  & 
                 12*MIN2*p1p4*p2p4**2)  & 
          +  & 
              sqrt(MIN2)*np4* & 
               ( & 
          -3*MIN2**3*p1p4  & 
          + 6*MIN2**2*MOU2*p1p4  & 
          -  & 
                 7*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
                 4*MIN2*p1p2**2*p1p4  & 
          -  & 
                 3*MOU2**2*(MIN2  & 
          - p1p4)*p1p4  & 
          -  & 
                 10*MIN2*MOU2*p1p4**2  & 
          -  & 
                 11*MIN2*p1p2*p1p4**2  & 
          +  & 
                 5*MOU2*p1p2*p1p4**2  & 
          - 6*MIN2*p1p4**3  & 
          +  & 
                 6*MOU2*p1p4**3  & 
          + 10*p1p2*p1p4**3  & 
          +  & 
                 6*MIN2**3*p2p4  & 
          - 6*MIN2**2*MOU2*p2p4  & 
          +  & 
                 6*MIN2*MOU2*p1p2*p2p4  & 
          +  & 
                 8*MIN2*p1p2**2*p2p4  & 
          +  & 
                 20*MIN2*MOU2*p1p4*p2p4  & 
          +  & 
                 36*MIN2*p1p2*p1p4*p2p4  & 
          -  & 
                 6*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
                 8*p1p2**2*p1p4*p2p4  & 
          +  & 
                 20*MIN2*p1p4**2*p2p4  & 
          -  & 
                 12*MOU2*p1p4**2*p2p4  & 
          -  & 
                 16*p1p2*p1p4**2*p2p4  & 
          - 8*p1p4**3*p2p4  & 
          -  & 
                 6*MIN2*MOU2*p2p4**2  & 
          -  & 
                 16*MIN2*p1p2*p2p4**2  & 
          -  & 
                 32*MIN2*p1p4*p2p4**2  & 
          +  & 
                 6*MOU2*p1p4*p2p4**2  & 
          +  & 
                 16*p1p2*p1p4*p2p4**2  & 
          + 16*p1p4**2*p2p4**2  & 
          +  & 
                 8*MIN2*p2p4**3  & 
          - 8*p1p4*p2p4**3  & 
          +  & 
                 MIN2**2* & 
                  (7*p1p2*p1p4  & 
          + 7*p1p4**2  & 
          - 14*p1p2*p2p4  & 
          -  & 
                    22*p1p4*p2p4  & 
          + 14*p2p4**2))))* & 
         Bcache(3))/ & 
       (3.*p1p4**3*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (EL**4*GF**2*( & 
          -(CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4  & 
          - p2p4)* & 
              (6*MIN2*MOU2  & 
          - 6*MOU2**2  & 
          - 8*MOU2*p1p2  & 
          +  & 
                6*MIN2*p1p4  & 
          - 10*MOU2*p1p4  & 
          - 8*p1p2*p1p4  & 
          -  & 
                4*p1p4**2  & 
          - 6*MIN2*p2p4  & 
          + 14*MOU2*p2p4  & 
          +  & 
                8*p1p2*p2p4  & 
          + 12*p1p4*p2p4  & 
          - 8*p2p4**2))  & 
          +  & 
           2*( & 
          -2*MIN2**3*MOU2*p1p4  & 
          +  & 
              4*MIN2**2*MOU2**2*p1p4  & 
          - 2*MIN2*MOU2**3*p1p4  & 
          +  & 
              12*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
              18*MIN2*MOU2**2*p1p2*p1p4  & 
          +  & 
              6*MOU2**3*p1p2*p1p4  & 
          -  & 
              18*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
              14*MOU2**2*p1p2**2*p1p4  & 
          +  & 
              8*MOU2*p1p2**3*p1p4  & 
          - 2*MIN2**3*p1p4**2  & 
          +  & 
              3*MIN2**2*MOU2*p1p4**2  & 
          +  & 
              2*MIN2*MOU2**2*p1p4**2  & 
          - 3*MOU2**3*p1p4**2  & 
          +  & 
              12*MIN2**2*p1p2*p1p4**2  & 
          -  & 
              23*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
              7*MOU2**2*p1p2*p1p4**2  & 
          -  & 
              18*MIN2*p1p2**2*p1p4**2  & 
          +  & 
              20*MOU2*p1p2**2*p1p4**2  & 
          + 8*p1p2**3*p1p4**2  & 
          -  & 
              MIN2**2*p1p4**3  & 
          + 4*MIN2*MOU2*p1p4**3  & 
          -  & 
              3*MOU2**2*p1p4**3  & 
          - 2*MIN2*p1p2*p1p4**3  & 
          +  & 
              2*MOU2*p1p2*p1p4**3  & 
          - 2*MIN2*p1p4**4  & 
          +  & 
              2*MOU2*p1p4**4  & 
          + 8*p1p2*p1p4**4  & 
          +  & 
              2*MIN2**3*MOU2*p2p4  & 
          - 2*MIN2**2*MOU2**2*p2p4  & 
          -  & 
              12*MIN2**2*MOU2*p1p2*p2p4  & 
          +  & 
              8*MIN2*MOU2**2*p1p2*p2p4  & 
          +  & 
              18*MIN2*MOU2*p1p2**2*p2p4  & 
          -  & 
              6*MOU2**2*p1p2**2*p2p4  & 
          - 8*MOU2*p1p2**3*p2p4  & 
          +  & 
              4*MIN2**3*p1p4*p2p4  & 
          -  & 
              13*MIN2**2*MOU2*p1p4*p2p4  & 
          +  & 
              12*MIN2*MOU2**2*p1p4*p2p4  & 
          -  & 
              3*MOU2**3*p1p4*p2p4  & 
          -  & 
              24*MIN2**2*p1p2*p1p4*p2p4  & 
          +  & 
              57*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
              29*MOU2**2*p1p2*p1p4*p2p4  & 
          +  & 
              36*MIN2*p1p2**2*p1p4*p2p4  & 
          -  & 
              44*MOU2*p1p2**2*p1p4*p2p4  & 
          -  & 
              16*p1p2**3*p1p4*p2p4  & 
          -  & 
              7*MIN2**2*p1p4**2*p2p4  & 
          +  & 
              9*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
              2*MOU2**2*p1p4**2*p2p4  & 
          +  & 
              32*MIN2*p1p2*p1p4**2*p2p4  & 
          -  & 
              32*MOU2*p1p2*p1p4**2*p2p4  & 
          -  & 
              16*p1p2**2*p1p4**2*p2p4  & 
          +  & 
              4*MIN2*p1p4**3*p2p4  & 
          - 2*MOU2*p1p4**3*p2p4  & 
          -  & 
              16*p1p2*p1p4**3*p2p4  & 
          - 8*p1p4**4*p2p4  & 
          -  & 
              2*MIN2**3*p2p4**2  & 
          + 10*MIN2**2*MOU2*p2p4**2  & 
          -  & 
              4*MIN2*MOU2**2*p2p4**2  & 
          +  & 
              12*MIN2**2*p1p2*p2p4**2  & 
          -  & 
              34*MIN2*MOU2*p1p2*p2p4**2  & 
          +  & 
              6*MOU2**2*p1p2*p2p4**2  & 
          -  & 
              18*MIN2*p1p2**2*p2p4**2  & 
          +  & 
              24*MOU2*p1p2**2*p2p4**2  & 
          + 8*p1p2**3*p2p4**2  & 
          +  & 
              17*MIN2**2*p1p4*p2p4**2  & 
          -  & 
              24*MIN2*MOU2*p1p4*p2p4**2  & 
          +  & 
              9*MOU2**2*p1p4*p2p4**2  & 
          -  & 
              58*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
              46*MOU2*p1p2*p1p4*p2p4**2  & 
          +  & 
              32*p1p2**2*p1p4*p2p4**2  & 
          -  & 
              10*MIN2*p1p4**2*p2p4**2  & 
          +  & 
              6*MOU2*p1p4**2*p2p4**2  & 
          +  & 
              16*p1p2*p1p4**2*p2p4**2  & 
          + 16*p1p4**3*p2p4**2  & 
          -  & 
              9*MIN2**2*p2p4**3  & 
          + 11*MIN2*MOU2*p2p4**3  & 
          +  & 
              28*MIN2*p1p2*p2p4**3  & 
          - 16*MOU2*p1p2*p2p4**3  & 
          -  & 
              16*p1p2**2*p2p4**3  & 
          + 16*MIN2*p1p4*p2p4**3  & 
          -  & 
              6*MOU2*p1p4*p2p4**3  & 
          - 16*p1p2*p1p4*p2p4**3  & 
          -  & 
              8*p1p4**2*p2p4**3  & 
          - 8*MIN2*p2p4**4  & 
          +  & 
              8*p1p2*p2p4**4  & 
          +  & 
              sqrt(MIN2)*np4* & 
               (3*MOU2**3*(p1p4  & 
          + p2p4)  & 
          +  & 
                 (p1p4  & 
          - p2p4)**2* & 
                  (3*MIN2**2  & 
          - 7*MIN2*p1p2  & 
          + 4*p1p2**2  & 
          +  & 
                    2*MIN2*p1p4  & 
          - 6*p1p2*p1p4  & 
          +  & 
                    6*MIN2*p2p4  & 
          - 4*p1p2*p2p4  & 
          + 8*p1p4*p2p4) & 
          + MOU2**2* & 
                  ( & 
          -6*MIN2*p1p4  & 
          + 7*p1p2*p1p4  & 
          + 3*p1p4**2  & 
          +  & 
                    p1p2*p2p4  & 
          - 2*p1p4*p2p4  & 
          - 9*p2p4**2)  & 
          +  & 
                 MOU2*(p1p4  & 
          - p2p4)* & 
                  (3*MIN2**2  & 
          - 7*MIN2*p1p2  & 
          + 4*p1p2**2  & 
          -  & 
                    6*MIN2*p1p4  & 
          + 5*p1p2*p1p4  & 
          - 2*p1p4**2  & 
          +  & 
                    4*MIN2*p2p4  & 
          - p1p2*p2p4  & 
          - 6*p2p4**2))  & 
          +  & 
              sqrt(MIN2)*np2* & 
               ( & 
          -6*MOU2**3*p1p4  & 
          +  & 
                 MOU2*(p1p4  & 
          - p2p4)* & 
                  ( & 
          -6*MIN2**2  & 
          + 14*MIN2*p1p2  & 
          - 8*p1p2**2  & 
          +  & 
                    20*MIN2*p1p4  & 
          - 24*p1p2*p1p4  & 
          -  & 
                    7*p1p4**2  & 
          - 22*MIN2*p2p4  & 
          +  & 
                    24*p1p2*p2p4  & 
          + 31*p1p4*p2p4  & 
          - 16*p2p4**2) & 
          + (p1p4  & 
          - p2p4)**2* & 
                  ( & 
          -6*MIN2**2  & 
          + 14*MIN2*p1p2  & 
          - 8*p1p2**2  & 
          +  & 
                    5*MIN2*p1p4  & 
          - 4*p1p2*p1p4  & 
          - 2*p1p4**2  & 
          -  & 
                    16*MIN2*p2p4  & 
          + 16*p1p2*p2p4  & 
          +  & 
                    4*p1p4*p2p4  & 
          - 8*p2p4**2)  & 
          -  & 
                 2*MOU2**2* & 
                  ( & 
          -6*MIN2*p1p4  & 
          + 7*p1p2*p1p4  & 
          + 7*p1p4**2  & 
          +  & 
                    3*MIN2*p2p4  & 
          - 3*p1p2*p2p4  & 
          -  & 
                    14*p1p4*p2p4  & 
          + 3*p2p4**2))))* & 
         Bcache(4))/ & 
       (3.*p1p4*(p1p4  & 
          - p2p4)**3*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T1BUBAD
                          !!!!!!!!!!!!!!!!!!!!!!
