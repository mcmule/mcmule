
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MISC
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use misc_mat_el

  implicit none

  contains

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!        SINGULAR    LIMITS          !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION EE2NNG_S(p1,p2,q1,q2,lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ee2nng_s
  real (kind=prec), optional :: lin

  if (present(lin)) then
    ee2nng_s = 2*eik(p1,ksoft,p2)*ee2nnav(p1,p2,q1,q2,lin)
    ee2nng_s = (4.*pi*alpha)*ee2nng_s
    lin = lin*2*eik(p1,ksoft,p2)
    lin = lin*(4.*pi*alpha)
  else
    ee2nng_s = 2*eik(p1,ksoft,p2)*ee2nnav(p1,p2,q1,q2)
    ee2nng_s = (4.*pi*alpha)*ee2nng_s
  endif
  END FUNCTION EE2NNG_S

  FUNCTION EE2NNGL_S(p1,p2,q1,q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ee2nngl_s

  ee2nngl_s = 2*eik(p1,ksoft,p2)*ee2nnlav(p1,p2,q1,q2)
  ee2nngl_s = (4.*pi*alpha)*ee2nngl_s

  END FUNCTION EE2NNGL_S



  FUNCTION EE2NNGF_S(p1,p2,q1,q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ee2nngf_s, mat0, lin
  real (kind=prec) :: ctfin, ctpole,scms,epart
  scms = sq(p1+p2)
  Epart = sqrt(scms)

  mat0 = ee2nng_s(p1,p2,q1,q2,lin)
  ctfin = ee2nneik(p1, p2, xieik1, ctpole)

  ee2nngf_s = ee2nngl_s(p1,p2,q1,q2)
  ee2nngf_s = ee2nngf_s+ctfin*mat0+ctpole*lin

  END FUNCTION EE2NNGF_S




  FUNCTION EE2NNGG_SH(p1, p2, p3, p4, q3)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(q3)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(q3)
    !! for massive electrons
    !! average over neutrino tensor taken
  real (kind=prec) :: p1(4),p2(4),q3(4), p3(4), p4(4)
  real (kind=prec) :: ee2nngg_sh

  ee2nngg_sh = 2*eik(p1,ksoftA,p2)*ee2nngav(p1,p2,p3,p4, q3)
  ee2nngg_sh = (4.*pi*alpha)*ee2nngg_sh

  END FUNCTION EE2NNGG_SH
  FUNCTION EE2NNGG_HS(p1, p2, p3, p4, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q4) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q4) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken
  real (kind=prec) :: p1(4),p2(4),q2(4), p3(4), p4(4)
  real (kind=prec) :: ee2nngg_hs

  ee2nngg_hs = 2*eik(p1,ksoftB,p2)*ee2nngav(p1,p2,p3,p4, q2)
  ee2nngg_hs = (4.*pi*alpha)*ee2nngg_hs

  END FUNCTION EE2NNGG_HS
  FUNCTION EE2NNGG_SS(p1, p2, p3, p4)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(ksoft)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(ksoft) y(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken
  real (kind=prec) :: p1(4),p2(4), p3(4), p4(4)
  real (kind=prec) :: ee2nngg_ss

  ee2nngg_ss = (2*eik(p1,ksoftA,p2))*(2*eik(p1,ksoftB,p2))*ee2nnav(p1,p2, p3, p4)
  ee2nngg_ss = (4.*pi*alpha)**2*ee2nngg_ss

  END FUNCTION EE2NNGG_SS



                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MISC
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!

