
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MISC_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use misc_ee2nngg, only: ee2nnggav
  implicit none

  contains


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!     MATRIX   ELEMENTS       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  FUNCTION EE2NNeik(p1, p2, xicut, pole, lin)
    !! e+ (p1) e-(p2) -> whatever
    !! e- (p1) e+(p2) -> whatever
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4), p2(4), xicut
  real (kind=prec), optional :: lin,pole
  real (kind=prec) :: ee2nneik, epart, scms, mm2
  real (kind=prec) :: pm, ompm, b, logy, logy2, logy2b, logy3, f1, ls

  scms = sq(p1+p2)
  Epart = sqrt(scms)

  ee2nneik =   + alpha/(2.*pi)*(2*Ireg(xicut,Epart,p1,p2) &
            - Ireg(xicut,Epart,p1)- Ireg(xicut,Epart,p2))

  if(present(pole)) then
  pole     =  + alpha/(2.*pi)*(2*Isin(xicut,Epart,p1,p2) &
            - Isin(xicut,Epart,p1)- Isin(xicut,Epart,p2))
  endif
  if(present(lin)) then
    ! TODO this should be in functions.F95
    mm2 = sq(p1)
    b = sqrt(1 - 4*mm2/scms)
    pm = ((1.-b)/(1.+b))
    ompm = 1.-pm
    logy = log(pm)
    logy2b = log(ompm)
    logy2 = -Li2(pm) - logy*logy2b
    logy3 = -(logy**2*logy2b)/2. - 2*logy2*logy2b + z3 + (2*logy2b*Pi**2&
           &)/3. + 2*logy2b*Li2(ompm) - 2*Li3(ompm) - Li3(pm)
    f1 = (1+b**2)/b

    ls = -log(scms/musq)
    lin = (1 + (f1*logy)/2.)*ls**2 + (12*logy**2 + 2*b*f1*logy**3 + 48*logy2 + 24*logy3 + &
      &24*b**2*logy3 + 8*Pi**2 - 6*b*Pi**2 + b*f1*logy*Pi**2 - 24*b*f1*logy2b*Pi**2)/(1&
      &2.*b) + ((logy*(4 + logy + b**2*logy))/b + (2*f1*(6*logy2 + Pi**2))/3.)*Log(xicu&
      &t) + (4 + 2*f1*logy)*Log(xicut)**2 - ls*((2*(logy + logy2 + b**2*logy2))/b + (f1&
      &*(3*logy**2 + 2*Pi**2))/6. + (4 + 2*f1*logy)*Log(xicut))
    lin = lin * alpha / (2.*pi)

  endif


  END FUNCTION EE2NNeik


  FUNCTION EE2NN(p1, p2, q1, q2, linear)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  real (kind=prec), intent(in) :: p1(4), p2(4), q1(4), q2(4)
  real (kind=prec), optional :: linear
  real (kind=prec) mm2, scms, ee2nn, s21,s11

  mm2 = sq(p1)
  scms = sq(p1+p2)
  s21 = s(p2,q1) ; s11 = s(p1,q1)

  if (present(linear)) linear = -4*8*GF**2*scms**2

  ee2nn = 2*mm2*(s11 + s21) + s21*scms + s11*(-2*s21 + scms)
  ee2nn = 4*8*GF**2*ee2nn

  END FUNCTION EE2NN

  FUNCTION EE2NNL(p1, p2, q1, q2, pole, lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! Package-X convention

  real (kind=prec), intent(in) :: p1(4), p2(4), q1(4), q2(4)
  real (kind=prec), optional :: pole
  real (kind=prec), optional :: lin
  real (kind=prec) :: ee2nnl
  real (kind=prec) :: mm2, b, y, scms, logy, logy2, logy3
  real (kind=prec) :: tree, f1, ls, f2, pm,logy2b, ompm


  mm2 = sq(p1)
  scms = sq(p1+p2)


  b = sqrt(1 - 4*mm2/scms)
  pm = ((1.-b)/(1.+b))
  ompm = 1.-pm
  logy2b = Log(ompm)
  logy = log(pm)
  logy2 = -Li2(pm) - logy*logy2b
  ls = log(musq/mm2)
  y = (1 - 2*s(p1,q1)/scms) / b


  tree = 2+(y**2-1)*b**2
  f1 = (1+b**2)/b * tree

  if (present(pole)) then
    pole=-f1*logy - 2*tree
    pole=4*pole * scms**2*GF**2*alpha*2/pi
  endif

  if (present(lin)) then
    f2 = (-4+tree+2*tree*b**2)/b
    logy3 = -(logy**2*logy2b)/2. - 2*logy2*logy2b + z3 + (2*logy2b*Pi**2&
           &)/3. + 2*logy2b*Li2(ompm) - 2*Li3(ompm) - Li3(pm)
    lin = 8 - 2*f2*logy2 + ls**2*(-(f1*logy)/2. - tree) + (8*logy)/b +&
         & (2*logy**2)/b - 2*logy*b - (8*Pi**2)/(3.*b) + (tree*(-48*b &
         &- 3*logy**2*(1 + 2*b**2) + (4 - b + 8*b**2)*Pi**2))/(6.*b) +&
         & f1*(-logy**3/6. - 2*logy3 + logy*(-4 + (7*Pi**2)/12.)) + ls&
         &*(4 - f2*logy - 4*tree + f1*(-logy**2/2. - 2*logy2 + (2*Pi**&
         &2)/3.))
    lin=4*lin * scms**2*GF**2*alpha*2/pi
  endif
  ee2nnl = f1*(-logy**2/2. - 2*logy2 - logy*ls + 2*Pi**2/3.)
  ee2nnl = ee2nnl+logy*(2/b - (3 + y**2)*b)
  ee2nnl = ee2nnl-2*(-1 + y**2)*b**2*(2 + logy*b)
  ee2nnl = ee2nnl-4 - 2*ls*tree
  ee2nnl=4*ee2nnl * scms**2*GF**2*alpha*2/pi


  END FUNCTION EE2NNL


  FUNCTION EE2NNLav(p1, p2, p3, p4, pole, lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4)
    !! for massive electrons
    !! Package-X convention, m>0
    !! average over neutrino tensor taken

  real (kind=prec), intent(in) :: p1(4), p2(4), p3(4), p4(4)
  real (kind=prec), optional :: pole,lin
  real (kind=prec) :: ee2nnlav
  real (kind=prec) :: mm2, b, scms, logy, logy2,logy3
  real (kind=prec) :: tree, f1, ls


  mm2 = sq(p1)
  scms = sq(p1+p2)

  b = sqrt(1 - 4*mm2/scms)
  logy = log((1-b)/(1+b))
  logy2 = -Li2((1 - b)/(1 + b)) - logy*Log((2*b)/(1 + b))
  ls = log(musq/mm2)

  tree = -4*scms**2*(-3 + b**2)/3.
  f1 = (1+b**2)/b * tree

  if (present(pole)) then
    pole = -f1*logy - 2*tree
    pole = pole*alpha*GF**2/pi
  endif

  if (present(lin)) then
    call crash("Not fully implemeted yet")
    logy3 = 0. !TODO
    lin = ls**2*(-(f1*logy)/2. - tree) + ls*((4*(2*b*(-9 + 5*b**2) + (9 - 16*b**&
      &2 + 5*b**4)*logy)*scms**2)/(9.*b) - (f1*(3*logy**2 + 12*logy2 - 4*Pi**&
      &2))/6.) + (-18*b*f1*logy**3 + 24*(9 - 16*b**2 + 5*b**4)*logy**2*scms**&
      &2 + b*logy*(64*b*(-26 + 7*b**2)*scms**2 + 63*f1*Pi**2) - 8*(9*b*f1*log&
      &y3 + 216*b*scms**2 - 112*b**3*scms**2 - 12*(9 - 16*b**2 + 5*b**4)*logy&
      &2*scms**2 - 108*scms**2*z3 - 72*b**2*scms**2*z3 + 36*b**4*scms**2*z3 +&
      & (36 + 9*b - 64*b**2 - 3*b**3 + 20*b**4)*scms**2*Pi**2))/(108.*b)
    lin = lin*alpha*GF**2/pi
  endif

  ee2nnlav = ls*(-f1*logy-2*tree)-f1*(3*logy**2+12*logy2-4*pi**2)/6. &
         + (4*Scms**2*(2*b*(-9+5*b**2)+logy*(9-16*b**2+5*b**4)))/(9*b)
  ee2nnlav = ee2nnlav*alpha*GF**2/pi

  END FUNCTION EE2NNLav


  FUNCTION EE2NNG(p1, p2, q1, q2, q3)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! for massive electrons

  real (kind=prec) :: p1(4), p2(4), q1(4), q2(4), q3(4)
  real (kind=prec) mm2, scms, s12,s13,s22,s23,sq23
  real (kind=prec) :: ee2nng, if11, if12, if22, den1, den2

  mm2 = sq(p1)
  scms = sq(p1+p2)
  s12 = s(p1,q2) ; s13 = s(p1,q3)
  s22 = s(p2,q2) ; s23 = s(p2,q3)
  sq23 = s(q2,q3)

  den1=s23*(s12 + s22 - sq23)
  den2=s13*(s12 + s22 - sq23)

  if11=- 4*mm2**2*s12 - 2*mm2*scms*s12 + 2*mm2*s12*s13 - 4*mm2**2*s22       &
       - 2*mm2*scms*s22 + 4*mm2*s12*s22 + 2*mm2*s13*s22 + 6*mm2*s12*s23     &
       + s12*s13*s23 + 2*mm2*s22*s23 + s12*s23**2 + 4*mm2**2*sq23           &
       + 2*mm2*scms*sq23 - 4*mm2*s12*sq23 - 2*mm2*s13*sq23 - 2*mm2*s23*sq23 &
       + scms*s23*sq23 - 2*s12*s23*sq23 - s13*s23*sq23

  if12=- 8*mm2**2*s12 + 2*scms**2*s12 - 2*scms*s12*s13 - 8*mm2**2*s22       &
       + 2*scms**2*s22 + 8*mm2*s12*s22 - 4*scms*s12*s22 + 4*mm2*s13*s22     &
       - 2*scms*s13*s22 + 2*s12*s13*s22 + s13**2*s22 - 2*s13*s22**2         &
       + 4*mm2*s12*s23 - 2*scms*s12*s23 - 2*s12**2*s23 - s12*s13*s23        &
       - 2*scms*s22*s23 + 2*s12*s22*s23 - s13*s22*s23 + s12*s23**2          &
       + 8*mm2**2*sq23 - 2*scms**2*sq23 - 4*mm2*s12*sq23 + 2*scms*s12*sq23  &
       - 4*mm2*s13*sq23 + scms*s13*sq23 - 4*mm2*s22*sq23 + 2*scms*s22*sq23  &
       - 4*mm2*s23*sq23 + scms*s23*sq23 + 4*mm2*sq23**2

  if22=- 4*mm2**2*s12 - 2*mm2*scms*s12 + 2*mm2*s12*s13 - 4*mm2**2*s22       &
       - 2*mm2*scms*s22 + 4*mm2*s12*s22 + 6*mm2*s13*s22 + s13**2*s22        &
       + 2*mm2*s12*s23 + 2*mm2*s22*s23 + s13*s22*s23 + 4*mm2**2*sq23        &
       + 2*mm2*scms*sq23 - 2*mm2*s13*sq23 + scms*s13*sq23                   &
       - 4*mm2*s22*sq23 - 2*s13*s22*sq23 - 2*mm2*s23*sq23 - s13*s23*sq23

  ee2nng = if11 / den1**2 + if12 / den1 / den2 + if22 / den2**2
  ee2nng = ee2nng * 256 * pi**3 * alpha**3


  END FUNCTION EE2NNG

  FUNCTION EE2NNGav(p1, p2, q1, q2, q3,lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) y(q3)
    !! for massive electrons
    !! average over neutrino tensor taken

  real (kind=prec) :: p1(4), p2(4), q3(4), q1(4), q2(4)
  real (kind=prec) mm2, scms, s12,s22
  real (kind=prec) :: ee2nngav, den1, den2
  real (kind=prec) :: if11, if12, if22, ifl11, ifl12, ifl22

  real (kind=prec), optional :: lin

  mm2 = sq(p1)
  scms = sq(p1+p2)

  s12 = s(p1,q3)
  s22 = s(p2,q3)
  den1 = s12
  den2 = s22
  if11=-4*mm2**2*scms - 2*mm2*scms**2 + 4*mm2**2*s12 + 6*mm2*scms*s12 &
      - 3*mm2*s12**2 + (scms*s12**2)/2. + 4*mm2**2*s22 + 4*mm2*scms*s22 &
      - 6*mm2*s12*s22 + scms*s12*s22 - s12**2*s22 - 2*mm2*s22**2 - s12*s22**2
  if22=-4*mm2**2*scms - 2*mm2*scms**2 + 4*mm2**2*s12 + 4*mm2*scms*s12 &
      - 2*mm2*s12**2 + 4*mm2**2*s22 + 6*mm2*scms*s22 - 6*mm2*s12*s22 &
      + scms*s12*s22 - s12**2*s22 - 3*mm2*s22**2 + (scms*s22**2)/2. - s12*s22**2
  if12=-8*mm2**2*scms + 2*scms**3 + 8*mm2**2*s12 + 2*mm2*scms*s12 &
      - 4*scms**2*s12 - 2*mm2*s12**2 + 2*scms*s12**2 + 8*mm2**2*s22 &
      + 2*mm2*scms*s22 - 4*scms**2*s22 - 6*mm2*s12*s22 + 3*scms*s12*s22 &
      - 2*mm2*s22**2 + 2*scms*s22**2

  ee2nngav = if11 / den1**2 + if12 / den1 / den2 + if22 / den2**2
  ee2nngav = 2*ee2nngav * 64*Pi*alpha*GF**2/3.

  if (present(lin)) then
    ifl11 = 4*mm2**2*scms + 8*mm2*scms**2 - 4*mm2**2*s12 - 18*mm2*scms*s12 + 9*mm2*s12**2 - &
           &2*scms*s12**2 - 4*mm2**2*s22 - 16*mm2*scms*s22 + 18*mm2*s12*s22 - 7*scms*s12*s22&
           & + 7*s12**2*s22 + 8*mm2*s22**2 + 7*s12*s22**2
    ifl22 = 4*mm2**2*scms + 8*mm2*scms**2 - 4*mm2**2*s12 - 16*mm2*scms*s12 + 8*mm2*s12**2 - &
           &4*mm2**2*s22 - 18*mm2*scms*s22 + 18*mm2*s12*s22 - 7*scms*s12*s22 + 7*s12**2*s22 &
           &+ 9*mm2*s22**2 - 2*scms*s22**2 + 7*s12*s22**2
    ifl12 = 8*mm2**2*scms + 12*mm2*scms**2 - 8*scms**3 - 8*mm2**2*s12 - 26*mm2*scms*s12 + 16&
           &*scms**2*s12 + 14*mm2*s12**2 - 8*scms*s12**2 - 8*mm2**2*s22 - 26*mm2*scms*s22 + &
           &16*scms**2*s22 + 30*mm2*s12*s22 - 18*scms*s12*s22 + 6*s12**2*s22 + 14*mm2*s22**2&
           & - 8*scms*s22**2 + 6*s12*s22**2

    lin = ifl11 / den1**2 + ifl12 / den1 / den2 + ifl22 / den2**2
    lin = 2*lin * 64*Pi*alpha*GF**2/9.
  endif

  END FUNCTION


  FUNCTION EE2NNav(p1, p2, q1, q2, lin)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! average over neutrino tensor taken

  real (kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real (kind=prec) mm2, scms
  real (kind=prec) :: ee2nnav
  real (kind=prec), optional :: lin

  mm2 = sq(p1)
  scms = sq(p1+p2)

  ee2nnav = scms*(2*mm2+scms)*8./3.
  ee2nnav = 2*ee2nnav * GF**2

  if (present(lin)) then
    lin = -2*16. * GF**2*Scms*(mm2 + 2*Scms) / 9.
  endif
  END FUNCTION


  FUNCTION EE2NNGLAV(p1, p2, q1, q3, q2,pole)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q3) y(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q3) y(q2)
    !! for massive electrons
    !! average over neutrino tensor taken
    use misc_EE2NNGl, only: ls,m2,xi,tscms, y, rvbub, rvren, rvtrig,rvtrin,rvbox,rvpole
    implicit none
    real(kind=prec), intent(in) :: p1(4), p2(4), q2(4), q1(4), q3(4)
    real(kind=prec), optional :: pole
    real(kind=prec) :: ee2nnglav,b
    tscms = sq(p1+p2)

    call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
    call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)

    m2 = sq(p1)
    tscms = sq(p1+p2)
    b = sqrt(1 - 4*m2/tscms)
    ls = log(musq/m2)
    xi = s(p1+p2,q2) / tscms
    y = -s(p1-p2,q2)/(b*tscms*xi)


    ee2nnglav = rvbub (b) + rvbub (-b) + rvren (b) + rvren (-b) &
              + rvtrig(b) + rvtrig(-b) + rvtrin(b) + rvtrin(-b) &
              + rvbox (b) + rvbox (-b)

    ee2nnglav = ee2nnglav * 64* GF**2 * alpha**2 * tscms * (xi-1)


    if(present(pole)) then
      pole = rvpole(b) + rvpole(-b)
      pole = pole * 64 * GF**2 * alpha**2 * tscms * (xi-1)
    endif

  END FUNCTION EE2NNGLAV


  FUNCTION EE2NNGf(p1, p2, p3, p4, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) y(q2)
    !! for massive electrons
    !! average over neutrino tensor taken
  implicit none
  real(kind=prec), intent(in) :: p1(4), p2(4), p3(4), p4(4), q2(4)
  real(kind=prec) :: ee2nngf, pole
  real(kind=prec) :: mat0,ctfin,ctpole,epart,scms,lin

  scms = sq(p1+p2)
  Epart = sqrt(scms)

  mat0 = ee2nngav(p1,p2,p3,p4,q2,lin)

  ! (ctpole/ep + ctfin) * 16 * pi**2 = intEik
  ctfin = ee2nneik(p1, p2, xieik1, ctpole)

  ee2nngf = ee2nnglav(p1,p2,p3,p4,q2, pole)

  if (abs((ctpole*mat0+pole)/pole).gt.3e-9) print*,"ee2nngf is not finite"


  ee2nngf = ee2nngf+ctfin*mat0+ctpole*lin

  END FUNCTION EE2NNGf

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!         integrand function         !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EE2NNf(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
  real(kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real(kind=prec) :: ee2nnf, mat0, pole, lin, ctfin, ctpole

  mat0 = ee2nn(p1,p2,q1,q2,lin)
  ee2nnf = ee2nnl(p1,p2,q1,q2,pole)

  ctfin = ee2nneik(p1, p2, xieik1, ctpole)
  ee2nnf = ee2nnf + ctfin * mat0 + ctpole * lin
  if (abs(pole+mat0*ctpole) > 1.0e-8) print*,"poles do not cancel!"
  END FUNCTION

  FUNCTION EE2NNs(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
  real(kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real(kind=prec) :: mat0, y, ctfin, ctpole, ctlin, lin, lin0, pole, mat, ee2nns
  mat0 = ee2nnl(p1,p2,q1,q2,pole,lin0)

  y = (1 - 2*s(p1,q1)/scms) / sqrt(1 - 4*sq(p1)/scms)

  ctfin = ee2nneik(p1, p2, xieik1, ctpole, ctlin)

  ! Correct dim. of the neutrino phase space 4->d
  lin = lin0+(log(4*musq/scms)-Log(1-y**2))*mat0+(log(4*musq/scms)-Log(1-y**2))**2*pole/2.-pole*pi**2/12.
  mat = mat0+(log(4*musq/scms)-Log(1-y**2))*pole

  ee2nns = ctpole*pole                     ! double pole
  ee2nns = ctpole*mat + ctfin*pole         ! single pole
  ee2nns = ctfin*mat+ctpole*lin+ctlin*pole ! finite bit
  END FUNCTION


  FUNCTION EE2NNss(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
  real(kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real(kind=prec) :: qua, lin, mat, lin0, y, dp, sp, fi, ee2nnss
  real(kind=prec) :: ctpolea, ctfina, ctlina
  real(kind=prec) :: ctpoleb, ctfinb, ctlinb
  mat = ee2nn(p1,p2,q1,q2,lin0)

  y = (1 - 2*s(p1,q1)/scms) / sqrt(1 - 4*sq(p1)/scms)

  ctfinA = ee2nneik(p1, p2, xieik1, ctpoleA, ctlinA)
  ctfinB = ee2nneik(p1, p2, xieik2, ctpoleB, ctlinB)

  dp = ctpoleA*ctpoleB                                  ! ep^-2 of \hat E \hat E
  sp = ctpoleA*ctfinB + ctfinA*ctpoleB                  ! ep^-1 of \hat E \hat E
  fi = ctfinA*ctfinB + ctpoleA*ctlinB + ctpoleB*ctlinA  ! ep^0  of \hat E \hat E


  ! Correct dim. of the neutrino phase space 4->d
  lin = lin0+(log(4*musq/scms)-Log(1-y**2))*mat
  qua =  0. +(log(4*musq/scms)-Log(1-y**2))*lin0+(log(4*musq/scms)-Log(1-y**2))**2*mat/2.-mat*pi**2/12.

  ee2nnss = dp * mat                       ! double pole
  ee2nnss = dp * lin + sp * mat            ! single pole
  ee2nnss = dp * qua + sp * lin + fi * mat ! finite bit

  ee2nnss = ee2nnss/2
  END FUNCTION


  FUNCTION EE2NNcc(p1, p2, q1, q2)
    !! e+ (p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e- (p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
  real(kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real(kind=prec) :: ee2nncc, mats, matss
  real(kind=prec) :: pole, mat1, lin1, mat0, lin0
  real(kind=prec) :: ctpole, ctfin, ctlin, ct2dub, ct2sin, ct2fin

  mat1 = ee2nnl(p1,p2,q1,q2,pole,lin1)
  mat0 = ee2nn (p1,p2,q1,q2,     lin0)

  ctfin = ee2nneik(p1, p2, xieik1, ctpole, ctlin)
  ! S-S part
  ct2dub = ctpole**2                                    ! ep^-2 of \hat E^2
  ct2sin = 2*ctfin*ctpole                               ! ep^-1 of \hat E^2
  ct2fin = ctfin**2+2*ctlin*ctpole                      ! ep^-0 of \hat E^2

  matSS = ct2dub * mat0                                 ! double pole
  matSS = ct2dub * lin0 + ct2sin * mat0                 ! single pole
  matSS = ct2dub *  0.  + ct2sin * lin0 + ct2fin * mat0 ! finite bit

  matSS = matSS / 2!

  ! S part
  matS = ctpole*pole                                    ! double pole
  matS = ctpole*mat1 + ctfin*pole                       ! single pole
  matS = ctpole*lin1 + ctfin*mat1+ctlin*pole            ! finite bit


  ee2nncc = matss+mats
  END FUNCTION




                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MISC_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!

