# The McMule internal repository

*Please note that this is the **old** McMule repository that will no
longer be updated. Please head over to the new repository at
[gitlab.com/mule-tools/mcmule](https://gitlab.com/mule-tools/mcmule)*

McMule is a framework for fully differential higher-order QED
calculations of scattering and decay processes involving leptons. It
keeps finite lepton masses, which regularises collinear singularities.
Soft singularities are treated with dimensional regularisation and
using $`{\rm FKS}^\ell`$ subtraction

Please find the manual for McMule
[here](https://gitlab.psi.ch/mcmule/manual) or download it here [as a
pdf](https://gitlab.psi.ch/mcmule/manual/-/jobs/artifacts/master/raw/manual.pdf?job=compile)

If you find McMule useful, please consider citing
[[2007.01654]](https://arxiv.org/abs/2007.01654)

>  QED at NNLO with McMule
>
>  P. Banerjee, T. Engel, A. Signer, Y. Ulrich

## Getting started
To obtain a copy of McMule we recommend the following approach
```bash
$ git clone --recursive https://gitlab.psi.ch/mcmule/mcmule
````
To build McMule, a Fortran compiler such as `gfortran` and a python
installation is needed. The main executable can be compiled by running
```bash
$ ./configure
$ make mcmule
```
Alternatively, we provide a Docker container for easy deployment and
legacy results. In multi-user environments, udocker can be used
instead. In either case, a pre-compiled copy of the code can be
obtained by calling
```bash
$ docker pull yulrich/mcmule  # requires Docker to be installed
$ udocker pull yulrich/mcmule # requires uDocker to be installed
```
Please refer to the manual on how to use McMule.
