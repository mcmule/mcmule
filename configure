#! /bin/bash
# configure script
# note: has nothing to do with GNU autoconf
if [[ -z $LOGFILE ]]; then
  exec 2> /dev/null 3>&1
else
  exec 3> >(while read TEXT ; do echo $TEXT ; echo $(date +"%d.%m.%Y %H:%M:%S") $TEXT >> $LOGFILE; done)
  exec 2>> $LOGFILE
fi


shopt -s nullglob
export LC_ALL=C

printhelp() {
cat << EOF
\`configure' configures mcmule

Usage: ./configure [OPTION]...

See below for descriptions of some of the useful variables.

Defaults for the options are specified in brackets.

Optional features:
  --debug                 compile with debug flags and without optimization
  --coverage              perform coverage test
  --psi                   run on PSI systems
  --ci                    run CI. This is equivalent to --debug --coverage --psi
  -O=<n>                  level of optimisation used [1]
                          0: no optimisation
                          1: some optimisation
                          2: full optimisation (expect compile times O(4h))

Some influential environment variables:
  FC          Fortran compiler command
  FFLAGS      Fortran compiler flags
  LD          linker command
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
  CC          C compiler command
  CFLAGS      C compiler flags
  PATCH       the patch command
  PYMULE      pymule command
  SHA1        sha1 command
  ESHELL      shell for execution of subcommands

Use these variables to override the choices made by \`configure' or to help
it to find libraries and programs with nonstandard names/locations.

Report bugs to <yannick.ulrich@psi.ch>.

EOF
exit
}

CONF_OS="`uname -s`"


CONF_OPTLEVEL=1
allargs="$@"
DEBUG=false
COVERAGE=false
PSI=false
for arg in "$@" ; do
    case "$arg" in
        --debug)
            DEBUG=true ;;
        --coverage)
            COVERAGE=true ;;
        --ci)
            echo "Running in CI configuration. Will not perform speed tests!" 1>&3
            DEBUG=true
            PSI=true
            COVERAGE=true ;;
        --psi)
            PSI=true ;;
        --help )
            printhelp ;;
        -O=*)
            CONF_OPTLEVEL=`echo $arg | cut -f2 -d'='` ;;
        -*)
            echo "Warning: $arg is not a valid option." 1>&3
            printhelp ;;
        *=*)
            eval "$arg" ;;
        *)
            echo "Warning: $arg is not a valid argument." 1>&3
            printhelp ;;
    esac
done


test=test$$
trap "rm -fr $test* =." 0 1 2 3 15

HAVE_GIT=false
if type -P git &>/dev/null ; then
    if [ -d .git ]; then
        HAVE_GIT=true
    fi
fi

# This will be set by git-archive
VTAG="$Format: %D, %h$"


if $HAVE_GIT ; then
    VERSION=`git describe --tag`
    COMMIT=`git log -1 --pretty=format:"%h"`
elif [[ "$VTAG" == *"HEAD"* ]]; then
    VERSION=`echo $VTAG | perl -lpe 's/.*?(, tag: ([^,]*))?,.*? ([a-f\d]+)$/\2/g'`
    COMMIT=` echo $VTAG | perl -lpe 's/.*?(, tag: ([^,]*))?,.*? ([a-f\d]+)$/\3/g'`
    if [ -z "$VERSION" ]; then
        VERSION="untagged-$COMMIT"
    fi
else
    VERSION="v?.?.?"
    COMMIT="unknown"
fi



if $COVERAGE ; then
  $DEBUG || echo "Warning: --coverage implies --debug!" 1>&3
  DEBUG=true
fi


if $PSI ; then
    echo "Assuming a PSI system"
    FC=/afs/psi.ch/sys/psi.x86_64_slp6/Programming/gcc/8.2.0/bin/gfortran
    CC=/afs/psi.ch/sys/psi.x86_64_slp6/Programming/gcc/8.2.0/bin/gcc
    LD=/afs/psi.ch/sys/psi.x86_64_slp6/Programming/gcc/8.2.0/bin/gfortran
    if [[ $(hostname -s) = merlin-* ]]; then
        PATCH=/afs/psi.ch/project/muondecay/patch
    fi
fi

CONF_FFLAGS=$FFLAGS
CONF_CFLAGS=$CFLAGS
CONF_LFLAGS=$LDFLAGS
CONF_FC=$FC
CONF_CC=$CC
CONF_LD=$LD
CONF_PATCH=$PATCH
CONF_SHA1=$SHA1



## look for some programs

findprog() {
    echo -n "looking for $1... " 1>&3
    var="$2"
    set -- ${!var:+"${!var}"} "${@:3}"
    for prog in "$@" ; do
        echo "Checking option $prog in path" 1>&2
        full="`type -P "$prog"`" && {
            echo "$full" 1>&3
            printf -v "CONF_$var" "%q" "$full"
            return 0
        }
    done
    echo "no $@ in your path" 1>&3
    return 1
}
addflag() {
    eval "CONF_$1=\"${@:2} \${CONF_$1}\""
}

findprog patch PATCH patch  || exit 1
findprog sha1 SHA1 sha1sum  shasum || exit 1
findprog fortran FC gfortran  || exit 1
findprog cc CC gcc  || exit 1
findprog ar AR ar  || exit 1

set -- `eval $CONF_FC --version -c | sed '/^$/d;s/([^)]*)//;q' 2>&1`
case "$1,$2,$3" in
    GNU,Fortran,[1234].*)
        echo "Only version >= 4 are supported" 1>&3
        exit 1
    ;;
    GNU,Fortran,*)
        eval addflag FFLAGS -fdefault-real-8
        eval addflag FFLAGS -fdefault-double-8
        eval addflag FFLAGS -cpp
        eval addflag FFLAGS -Wall
        eval addflag FFLAGS -Wno-unused-dummy-argument
        eval addflag FFLAGS -pedantic-errors
        if $DEBUG ; then
            eval addflag FFLAGS -g
            eval addflag FFLAGS -fcheck=all
            eval addflag FFLAGS -frange-check
            eval addflag FFLAGS -fdump-core
            eval addflag FFLAGS -fbacktrace
            eval addflag FFLAGS -ffpe-trap=invalid,overflow
        fi
        if $COVERAGE ; then
            eval addflag FFLAGS --coverage
            eval addflag LFLAGS -lgcov
        fi
    ;;
    *)
        echo "Unknown compiler" 1>&3
        exit
    ;;
esac

echo "Trying the following program as $test.f90" 1>&2
tee $test.f90 << _EOF_ 1>&2
    program test
    integer i
    common /uscore/ i
    call exit(i)
    end
_EOF_

echo "Executing $CONF_FC  -v -o $test $test.f90" 1>&2

CONF_LD=${LD:-$CONF_FC}


if $COVERAGE ; then
    findprog gcov GCOV gcov gcov-7 gcov-6 gcov-5 gcov-34 || exit 1
    if ! findprog lcov LCOV lcov ; then
      echo "graphical coverage report won't be supported" 1>&3
      HAVE_LCOV=false
    else
      if ! findprog genhtml GENHTML genhtml ; then
        echo "graphical coverage report won't be supported" 1>&3
        HAVE_LCOV=false
      else
        HAVE_LCOV=true
      fi
    fi
fi

if ! findprog pymule PYMULE pymule; then
    echo "Using shipped pymule instead" 1>&3
    findprog python PYTHON python2.7 python2 python
    CONF_PYMULE="PYTHONPATH=`pwd`/tools/pymule/ $CONF_PYTHON -m pymule"
fi
pmoutput=$(sh -c "$CONF_PYMULE -h" 2>&1) || {
    echo "pymule ($CONF_PYMULE) is not working." 1>&3
    echo "Please check your python installation" 1>&3
    exit 1
}
if [[ $pmoutput == *"ImportError"* ]]; then
    echo "Some pymule features are not available" 1>&3
fi


if [ -z $ESHELL ]; then
    if [ "$CONF_OS" == "Darwin" ]; then
        CONF_ESHELL="bash"
    else
        CONF_ESHELL="sh"
    fi
else
    CONF_ESHELL=$ESHELL
fi

echo "creating makefile" 1>&3

cat > makefile.sys <<EOF
FC=$CONF_FC
CC=$CONF_CC
LD=$CONF_LD

FFLAGS=$CONF_FFLAGS
CFLAGS=$CONF_CFLAGS
LFLAGS=$CONF_LFLAGS

PYMULE=$CONF_PYMULE
PATCH=$CONF_PATCH
SHA1=$CONF_SHA1 | cut -c1-40
SH=$CONF_ESHELL -c
AR=ar rvs

EOF

case "$CONF_OPTLEVEL" in
    0)
        echo -e "opt1=\nopt2=\nmopt1=(disabled)\nmopt2=(disabled)\n" >> makefile.sys
        ;;
    1)
        echo -e "opt1=-O3\nopt2=\nmopt1=\nmopt2=(disabled)\n" >> makefile.sys
        ;;
    2)
        echo -e "opt1=-O3\nopt2=-O3\nmopt1=\nmopt2=\n" >> makefile.sys
        ;;
esac
