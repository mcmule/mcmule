#!/bin/bash
set -e


if type -P docker &>/dev/null ; then
    HAVE_DOCKER=true
    ENGINE=docker
else
    HAVE_DOCKER=false
    ENGINE=udocker
fi

if ! type -P realpath &>/dev/null ; then
    function realpath {
        readlink -f -- "$@"
    }
fi

function start_container {
    local image=$1
    local folder=$2
    if $HAVE_DOCKER ; then
        $ENGINE run -v $folder:/root/out -d $image sleep infinity
    else
        local container=`$ENGINE create $image`
        $ENGINE protect $container
        echo $container
    fi
}

function cp_user {
    local container=$1
    local file=$2
    if $HAVE_DOCKER ; then
        $ENGINE cp $file $container:/monte-carlo/src/user.f95
        $ENGINE exec $container sh -c "cd /monte-carlo && touch src/mat_el.f95 && make" 
    else
        cat $file | $ENGINE run $container sh -c "cat > /monte-carlo/src/user.f95"
        $ENGINE run $container sh -c "cd /monte-carlo && touch src/mat_el.f95 && make"
    fi
}

function start_mcmule {
    local container=$1
    local folder=$2
    if $HAVE_DOCKER ; then
        $ENGINE exec -i -w /root $container /monte-carlo/mcmule
    else
        $ENGINE run --volume=$folder/:/root/out $container /monte-carlo/mcmule
    fi
}

function clean {
    local container=$1
    if $HAVE_DOCKER ; then
        $ENGINE stop $container
        $ENGINE rm $container
    else
        $ENGINE unprotect $container
        $ENGINE rm $container
    fi
}

function showhelp {
    echo "Error $1"
    exit 1
}

function main {
    run=""
    image=""
    container=""
    file=""
    folder="out"
    while [[ $# -gt 0 ]]
    do
        key="$1"
        case $key in
            -i|--image)
                image="$2"
                shift ; shift ;;
            -c|--container)
                container="$2"
                shift ; shift ;;
            -u|--user)
                file="$2"
                shift ; shift ;;
            -o|--output)
                folder="$2"
                shift ; shift ;;
            -r|--run)
                run="yes"
                shift ;;
            -e|--engine)
                ENGINE="$2"
                if [[ "$ENGINE" == *"udocker"* ]]; then
                    HAVE_DOCKER=false
                else
                    HAVE_DOCKER=true
                fi
                shift ; shift ;;
            *)
                showhelp "Unknown option $key"
                exit ;;
        esac
    done
    folder=`realpath $folder`

    if [ -n "$image" ]; then
        if [ -z "$folder" ]; then
            showhelp "Image requires folder"
        fi
        if [ -n "$container" ]; then
            showhelp "Image and container are incompatible"
        fi
        container=`start_container $image $folder`
        echo "Created container $container for image $image"
        trap "clean $container" INT
    fi

    if [ -n "$file" ]; then
        if [ -z "$container" ]; then
            showhelp "No container specified"
        fi
        echo "Copy $file to container $container"
        cp_user $container $file
    fi

    if [ -n "$run" ]; then
        if [ -z "$folder" ]; then
            showhelp "Run requires folder"
        fi
        echo "Running McMule in container $container"
        start_mcmule $container $folder

        if [ -n "$image" ]; then
            echo "Cleaning up $container"
            clean $container
        fi
    fi
}

main $@
