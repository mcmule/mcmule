#!/bin/bash

cd $1

menu=`cat menu.txt`
echo "Menu has `echo "$menu" | wc -l` lines".
for i in `ls worker_*`
do
  if grep -q result $i; then
    str=`echo $i | perl -pe 's/worker_([^_]*)_([^_]*)_([^_]*)_([^_]*)_.*/run \1 \2 \3 \4 \5 0/g' | tr -s ' '`
    echo "Dropping line $str"
    menu=`echo "$menu" | grep -v "$str"`
  else
    echo "Job $i is not complete `cat $i | grep internal | wc -l` iterations done"
  fi
done
echo "$menu" > menu_c.txt
