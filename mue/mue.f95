
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MUE
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use mue_mat_el


!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains




          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!        SINGULAR    LIMITS          !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION EM2EMG_EE_S(p1,p2,q1,q2, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emg_ee_s
  real (kind=prec), optional :: lin

  em2emg_ee_s = 2*eik(p1,ksoft,q1)*em2em(p1,p2,q1,q2)
  em2emg_ee_s = (4.*pi*alpha)*em2emg_ee_s

  if (present(lin)) then
    lin = 2*eik(p1,ksoft,q1) * (-32*pi**2*alpha**2)
    lin = (4.*pi*alpha)*lin
  endif
  END FUNCTION EM2EMG_EE_S

  FUNCTION EM2EMG_MM_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emg_mm_s
  !Corresponds to ee contribution after p2<->p1,q1<->q2
  em2emg_mm_s = em2emg_ee_s(p2,p1,q2,q1)
  END FUNCTION EM2EMG_MM_S


  FUNCTION EM2EMG_EM_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emg_em_s

  em2emg_em_s = 2*(eik(p1,ksoft,q2) - eik(p1,ksoft,p2)&
                 +eik(q1,ksoft,p2) - eik(q1,ksoft,q2))*em2em(p1,p2,q1,q2)
  em2emg_em_s = (4.*pi*alpha)*em2emg_em_s
  END FUNCTION EM2EMG_EM_S




          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EM2EMG_AEE_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emg_aee_s

  em2emg_aee_s = 2*eik(p1,ksoft,q1)*em2em_a(p1,p2,q1,q2)
  em2emg_aee_s = (4.*pi*alpha)*em2emg_aee_s

  END FUNCTION EM2EMG_AEE_S


  FUNCTION EM2EMGL_EEEE_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgl_eeee_s

  em2emgl_eeee_s = 2*eik(p1,ksoft,q1)*em2eml_ee(p1,p2,q1,q2)
  em2emgl_eeee_s = (4.*pi*alpha)*em2emgl_eeee_s
  END FUNCTION EM2EMGL_EEEE_S


  FUNCTION EM2EMGf_EEEE_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgf_eeee_s
  real (kind=prec) :: mat0, ctpole, ctfin

  mat0 = em2emg_ee_s(p1,p2,q1,q2)
  ctfin = em2emeik_ee(p1,p2,q1,q2,xieik1,ctpole)


  em2emgf_eeee_s = em2emgl_eeee_s(p1,p2,q1,q2)
  em2emgf_eeee_s = em2emgf_eeee_s + ctfin * mat0
  END FUNCTION EM2EMGf_EEEE_S


  FUNCTION EM2EMGG_EEEE_SH(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft) y(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q4(4)
  real (kind=prec) :: em2emgg_eeee_sh

  em2emgg_eeee_sh = 2*eik(p1,ksoftA,q1)*em2emg_ee(p1,p2,q1,q2,q4)
  em2emgg_eeee_sh = (4.*pi*alpha)*em2emgg_eeee_sh

  END FUNCTION EM2EMGG_EEEE_SH
  FUNCTION EM2EMGG_EEEE_HS(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3) y(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgg_eeee_hs

  em2emgg_eeee_hs = 2*eik(p1,ksoftB,q1)*em2emg_ee(p1,p2,q1,q2,q3)
  em2emgg_eeee_hs = (4.*pi*alpha)*em2emgg_eeee_hs

  END FUNCTION EM2EMGG_EEEE_HS
  FUNCTION EM2EMGG_EEEE_SS(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(ksoft) y(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgg_eeee_ss

  em2emgg_eeee_ss = (2*eik(p1,ksoftA,q1))*(2*eik(p1,ksoftB,q1))*em2em(p1,p2,q1,q2)
  em2emgg_eeee_ss = (4.*pi*alpha)**2*em2emgg_eeee_ss

  END FUNCTION EM2EMGG_EEEE_SS




                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUPAIR           !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  FUNCTION EE2MMG_S(p1,p2,q1,q2)
    !! e-(p1) e+(p2) -> mu+(q1) mu-(q2) y(ksoft)
    !! for massive electrons and positrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ee2mmg_s

  ee2mmg_s = em2emg_ee_s(p1,-q1,-p2,q2)&
             +em2emg_em_s(p1,-q1,-p2,q2)&
             +em2emg_mm_s(p1,-q1,-p2,q2)
  END FUNCTION EE2MMG_S



                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUSE             !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION MP2MPG_S(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive (and massless) muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mpg_s

  mp2mpg_s = 2*eik(p1,ksoft,q1)*mp2mp(p1,p2,q1,q2)
  mp2mpg_s = (4.*pi*alpha)*mp2mpg_s

  END FUNCTION MP2MPG_S


  FUNCTION MP2MPGL_S(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mpgl_s

  mp2mpgl_s = 2*eik(p1,ksoft,q1)*mp2mpl(p1,p2,q1,q2)
  mp2mpgl_s = (4.*pi*alpha)*mp2mpgl_s
  END FUNCTION MP2MPGL_S



          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION MP2MPG_A_S(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mpg_a_s

  mp2mpg_a_s = 2*eik(p1,ksoft,q1)*mp2mp_a(p1,p2,q1,q2)
  mp2mpg_a_s = (4.*pi*alpha)*mp2mpg_a_s
  END FUNCTION MP2MPG_A_S


  FUNCTION MP2MPGf_S(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mpgf_s
  real (kind=prec) :: mat0, ctfin

  mat0 = mp2mpg_s(p1,p2,q1,q2)
  ctfin = mp2mpeik(p1,p2,q1,q2,xieik1)

  mp2mpgf_s = mp2mpgl_s(p1,p2,q1,q2)
  mp2mpgf_s = mp2mpgf_s + ctfin * mat0 !no lin bit since FDH
  END FUNCTION MP2MPGf_S


  FUNCTION MP2MPGG_SH(p1, p2, q1, q2, q4)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft) y(q3)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q4(4)
  real (kind=prec) :: mp2mpgg_sh

  mp2mpgg_sh = 2*eik(p1,ksoftA,q1)*mp2mpg(p1,p2,q1,q2,q4)
  mp2mpgg_sh = (4.*pi*alpha)*mp2mpgg_sh
  END FUNCTION MP2MPGG_SH

  FUNCTION MP2MPGG_HS(p1, p2, q1, q2, q3)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft) y(q4)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpgg_hs

  mp2mpgg_hs = 2*eik(p1,ksoftB,q1)*mp2mpg(p1,p2,q1,q2,q3)
  mp2mpgg_hs = (4.*pi*alpha)*mp2mpgg_hs
  END FUNCTION MP2MPGG_HS

  FUNCTION MP2MPGG_SS(p1, p2, q1, q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(ksoft) y(ksoft)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mpgg_ss

  mp2mpgg_ss = (2*eik(p1,ksoftA,q1))*(2*eik(p1,ksoftB,q1))*mp2mp(p1,p2,q1,q2)
  mp2mpgg_ss = (4.*pi*alpha)**2*mp2mpgg_ss
  END FUNCTION MP2MPGG_SS


                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MUE
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!

