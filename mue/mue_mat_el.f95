
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MUE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use mue_em2emgleeee, only: em2emgl_eeee_old
  use mue_em2emggeeee, only: em2emgg_eeee_old
  use mue_em2emeeee, only: em2emeeee
  use mue_mp2mpgg, only: mp2mpgg, em2emgg_eeee
  use mue_mp2mpgl_pvred, only: mp2mpgl_pvred, em2emgl_eeee_pvred
  use mue_mp2mpgl_coll, only: mp2mpgl_coll, em2emgl_eeee_coll
  use mue_mp2mpgl_fullcoll, only: mp2mpgl_fullcoll, em2emgl_eeee_fullcoll
  use mue_mp2mp_nf_formfac_el
  use mue_mp2mp_nf_formfac_mu


!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!     MATRIX   ELEMENTS       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUONE            !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




  !!!!!!!!!!    Born    !!!!!!!!!!!!

  FUNCTION EM2EM(p1,p2,q1,q2, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, me2,mm2
  real (kind=prec) :: em2em
  real (kind=prec), optional :: lin

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)

  em2em = (2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2)/(tt**2)

  em2em = 32*pi**2*alpha**2*em2em

  if (present(lin)) then
    lin = 32*pi**2*alpha**2* (-1._prec)
  endif
  END FUNCTION EM2EM


  !!!!!!!!!!    VP    !!!!!!!!!!!!


  FUNCTION EM2EM_A(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2em_a,tt,deltalph_tot

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  em2em_a = 2*deltalph_tot*em2em(p1,p2,q1,q2)

  END FUNCTION EM2EM_A



  !!!!!!!!!!!    Ql**2*Qe**2*Qmu**2 (i.e. VP)      !!!!!!!!!!!!


  !FUNCTION EM2EM_NLO_VP(p1,p2,q1,q2,ml,Ql)
  !real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  !real (kind=prec) :: ss,tt,me2,mm2,mu2,logme,ml,Ql
  !real (kind=prec) :: em2em_nlo_vp

  !ss = sq(p1+p2); tt = sq(p1-q1)
  !me2=sq(p1); mm2=sq(p2)
  !mu2 = musq

  !em2em_nlo_vp = -(2*me2**2 + 2*mm2**2 + 4*me2*(mm2 - ss) - 4*mm2*ss + 2*ss**2 + 2*ss*tt + tt**2)&
  !               *(12*ml**2 + 5*tt + 3*(2*ml**2 + tt)*DiscB(tt,ml))

  !em2em_nlo_vp = 64*pi*alpha**3*Ql**2*em2em_nlo_vp/(9*tt**3)

  !END FUNCTION EM2EM_NLO_VP



  !!!!!!!!!!    Qe**4*Qmu**2      !!!!!!!!!!!!

  FUNCTION EM2EML_EE(p1,p2,q1,q2, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2eml_ee
  real (kind=prec) :: ss,tt,me2,mm2,mu2,me,logme,discbt,scalarc0ir6t
  real (kind=prec), optional :: pole

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)
  me=sqrt(me2)
  mu2 = musq
  logme = log(mu2/me2)
  discbt = DiscB(tt,me)
  scalarc0ir6t = ScalarC0IR6(tt,me)

  em2eml_ee = -16*me2*(2 + discbt - 2*me2*scalarc0ir6t)*(me2 + mm2 - ss)**2 + 2*(-12*me2**3*sc&
            &alarc0ir6t + (4 + 3*discbt)*(mm2 - ss)**2 + me2**2*(4 + 3*discbt - 24*mm2*scalar&
            &c0ir6t + 40*scalarc0ir6t*ss) + 2*me2*(-6*mm2**2*scalarc0ir6t - ss*(12 + 7*discbt&
            & + 6*scalarc0ir6t*ss) + mm2*(4 + discbt + 12*scalarc0ir6t*ss)))*tt + 2*(10*me2**&
            &2*scalarc0ir6t + 2*scalarc0ir6t*(mm2 - ss)**2 + (4 + 3*discbt)*ss - 2*me2*(4 + 3&
            &*discbt - 2*mm2*scalarc0ir6t + 8*scalarc0ir6t*ss))*tt**2 + (4 + 3*discbt + 4*sca&
            &larc0ir6t*(-3*me2 + ss))*tt**3 + 2*scalarc0ir6t*tt**4 - 2*logme*(2*(2 + discbt)*&
            &me2 - (1 + discbt)*tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2)

  em2eml_ee = 16*pi*alpha**3*em2eml_ee/((4*me2-tt)*tt**2)

  if (present(pole)) then
    pole = -((2*(2 + discbt)*me2 - (1 + discbt)*tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2))
    pole = 32*pi*alpha**3*pole/((4*me2-tt)*tt**2)
  endif

  END FUNCTION EM2EML_EE


  FUNCTION EM2EMG_EE(p1,p2,q1,q2,q3, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: me2,mm2,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45
  real (kind=prec) :: if11,if12,if22,den11,den22,den12,denAll
  real (kind=prec) :: em2emg_ee
  real (kind=prec), optional :: lin

  s12 = s(p1,p2); s13 = s(p1,q1); s14 = s(p1,q2); s15 = s(p1,q3)
  s23= s(p2,q1); s24 = s(p2,q2); s25 = s(p2,q3)
  s34 = s(q1,q2); s35 = s(q1,q3); s45 = s(q2,q3)
  me2=sq(p1); mm2=sq(p2)
  den11 = s15**2; den22 = s35**2; den12 = 2*s15*s35; denAll = (s24-2*mm2)**2

  if11 = 4*me2**2*(-4*mm2 + s24) + 2*me2*(-(s14*s23) - s15*s24 - s12*s34 + s25*s34&
         + 2*mm2*(s13 + 2*s15 - s35) + s23*s45) + s15*(s25*s34 - 2*mm2*s35 + s23*s45)

  if22 = -4*me2**2*(4*mm2 - s24) + s35*(-2*mm2*s15 + s14*s25 + s12*s45) - 2*me2*(s14*(s23&
         + s25) + s12*s34 - 2*mm2*(s13 + s15 - 2*s35) - s24*s35 + s12*s45)

  if12 = 2*s13*s14*s23 + s14*s15*s23 + s13*s14*s25 + 2*s12*s13*s34 + s12*s15*s34&
         + 2*s15*s23*s34 - s13*s25*s34 - 4*mm2*s13*(s13 + s15 - s35) - 2*s12*s14*s35&
         - s14*s23*s35 - s12*s34*s35 + s12*s13*s45 - s13*s23*s45 + 2*me2*(-2*s13*s24&
         - s15*s24 + 2*mm2*(4*s13 + s15 - s35) + s24*s35 - 2*s25*s45)

  em2emg_ee = 256*pi**3*alpha**3&
                *(if11/den11 + 2*if12/den12 + if22/den22)/denAll

  if (present(lin)) then
    if11 = me2**2*(8*mm2 - 4*s24) - 2*me2*(2*mm2 - s24)*(s13 + s15 - s35) &
          - s15*(s25*s34 - 4*mm2*s35 + s24*s35 + s23*s45)
    if22 = me2**2*(8*mm2 - 4*s24) - 2*me2*(2*mm2 - s24)*(s13 + s15 - s35) &
          + s35*(4*mm2*s15 - s15*s24 - s14*s25 - s12*s45)
    if12 = -2*s13**2*s24 - 2*s13*s15*s24 + s15*s25*s34 + 4*mm2*(s13 + s15)*(s13 - s35) &
          + 2*s13*s24*s35 + s14*s25*s35 + s15*s23*s45 -   2*s13*s25*s45 + s12*s35*s45 &
          + me2*(-8*mm2*s13 + 4*s13*s24 + 4*s25*s45)
    lin = 256*pi**3*alpha**3&
                  *(if11/den11 + 2*if12/den12 + if22/den22)/denAll
  endif

  END FUNCTION EM2EMG_EE


  !!!!!!!!!!    Qe**2*Qmu**4      !!!!!!!!!!!!


  FUNCTION EM2EML_MM(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2eml_mm
  real (kind=prec), optional :: pole
  !Corresponds to ee contribution after p2<->p1,q1<->q2
  if(present(pole)) then
    em2eml_mm = em2eml_ee(p2,p1,q2,q1,pole)
  else
   em2eml_mm = em2eml_ee(p2,p1,q2,q1)
  endif
  END FUNCTION EM2EML_MM


  FUNCTION EM2EMG_MM(p1,p2,q1,q2,q3,lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emg_mm
  real (kind=prec), optional :: lin
  !Corresponds to ee contribution after p2<->p1,q1<->q2
  if(present(lin)) then
    em2emg_mm = em2emg_ee(p2,p1,q2,q1,q3,lin)
  else
    em2emg_mm = em2emg_ee(p2,p1,q2,q1,q3)
  endif
  END FUNCTION EM2EMG_MM


  !!!!!!!!!!    Qe**3*Qmu**3     !!!!!!!!!!!!


  FUNCTION EM2EML_EM(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec) :: em2eml_em
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt,me2,mm2,mu2,me,mm
  real (kind=prec) :: logm,logmu,logt,discbs,discbu
  real (kind=prec) :: scalarc0tme,scalarc0tmm,scalarc0ir6s,scalarc0ir6u
  real (kind=prec), optional :: pole


  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)
  me=sqrt(me2);mm=sqrt(mm2)
  mu2 = musq
  logm = log(me2/mm2)
  logt = log(abs(-me2/tt))
  logmu = log(mu2/me2)
  discbs = DiscB(ss,me,mm)
  discbu = DiscB(2*me2+2*mm2-ss-tt,me,mm)
  scalarc0tme = ScalarC0(tt,me)
  scalarc0tmm = ScalarC0(tt,mm)
  scalarc0ir6s = ScalarC0IR6(ss,me,mm)
  scalarc0ir6u = ScalarC0IR6(2*me2+2*mm2-ss-tt,me,mm)

  em2eml_em = -2*scalarc0ir6u*(4*me2 + 4*mm2 - 2*ss - tt)*(me2 + mm2 - ss - tt)*tt - 2*scalarc&
            &0ir6s*(me2 + mm2 - ss)*tt*(2*ss + tt) + (2*logt*(2*(me2 + mm2 - ss) - tt)*tt*(16&
            &*me2*mm2 - tt**2))/((4*me2 - tt)*(-4*mm2 + tt)) + (2*scalarc0tme*(2*(me2 + mm2 -&
            & ss) - tt)*tt*(8*me2**2 - 8*me2*tt + tt**2))/(4*me2 - tt) + (2*scalarc0tmm*(2*(m&
            &e2 + mm2 - ss) - tt)*tt*(8*mm2**2 - 8*mm2*tt + tt**2))/(4*mm2 - tt) + (logm*(2*(&
            &me2 + mm2 - ss) - tt)*tt*(4*mm2*(-me2**2 + (mm2 - ss)**2 - 2*me2*ss) + (me2**2 +&
            & 3*mm2**2 + 2*mm2*ss + ss**2 - 2*me2*(2*mm2 + ss))*tt + (me2 - mm2 + ss)*tt**2))&
            &/(ss*(4*mm2 - tt)*(-2*me2 - 2*mm2 + ss + tt)) + (discbs*(-8*logt*(me2 + mm2 - ss&
            &)**3*ss - 2*(me2 + mm2 - ss)*((me2 - mm2)**2 - 2*(me2 + mm2)*ss + (1 + logm + 2*&
            &logt)*ss**2)*tt - (2*(me2 - mm2)**2 + (logm + 2*logt)*(me2 + mm2)*ss - (2 + logm&
            & + 2*logt)*ss**2)*tt**2))/(me2**2 + (mm2 - ss)**2 - 2*me2*(mm2 + ss)) + (discbu*&
            &(-2*logt*(me2 + mm2 - ss - tt)*(2*me2 + 2*mm2 - ss - tt)*(4*(me2 + mm2 - ss)**2 &
            &- 2*(2*(me2 + mm2) - 3*ss)*tt + 3*tt**2) + tt*(-2*(me2 + mm2 - ss)*(me2**2 + 4*l&
            &ogm*me2**2 - 2*me2*mm2 + 8*logm*me2*mm2 + mm2**2 + 4*logm*mm2**2 - 2*(1 + 2*logm&
            &)*(me2 + mm2)*ss + (1 + logm)*ss**2) + (-16*me2*mm2 + 14*logm*(me2 + mm2)**2 - (&
            &4 + 17*logm)*(me2 + mm2)*ss + (4 + 5*logm)*ss**2)*tt + (-((-2 + 7*logm)*(me2 + m&
            &m2)) + 2*(1 + 2*logm)*ss)*tt**2 + logm*tt**3)))/(me2**2 + (-mm2 + ss + tt)**2 - &
            &2*me2*(mm2 + ss + tt)) + 4*logmu*((discbs*(me2 + mm2 - ss)*ss*(-2*(me2 + mm2 - s&
            &s)**2 - 2*ss*tt - tt**2))/(me2**2 + (mm2 - ss)**2 - 2*me2*(mm2 + ss)) - (discbu*&
            &(me2 + mm2 - ss - tt)*(2*me2 + 2*mm2 - ss - tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt&
            & + tt**2))/(me2**2 + (-mm2 + ss + tt)**2 - 2*me2*(mm2 + ss + tt)))

  em2eml_em = 16*pi*alpha**3*em2eml_em/tt**2

  if (present(pole)) then
    pole = (discbs*(me2 + mm2 - ss)*ss*(-2*(me2 + mm2 - ss)**2 - 2*ss*tt - tt**2))/(me2**2 &
       &+ (mm2 - ss)**2 - 2*me2*(mm2 + ss)) - (discbu*(me2 + mm2 - ss - tt)*(2*me2 + 2*m&
       &m2 - ss - tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2))/(me2**2 + (-mm2 + ss + &
       &tt)**2 - 2*me2*(mm2 + ss + tt))
    pole = 64*pi*alpha**3*pole/tt**2
  endif

  END FUNCTION EM2EML_EM


  FUNCTION EM2EMG_EM(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: me2,mm2,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45
  real (kind=prec) :: if13,if14,if23,if24,den13,den14,den23,den24,denAll
  real (kind=prec) :: em2emg_em

  s12 = s(p1,p2); s13 = s(p1,q1); s14 = s(p1,q2); s15 = s(p1,q3)
  s23= s(p2,q1); s24 = s(p2,q2); s25 = s(p2,q3)
  s34 = s(q1,q2); s35 = s(q1,q3); s45 = s(q2,q3)
  me2=sq(p1); mm2=sq(p2)
  den13 = s15*s25; den14 = s25*s35; den23 = s15*s45
  den24 = s35*s45; denAll = (2*me2-s13)*(2*mm2-s24)

  if13 = -2*s12*s14*s23 + s14*s15*s23 - s15*s23*s24 - s13*s14*s25 + s14*s23*s25 - 2*s12**2*s34&
         + 2*s12*s15*s34 + 2*s12*s25*s34 - 2*s15*s25*s34 + s12*s14*s35 + 2*mm2*(-(s13*(s15 + s25))&
         + s12*(2*s13 - s35) + s15*(s23 - s34 + s35)) + s12*s23*s45 - 2*me2*(s15*s24 - s14*s25&
         + s24*s25 + mm2*(8*s12 - 4*(s15 + s25)) + s25*s34 - s25*s45 + s12*(-2*s24 + s45))

  if14 = 2*s14*s23**2 + 2*s14*s23*s25 + 2*s12*s23*s34 - s15*s23*s34 + s12*s25*s34 + s13*s25*s34&
         - 2*s14*s23*s35 - s12*s24*s35 - 2*s14*s25*s35 - s12*s34*s35 - 2*mm2*(s15*s23 + s13*(2*s23&
         + s25 - s35) - s12*s35 + s14*s35 - s15*s35) + s12*s23*s45 + 2*me2*(-2*s23*s24 + s14*s25&
         - s24*s25 - s25*s34 + 4*mm2*(2*s23 + s25 - s35) + s24*s35 + s23*s45 + s25*s45)

  if23 = 2*s14**2*s23 + 2*s14*s15*s23 + 2*s12*s14*s34 + s12*s15*s34 + s15*s24*s34 - s14*s25*s34&
         + s12*s14*s35 - s12*s13*s45 - 2*s14*s23*s45 - 2*s15*s23*s45 - s12*s34*s45 + 2*me2*(-(s15*s24)&
         - s14*(2*s24 + s25) + 4*mm2*(2*s14 + s15 - s45) + s12*s45 - s23*s45 + s24*s45 + s25*s45)&
         + 2*mm2*(s14*s35 + s15*(s23 - s34 + s35) + s13*(-2*s14 - s15 + s45))

  if24 = -2*s14*s23*s34 - s15*s23*s34 - s14*s25*s34 - 2*s12*s34**2 - s14*s23*s35 + s14*s24*s35&
         - 2*s12*s34*s35 + s13*s23*s45 - s14*s23*s45 - 2*s12*s34*s45 - 2*s12*s35*s45 + 2*mm2*((s12&
         - s14)*s35 + s15*(s34 + s35) + s13*(2*s34 + s35 + s45)) + 2*me2*(s25*s34 + s12*s45 - s23*s45&
         + s25*s45 - 4*mm2*(2*s34 + s35 + s45) + s24*(2*s34 + s35 + s45))

  em2emg_em = 128*pi**3*alpha**3&
               *2*(if13/den13 + if14/den14 + if23/den23 + if24/den24)/denAll
  END FUNCTION EM2EMG_EM




          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EM2EMeik_EE(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) + whatever
    !! for massive electrons
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real (kind=prec) :: em2emeik_ee, xicut, Epart, scms
  real (kind=prec), optional :: pole, lin

  scms = sq(p1+p2)
  Epart = sqrt(scms)

  em2emeik_ee = alpha/(2.*pi)*(2*Ireg(xicut,Epart,p1,p3) &
                  - Ireg(xicut,Epart,p1)- Ireg(xicut,Epart,p3))

  if (present(pole)) then
    pole = alpha/(2.*pi)*(2*Isin(xicut,Epart,p1,p3) &
                  - Isin(xicut,Epart,p1)- Isin(xicut,Epart,p3))
  endif
  if (present(lin)) then
    call crash("Not fully implemeted yet")
  endif

  END FUNCTION EM2EMeik_EE

  FUNCTION EM2EMll_EEEE(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  use mue_heavy_quark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), em2emll_eeee
  real(kind=prec) :: x, ss, tt, me2, mm2, lm
  real(kind=prec) :: f11, f12, f22, f11coeff, f12coeff, f22coeff

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p2)
  x = (sqrt(-tt+4*me2)-sqrt(-tt))/(sqrt(-tt+4*me2)+sqrt(-tt))

  call init_hpls(x)

  lm = log(musq/me2)
  f11 = ff1a1ef()**2 + 2*ff1a1el()*ff1a1es()                  &
        + Lm**2*(2*ff1a1es()**2 + 4*ff1a2ed()) + 2*ff1a2ef()  &
        + Lm*(4*ff1a1ef()*ff1a1es() + 4*ff1a2es())
  f22 = ff2a1ef()**2
  f12 = ff1a1ef()*ff2a1ef() + ff1a1es()*ff2a1el() + ff2a2ef() &
        + Lm*(2*ff1a1es()*ff2a1ef() + 2*ff2a2es())

  f11coeff = 4.*(mm2 - ss)**2*x**2 - 4.*me2*x*(ss - 2.*mm2*x + ss*x**2) &
              + 2.*me2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mm2 - ss)**2*x + me2**2*(2. - 3.*x + 2*x**2) &
                + me2*(mm2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * me2*(x-1.)**2*(me2 * (x-1.)**2 - 2*mm2*x)

  em2emll_eeee = f11coeff * f11 + f22coeff * f22 + f12coeff * f12
  em2emll_eeee = em2emll_eeee * 4.*alpha**4 / (me2**2 * (1-x)**4)
  END FUNCTION EM2EMLL_EEEE

  FUNCTION EM2EMff_EEEE(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons, requires xieik1 = xieik2
  use mue_heavy_quark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), em2emff_eeee
  real(kind=prec) :: x, ss, tt, me2, mm2
  real(kind=prec) :: f1(0:2), f2(0:2), f11, f22, f12
  real(kind=prec) :: f11coeff, f12coeff, f22coeff
  real(kind=prec) :: ctfin, ctpole, oldmu



  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p2)
  x = (sqrt(-tt+4*me2)-sqrt(-tt))/(sqrt(-tt+4*me2)+sqrt(-tt))

  oldmu = musq
  musq = me2
  ctfin = em2emeik_ee(p1, p2, p3, p4, xieik2, ctpole)
  musq = oldmu

  call init_hpls(x)

  ! TODO this is rather ugly..

  f1(0) = 1.

  f1(1) = ctfin
  f1(1) = f1(1) + ff1a1ef() / pi

  f1(2) = 0.5*ctfin**2
  f1(2) = f1(2) + (ff1a1ef() * ctfin + ff1a1el() * ctpole) / pi
  f1(2) = f1(2) + ff1a2ef() / pi**2

  f2(0) = 0.

  f2(1) = ff2a1ef() / pi

  f2(2) = 0.
  f2(2) = f2(2) + (ff2a1ef() * ctfin + ff2a1el() * ctpole) / pi
  f2(2) = f2(2) + ff2a2ef() / pi**2

  f11 = f1(1)**2 + 2*f1(0)*f1(2)
  f22 = f2(1)**2 + 2*f2(0)*f2(2)
  f12 = f1(2)*f2(0) + f1(1)*f2(1) + f1(0)*f2(2)

  f11coeff = 4.*(mm2 - ss)**2*x**2 - 4.*me2*x*(ss - 2.*mm2*x + ss*x**2) &
              + 2.*me2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mm2 - ss)**2*x + me2**2*(2. - 3.*x + 2*x**2) &
                + me2*(mm2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * me2*(x-1.)**2*(me2 * (x-1.)**2 - 2*mm2*x)

  em2emff_eeee = f11coeff * f11 + f22coeff * f22 + f12coeff * f12
  em2emff_eeee = em2emff_eeee * 4.*pi**2*alpha**4 / (me2**2 * (1-x)**4)
  END FUNCTION EM2EMff_EEEE


  FUNCTION EM2EMGf_EEEE(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf_eeee
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  em2emgf_eeee=0.

  mat0 = em2emg_ee(p1,p2,q1,q2, q3)

  ctfin = em2emeik_ee(p1, p2, q1, q2, xieik1, ctpole)

  em2emgf_eeee = em2emgl_eeee(p1,p2,q1,q2,q3, pole)

  if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"em2emgf is not finite",abs((ctpole*mat0+pole)/pole)
  em2emgf_eeee = em2emgf_eeee+ctfin*mat0

  END FUNCTION EM2EMGf_EEee



  FUNCTION EM2EMGl_eeee(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive electrons
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_eeee, p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: s13, s15, s35, mm2, gramdet

  s13 = s(p1,p3); s15 = s(p1,p5);
  s35 = s(p3,p5); mm2=sq(p1)

  gramdet = (-(s13*s15*s35) + mm2*(s15**2 + s35**2))

  if(present(pole)) then
    if(abs(gramdet)<1._prec) then
      em2emgl_eeee = em2emgl_eeee_coll(p1,p2,p3,p4,p5,pole)
    else
      em2emgl_eeee = em2emgl_eeee_pvred(p1,p2,p3,p4,p5,pole)
    endif
  else
    if(abs(gramdet)<1._prec) then
      em2emgl_eeee = em2emgl_eeee_coll(p1,p2,p3,p4,p5)
    else
      em2emgl_eeee = em2emgl_eeee_pvred(p1,p2,p3,p4,p5)
    endif
  endif
  END FUNCTION EM2EMGl_eeee


  !!! vacuum polarisation corrections !!!


  !! classification according to [1901.03106]


  !! class I (+ genuine 2-loop)
  FUNCTION EM2EM_AA(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2em_aa,deltalph0_tot,deltalph1_tot,tt

  tt = sq(p1-q1)
  ! bubble chain
  deltalph0_tot = deltalph_0_tot(tt)

  ! genuine 2-loop
  deltalph1_tot = deltalph_l_1_tot(tt)

  em2em_aa = (3*deltalph0_tot**2+2*deltalph1_tot)*em2em(p1,p2,q1,q2)
  END FUNCTION EM2EM_AA


  !! class II
  FUNCTION EM2EM_ALEE(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2em_alee,deltalph_tot,tt

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  em2em_alee = 2*deltalph_tot*em2eml_ee(p1,p2,q1,q2)

  END FUNCTION EM2EM_ALEE


  !! class III
  FUNCTION EM2EMG_AEE(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive electrons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emg_aee,deltalph_tot,tt

  tt = sq(p1-q1-q3)
  deltalph_tot = deltalph_0_tot(tt)
  em2emg_aee = 2*deltalph_tot*em2emg_ee(p1,p2,q1,q2,q3)
  END FUNCTION EM2EMG_AEE


  !! class IV
  FUNCTION EM2EM_NFEE(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2em_nfee, ss, x, mm2, tt, mp2
  real (kind=prec) :: F1, F2

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  F1 = 0._prec; F2 = 0._prec
  if(nel==1) then
    F1 = F1 + f1el_electron(tt)
    F2 = F2 + f2el_electron(tt)
  end if
  if(nmu==1) then
    F1 = F1 + f1el_muon(tt)
    F2 = F2 + f2el_muon(tt)
  end if
  if(ntau==1) then
    F1 = F1 + f1el_tau(tt)
    F2 = F2 + f2el_tau(tt)
  end if
  if(nhad==1) then
    F1 = F1 + f1el_had(tt)
    F2 = F2 + f2el_had(tt)
  end if


  em2em_nfee  = 64*mp2*x**2*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 2*mp2*x)&
                  + F1*(2*(mp2 - ss)**2*x**2 - 2*mm2*x*(ss - 2*mp2*x + ss*x**2)&
                  + mm2**2*(1 - 4*x + 8*x**2 - 4*x**3 + x**4)))

  em2em_nfee = alpha**2*pi**2/(mm2**2*mp2*(1-x)**4*x**2)*em2em_nfee

  END FUNCTION EM2EM_NFEE


  !!!  end vacuum polarisation corrections !!!




                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUPAIR           !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




  !!!!!!!!!!    Born    !!!!!!!!!!!!

  FUNCTION EE2MM(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: ee2mm

  ee2mm = em2em(p1,-p3,-p2,p4)

  END FUNCTION


  FUNCTION EE2MML(p1, p2, p3, p4,sing)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2mml
  real(kind=prec),optional :: sing
  real(kind=prec) :: sing_ee, sing_em, sing_mm

  if(present(sing)) then
    ee2mml = em2eml_ee(p1,-p3,-p2,p4,sing_ee)&
             +em2eml_em(p1,-p3,-p2,p4,sing_em)&
             +em2eml_mm(p1,-p3,-p2,p4,sing_mm)
    sing = sing_ee + sing_em + sing_mm
  else
    ee2mml = em2eml_ee(p1,-p3,-p2,p4)&
             +em2eml_em(p1,-p3,-p2,p4)&
             +em2eml_mm(p1,-p3,-p2,p4)
  end if
  END FUNCTION


  FUNCTION EE2MMG(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4) g(p5)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real(kind=prec) :: ee2mmg

  ee2mmg = em2emg_ee(p1,-p3,-p2,p4,p5)&
           +em2emg_em(p1,-p3,-p2,p4,p5)&
           +em2emg_mm(p1,-p3,-p2,p4,p5)

  END FUNCTION



                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUSE             !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  !!!!!!!!!!    Born    !!!!!!!!!!!!

  FUNCTION MP2MP(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive (and massless) muons
  real(kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt,tau,mp2,mm2,Ge,Gm,F1p,F2p
  real (kind=prec) :: mp2mp, c_F1,c_F1F2,c_F2

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  tau = -tt/4/mp2

  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  c_F1 = (2*(2*(mm2 + mp2 - ss)**2 + 2*ss*tt + tt**2))/tt
  c_F1F2 = 4*(2*mm2 + tt)
  c_F2 = -((mm2**2 + mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*mp2 + 2*ss + tt))/mp2)

  mp2mp = 16*pi**2*alpha**2/tt*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)
  END FUNCTION MP2MP


  !!!!!!!!!!    VP    !!!!!!!!!!!!


  FUNCTION MP2MP_A(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_a,tt,deltalph_tot

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  mp2mp_A = 2*deltalph_tot*mp2mp(p1,p2,q1,q2)
  END FUNCTION MP2MP_A


  !!!!!!!!!!    Vertex    !!!!!!!!!!!!

  FUNCTION MP2MPl(p1,p2,q1,q2,pole)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt,tau,mm2,mp2,mu2,logmm
  real (kind=prec) :: discbtmm,scalarc0ir6tmm,F1p,F2p,Ge,Gm
  real (kind=prec) :: mp2mpl,c_F1,c_F1F2,c_F2
  real (kind=prec), optional :: pole

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  tau = -tt/4/mp2
  mu2 = musq
  logmm = log(mu2/mm2)

  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)
  discbtmm = discb(tt,sqrt(mm2))
  scalarc0ir6tmm = scalarc0ir6(tt,sqrt(mm2))

  c_F1 = (2*(-2*(2*(mm2 + mp2 - ss)**2 + 2*ss*tt + tt**2)*(2 + logmm + scalarc0ir6tmm*(-2&
       &*mm2 + tt)) + (discbtmm*(-16*mm2*(mm2 + mp2 - ss)**2 + 2*(3*mm2**2 + 2*mm2*(mp2 &
       &- 7*ss) + 3*(mp2 - ss)**2)*tt + 6*(-2*mm2 + ss)*tt**2 + 3*tt**3 - 2*logmm*(2*mm2&
       & - tt)*(2*(mm2 + mp2 - ss)**2 + 2*ss*tt + tt**2)))/(4*mm2 - tt)))/tt

  c_F1F2 = (4*(2*(-2 - logmm + scalarc0ir6tmm*(2*mm2 - tt))*(4*mm2 - tt)*(2*mm2 + tt) + dis&
         &cbtmm*(-16*mm2**2 - 8*mm2*tt + 3*tt**2 + 2*logmm*(-4*mm2**2 + tt**2))))/(4*mm2 -&
         & tt)

  c_F2 = -((2*(-2 - logmm + scalarc0ir6tmm*(2*mm2 - tt))*(4*mm2 - tt)*(mm2**2 + mp2**2 - &
       &2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*(mp2 + ss) + tt)) + discbtmm*(-8*mm2**3 &
       &+ mm2**2*(16*(mp2 + ss) + 11*tt) - 2*mm2*(4*(mp2 - ss)**2 - 9*mp2*tt + 7*ss*tt +&
       & tt**2) + 3*tt*(mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt)) - 2*logmm*(2*mm2 - tt)*&
       &(mm2**2 + mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*(mp2 + ss) + tt))))/(&
       &mp2*(4*mm2 - tt)))

  mp2mpl = 8*pi*alpha**3/tt*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)

  if (present(pole)) then
    c_F1 = (-4*(2*mm2**2 + 2*mp2**2 + 4*mm2*(mp2 - ss) - 4*mp2*ss + 2*ss**2 + 2*ss*tt + tt**2)&
        *(-4*mm2 + tt + discbtmm*(-2*mm2 + tt)))/(tt*(-4*mm2 + tt))

    c_F1F2 = (-8*(2*mm2 + tt)*(-4*mm2 + tt + discbtmm*(-2*mm2 + tt)))/(-4*mm2 + tt)

    c_F2 =  (2*(4*mm2 + discbtmm*(2*mm2 - tt) - tt)* &
        (mm2**2 + mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*mp2 + 2*ss + tt)) &
        )/(mp2*(4*mm2 - tt))
    pole = 8*pi*alpha**3/tt*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)
  endif
  END FUNCTION MP2MPl


  !!!!!!!!!!    Real    !!!!!!!!!!!!

  FUNCTION MP2MPG(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive (and massless) muons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mm2,mp2,tau,tt,s12,s13,s15,s23,s25,s35,F1p,F2p,Ge,Gm
  real (kind=prec) :: mp2mpg,c_F1,c_F1F2,c_F2

  s12 = s(p1,p2); s13 = s(p1,q1); s15 = s(p1,q3)
  s23= s(p2,q1); s25 = s(p2,q3); s35 = s(q1,q3)
  mm2=sq(p1); mp2=sq(p2)
  tt = sq(p1-q1-q3); tau = -tt/4/mp2
  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  c_F1 = -8*(2*mm2*s15**2*(s12*s13 + s12*s15 - 2*mp2*(s13 + s15) + 2*s12*s23 - s13*s23 - &
       &s15*s23 + 2*s12*s25 - s13*s25 - s15*s25 + 4*mm2*(mp2 - s12 + s23 + s25)) + s15*(&
       &4*mp2*s13**2 - 2*s12*s13**2 + 4*mp2*s13*s15 - 2*s12*s13*s15 + 2*mp2*s15**2 - s12&
       &*s15**2 - 4*s12*s13*s23 + 2*s13**2*s23 - 2*s12*s15*s23 + 2*s13*s15*s23 + s15**2*&
       &s23 - 2*s15*s23**2 + (2*s13**2 + s15**2 - 2*s12*(s13 + s15) + 2*s13*(s15 + s23))&
       &*s25 + 2*mm2*(4*s12*s13 - s12*s15 + 2*mp2*(-2*s13 + s15) - 4*s13*s23 + s15*s23 -&
       & 4*s13*s25 + s15*s25 + 2*s25**2))*s35 + 2*(4*mm2**2*(mp2 - s12 + s23 + s25) + s1&
       &5*(s12**2 - s13*(2*mp2 + s23) + s12*(s13 + s23) - (s13 + s23)*s25) - mm2*(2*mp2*&
       &(s13 + s15) + (s13 + s15)*s23 - s12*(s13 + s15 + 2*s23) + (s13 + s15 + 2*s23)*s2&
       &5))*s35**2 + (2*mm2 + s15)*(2*mp2 - s12 + s23 + s25)*s35**3)

  c_F1F2 = -8*(8*mm2**3*(s15**2 + s35**2) - s15*(s12 + s13 + s15 - s23 - s25 - s35)*s35*(2*&
         &s13**2 + s15**2 + 2*s13*(s15 - s35) + s35**2) - 4*mm2**2*(3*s12*(s15**2 + s35**2&
         &) + (2*s15 - 3*(s23 + s25) - 2*s35)*(s15**2 + s35**2) + 2*s13*(s15**2 + s15*s35 &
         &+ s35**2)) + 2*mm2*(s13**2*(s15**2 + 4*s15*s35 + s35**2) + (s15**2 + s35**2)*(s1&
         &5**2 - s15*(s23 + s25 + s35) + s35*(s23 + s25 + s35)) + s13*(2*s15**3 - s15**2*(&
         &s23 + s25 - 2*s35) - 2*s15*s35*(3*(s23 + s25) + s35) - s35**2*(s23 + s25 + 2*s35&
         &)) + s12*((s15 - s35)*(s15**2 + s35**2) + s13*(s15**2 + 6*s15*s35 + s35**2))))

  c_F2 = -((8*mm2**3*(4*mp2 - s12 + s23 + s25)*(s15**2 + s35**2) - 4*mm2**2*((s12 - s23 -&
       & s25)*(s12*(s15**2 + s35**2) - (2*s15 + s23 + s25 - 2*s35)*(s15**2 + s35**2) - 2&
       &*s13*(s15**2 + s15*s35 + s35**2)) + 8*mp2*(s12*(s15**2 + s35**2) + (s15 - s23 - &
       &s25 - s35)*(s15**2 + s35**2) + s13*(s15**2 + s15*s35 + s35**2))) + s15*s35*(4*s1&
       &2**3*s35 - 4*mp2*(s13 + s15 - s35)*(2*s13**2 + s15**2 + 2*s13*(s15 - s35) + s35*&
       &*2) - s12**2*(2*s13**2 + s15*(s15 + 4*(s23 + s25)) + 2*s13*(s15 + 4*s23 + 2*s25 &
       &- s35) + 4*s25*s35 + s35**2) - (s23 + s25)*(2*s13**3 + s15*(s15**2 - 4*s23**2 + &
       &s15*(s23 + s25)) + 2*s13**2*(2*s15 + s23 + s25 - 2*s35) - (s15**2 + 4*s23*s25)*s&
       &35 + (s15 + s23 + s25)*s35**2 - s35**3 + s13*(3*s15**2 + 4*s23*s25 + 2*s15*(s23 &
       &+ s25 - 2*s35) - 2*(s23 + s25)*s35 + 3*s35**2)) + s12*(2*s13**3 + s15*(s15**2 + &
       &2*s15*(s23 + s25) + 4*s25*(2*s23 + s25)) + 4*s13**2*(s15 + s23 + s25 - s35) - (s&
       &15**2 + 4*s23*(s23 + 2*s25))*s35 + (s15 + 2*(s23 + s25))*s35**2 - s35**3 + s13*(&
       &3*s15**2 + 4*s15*s23 + 8*s23**2 + 4*s15*s25 + 16*s23*s25 + 4*s25**2 - 4*(s15 + s&
       &23 + s25)*s35 + 3*s35**2))) + 2*mm2*(s12**2*(s15**2*(s13 + s15 + 4*(s23 + s25)) &
       &- s15*(-2*s13 + s15)*s35 + (s13 + s15 + 4*s23)*s35**2 - s35**3) + s12*(-(s15**2*&
       &(s13**2 + s15**2 + 2*s15*(s23 + s25) + 4*(s23 + s25)**2 + 2*s13*(s15 + s23 + s25&
       &))) + s15*(16*mp2*s13 - 4*s13**2 + s15**2 + 2*s15*s23 + 2*s15*s25 + 4*s25**2 - 2&
       &*s13*(s15 + 2*(s23 + s25)))*s35 - (s13**2 + 2*s13*(-s15 + s23 + s25) + 2*(s15**2&
       & + s15*(s23 + s25) + 2*s23*(s23 + 2*s25)))*s35**2 + (2*s13 + s15 + 2*(s23 + s25)&
       &)*s35**3 - s35**4) + (s23 + s25)*(s15**3*(s15 + s23 + s25) - s15*(s15**2 + 4*s25&
       &**2 + s15*(s23 + s25))*s35 + (2*s15**2 + 4*s23*s25 + s15*(s23 + s25))*s35**2 - (&
       &s15 + s23 + s25)*s35**3 + s35**4 + s13*(2*s15 + s23 + s25 - 2*s35)*(s15 + s35)**&
       &2 + s13**2*(s15**2 + 4*s15*s35 + s35**2)) + 4*mp2*((s15**2 + s35**2)*(s15**2 - s&
       &15*s35 + s35**2) + s13**2*(s15**2 + 4*s15*s35 + s35**2) + 2*s13*(s15**3 + s15**2&
       &*s35 - s35**3 - s15*s35*(2*(s23 + s25) + s35)))))/mp2)

  mp2mpg = 32*pi**3*alpha**3/(tt*s15*s35)**2*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)
  END FUNCTION MP2MPG






          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION MP2MPll(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  use mue_heavy_quark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), mp2mpll
  real(kind=prec) :: x, ss, tt, mm2, mp2
  real(kind=prec) :: f11, f12, f22, f11coeff, f12coeff, f22coeff
  real(kind=prec) :: f1p, f2p, tau, Ge, Gm, lm

  ss = sq(p1+p2); tt = sq(p1-p3)
  mm2=sq(p1); mp2=sq(p2)
  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  tau = -tt/4/mp2
  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  call init_hpls(x)

  lm = log(musq/mm2)
  f11 = ff1a1ef()**2 + 2*ff1a1el()*ff1a1es()                  &
        + Lm**2*(2*ff1a1es()**2 + 4*ff1a2ed()) + 2*ff1a2ef()  &
        + Lm*(4*ff1a1ef()*ff1a1es() + 4*ff1a2es())
  f22 = ff2a1ef()**2
  f12 = ff1a1ef()*ff2a1ef() + ff1a1es()*ff2a1el() + ff2a2ef() &
        + Lm*(2*ff1a1es()*ff2a1ef() + 2*ff2a2es())

  ! F1p contribution
  f11coeff = 4.*(mp2 - ss)**2*x**2 - 4.*mm2*x*(ss - 2.*mp2*x + ss*x**2) &
              + 2.*mm2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mp2 - ss)**2*x + mm2**2*(2. - 3.*x + 2*x**2) &
                + mm2*(mp2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * mm2*(x-1.)**2*(mm2 * (x-1.)**2 - 2*mp2*x)

  mp2mpll = f1p**2 / (mm2**2 * (1-x)**4) * &
      (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F1p F2p contribution
  f11coeff = 4. * (1. - 4.*x + x**2)/(x-1.)**2
  f22coeff = -(1.-10.*x+x**2) / 2. / x
  f12coeff = 12
  mp2mpll = mp2mpll + f1p*f2p * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F2p**2 contribution
  f11coeff = x**2 * ((mp2 - ss)**2*x + mm2**2*(1.-x+x**2) &
        + mm2*(2.*mp2*(1. - 3.*x + x**2)- ss*(1. + x**2) )) / (x-1.)**2
  f22coeff = 0.25*(mp2 - ss)**2*x**2 - 0.25*mm2*x*(ss - 6.*mp2*x + ss*x**2) &
        + mm2**2*(1. - 4.*x + 10.*x**2 - 4.*x**3 + x**4) / 16.
  f12coeff = -0.5 * mm2 * x * (mm2 * (x-1)**2 - 8. * mp2 * x)

  mp2mpll = mp2mpll + f2p**2 * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12) / (mp2 * mm2 * x**2)

  mp2mpll = mp2mpll * alpha**4 * 4.

  END FUNCTION MP2MPLL


  FUNCTION MP2MPff(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  use mue_heavy_quark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), mp2mpff
  real(kind=prec) :: x, ss, tt, mm2, mp2
  real(kind=prec) :: f1(0:2), f2(0:2), f11, f22, f12
  real(kind=prec) :: f11coeff, f12coeff, f22coeff
  real(kind=prec) :: f1p, f2p, tau, Ge, Gm
  real(kind=prec) :: ctfin, ctpole, oldmu

  ss = sq(p1+p2); tt = sq(p1-p3)
  mm2=sq(p1); mp2=sq(p2)
  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  tau = -tt/4/mp2
  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  oldmu = musq
  musq = mm2
  ctfin = em2emeik_ee(p1, p2, p3, p4, xieik2, ctpole)
  musq = oldmu

  call init_hpls(x)

  ! TODO this is rather ugly..

  f1(0) = 1.

  f1(1) = ctfin
  f1(1) = f1(1) + ff1a1ef() / pi

  f1(2) = 0.5*ctfin**2
  f1(2) = f1(2) + (ff1a1ef() * ctfin + ff1a1el() * ctpole) / pi
  f1(2) = f1(2) + ff1a2ef() / pi**2

  f2(0) = 0.

  f2(1) = ff2a1ef() / pi

  f2(2) = 0.
  f2(2) = f2(2) + (ff2a1ef() * ctfin + ff2a1el() * ctpole) / pi
  f2(2) = f2(2) + ff2a2ef() / pi**2

  f11 = f1(1)**2 + 2*f1(0)*f1(2)
  f22 = f2(1)**2 + 2*f2(0)*f2(2)
  f12 = f1(2)*f2(0) + f1(1)*f2(1) + f1(0)*f2(2)

  ! F1p contribution
  f11coeff = 4.*(mp2 - ss)**2*x**2 - 4.*mm2*x*(ss - 2.*mp2*x + ss*x**2) &
              + 2.*mm2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mp2 - ss)**2*x + mm2**2*(2. - 3.*x + 2*x**2) &
                + mm2*(mp2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * mm2*(x-1.)**2*(mm2 * (x-1.)**2 - 2*mp2*x)

  mp2mpff = f1p**2 / (mm2**2 * (1-x)**4) * &
      (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F1p F2p contribution
  f11coeff = 4. * (1. - 4.*x + x**2)/(x-1.)**2
  f22coeff = -(1.-10.*x+x**2) / 2. / x
  f12coeff = 12
  mp2mpff = mp2mpff + f1p*f2p * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F2p**2 contribution
  f11coeff = x**2 * ((mp2 - ss)**2*x + mm2**2*(1.-x+x**2) &
        + mm2*(2.*mp2*(1. - 3.*x + x**2)- ss*(1. + x**2) )) / (x-1.)**2
  f22coeff = 0.25*(mp2 - ss)**2*x**2 - 0.25*mm2*x*(ss - 6.*mp2*x + ss*x**2) &
        + mm2**2*(1. - 4.*x + 10.*x**2 - 4.*x**3 + x**4) / 16.
  f12coeff = -0.5 * mm2 * x * (mm2 * (x-1)**2 - 8. * mp2 * x)

  mp2mpff = mp2mpff + f2p**2 * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12) / (mp2 * mm2 * x**2)

  mp2mpff = mp2mpff * alpha**4 * 4. * pi**2

  END FUNCTION MP2MPff

  !!! vacuum polarisation corrections !!!


  !! The categorisation in classes I-IV follows [1901.03106]


  !! class I (+genuine 2-loop!)
  FUNCTION MP2MP_AA(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_aa,deltalph0_tot,deltalph1_tot,tt

  tt = sq(p1-q1)
  ! bubble chain
  deltalph0_tot = deltalph_0_tot(tt)
  ! genuine 2-loop
  deltalph1_tot = deltalph_l_1_tot(tt)
  mp2mp_aa = (3*deltalph0_tot**2+2*deltalph1_tot)*mp2mp(p1,p2,q1,q2)
  END FUNCTION MP2MP_AA


  !! class II
  FUNCTION MP2MP_AL(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_al,deltalph_tot,tt

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  mp2mp_al = 2*deltalph_tot*mp2mpl(p1,p2,q1,q2)
  END FUNCTION MP2MP_AL


  !! class III
  FUNCTION MP2MPG_A(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpg_a,deltalph_tot,tt

  tt = sq(p1-q1-q3)
  deltalph_tot = deltalph_0_tot(tt)
  mp2mpg_a = 2*deltalph_tot*mp2mpg(p1,p2,q1,q2,q3)
  END FUNCTION MP2MPG_A


  !! class IV
  FUNCTION MP2MP_NF(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_nf, ss, x, mm2, tt, mp2
  real (kind=prec) :: Ge, Gm, tau, F1p, F2p, F1, F2, c_F1, c_F2, c_F1F2
  character(len=1) :: particle

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  tau = -tt/4/mp2

  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  if(abs(sqrt(mm2)-mel)/mel<1.E-6) then
    particle = 'e'
  elseif(abs(sqrt(sq(p1))-mmu)/mmu<1.E-6) then
    particle = 'm'
  else
    call crash("mp2mp_nf evaluated for masses other than muon and electron")
  endif

  F1 = 0._prec; F2 = 0._prec
  if(nel==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_electron(tt)
        F2 = F2 + f2el_electron(tt)
      case('m')
        F1 = F1 + f1mu_electron(tt)
        F2 = F2 + f2mu_electron(tt)
    end select
  end if
  if(nmu==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_muon(tt)
        F2 = F2 + f2el_muon(tt)
      case('m')
        F1 = F1 + f1mu_muon(tt)
        F2 = F2 + f2mu_muon(tt)
    end select
  end if
  if(ntau==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_tau(tt)
        F2 = F2 + f2el_tau(tt)
      case('m')
        F1 = F1 + f1mu_tau(tt)
        F2 = F2 + f2mu_tau(tt)
    end select
  end if
  if(nhad==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_had(tt)
        F2 = F2 + f2el_had(tt)
      case('m')
        F1 = F1 + f1mu_had(tt)
        F2 = F2 + f2mu_had(tt)
    end select
  end if


  c_F1 = 64*mp2*x**2*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 2*mp2*x)&
          + F1*(2*(mp2 - ss)**2*x**2 - 2*mm2*x*(ss - 2*mp2*x + ss*x**2)&
          + mm2**2*(1 - 4*x + 8*x**2 - 4*x**3 + x**4)))

  c_F1F2 = -64*mm2**2*mp2*(-1 + x)**2*x**2*(-3*F2*(-1 + x)**2&
           - 2*F1*(1 - 4*x + x**2))

  c_F2 = -8*mm2*(-1 + x)**2*x*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 8*mp2*x)&
         - 4*F1*x*((mp2 - ss)**2*x + mm2**2*(1 - x + x**2) + mm2*(-(ss*(1 + x**2))&
         + 2*mp2*(1 - 3*x + x**2))))

  mp2mp_nf = alpha**2*pi**2/(mm2**2*mp2*(1-x)**4*x**2)&
                *(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)

  END FUNCTION MP2MP_NF





  !!!  end vacuum polarisation corrections !!!



  FUNCTION MP2MPGl(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons
  real(kind=prec), optional :: pole
  real(kind=prec) :: mp2mpgl, p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: s13, s15, s35, mm2, gramdet

  s13 = s(p1,p3); s15 = s(p1,p5);
  s35 = s(p3,p5); mm2=sq(p1)

  gramdet = (-(s13*s15*s35) + mm2*(s15**2 + s35**2))

  if(present(pole)) then
    if(abs(gramdet)<1._prec) then
      mp2mpgl = mp2mpgl_coll(p1,p2,p3,p4,p5,pole)
    else
      mp2mpgl = mp2mpgl_pvred(p1,p2,p3,p4,p5,pole)
    endif
  else
    if(abs(gramdet)<1._prec) then
      mp2mpgl = mp2mpgl_coll(p1,p2,p3,p4,p5)
    else
      mp2mpgl = mp2mpgl_pvred(p1,p2,p3,p4,p5)
    endif
  endif
  END FUNCTION MP2MPGl


  FUNCTION MP2MPeik(p1,p2,p3,p4, xicut, pole, lin)
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real (kind=prec) :: mp2mpeik, xicut, Epart, scms
  real (kind=prec), optional :: pole, lin

  scms = sq(p1+p2)
  Epart = sqrt(scms)

  mp2mpeik = alpha/(2.*pi)*(2*Ireg(xicut,Epart,p1,p3) &
                  - Ireg(xicut,Epart,p1)- Ireg(xicut,Epart,p3))

  if (present(pole)) then
    pole = alpha/(2.*pi)*(2*Isin(xicut,Epart,p1,p3) &
                  - Isin(xicut,Epart,p1)- Isin(xicut,Epart,p3))
  endif
  if (present(lin)) then
    call crash("Not fully implemeted yet")
  endif
  END FUNCTION MP2MPeik

  FUNCTION MP2MPGf(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpgf
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = mp2mpg(p1,p2,q1,q2,q3)

  ctfin = mp2mpeik(p1, p2, q1, q2, xieik1,ctpole)

  mp2mpgf = mp2mpgl(p1,p2,q1,q2,q3, pole)

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"mp2mpgf is not finite",abs((ctpole*mat0+pole)/pole)
  mp2mpgf = mp2mpgf+ctfin*mat0 !no lin bit since FDH
  END FUNCTION MP2MPGf


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!         integrand function         !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EM2EMf_EE(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emf_ee, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emf_ee = em2eml_ee(p1,p2,p3,p4)
  em2emf_ee = em2emf_ee + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, p1, p3)
  END FUNCTION EM2EMf_EE
  FUNCTION EM2EMf_EM(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emf_em, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emf_em = em2eml_em(p1,p2,p3,p4)
  em2emf_em = em2emf_em + alpha/(2.*pi)*mat0*2*(Ireg(xieik1,Epart,p1,p4)&
             + Ireg(xieik1,Epart,p2,p3) - Ireg(xieik1,Epart,p1,p2)&
              - Ireg(xieik1,Epart,p3,p4))
  END FUNCTION EM2EMf_EM
  FUNCTION EM2EMf_MM(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emf_mm, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emf_mm = em2eml_mm(p1,p2,p3,p4)
  em2emf_mm = em2emf_mm + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, p2, p4)
  END FUNCTION EM2EMf_MM



  FUNCTION EM2EM_AFEE(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2em_afee, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em_a(p1,p2,p3,p4)
  em2em_afee = em2em_alee(p1,p2,p3,p4)
  em2em_afee = em2em_afee + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, p1, p3)
  END FUNCTION EM2EM_AFEE


  FUNCTION EE2MMF(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons and positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: ee2mmf, mat0, epart
  Epart = sqrt(scms)
  mat0 = ee2mm(p1,p2,p3,p4)
  ee2mmf =  ee2mml(p1,p2,p3,p4)
  ee2mmf = ee2mmf + alpha / (2 * pi) * mat0 * (Ieik(xieik1,Epart,p1,p2)&
                                 - Ieik(xieik1,Epart,p1,p3) + Ieik(xieik1,Epart,p1,p4)&
                                 + Ieik(xieik1,Epart,p2,p3) - Ieik(xieik1,Epart,p2,p4)&
                                 + Ieik(xieik1,Epart,p3,p4))

  END FUNCTION EE2MMF


  FUNCTION MP2MPf(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: mp2mpf, mat0, Epart
  Epart = sqrt(scms)

  mat0 = mp2mp(p1,p2,p3,p4)
  mp2mpf = mp2mpl(p1,p2,p3,p4)
  mp2mpf = mp2mpf + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, p1, p3)
  END FUNCTION MP2MPf


  FUNCTION MP2MP_AF(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: mp2mp_af, mat0, Epart
  Epart = sqrt(scms)
  mat0 = mp2mp_a(p1,p2,p3,p4)
  mp2mp_af = mp2mp_al(p1,p2,p3,p4)
  mp2mp_af = mp2mp_af + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, p1, p3)
  END FUNCTION MP2MP_AF



  FUNCTION EM2EMl(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2eml
  em2eml = em2eml_ee(p1,p2,p3,p4) + em2eml_mm(p1,p2,p3,p4) + em2eml_em(p1,p2,p3,p4)
  END FUNCTION EM2EMl

  FUNCTION EM2EMC(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emc, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emc = alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, p1, p3)

  em2emc = em2emc + alpha/(2.*pi)*mat0*2*(Ireg(xieik1,Epart,p1,p4)&
             + Ireg(xieik1,Epart,p2,p3) - Ireg(xieik1,Epart,p1,p2)&
              - Ireg(xieik1,Epart,p3,p4))

  em2emc = em2emc + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, p2, p4)
  END FUNCTION EM2EMC


                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MUE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!

