
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                  MODULE MUDECRARE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use mudecrare_pm2ennee
  implicit none

  contains


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!     MATRIX   ELEMENTS       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  FUNCTION PT2MNNEEav(p1,n1,p2,qA,qB,p3,p4)
    !! tau+(p1) -> mu+(p2) \nu_mu() \bar{\tau}_\mu() e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e( )  \nu_\tau() e+(p3) e-(p4)
  use mudecrare_1l_twotrace, only: born, bornPol,        &
                               s12,s13,s14, s23,s24, s34,&
                               M12, m22, m32, initevent

  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), pt2mnneeav
  real(kind=prec) :: qA(4), qB(4)

  if (any(abs(n1)>0)) then
    call initevent(p1,p2,p3,p4, -n1, .false.)
    pt2mnneeav = bornPol(s12, s13, s14, s23, s24, s34,M12, m22, m32) * sqrt(m12)
  else
    pt2mnneeav = 0.
    call initevent(p1,p2,p3,p4, collier=.false.)
  endif
  pt2mnneeav = pt2mnneeav + born(s12, s13, s14, s23, s24, s34,M12, m22, m32)
  pt2mnneeav = pt2mnneeav*4*pi**2*alpha**2*GF**2
  END FUNCTION

  FUNCTION PM2ENNEEav(p1,n1,p2,qa,qb,p3,p4)
    !! mu+(p1) -> e+(p2) \nu_mu(qA) \bar{\nu}_\mu(qB) e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e(qA)  \nu_\tau(qB) e-(p4) e+(p3)
  use mudecrare_1l_twotrace, only: tborn=>born, tbornPol=>bornPol,    &
                               tinitevent=>initevent,                 &
                               s12, s13, s14, s23, s24, s34, m12, m22

  use mudecrare_1l_onetrace, only: oborn=>born,obornPol=>bornPol,     &
                               oimborn=>imborn, oimbornPol=>imbornpol,&
                               oinitevent=>initevent

  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), pm2enneeav
  real(kind=prec) :: qA(4), qB(4)
  logical polarised

  polarised = any(abs(n1)>0)

  if (polarised) then
    call tinitevent(p1,p2,p3,p4,-n1, collier=.false.)
    call oinitevent(p1,p2,p3,p4,-n1, collier=.false.)
    pm2enneeav = (  tbornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22) &
                 +2*obornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22)) * sqrt(m12)
  else
    call tinitevent(p1,p2,p3,p4, collier=.false.)
    call oinitevent(p1,p2,p3,p4, collier=.false.)
    pm2enneeav = 0.
  endif
  pm2enneeav = pm2enneeav +   tborn(s12, s13, s14, s23, s24, s34, M12, m22, m22) &
                          + 2*oborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)

  if (polarised) then
    call tinitevent(p1,p4,p3,p2,-n1, collier=.false.)
    call oinitevent(p1,p4,p3,p2,-n1, collier=.false.)
    pm2enneeav = pm2enneeav + &
               (  tbornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22) &
               +2*obornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22)) * sqrt(m12)
  else
    call tinitevent(p1,p4,p3,p2, collier=.false.)
    call oinitevent(p1,p4,p3,p2, collier=.false.)
  endif

  pm2enneeav = pm2enneeav +   tborn(s12, s13, s14, s23, s24, s34, M12, m22, m22) &
                          + 2*oborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)

  pm2enneeav = 4*pm2enneeav*pi**2*alpha**2*GF**2
  END FUNCTION


  FUNCTION PT2MNNEElav(p1,n1,p2,qA,qB,p3,p4, single)
    !! tau+(p1) -> mu+(p2) \nu_mu() \bar{\tau}_\mu() e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e( )  \nu_\tau() e+(p3) e-(p4)
  use mudecrare_1l_twotrace, only: bubble, triag, box1, box2,pentagon,&
                               eoepart, renorm, pole, born, bornPol,  &
                               asympart,asympartpol,asym,             &
                               bubblepol, triagpol, box1pol, box2pol, &
                               pentagonpol, eoepartpol, renormpol,    &
                               asympart123,asy123, asympart124,asy124,&
                               asympart134,asy134, asympart234,asy234,&
                               s12,s13,s14, s23,s24, s34,             &
                               M12, m22, m32, initevent, vppref

  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real (kind=prec) :: pt2mnneelav, fullborn
  real (kind=prec), optional :: single

  if (any(abs(n1)>0)) then
    call initevent(p1,p2,p3,p4, -n1)
    pt2mnneelav =             + bubblePol  ()
    pt2mnneelav = pt2mnneelav + triagPol   ()
    pt2mnneelav = pt2mnneelav + box1Pol    ()
    pt2mnneelav = pt2mnneelav + box2Pol    ()
    pt2mnneelav = pt2mnneelav + pentagonPol()
    pt2mnneelav = pt2mnneelav + eoepartPol ()
    pt2mnneelav = pt2mnneelav + renormPol  ()

    pt2mnneelav = pt2mnneelav + asympartPol()*asym
    pt2mnneelav = pt2mnneelav + asympart123()*asy123
    pt2mnneelav = pt2mnneelav + asympart124()*asy124
    pt2mnneelav = pt2mnneelav + asympart134()*asy134
    pt2mnneelav = pt2mnneelav + asympart234()*asy234

    fullborn = bornPol(s12, s13, s14, s23, s24, s34,M12, m22, m32) * sqrt(m12)


    !imborn = (4*m32**2 - (s13 + s14 - s34)*s34 + m32*(-s13 - s14 + s23 + s24 + 4*s34))
    !imborn = imborn *
    !imborn = imborn / (-2*m32 + s13 + s14 - s34)**2/(2*m32 + s34)**2/(2*m32 + s23 + s24 +  s34)
    !imborn = imborn * (-2./3.)

    !call crash("imborn does not work yet")


    pt2mnneelav = pt2mnneelav * sqrt(M12)
  else
    call initevent(p1,p2,p3,p4)
    pt2mnneelav = 0.
    fullborn = 0.
  endif

  fullborn = fullborn + born(s12, s13, s14, s23, s24, s34,M12, m22, m32)


  pt2mnneelav = pt2mnneelav + bubble  ()
  pt2mnneelav = pt2mnneelav + triag   ()
  pt2mnneelav = pt2mnneelav + box1    ()
  pt2mnneelav = pt2mnneelav + box2    ()
  pt2mnneelav = pt2mnneelav + pentagon()
  pt2mnneelav = pt2mnneelav + eoepart ()
  pt2mnneelav = pt2mnneelav + renorm  ()
  pt2mnneelav = pt2mnneelav + asympart()*asym


  pt2mnneelav = pt2mnneelav*64*pi*alpha**3*GF**2


  if (present(single)) then
    single =        pole(s12, s13, s14, s23, s24, s34,M12, m22, m32)
    single = single*fullborn
    single = single*GF**2*alpha**3*4*pi
  endif

  END FUNCTION PT2MNNEElav


  FUNCTION PM2ENNEElav(p1,n1,p2,qA,qB,p3,p4, single)
    !! mu+(p1) -> e+(p2) \nu_mu() \bar{\tau}_\mu() e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e( )  \nu_\tau() e-(p4) e+(p3)
  use mudecrare_1l_twotrace, only: tbubble=>bubble, tbubblePol=>bubblePol,   &
                               ttriag=>triag, ttriagPol=>triagPol,           &
                               tbox1=>box1, tbox1Pol=>box1Pol,               &
                               tbox2=>box2, tbox2Pol=>box2Pol,               &
                               tpentagon=>pentagon,tpentagonPol=>pentagonPol,&
                               teoepart=>eoepart, teoepartPol=>eoepartPol,   &
                               tasympart=>asympart,tasympartPol=>asympartPol,&
                               trenorm=>renorm, trenormPol=>renormPol,       &
                               tasympart123=>asympart123,asy123,             &
                               tasympart124=>asympart124,asy124,             &
                               tasympart134=>asympart134,asy134,             &
                               tasympart234=>asympart234,asy234,             &
                               tborn=>born, tbornPol=>bornPol,               &
                               tinitevent=>initevent, asym, vppref, pole,    &
                               s12, s13, s14, s23, s24, s34, m12, m22

  use mudecrare_1l_onetrace, only: obubble=>bubble, obubblePol=>bubblePol,   &
                               otriag=>triag, otriagPol=>triagPol,           &
                               obox1=>box1, obox1Pol=>box1Pol,               &
                               obox2=>box2, obox2Pol=>box2Pol,               &
                               opentagon=>pentagon,opentagonPol=>pentagonPol,&
                               oeoepart=>eoepart, oeoepartPol=>eoepartPol,   &
                               oasympart=>asympart,oasympartPol=>asympartPol,&
                               orenorm=>renorm, orenormPol=>renormPol,       &
                               oasympart123=>asympart123,                    &
                               oasympart124=>asympart124,                    &
                               oasympart134=>asympart134,                    &
                               oasympart234=>asympart234,                    &
                               oborn=>born,obornPol=>bornPol,                &
                               oimborn=>imborn, oimbornPol=>imbornpol,       &
                               oinitevent=>initevent


  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real (kind=prec) :: pm2enneelav, fullborn, polinter
  real (kind=prec), optional :: single
  logical polarised
  polarised = any(abs(n1)>0)

  if (polarised) then
    call tinitevent(p1,p2,p3,p4,-n1, sameq=.true.)
    call oinitevent(p1,p2,p3,p4,-n1)
    fullborn = (  tbornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22) &
               +2*obornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22)) * sqrt(m12)

    polinter =          + tbubblePol  ()
    polinter = polinter + ttriagPol   ()
    polinter = polinter + tbox1Pol    ()
    polinter = polinter + tbox2Pol    ()
    polinter = polinter + tpentagonPol()
    polinter = polinter + teoepartPol ()
    polinter = polinter + trenormPol  ()

    polinter = polinter + tasympartPol()*asym
    polinter = polinter + tasympart123()*asy123
    polinter = polinter + tasympart124()*asy124
    polinter = polinter + tasympart134()*asy134
    polinter = polinter + tasympart234()*asy234


    polinter = polinter + 2*obubblePol  ()
    polinter = polinter + 2*otriagPol   ()
    polinter = polinter + 2*obox1Pol    ()
    polinter = polinter + 2*obox2Pol    ()
    polinter = polinter + 2*opentagonPol()
    polinter = polinter + 2*oeoepartPol ()
    polinter = polinter + 2*orenormPol  ()

    polinter = polinter + 2*oasympartPol()*asym
    polinter = polinter + 2*oasympart123()*asy123
    polinter = polinter + 2*oasympart124()*asy124
    polinter = polinter + 2*oasympart134()*asy134
    polinter = polinter + 2*oasympart234()*asy234

    pm2enneelav = polinter * sqrt(M12)
  else
    call tinitevent(p1,p2,p3,p4, sameq=.true.)
    call oinitevent(p1,p2,p3,p4)
    fullborn = 0.
    pm2enneelav = 0.
  endif

  fullborn = fullborn +   tborn(s12, s13, s14, s23, s24, s34, M12, m22, m22) &
                      + 2*oborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)

  pm2enneelav = pm2enneelav + tbubble  ()
  pm2enneelav = pm2enneelav + ttriag   ()
  pm2enneelav = pm2enneelav + tbox1    ()
  pm2enneelav = pm2enneelav + tbox2    ()
  pm2enneelav = pm2enneelav + tpentagon()
  pm2enneelav = pm2enneelav + teoepart ()
  pm2enneelav = pm2enneelav + trenorm  ()
  pm2enneelav = pm2enneelav + tasympart()*asym

  pm2enneelav = pm2enneelav + 2*obubble  ()
  pm2enneelav = pm2enneelav + 2*otriag   ()
  pm2enneelav = pm2enneelav + 2*obox1    ()
  pm2enneelav = pm2enneelav + 2*obox2    ()
  pm2enneelav = pm2enneelav + 2*opentagon()
  pm2enneelav = pm2enneelav + 2*oeoepart ()
  pm2enneelav = pm2enneelav + 2*orenorm  ()
  pm2enneelav = pm2enneelav + 2*oasympart()*asym

  if (present(single)) then
    single = fullborn
  endif



  if (polarised) then
    call tinitevent(p1,p4,p3,p2,-n1, sameq=.true.)
    call oinitevent(p1,p4,p3,p2,-n1)
    fullborn = (  tbornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22) &
               +2*obornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22)) * sqrt(m12)

    polinter =          + tbubblePol  ()
    polinter = polinter + ttriagPol   ()
    polinter = polinter + tbox1Pol    ()
    polinter = polinter + tbox2Pol    ()
    polinter = polinter + tpentagonPol()
    polinter = polinter + teoepartPol ()
    polinter = polinter + trenormPol  ()

    polinter = polinter + tasympartPol()*asym
    polinter = polinter + tasympart123()*asy123
    polinter = polinter + tasympart124()*asy124
    polinter = polinter + tasympart134()*asy134
    polinter = polinter + tasympart234()*asy234


    polinter = polinter + 2*obubblePol  ()
    polinter = polinter + 2*otriagPol   ()
    polinter = polinter + 2*obox1Pol    ()
    polinter = polinter + 2*obox2Pol    ()
    polinter = polinter + 2*opentagonPol()
    polinter = polinter + 2*oeoepartPol ()
    polinter = polinter + 2*orenormPol  ()

    polinter = polinter + 2*oasympartPol()*asym
    polinter = polinter + 2*oasympart123()*asy123
    polinter = polinter + 2*oasympart124()*asy124
    polinter = polinter + 2*oasympart134()*asy134
    polinter = polinter + 2*oasympart234()*asy234

    pm2enneelav = pm2enneelav + polinter * sqrt(M12)
  else
    call tinitevent(p1,p4,p3,p2, sameq=.true.)
    call oinitevent(p1,p4,p3,p2)
    fullborn = 0.
  endif

  fullborn = fullborn +   tborn(s12, s13, s14, s23, s24, s34, M12, m22, m22) &
                      + 2*oborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)

  pm2enneelav = pm2enneelav + tbubble  ()
  pm2enneelav = pm2enneelav + ttriag   ()
  pm2enneelav = pm2enneelav + tbox1    ()
  pm2enneelav = pm2enneelav + tbox2    ()
  pm2enneelav = pm2enneelav + tpentagon()
  pm2enneelav = pm2enneelav + teoepart ()
  pm2enneelav = pm2enneelav + tasympart()*asym
  pm2enneelav = pm2enneelav + trenorm  ()

  pm2enneelav = pm2enneelav + 2*obubble  ()
  pm2enneelav = pm2enneelav + 2*otriag   ()
  pm2enneelav = pm2enneelav + 2*obox1    ()
  pm2enneelav = pm2enneelav + 2*obox2    ()
  pm2enneelav = pm2enneelav + 2*opentagon()
  pm2enneelav = pm2enneelav + 2*oeoepart ()
  pm2enneelav = pm2enneelav + 2*orenorm  ()
  pm2enneelav = pm2enneelav + 2*oasympart()*asym

  if (present(single)) then
    single = single+fullborn
    single = single*pole(s12, s13, s14, s23, s24, s34,M12, m22, m22)
    single = single*GF**2*alpha**3*4*pi
  endif


  pm2enneelav = 2*pm2enneelav*32*pi*alpha**3*GF**2

  END FUNCTION PM2ENNEElav


  FUNCTION PT2MNNEEGav(p1,n1,p2,qA,qB,p3,p4,p7)
    !! M1^+(p1)->M2^+(p2),M3^-(p3),M3^+(p4),2\nu,\gamma(p7)

  use mudecrare_pm2enneeg_two, only: msq, msqpol
  real(kind=prec) :: pt2mnneegav
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p7(4), n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real(kind=prec) :: den(10)
  real(kind=prec) :: m12, m22, m32
  real(kind=prec) :: s12, s13, s14, s17, s23, s24, s27, s34, s37, s47
  real(kind=prec) :: s2n, s3n, s4n, s7n

  m12 = sq(p1) ; m22 = sq(p2); m32 = sq(p3)

  s12 = s(p1,p2)
  s13 = s(p1,p3)
  s14 = s(p1,p4)
  s17 = s(p1,p7)
  s23 = s(p2,p3)
  s24 = s(p2,p4)
  s27 = s(p2,p7)
  s34 = s(p3,p4)
  s37 = s(p3,p7)
  s47 = s(p4,p7)

  s2n = -s(p2, n1)
  s3n = -s(p3, n1)
  s4n = -s(p4, n1)
  s7n = -s(p7, n1)

  den = 1._prec/(/  (M32+s47-M32), &
                    (2*M32+s47+s37+s34), &
                    (M12+2*M32-s17+s47+s37+s34-s14-s13-M12), &
                    (M22+2*M32+s27+s47+s37+s34+s24+s23-M22), &
                    (M32+s37-M32), &
                    (2*M32+s34), &
                    (M12+2*M32+s34-s14-s13-M12), &
                    (M12-s17-M12), &
                    (M22+2*M32+s34+s24+s23-M22), &
                    (M22+s27-M22) /)

  pt2mnneegav = MSq(den, M12, M22, M32, s12, s13, s14, s17, s23, s24, s27, s34, s37, s47)

  if (any(abs(n1)>0)) then
    pt2mnneegav = pt2mnneegav + sqrt(m12)*MSqPol(den, M12, M22, M32, &
           s12, s13, s14, s17, s23, s24, s27, s34, s37, s47, s2n, s3n, s4n, s7n)

  endif


  pt2mnneegav = 128 * alpha**3*pi**3*pt2mnneegav
  END FUNCTION


  FUNCTION PM2ENNEEGav(p1,n1,p2,qA,qB,p3,p4,p7)
    !!  M1^+(p1)->M2^+(p2),M2^-(p3),M2^+(p4),2\nu,\gamma(p7)
  use mudecrare_pm2enneeg_two, only: msq, msqpol
  use mudecrare_pm2enneeg_one, only: mint, mintpol

  implicit none
  real(kind=prec) :: pm2enneegav
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p7(4), n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real(kind=prec) :: m12, m22
  real(kind=prec) :: den(20)
  real(kind=prec) :: s12, s13, s14, s17, s23, s24, s27, s34, s37, s47
  real(kind=prec) :: s2n, s3n, s4n, s7n

  m12 = sq(p1) ; m22 = sq(p2)

  s12 = s(p1,p2)
  s13 = s(p1,p3)
  s14 = s(p1,p4)
  s17 = s(p1,p7)
  s23 = s(p2,p3)
  s24 = s(p2,p4)
  s27 = s(p2,p7)
  s34 = s(p3,p4)
  s37 = s(p3,p7)
  s47 = s(p4,p7)

  s2n = -s(p2, n1)
  s3n = -s(p3, n1)
  s4n = -s(p4, n1)
  s7n = -s(p7, n1)

  den = 1._prec/(/  (M22+s47-M22), &
                    (2.*M22+s47+s37+s34), &
                    (M12+2.*M22-s17+s47+s37+s34-s14-s13-M12), &
                    (M22+2.*M22+s27+s47+s37+s34+s24+s23-M22), &
                    (M22+s37-M22), &
                    (2.*M22+s34), &
                    (M12+2.*M22+s34-s14-s13-M12), &
                    (M12-s17-M12), &
                    (M22+2.*M22+s34+s24+s23-M22), &
                    (M22+s27-M22), &
                    (M22+s27-M22), &
                    (2.*M22+s27+s37+s23), &
                    (M12+2.*M22-s17+s27+s37+s23-s12-s13-M12), &
                    (M22+2.*M22+s47+s27+s37+s23+s24+s34-M22), &
                    (M22+s37-M22), &
                    (2.*M22+s23), &
                    (M12+2.*M22+s23-s12-s13-M12), &
                    (M12-s17-M12), &
                    (M22+2.*M22+s23+s24+s34-M22), &
                    (M22+s47-M22)                    /)

  pm2enneegav = 0.
  if (any(abs(n1)>0)) then
    pm2enneegav = pm2enneegav +   MSqpol (den(:10), M12, M22, M22, s12, s13, s14, s17, s23, s24, s27, s34, s37, s47, &
        s2n, s3n, s4n, s7n)
    pm2enneegav = pm2enneegav + 2*Mintpol(den(:17), M12, M22,      s12, s13, s14, s17, s23, s24, s27, s34, s37, s47, &
        s2n, s3n, s4n, s7n)
    pm2enneegav = pm2enneegav +   MSqPol (den(11:), M12, M22, M22, s14, s13, s12, s17, s34, s24, s47, s23, s37, s27, &
        s4n, s3n, s2n, s7n)
    pm2enneegav = sqrt(m12)*pm2enneegav
  endif

  pm2enneegav = pm2enneegav +   MSq (den(:10), M12, M22, M22, s12, s13, s14, s17, s23, s24, s27, s34, s37, s47)
  pm2enneegav = pm2enneegav + 2*Mint(den(:17), M12, M22,      s12, s13, s14, s17, s23, s24, s27, s34, s37, s47)
  pm2enneegav = pm2enneegav +   MSq (den(11:), M12, M22, M22, s14, s13, s12, s17, s34, s24, s47, s23, s37, s27)

  pm2enneegav = 2*64  * alpha**3*pi**3*pm2enneegav

  END FUNCTION


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) e+(p5) e-(p6) g(p7)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) e-(p5) e+(p6) g(p7)
  ! RELIES ON GOSAM
  ! for massive (and massless) electron
  FUNCTION PM2ENNEEG(k1,pol,k2,k3,k4,k5,k6,k7)
  use mudecrare_all_amps, only: amplitude, pol_init

  real (kind=prec) :: k1(4),k7(4),k2(4),k3(4),k4(4),k5(4),k6(4)
  real (kind=prec) :: pm2enneeg,pol(4),npol(4)

  npol = -pol
  call pol_init(npol)
  pm2enneeg=2*amplitude(k1,k4,k2,k3,k6,k5,k7)
       !! contains 1/2 from spin average

       !! amplitude contains 1/2 from identical particles in final state
       !! factor 2 in  m2enneeg=2*amplitude to take this one out again

  END FUNCTION PM2ENNEEG



  FUNCTION PM2ENNEEcav(p1, n1, p2, p3, p4, p5, p6)
    !! mu+(p1) -> e+(p2) \nu_mu(qA) \bar{\nu}_\mu(qB) e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e(qA)  \nu_\tau(qB) e-(p4) e+(p3)
    !! for massive electron
  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), p5(4), p6(4)
  real(kind=prec) :: pm2enneecav, mat0, Epart
  Epart = Mm
  mat0 = pm2enneeav(p1,pol1,p2,p3,p4,p5,p6)
  pm2enneecav = alpha/(2.*pi)*mat0*(  &
         - Ireg(xieik1,Epart,p1) - Ireg(xieik1,Epart,p2) - &
           Ireg(xieik1,Epart,p6) - Ireg(xieik1,Epart,p5) + &
           2*Ireg(xieik1,Epart,p1,p2) + 2*Ireg(xieik1,Epart,p1,p6) - &
           2*Ireg(xieik1,Epart,p1,p5) - 2*Ireg(xieik1,Epart,p2,p6) + &
           2*Ireg(xieik1,Epart,p2,p5) + 2*Ireg(xieik1,Epart,p6,p5))
  END FUNCTION


  FUNCTION PT2MNNEEcav(p1, n1, p2, p3, p4, p5, p6)
    !! tau+(p1) -> mu+(p2) \nu_mu(qA) \bar{\nu}_\mu(qB) e+(p4) e-(p3)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e(qA)  \nu_\tau(qB) e-(p4) e+(p3)
    !! for massive electron
  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), p5(4), p6(4)
  real(kind=prec) :: pt2mnneecav, mat0, Epart
  Epart = sqrt(sq(p1))
  mat0 = pt2mnneeav(p1,pol1,p2,p3,p4,p5,p6)
  pt2mnneecav = alpha/(2.*pi)*mat0*(  &
         - Ireg(xieik1,Epart,p1) - Ireg(xieik1,Epart,p2) - &
           Ireg(xieik1,Epart,p6) - Ireg(xieik1,Epart,p5) + &
           2*Ireg(xieik1,Epart,p1,p2) + 2*Ireg(xieik1,Epart,p1,p6) - &
           2*Ireg(xieik1,Epart,p1,p5) - 2*Ireg(xieik1,Epart,p2,p6) + &
           2*Ireg(xieik1,Epart,p2,p5) + 2*Ireg(xieik1,Epart,p6,p5))
  END FUNCTION




  FUNCTION PT2MNNEEav_a(p1,n1,p2,qA,qB,p3,p4)
    !! tau+(p1) -> mu+(p2) \nu_mu() \bar{\tau}_\mu() e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{mu}_e( )  \nu_\tau() e+(p3) e-(p4)
  use mudecrare_1l_twotrace, only: born, bornPol,        &
                               s12,s13,s14, s23,s24, s34,&
                               M12, m22, m32, initevent

  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), pt2mnneeav_a
  real(kind=prec) :: qA(4), qB(4), deltalph_tot

  if (any(abs(n1)>0)) then
    call initevent(p1,p2,p3,p4, -n1, .false.)
    pt2mnneeav_a = bornPol(s12, s13, s14, s23, s24, s34,M12, m22, m32) * sqrt(m12)
  else
    pt2mnneeav_a = 0.
    call initevent(p1,p2,p3,p4, collier=.false.)
  endif

  deltalph_tot = Nel *deltalph_l_0(2*m32+s34,mel) &
               + Nmu *deltalph_l_0(2*m32+s34,mmu) &
               + Ntau*deltalph_l_0(2*m32+s34,mtau)&
               + Nhad*deltalph_h  (2*m32+s34)

  pt2mnneeav_a = pt2mnneeav_a + born(s12, s13, s14, s23, s24, s34,M12, m22, m32)
  pt2mnneeav_a = deltalph_tot * pt2mnneeav_a*4*pi**2*alpha**2*GF**2 * 2

  END FUNCTION

  FUNCTION PM2ENNEEav_a(p1,n1,p2,qA,qB,p3,p4)
    !! mu+(p1) -> e+(p2) \nu_mu() \bar{\tau}_\mu() e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e( )  \nu_\tau() e-(p4) e+(p3)
    !! for massive electron
  use mudecrare_1l_twotrace, only: tborn=>born, tbornPol=>bornPol,    &
                               tinitevent=>initevent,                 &
                               s12, s13, s14, s23, s24, s34, m12, m22

  use mudecrare_1l_onetrace, only: oborn=>born,obornPol=>bornPol,     &
                               oimborn=>imborn, oimbornPol=>imbornpol,&
                               oinitevent=>initevent

  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), qa(4), qb(4)
  real(kind=prec) :: deltalph_tot, born, pm2enneeav_a
  logical polarised

  polarised = any(abs(n1)>0)
  pm2enneeav_a = 0.
  if (polarised) then
    call tinitevent(p1,p2,p3,p4,-n1, collier=.false., sameq=.true.)
    call oinitevent(p1,p2,p3,p4,-n1)
    born = (  tbornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22) &
           +2*obornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22)) * sqrt(m12)
  else
    call tinitevent(p1,p2,p3,p4, collier=.false.,  sameq=.true.)
    call oinitevent(p1,p2,p3,p4)
    born = 0.
  endif
  born = born +   tborn(s12, s13, s14, s23, s24, s34, M12, m22, m22) &
              + 2*oborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)

  deltalph_tot = Nel *deltalph_l_0(2*m22+s34,mel) &
               + Nmu *deltalph_l_0(2*m22+s34,mmu) &
               + Ntau*deltalph_l_0(2*m22+s34,mtau)&
               + Nhad*deltalph_h  (2*m22+s34)
  pm2enneeav_a = pm2enneeav_a + deltalph_tot * born

  if (polarised) then
    call tinitevent(p1,p4,p3,p2,-n1, collier=.false.,  sameq=.true.)
    call oinitevent(p1,p4,p3,p2,-n1)
    born = (  tbornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22) &
           +2*obornPol(s12, s13, s14, s23, s24, s34,M12, m22, m22)) * sqrt(m12)
  else
    call tinitevent(p1,p4,p3,p2, collier=.false.,  sameq=.true.)
    call oinitevent(p1,p4,p3,p2)
    born = 0.
  endif

  born = born +   tborn(s12, s13, s14, s23, s24, s34, M12, m22, m22) &
              + 2*oborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)

  deltalph_tot = Nel *deltalph_l_0(2*m22+s34,mel) &
               + Nmu *deltalph_l_0(2*m22+s34,mmu) &
               + Ntau*deltalph_l_0(2*m22+s34,mtau)&
               + Nhad*deltalph_h  (2*m22+s34)
  pm2enneeav_a = pm2enneeav_a + deltalph_tot  * born

  pm2enneeav_a = 8*pi**2*alpha**2*GF**2*pm2enneeav_a
  END FUNCTION PM2ENNEEav_a


  FUNCTION PM2ENNEEav_ai(p1,n1,p2,qA,qB,p3,p4)
    !! mu+(p1) -> e+(p2) \nu_mu() \bar{\tau}_\mu() e+(p4) e-(p3)
    !! mu-(p1) -> e-(p2) \bar{mu}_e( )  \nu_\tau() e-(p4) e+(p3)
    !! for massive electron
  use mudecrare_1l_twotrace, only: tborn=>born, tbornPol=>bornPol,           &
                               tinitevent=>initevent,                        &
                               s12, s13, s14, s23, s24, s34, m12, m22

  use mudecrare_1l_onetrace, only: oborn=>born,obornPol=>bornPol,            &
                               oimborn=>imborn, oimbornPol=>imbornpol,       &
                               oinitevent=>initevent

  real(kind=prec) :: p1(4), n1(4), p2(4), p3(4), p4(4), qa(4), qb(4)
  real(kind=prec) :: pm2enneeav_ai, fullborn, vpim
  logical polarised



  polarised = any(abs(n1)>0)
  pm2enneeav_ai = 0.
  if (polarised) then
    call tinitevent(p1,p2,p3,p4,-n1, collier=.false., sameq=.true.)
    call oinitevent(p1,p2,p3,p4,-n1)
    fullborn = oimbornpol(s12, s13, s14, s23, s24, s34, M12, m22, m22)*sqrt(m12)
  else
    call tinitevent(p1,p2,p3,p4, collier=.false.,  sameq=.true.)
    call oinitevent(p1,p2,p3,p4)
    fullborn = 0.
  endif
  fullborn = fullborn + oimborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)
  vpim = -16._prec/3.*pi*(4*m22+s34)*sqrt(s34**2-4*m22**2)/(2*m22 + s34)**2

  pm2enneeav_ai = - vpim*fullborn/ 128.*2.

  if (polarised) then
    call tinitevent(p1,p4,p3,p2,-n1, collier=.false.,  sameq=.true.)
    call oinitevent(p1,p4,p3,p2,-n1)
    fullborn = oimbornpol(s12, s13, s14, s23, s24, s34, M12, m22, m22)*sqrt(m12)
  else
    call tinitevent(p1,p4,p3,p2, collier=.false.,  sameq=.true.)
    call oinitevent(p1,p4,p3,p2)
    fullborn = 0.
  endif
  fullborn = fullborn + oimborn(s12, s13, s14, s23, s24, s34, M12, m22, m22)

  vpim = -16._prec/3.*pi*(4*m22+s34)*sqrt(s34**2-4*m22**2)/(2*m22 + s34)**2
  pm2enneeav_ai = pm2enneeav_ai - vpim*fullborn/ 128.*2.

  pm2enneeav_ai = 2*pm2enneeav_ai*32*pi*alpha**3*GF**2

  END FUNCTION PM2ENNEEav_ai





                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  END MODULE MUDECRARE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!


