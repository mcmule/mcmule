
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                    MODULE MUDECRARE
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use mudecrare_mat_el

!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains



          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!        SINGULAR    LIMITS          !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  FUNCTION PM2ENNEEG_S(q1,n1,q2,q3,q4,q5,q6)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),q6(4),n1(4)
  real (kind=prec) :: pm2enneeg_s

!  nn1 = (/ 0._prec,0._prec,0._prec,0._prec /)

  pm2enneeg_s = 2*(eik(q1,ksoft,q2) + eik(q1,ksoft,q5)    &
                - eik(q1,ksoft,q6) - eik(q2,ksoft,q5)    &
                + eik(q2,ksoft,q6) + eik(q5,ksoft,q6) )  &
             *pm2ennee(q1,n1,q2,q3,q4,q5,q6)
  pm2enneeg_s = (4.*pi*alpha)*pm2enneeg_s

  END FUNCTION PM2ENNEEG_S


  FUNCTION PM2ENNEEGav_S(q1,n1,q2,qA,qB,q3,q4)
    !!  M1^+(p1)->M2^+(p2),M2^-(p3),M2^+(p4),2\nu,\gamma(p7)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real (kind=prec) :: pm2enneegav_s

!  nn1 = (/ 0._prec,0._prec,0._prec,0._prec /)

  pm2enneegav_s = (eik(q1,ksoft,q2) + eik(q1,ksoft,q4)    &
                - eik(q1,ksoft,q3) - eik(q2,ksoft,q4)    &
                + eik(q2,ksoft,q3) + eik(q4,ksoft,q3) )  &
             *pm2enneeav(q1,n1,q2,qA,qB,q3,q4)
  pm2enneegav_s = 2*(4.*pi*alpha)*pm2enneegav_s

  END FUNCTION PM2ENNEEGav_S

  FUNCTION PT2MNNEEGav_S(q1,n1,q2,qA,qB,q3,q4)
    !!  M1^+(p1)->M2^+(p2),M3^-(p3),M3^+(p4),2\nu,\gamma(p7)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real (kind=prec) :: pt2mnneegav_s

!  nn1 = (/ 0._prec,0._prec,0._prec,0._prec /)

  pt2mnneegav_s = 2*(eik(q1,ksoft,q2) + eik(q1,ksoft,q4)    &
                - eik(q1,ksoft,q3) - eik(q2,ksoft,q4)    &
                + eik(q2,ksoft,q3) + eik(q4,ksoft,q3) )  &
             *pt2mnneeav(q1,n1,q2,qA,qB,q3,q4)
  pt2mnneegav_s = (4.*pi*alpha)*pt2mnneegav_s

  END FUNCTION PT2MNNEEGav_S





                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    END MODULE MUDECRARE
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!

